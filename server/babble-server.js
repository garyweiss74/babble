


var fs = require('fs');
var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var app = express();
// var geoip = require('geoip-lite');


app.use(cors());
app.use(bodyParser.json());

// constants
var babblesFileName = 'babbles.json';

// get the babbles data
app.get('/', function (req, res) {
    console.log((new Date()).toString() + ': got a request for all babbles');
    var babblesFile = fs.readFileSync(babblesFileName);
    var babblesStrings = babblesFile.toString().split('\n');

    var babblesArray = [];
    babblesStrings.forEach(function(b) { if (b) babblesArray.push(JSON.parse(b)) })

    res.send(JSON.stringify(babblesArray));
});

// post a new babble
app.post('/', function (req, res) {
    var babble = req.body;
    // babble.geo = geoip.lookup(req.ip);
    console.log((new Date()).toString() + ': got a babble:');
    console.log(babble);
    fs.appendFileSync(babblesFileName, JSON.stringify(babble) + '\n');
    res.send('OK');
});


var server = app.listen(8000, function () {

  var host = 'ec2-54-149-127-252.us-west-2.compute.amazonaws.com';
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);

});
