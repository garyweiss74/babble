from flask import Flask, request
from flask.ext.cors import CORS, cross_origin
import json


app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route("/", methods=['GET', 'POST'])
@cross_origin() 
def babbles():

    # POST just adds the data to a babbles.json file
    if request.method == 'POST':
        print request.get_json()
        print json.dumps(request.get_json())
        with open('babbles.json', 'a') as fout:
            fout.write(json.dumps(request.get_json()) + '\n')
            return "OK"

    # GET retrieves all the data
    babbles_list = []
    with open('babbles.json', 'r') as fin:
        for line in fin:
            babbles_list.append(json.loads(line))
    return json.dumps(babbles_list)

if __name__ == "__main__":
    app.debug = True
    app.run(host="ec2-54-149-127-252.us-west-2.compute.amazonaws.com", port=8000)
