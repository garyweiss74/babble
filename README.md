# Interlude Boilerplate

Interlude Boilerplate is a professional front-end template for building fast,
robust, and adaptable interactive video experiences.

This project is based on the [HTML5 Boilerplate project](https://html5boilerplate.com).
