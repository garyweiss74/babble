require.config({
    baseUrl: './js',
    paths: {
        'InterludePlayer': './vendor/interlude/player',
        'InterludePlayerPlugins': './vendor/interlude/plugins',
        'text': 'http://cdnjs.cloudflare.com/ajax/libs/require-text/2.0.12/text.min',
        'jquery': 'http://code.jquery.com/jquery-2.1.3.min'
    },
    shim: {
        'InterludePlayerPlugins': {
            deps: ['InterludePlayer']
        }
    },
    waitSeconds: 100
});

require([
    'InterludePlayer',
    'InterludePlayerPlugins',
    'text!./segments.json',
    'text!./nodes.json',
    'text!./skins.json',
    'text!./babbles.json',
    './overlays',
    'jquery'
],
function(InterludePlayer, InterludePlayerPlugins, segments, nodes, skins, babbles, overlays, $) {
    'use strict';

    var player = window.player = new InterludePlayer('#player', {
        EngineTech: {
            forceTech: 'flash'
        },
        plugins: {
            bandwidth: {},
            graph: {},
            decision: {},
            project: {},
            time: {},
            seek: {},
            urls: {},
            uitools: {},
            controlbar: {},
            // gui: {},
            rts: {},
            soundtrack: {}
        }
    });

    // Deserialize JSON objects.
    segments = JSON.parse(segments);
    nodes = JSON.parse(nodes);
    skins = JSON.parse(skins);
    babbles = JSON.parse(babbles);

    // Globals
    var babbleApp = {
        commentId: 0,
        duration: 5000,
        minYClick: 65,
        maxYClick: 420,
        editing: false,
        babbleServer: 'http://ec2-54-149-127-252.us-west-2.compute.amazonaws.com:8000/'
    };


    // babble
    // input parm: { text:..., x:..., y:..., start:... }
    function babble(b) {

        // set the babble id
        var elid = 'babble' + babbleApp.commentId;
        babbleApp.commentId++;

        // start function
        function babbleStart() {
            var newDiv = $('<div/>')
                .attr('id', elid)
                .addClass('animated bounceIn')
                .css({ position: 'absolute',
                    top: b.y,
                    left: b.x,
                    color: 'green',
                    padding: '0px 20px 0px 20px',
                    'border-radius': '10px',
                    'font-family': 'verdana',
                    background: '#eee' });
            var newText = $('<p/>').text(b.text);
            newDiv.append(newText);
            $('div#player').first().append(newDiv);
        }

        // end function
        function babbleEnd() {
            $('#' + elid)
                .first()
                .attr('display', 'none')
                .addClass('fadeOut');
        }

        // set the appearance and dissapearance
        setTimeout(babbleStart, b.start);
        setTimeout(babbleEnd, b.start + babbleApp.duration);
    }

    // collect babbles from server, and start the project
    $.get(babbleApp.babbleServer) .done(function(data) { startAll(JSON.parse(data)); });

    // player.gui.promise.done(function() {
    function startAll(babbleData) {

        // Project structure.
        player.project.addSegments(segments);
        player.project.addNodes(nodes);
        player.project.buildGraphAndDecision(nodes);

        // GUI.
        // player.gui.addSkin(skins);
        // player.gui.add(overlays.overlays);

        // handle a click
        $('div#player').click(function(ev) {

            // calculate offsets
            var $player = $('div#player').first();
            var offsetX = ev.pageX - $player.offset().left;
            var offsetY = ev.pageY - $player.offset().top;

            // console.log(ev);
            // console.log('eventX = ' + ev.pageX + ' eventY = ' + ev.pageY);
            // console.log('playerX = ' + $player.offset().left + ' player Y = ' + $player.offset().top);
            // console.log('offsetX = ' + offsetX + ' Y= ' + offsetY);

            // only within bounds
            if (offsetY < babbleApp.minYClick || offsetY > babbleApp.maxYClick) {
                return;
            }

            // open an editor line if it's not already there
            if (babbleApp.editing) {
                $('#babbleInput').remove();
                babbleApp.editing = false;
                return;
            }
            babbleApp.editing = true;

            // find location in relative terms
            var element = $(ev.currentTarget).first();
            console.log('click positionX:' + offsetX + '/' + element.width() + ' positionY:' + offsetY + '/' + element.height());

            // add an input element for babble text
            var inputElement = $('<input/>')
                .attr({ id: 'babbleInput', placeholder: 'This is Awesome ! - Jane' })
                .addClass('animated bounceIn')
                .css({ position: 'absolute', top: offsetY, left: offsetX, color: 'green', background: 'white', padding: '20px', 'font-size': '1.4em', 'border-radius': '10px', 'font-family': 'verdana' });
            $player.append(inputElement);

            // when user clicks <Enter>, create the babble
            $('#babbleInput').focus().keyup(function(keyEvent) {
                if (keyEvent.keyCode === 13) {

                    // get the text
                    var babbleText = $('#babbleInput').first().val();
                    var babbleTime = (new Date()) - babbleApp.startTime;

                    if (babbleText && babbleText !== '') {
                        var b = {
                            text: babbleText,
                            x: offsetX,
                            y: offsetY,
                            start: 100
                        };
                        babble(b);
                        b.start = babbleTime;
                        console.log('time ... ');
                        console.log(player.time);
                        $.ajax(babbleApp.babbleServer, {
                            data : JSON.stringify(b),
                            contentType : 'application/json',
                            type : 'POST' });
                    }
                }

                // on enter, or escape, remove the input element
                if (keyEvent.keyCode === 13 || keyEvent.keyCode === 27) {
                    $('#babbleInput').remove();
                    babbleApp.editing = false;
                }

            });
        });

        // set up the babbles
        babbleData.forEach(function(b) { babble(b); });

        // Let's go!
        babbleApp.startTime = new Date();
        player.playlist.push('start');
        player.play();

        // Remove buttons that don't work
        $('#controlbar0_pauseBtn').hide();
        $('#controlbar0_fullscreenBtn').hide();

    }

});
