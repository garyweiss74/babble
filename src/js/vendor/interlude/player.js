(function (root, factory) {root.InterludePlayer = factory();if (typeof define === 'function' && define.amd) {define([], function() { return root.InterludePlayer; });}}(this, function () {/**
 * @license almond 0.3.0 Copyright (c) 2011-2014, The Dojo Foundation All Rights Reserved.
 * Available via the MIT or new BSD license.
 * see: http://github.com/jrburke/almond for details
 */
//Going sloppy to avoid 'use strict' string cost, but strict practices should
//be followed.
/*jslint sloppy: true */
/*global setTimeout: false */

var requirejs, require, define;
(function (undef) {
    var main, req, makeMap, handlers,
        defined = {},
        waiting = {},
        config = {},
        defining = {},
        hasOwn = Object.prototype.hasOwnProperty,
        aps = [].slice,
        jsSuffixRegExp = /\.js$/;

    function hasProp(obj, prop) {
        return hasOwn.call(obj, prop);
    }

    /**
     * Given a relative module name, like ./something, normalize it to
     * a real name that can be mapped to a path.
     * @param {String} name the relative name
     * @param {String} baseName a real name that the name arg is relative
     * to.
     * @returns {String} normalized name
     */
    function normalize(name, baseName) {
        var nameParts, nameSegment, mapValue, foundMap, lastIndex,
            foundI, foundStarMap, starI, i, j, part,
            baseParts = baseName && baseName.split("/"),
            map = config.map,
            starMap = (map && map['*']) || {};

        //Adjust any relative paths.
        if (name && name.charAt(0) === ".") {
            //If have a base name, try to normalize against it,
            //otherwise, assume it is a top-level require that will
            //be relative to baseUrl in the end.
            if (baseName) {
                //Convert baseName to array, and lop off the last part,
                //so that . matches that "directory" and not name of the baseName's
                //module. For instance, baseName of "one/two/three", maps to
                //"one/two/three.js", but we want the directory, "one/two" for
                //this normalization.
                baseParts = baseParts.slice(0, baseParts.length - 1);
                name = name.split('/');
                lastIndex = name.length - 1;

                // Node .js allowance:
                if (config.nodeIdCompat && jsSuffixRegExp.test(name[lastIndex])) {
                    name[lastIndex] = name[lastIndex].replace(jsSuffixRegExp, '');
                }

                name = baseParts.concat(name);

                //start trimDots
                for (i = 0; i < name.length; i += 1) {
                    part = name[i];
                    if (part === ".") {
                        name.splice(i, 1);
                        i -= 1;
                    } else if (part === "..") {
                        if (i === 1 && (name[2] === '..' || name[0] === '..')) {
                            //End of the line. Keep at least one non-dot
                            //path segment at the front so it can be mapped
                            //correctly to disk. Otherwise, there is likely
                            //no path mapping for a path starting with '..'.
                            //This can still fail, but catches the most reasonable
                            //uses of ..
                            break;
                        } else if (i > 0) {
                            name.splice(i - 1, 2);
                            i -= 2;
                        }
                    }
                }
                //end trimDots

                name = name.join("/");
            } else if (name.indexOf('./') === 0) {
                // No baseName, so this is ID is resolved relative
                // to baseUrl, pull off the leading dot.
                name = name.substring(2);
            }
        }

        //Apply map config if available.
        if ((baseParts || starMap) && map) {
            nameParts = name.split('/');

            for (i = nameParts.length; i > 0; i -= 1) {
                nameSegment = nameParts.slice(0, i).join("/");

                if (baseParts) {
                    //Find the longest baseName segment match in the config.
                    //So, do joins on the biggest to smallest lengths of baseParts.
                    for (j = baseParts.length; j > 0; j -= 1) {
                        mapValue = map[baseParts.slice(0, j).join('/')];

                        //baseName segment has  config, find if it has one for
                        //this name.
                        if (mapValue) {
                            mapValue = mapValue[nameSegment];
                            if (mapValue) {
                                //Match, update name to the new value.
                                foundMap = mapValue;
                                foundI = i;
                                break;
                            }
                        }
                    }
                }

                if (foundMap) {
                    break;
                }

                //Check for a star map match, but just hold on to it,
                //if there is a shorter segment match later in a matching
                //config, then favor over this star map.
                if (!foundStarMap && starMap && starMap[nameSegment]) {
                    foundStarMap = starMap[nameSegment];
                    starI = i;
                }
            }

            if (!foundMap && foundStarMap) {
                foundMap = foundStarMap;
                foundI = starI;
            }

            if (foundMap) {
                nameParts.splice(0, foundI, foundMap);
                name = nameParts.join('/');
            }
        }

        return name;
    }

    function makeRequire(relName, forceSync) {
        return function () {
            //A version of a require function that passes a moduleName
            //value for items that may need to
            //look up paths relative to the moduleName
            var args = aps.call(arguments, 0);

            //If first arg is not require('string'), and there is only
            //one arg, it is the array form without a callback. Insert
            //a null so that the following concat is correct.
            if (typeof args[0] !== 'string' && args.length === 1) {
                args.push(null);
            }
            return req.apply(undef, args.concat([relName, forceSync]));
        };
    }

    function makeNormalize(relName) {
        return function (name) {
            return normalize(name, relName);
        };
    }

    function makeLoad(depName) {
        return function (value) {
            defined[depName] = value;
        };
    }

    function callDep(name) {
        if (hasProp(waiting, name)) {
            var args = waiting[name];
            delete waiting[name];
            defining[name] = true;
            main.apply(undef, args);
        }

        if (!hasProp(defined, name) && !hasProp(defining, name)) {
            throw new Error('No ' + name);
        }
        return defined[name];
    }

    //Turns a plugin!resource to [plugin, resource]
    //with the plugin being undefined if the name
    //did not have a plugin prefix.
    function splitPrefix(name) {
        var prefix,
            index = name ? name.indexOf('!') : -1;
        if (index > -1) {
            prefix = name.substring(0, index);
            name = name.substring(index + 1, name.length);
        }
        return [prefix, name];
    }

    /**
     * Makes a name map, normalizing the name, and using a plugin
     * for normalization if necessary. Grabs a ref to plugin
     * too, as an optimization.
     */
    makeMap = function (name, relName) {
        var plugin,
            parts = splitPrefix(name),
            prefix = parts[0];

        name = parts[1];

        if (prefix) {
            prefix = normalize(prefix, relName);
            plugin = callDep(prefix);
        }

        //Normalize according
        if (prefix) {
            if (plugin && plugin.normalize) {
                name = plugin.normalize(name, makeNormalize(relName));
            } else {
                name = normalize(name, relName);
            }
        } else {
            name = normalize(name, relName);
            parts = splitPrefix(name);
            prefix = parts[0];
            name = parts[1];
            if (prefix) {
                plugin = callDep(prefix);
            }
        }

        //Using ridiculous property names for space reasons
        return {
            f: prefix ? prefix + '!' + name : name, //fullName
            n: name,
            pr: prefix,
            p: plugin
        };
    };

    function makeConfig(name) {
        return function () {
            return (config && config.config && config.config[name]) || {};
        };
    }

    handlers = {
        require: function (name) {
            return makeRequire(name);
        },
        exports: function (name) {
            var e = defined[name];
            if (typeof e !== 'undefined') {
                return e;
            } else {
                return (defined[name] = {});
            }
        },
        module: function (name) {
            return {
                id: name,
                uri: '',
                exports: defined[name],
                config: makeConfig(name)
            };
        }
    };

    main = function (name, deps, callback, relName) {
        var cjsModule, depName, ret, map, i,
            args = [],
            callbackType = typeof callback,
            usingExports;

        //Use name if no relName
        relName = relName || name;

        //Call the callback to define the module, if necessary.
        if (callbackType === 'undefined' || callbackType === 'function') {
            //Pull out the defined dependencies and pass the ordered
            //values to the callback.
            //Default to [require, exports, module] if no deps
            deps = !deps.length && callback.length ? ['require', 'exports', 'module'] : deps;
            for (i = 0; i < deps.length; i += 1) {
                map = makeMap(deps[i], relName);
                depName = map.f;

                //Fast path CommonJS standard dependencies.
                if (depName === "require") {
                    args[i] = handlers.require(name);
                } else if (depName === "exports") {
                    //CommonJS module spec 1.1
                    args[i] = handlers.exports(name);
                    usingExports = true;
                } else if (depName === "module") {
                    //CommonJS module spec 1.1
                    cjsModule = args[i] = handlers.module(name);
                } else if (hasProp(defined, depName) ||
                           hasProp(waiting, depName) ||
                           hasProp(defining, depName)) {
                    args[i] = callDep(depName);
                } else if (map.p) {
                    map.p.load(map.n, makeRequire(relName, true), makeLoad(depName), {});
                    args[i] = defined[depName];
                } else {
                    throw new Error(name + ' missing ' + depName);
                }
            }

            ret = callback ? callback.apply(defined[name], args) : undefined;

            if (name) {
                //If setting exports via "module" is in play,
                //favor that over return value and exports. After that,
                //favor a non-undefined return value over exports use.
                if (cjsModule && cjsModule.exports !== undef &&
                        cjsModule.exports !== defined[name]) {
                    defined[name] = cjsModule.exports;
                } else if (ret !== undef || !usingExports) {
                    //Use the return value from the function.
                    defined[name] = ret;
                }
            }
        } else if (name) {
            //May just be an object definition for the module. Only
            //worry about defining if have a module name.
            defined[name] = callback;
        }
    };

    requirejs = require = req = function (deps, callback, relName, forceSync, alt) {
        if (typeof deps === "string") {
            if (handlers[deps]) {
                //callback in this case is really relName
                return handlers[deps](callback);
            }
            //Just return the module wanted. In this scenario, the
            //deps arg is the module name, and second arg (if passed)
            //is just the relName.
            //Normalize module name, if it contains . or ..
            return callDep(makeMap(deps, callback).f);
        } else if (!deps.splice) {
            //deps is a config object, not an array.
            config = deps;
            if (config.deps) {
                req(config.deps, config.callback);
            }
            if (!callback) {
                return;
            }

            if (callback.splice) {
                //callback is an array, which means it is a dependency list.
                //Adjust args if there are dependencies
                deps = callback;
                callback = relName;
                relName = null;
            } else {
                deps = undef;
            }
        }

        //Support require(['a'])
        callback = callback || function () {};

        //If relName is a function, it is an errback handler,
        //so remove it.
        if (typeof relName === 'function') {
            relName = forceSync;
            forceSync = alt;
        }

        //Simulate async callback;
        if (forceSync) {
            main(undef, deps, callback, relName);
        } else {
            //Using a non-zero value because of concern for what old browsers
            //do, and latest browsers "upgrade" to 4 if lower value is used:
            //http://www.whatwg.org/specs/web-apps/current-work/multipage/timers.html#dom-windowtimers-settimeout:
            //If want a value immediately, use require('id') instead -- something
            //that works in almond on the global level, but not guaranteed and
            //unlikely to work in other AMD implementations.
            setTimeout(function () {
                main(undef, deps, callback, relName);
            }, 4);
        }

        return req;
    };

    /**
     * Just drops the config on the floor, but returns req in case
     * the config return value is used.
     */
    req.config = function (cfg) {
        return req(cfg);
    };

    /**
     * Expose module registry for debugging and tooling
     */
    requirejs._defined = defined;

    define = function (name, deps, callback) {

        //This module may not have dependencies
        if (!deps.splice) {
            //deps is not an array, so probably means
            //an object literal or factory function for
            //the value. Adjust args.
            callback = deps;
            deps = [];
        }

        if (!hasProp(defined, name) && !hasProp(waiting, name)) {
            waiting[name] = [name, deps, callback];
        }
    };

    define.amd = {
        jQuery: true
    };
}());

define("../node_modules/almond/almond", function(){});

/*! loglevel - v1.2.0 - https://github.com/pimterry/loglevel - (c) 2014 Tim Perry - licensed MIT */
!function(a,b){"object"==typeof module&&module.exports&&"function"==typeof require?module.exports=b():"function"==typeof define&&"object"==typeof define.amd?define('utils/../../vendor/loglevel/dist/loglevel.min',b):a.log=b()}(this,function(){function a(a){return typeof console===i?!1:void 0!==console[a]?b(console,a):void 0!==console.log?b(console,"log"):h}function b(a,b){var c=a[b];if("function"==typeof c.bind)return c.bind(a);try{return Function.prototype.bind.call(c,a)}catch(d){return function(){return Function.prototype.apply.apply(c,[a,arguments])}}}function c(a,b){return function(){typeof console!==i&&(d(b),g[a].apply(g,arguments))}}function d(a){for(var b=0;b<j.length;b++){var c=j[b];g[c]=a>b?h:g.methodFactory(c,a)}}function e(a){var b=(j[a]||"silent").toUpperCase();try{return void(window.localStorage.loglevel=b)}catch(c){}try{window.document.cookie="loglevel="+b+";"}catch(c){}}function f(){var a;try{a=window.localStorage.loglevel}catch(b){}if(typeof a===i)try{a=/loglevel=([^;]+)/.exec(window.document.cookie)[1]}catch(b){}void 0===g.levels[a]&&(a="WARN"),g.setLevel(g.levels[a])}var g={},h=function(){},i="undefined",j=["trace","debug","info","warn","error"];g.levels={TRACE:0,DEBUG:1,INFO:2,WARN:3,ERROR:4,SILENT:5},g.methodFactory=function(b,d){return a(b)||c(b,d)},g.setLevel=function(a){if("string"==typeof a&&void 0!==g.levels[a.toUpperCase()]&&(a=g.levels[a.toUpperCase()]),!("number"==typeof a&&a>=0&&a<=g.levels.SILENT))throw"log.setLevel() called with invalid level: "+a;return e(a),d(a),typeof console===i&&a<g.levels.SILENT?"No console available for logging":void 0},g.enableAll=function(){g.setLevel(g.levels.TRACE)},g.disableAll=function(){g.setLevel(g.levels.SILENT)};var k=typeof window!==i?window.log:void 0;return g.noConflict=function(){return typeof window!==i&&window.log===g&&(window.log=k),g},f(),g});
define(
'utils/loggerFactory',[
    '../../vendor/loglevel/dist/loglevel.min'
],
function(log) {
    'use strict';

    return {

        /**
         * @alias player.loggerFactory.createLogger
         * @memberof! player.loggerFactory#
         * @method player.loggerFactory.createLogger
         * @param {string} name
         */
        createLogger: function createLogger(name) {
            var prefix = '[' + name + '] ';
            var enabled = true;
            var level = 0;
            var levelStringToInt = function(str) {
                    var retVal = 0;

                    if (typeof str !== 'string') {
                        return retVal;
                    }

                    switch (str.toLowerCase().trim()) {
                        case 'enable':
                        case 'enabled':
                        case 'trace':
                            retVal = 0;
                            break;
                        case 'debug':
                            retVal = 1;
                            break;
                        case 'info':
                            retVal = 2;
                            break;
                        case 'warn':
                            retVal = 3;
                            break;
                        case 'error':
                            retVal = 4;
                            break;
                        case 'disable':
                        case 'disabled':
                            retVal = 5;
                            break;
                        default:
                            break;
                    }

                    return retVal;
                };

            return {
                trace: function trace(msg) {
                    arguments[0] = prefix + msg;
                    if (enabled && level <= 0) {
                        log.trace.apply(null, arguments);
                    }
                },
                debug: function debug(msg) {
                    arguments[0] = prefix + msg;
                    if (enabled && level <= 1) {
                        log.debug.apply(null, arguments);
                    }
                },
                info: function info(msg) {
                    arguments[0] = prefix + msg;
                    if (enabled && level <= 2) {
                        log.info.apply(null, arguments);
                    }
                },
                warn: function warn(msg) {
                    arguments[0] = prefix + msg;
                    if (enabled && level <= 3) {
                        log.warn.apply(null, arguments);
                    }
                },
                error: function error(msg) {
                    arguments[0] = prefix + msg;
                    if (enabled && level <= 4) {
                        log.error.apply(null, arguments);
                    }
                },
                /* Simple implementation for setting the level of only this instance. */
                setLevel: function setLevel(levelStr) {
                    level = levelStringToInt(levelStr);
                },
                /* Getter for int level. */
                getIntLevel: function getIntLevel() {
                    return level;
                },
                /* Disable logging only for this instnace. */
                disable: function disable() {
                    enabled = false;
                },
                /* Enable logging only for this instance. Affected by global state of logging. */
                enable: function enable() {
                    enabled = true;
                }
            };
        },

        /**
         * @alias player.loggerFactory.enableAll
         * @memberof! player.loggerFactory#
         * @method player.loggerFactory.enableAll
         */
        enableAll: function enableAll() {
            log.enableAll();
        },

        /**
         * @alias player.loggerFactory.disableAll
         * @memberof! player.loggerFactory#
         * @method player.loggerFactory.disableAll
         */
        disableAll: function disableAll() {
            log.disableAll();
        },

        /**
         * @alias player.loggerFactory.setLevel
         * @memberof! player.loggerFactory#
         * @method player.loggerFactory.setLevel
         * @param {string} level
         */
        setLevel: function setLevel(level) {
            log.setLevel(level);
        }
    };
});

//     uuid.js
//
//     Copyright (c) 2010-2012 Robert Kieffer
//     MIT License - http://opensource.org/licenses/mit-license.php

(function() {
  var _global = this;

  // Unique ID creation requires a high quality random # generator.  We feature
  // detect to determine the best RNG source, normalizing to a function that
  // returns 128-bits of randomness, since that's what's usually required
  var _rng;

  // Node.js crypto-based RNG - http://nodejs.org/docs/v0.6.2/api/crypto.html
  //
  // Moderately fast, high quality
  if (typeof(_global.require) == 'function') {
    try {
      var _rb = _global.require('crypto').randomBytes;
      _rng = _rb && function() {return _rb(16);};
    } catch(e) {}
  }

  if (!_rng && _global.crypto && crypto.getRandomValues) {
    // WHATWG crypto-based RNG - http://wiki.whatwg.org/wiki/Crypto
    //
    // Moderately fast, high quality
    var _rnds8 = new Uint8Array(16);
    _rng = function whatwgRNG() {
      crypto.getRandomValues(_rnds8);
      return _rnds8;
    };
  }

  if (!_rng) {
    // Math.random()-based (RNG)
    //
    // If all else fails, use Math.random().  It's fast, but is of unspecified
    // quality.
    var  _rnds = new Array(16);
    _rng = function() {
      for (var i = 0, r; i < 16; i++) {
        if ((i & 0x03) === 0) r = Math.random() * 0x100000000;
        _rnds[i] = r >>> ((i & 0x03) << 3) & 0xff;
      }

      return _rnds;
    };
  }

  // Buffer class to use
  var BufferClass = typeof(_global.Buffer) == 'function' ? _global.Buffer : Array;

  // Maps for number <-> hex string conversion
  var _byteToHex = [];
  var _hexToByte = {};
  for (var i = 0; i < 256; i++) {
    _byteToHex[i] = (i + 0x100).toString(16).substr(1);
    _hexToByte[_byteToHex[i]] = i;
  }

  // **`parse()` - Parse a UUID into it's component bytes**
  function parse(s, buf, offset) {
    var i = (buf && offset) || 0, ii = 0;

    buf = buf || [];
    s.toLowerCase().replace(/[0-9a-f]{2}/g, function(oct) {
      if (ii < 16) { // Don't overflow!
        buf[i + ii++] = _hexToByte[oct];
      }
    });

    // Zero out remaining bytes if string was short
    while (ii < 16) {
      buf[i + ii++] = 0;
    }

    return buf;
  }

  // **`unparse()` - Convert UUID byte array (ala parse()) into a string**
  function unparse(buf, offset) {
    var i = offset || 0, bth = _byteToHex;
    return  bth[buf[i++]] + bth[buf[i++]] +
            bth[buf[i++]] + bth[buf[i++]] + '-' +
            bth[buf[i++]] + bth[buf[i++]] + '-' +
            bth[buf[i++]] + bth[buf[i++]] + '-' +
            bth[buf[i++]] + bth[buf[i++]] + '-' +
            bth[buf[i++]] + bth[buf[i++]] +
            bth[buf[i++]] + bth[buf[i++]] +
            bth[buf[i++]] + bth[buf[i++]];
  }

  // **`v1()` - Generate time-based UUID**
  //
  // Inspired by https://github.com/LiosK/UUID.js
  // and http://docs.python.org/library/uuid.html

  // random #'s we need to init node and clockseq
  var _seedBytes = _rng();

  // Per 4.5, create and 48-bit node id, (47 random bits + multicast bit = 1)
  var _nodeId = [
    _seedBytes[0] | 0x01,
    _seedBytes[1], _seedBytes[2], _seedBytes[3], _seedBytes[4], _seedBytes[5]
  ];

  // Per 4.2.2, randomize (14 bit) clockseq
  var _clockseq = (_seedBytes[6] << 8 | _seedBytes[7]) & 0x3fff;

  // Previous uuid creation time
  var _lastMSecs = 0, _lastNSecs = 0;

  // See https://github.com/broofa/node-uuid for API details
  function v1(options, buf, offset) {
    var i = buf && offset || 0;
    var b = buf || [];

    options = options || {};

    var clockseq = options.clockseq != null ? options.clockseq : _clockseq;

    // UUID timestamps are 100 nano-second units since the Gregorian epoch,
    // (1582-10-15 00:00).  JSNumbers aren't precise enough for this, so
    // time is handled internally as 'msecs' (integer milliseconds) and 'nsecs'
    // (100-nanoseconds offset from msecs) since unix epoch, 1970-01-01 00:00.
    var msecs = options.msecs != null ? options.msecs : new Date().getTime();

    // Per 4.2.1.2, use count of uuid's generated during the current clock
    // cycle to simulate higher resolution clock
    var nsecs = options.nsecs != null ? options.nsecs : _lastNSecs + 1;

    // Time since last uuid creation (in msecs)
    var dt = (msecs - _lastMSecs) + (nsecs - _lastNSecs)/10000;

    // Per 4.2.1.2, Bump clockseq on clock regression
    if (dt < 0 && options.clockseq == null) {
      clockseq = clockseq + 1 & 0x3fff;
    }

    // Reset nsecs if clock regresses (new clockseq) or we've moved onto a new
    // time interval
    if ((dt < 0 || msecs > _lastMSecs) && options.nsecs == null) {
      nsecs = 0;
    }

    // Per 4.2.1.2 Throw error if too many uuids are requested
    if (nsecs >= 10000) {
      throw new Error('uuid.v1(): Can\'t create more than 10M uuids/sec');
    }

    _lastMSecs = msecs;
    _lastNSecs = nsecs;
    _clockseq = clockseq;

    // Per 4.1.4 - Convert from unix epoch to Gregorian epoch
    msecs += 12219292800000;

    // `time_low`
    var tl = ((msecs & 0xfffffff) * 10000 + nsecs) % 0x100000000;
    b[i++] = tl >>> 24 & 0xff;
    b[i++] = tl >>> 16 & 0xff;
    b[i++] = tl >>> 8 & 0xff;
    b[i++] = tl & 0xff;

    // `time_mid`
    var tmh = (msecs / 0x100000000 * 10000) & 0xfffffff;
    b[i++] = tmh >>> 8 & 0xff;
    b[i++] = tmh & 0xff;

    // `time_high_and_version`
    b[i++] = tmh >>> 24 & 0xf | 0x10; // include version
    b[i++] = tmh >>> 16 & 0xff;

    // `clock_seq_hi_and_reserved` (Per 4.2.2 - include variant)
    b[i++] = clockseq >>> 8 | 0x80;

    // `clock_seq_low`
    b[i++] = clockseq & 0xff;

    // `node`
    var node = options.node || _nodeId;
    for (var n = 0; n < 6; n++) {
      b[i + n] = node[n];
    }

    return buf ? buf : unparse(b);
  }

  // **`v4()` - Generate random UUID**

  // See https://github.com/broofa/node-uuid for API details
  function v4(options, buf, offset) {
    // Deprecated - 'format' argument, as supported in v1.2
    var i = buf && offset || 0;

    if (typeof(options) == 'string') {
      buf = options == 'binary' ? new BufferClass(16) : null;
      options = null;
    }
    options = options || {};

    var rnds = options.random || (options.rng || _rng)();

    // Per 4.4, set bits for version and `clock_seq_hi_and_reserved`
    rnds[6] = (rnds[6] & 0x0f) | 0x40;
    rnds[8] = (rnds[8] & 0x3f) | 0x80;

    // Copy bytes to buffer, if provided
    if (buf) {
      for (var ii = 0; ii < 16; ii++) {
        buf[i + ii] = rnds[ii];
      }
    }

    return buf || unparse(rnds);
  }

  // Export public API
  var uuid = v4;
  uuid.v1 = v1;
  uuid.v4 = v4;
  uuid.parse = parse;
  uuid.unparse = unparse;
  uuid.BufferClass = BufferClass;

  if (typeof(module) != 'undefined' && module.exports) {
    // Publish as node.js module
    module.exports = uuid;
  } else  if (typeof define === 'function' && define.amd) {
    // Publish as AMD module
    define('uuid',[],function() {return uuid;});
 

  } else {
    // Publish as global (in browsers)
    var _previousRoot = _global.uuid;

    // **`noConflict()` - (browser only) to reset global 'uuid' var**
    uuid.noConflict = function() {
      _global.uuid = _previousRoot;
      return uuid;
    };

    _global.uuid = uuid;
  }
}).call(this);

/*!
  * Reqwest! A general purpose XHR connection manager
  * license MIT (c) Dustin Diaz 2014
  * https://github.com/ded/reqwest
  */

!function (name, context, definition) {
  if (typeof module != 'undefined' && module.exports) module.exports = definition()
  else if (typeof define == 'function' && define.amd) define('utils/../../vendor/reqwest/reqwest',definition)
  else context[name] = definition()
}('reqwest', this, function () {

  var win = window
    , doc = document
    , httpsRe = /^http/
    , protocolRe = /(^\w+):\/\//
    , twoHundo = /^(20\d|1223)$/ //http://stackoverflow.com/questions/10046972/msie-returns-status-code-of-1223-for-ajax-request
    , byTag = 'getElementsByTagName'
    , readyState = 'readyState'
    , contentType = 'Content-Type'
    , requestedWith = 'X-Requested-With'
    , head = doc[byTag]('head')[0]
    , uniqid = 0
    , callbackPrefix = 'reqwest_' + (+new Date())
    , lastValue // data stored by the most recent JSONP callback
    , xmlHttpRequest = 'XMLHttpRequest'
    , xDomainRequest = 'XDomainRequest'
    , noop = function () {}

    , isArray = typeof Array.isArray == 'function'
        ? Array.isArray
        : function (a) {
            return a instanceof Array
          }

    , defaultHeaders = {
          'contentType': 'application/x-www-form-urlencoded'
        , 'requestedWith': xmlHttpRequest
        , 'accept': {
              '*':  'text/javascript, text/html, application/xml, text/xml, */*'
            , 'xml':  'application/xml, text/xml'
            , 'html': 'text/html'
            , 'text': 'text/plain'
            , 'json': 'application/json, text/javascript'
            , 'js':   'application/javascript, text/javascript'
          }
      }

    , xhr = function(o) {
        // is it x-domain
        if (o['crossOrigin'] === true) {
          var xhr = win[xmlHttpRequest] ? new XMLHttpRequest() : null
          if (xhr && 'withCredentials' in xhr) {
            return xhr
          } else if (win[xDomainRequest]) {
            return new XDomainRequest()
          } else {
            throw new Error('Browser does not support cross-origin requests')
          }
        } else if (win[xmlHttpRequest]) {
          return new XMLHttpRequest()
        } else {
          return new ActiveXObject('Microsoft.XMLHTTP')
        }
      }
    , globalSetupOptions = {
        dataFilter: function (data) {
          return data
        }
      }

  function succeed(r) {
    var protocol = protocolRe.exec(r.url);
    protocol = (protocol && protocol[1]) || window.location.protocol;
    return httpsRe.test(protocol) ? twoHundo.test(r.request.status) : !!r.request.response;
  }

  function handleReadyState(r, success, error) {
    return function () {
      // use _aborted to mitigate against IE err c00c023f
      // (can't read props on aborted request objects)
      if (r._aborted) return error(r.request)
      if (r._timedOut) return error(r.request, 'Request is aborted: timeout')
      if (r.request && r.request[readyState] == 4) {
        r.request.onreadystatechange = noop
        if (succeed(r)) success(r.request)
        else
          error(r.request)
      }
    }
  }

  function setHeaders(http, o) {
    var headers = o['headers'] || {}
      , h

    headers['Accept'] = headers['Accept']
      || defaultHeaders['accept'][o['type']]
      || defaultHeaders['accept']['*']

    var isAFormData = typeof FormData === 'function' && (o['data'] instanceof FormData);
    // breaks cross-origin requests with legacy browsers
    if (!o['crossOrigin'] && !headers[requestedWith]) headers[requestedWith] = defaultHeaders['requestedWith']
    if (!headers[contentType] && !isAFormData) headers[contentType] = o['contentType'] || defaultHeaders['contentType']
    for (h in headers)
      headers.hasOwnProperty(h) && 'setRequestHeader' in http && http.setRequestHeader(h, headers[h])
  }

  function setCredentials(http, o) {
    if (typeof o['withCredentials'] !== 'undefined' && typeof http.withCredentials !== 'undefined') {
      http.withCredentials = !!o['withCredentials']
    }
  }

  function generalCallback(data) {
    lastValue = data
  }

  function urlappend (url, s) {
    return url + (/\?/.test(url) ? '&' : '?') + s
  }

  function handleJsonp(o, fn, err, url) {
    var reqId = uniqid++
      , cbkey = o['jsonpCallback'] || 'callback' // the 'callback' key
      , cbval = o['jsonpCallbackName'] || reqwest.getcallbackPrefix(reqId)
      , cbreg = new RegExp('((^|\\?|&)' + cbkey + ')=([^&]+)')
      , match = url.match(cbreg)
      , script = doc.createElement('script')
      , loaded = 0
      , isIE10 = navigator.userAgent.indexOf('MSIE 10.0') !== -1

    if (match) {
      if (match[3] === '?') {
        url = url.replace(cbreg, '$1=' + cbval) // wildcard callback func name
      } else {
        cbval = match[3] // provided callback func name
      }
    } else {
      url = urlappend(url, cbkey + '=' + cbval) // no callback details, add 'em
    }

    win[cbval] = generalCallback

    script.type = 'text/javascript'
    script.src = url
    script.async = true
    if (typeof script.onreadystatechange !== 'undefined' && !isIE10) {
      // need this for IE due to out-of-order onreadystatechange(), binding script
      // execution to an event listener gives us control over when the script
      // is executed. See http://jaubourg.net/2010/07/loading-script-as-onclick-handler-of.html
      script.htmlFor = script.id = '_reqwest_' + reqId
    }

    script.onload = script.onreadystatechange = function () {
      if ((script[readyState] && script[readyState] !== 'complete' && script[readyState] !== 'loaded') || loaded) {
        return false
      }
      script.onload = script.onreadystatechange = null
      script.onclick && script.onclick()
      // Call the user callback with the last value stored and clean up values and scripts.
      fn(lastValue)
      lastValue = undefined
      head.removeChild(script)
      loaded = 1
    }

    // Add the script to the DOM head
    head.appendChild(script)

    // Enable JSONP timeout
    return {
      abort: function () {
        script.onload = script.onreadystatechange = null
        err({}, 'Request is aborted: timeout', {})
        lastValue = undefined
        head.removeChild(script)
        loaded = 1
      }
    }
  }

  function getRequest(fn, err) {
    var o = this.o
      , method = (o['method'] || 'GET').toUpperCase()
      , url = typeof o === 'string' ? o : o['url']
      // convert non-string objects to query-string form unless o['processData'] is false
      , data = (o['processData'] !== false && o['data'] && typeof o['data'] !== 'string')
        ? reqwest.toQueryString(o['data'])
        : (o['data'] || null)
      , http
      , sendWait = false

    // if we're working on a GET request and we have data then we should append
    // query string to end of URL and not post data
    if ((o['type'] == 'jsonp' || method == 'GET') && data) {
      url = urlappend(url, data)
      data = null
    }

    if (o['type'] == 'jsonp') return handleJsonp(o, fn, err, url)

    // get the xhr from the factory if passed
    // if the factory returns null, fall-back to ours
    http = (o.xhr && o.xhr(o)) || xhr(o)

    http.open(method, url, o['async'] === false ? false : true)
    setHeaders(http, o)
    setCredentials(http, o)
    if (win[xDomainRequest] && http instanceof win[xDomainRequest]) {
        http.onload = fn
        http.onerror = err
        // NOTE: see
        // http://social.msdn.microsoft.com/Forums/en-US/iewebdevelopment/thread/30ef3add-767c-4436-b8a9-f1ca19b4812e
        http.onprogress = function() {}
        sendWait = true
    } else {
      http.onreadystatechange = handleReadyState(this, fn, err)
    }
    o['before'] && o['before'](http)
    if (sendWait) {
      setTimeout(function () {
        http.send(data)
      }, 200)
    } else {
      http.send(data)
    }
    return http
  }

  function Reqwest(o, fn) {
    this.o = o
    this.fn = fn

    init.apply(this, arguments)
  }

  function setType(header) {
    // json, javascript, text/plain, text/html, xml
    if (header.match('json')) return 'json'
    if (header.match('javascript')) return 'js'
    if (header.match('text')) return 'html'
    if (header.match('xml')) return 'xml'
  }

  function init(o, fn) {

    this.url = typeof o == 'string' ? o : o['url']
    this.timeout = null

    // whether request has been fulfilled for purpose
    // of tracking the Promises
    this._fulfilled = false
    // success handlers
    this._successHandler = function(){}
    this._fulfillmentHandlers = []
    // error handlers
    this._errorHandlers = []
    // complete (both success and fail) handlers
    this._completeHandlers = []
    this._erred = false
    this._responseArgs = {}

    var self = this

    fn = fn || function () {}

    if (o['timeout']) {
      this.timeout = setTimeout(function () {
        timedOut()
      }, o['timeout'])
    }

    if (o['success']) {
      this._successHandler = function () {
        o['success'].apply(o, arguments)
      }
    }

    if (o['error']) {
      this._errorHandlers.push(function () {
        o['error'].apply(o, arguments)
      })
    }

    if (o['complete']) {
      this._completeHandlers.push(function () {
        o['complete'].apply(o, arguments)
      })
    }

    function complete (resp) {
      o['timeout'] && clearTimeout(self.timeout)
      self.timeout = null
      while (self._completeHandlers.length > 0) {
        self._completeHandlers.shift()(resp)
      }
    }

    function success (resp) {
      var type = o['type'] || resp && setType(resp.getResponseHeader('Content-Type')) // resp can be undefined in IE
      resp = (type !== 'jsonp') ? self.request : resp
      // use global data filter on response text
      var filteredResponse = globalSetupOptions.dataFilter(resp.responseText, type)
        , r = filteredResponse
      try {
        resp.responseText = r
      } catch (e) {
        // can't assign this in IE<=8, just ignore
      }
      if (r) {
        switch (type) {
        case 'json':
          try {
            resp = win.JSON ? win.JSON.parse(r) : eval('(' + r + ')')
          } catch (err) {
            return error(resp, 'Could not parse JSON in response', err)
          }
          break
        case 'js':
          resp = eval(r)
          break
        case 'html':
          resp = r
          break
        case 'xml':
          resp = resp.responseXML
              && resp.responseXML.parseError // IE trololo
              && resp.responseXML.parseError.errorCode
              && resp.responseXML.parseError.reason
            ? null
            : resp.responseXML
          break
        }
      }

      self._responseArgs.resp = resp
      self._fulfilled = true
      fn(resp)
      self._successHandler(resp)
      while (self._fulfillmentHandlers.length > 0) {
        resp = self._fulfillmentHandlers.shift()(resp)
      }

      complete(resp)
    }

    function timedOut() {
      self._timedOut = true
      self.request.abort()      
    }

    function error(resp, msg, t) {
      resp = self.request
      self._responseArgs.resp = resp
      self._responseArgs.msg = msg
      self._responseArgs.t = t
      self._erred = true
      while (self._errorHandlers.length > 0) {
        self._errorHandlers.shift()(resp, msg, t)
      }
      complete(resp)
    }

    this.request = getRequest.call(this, success, error)
  }

  Reqwest.prototype = {
    abort: function () {
      this._aborted = true
      this.request.abort()
    }

  , retry: function () {
      init.call(this, this.o, this.fn)
    }

    /**
     * Small deviation from the Promises A CommonJs specification
     * http://wiki.commonjs.org/wiki/Promises/A
     */

    /**
     * `then` will execute upon successful requests
     */
  , then: function (success, fail) {
      success = success || function () {}
      fail = fail || function () {}
      if (this._fulfilled) {
        this._responseArgs.resp = success(this._responseArgs.resp)
      } else if (this._erred) {
        fail(this._responseArgs.resp, this._responseArgs.msg, this._responseArgs.t)
      } else {
        this._fulfillmentHandlers.push(success)
        this._errorHandlers.push(fail)
      }
      return this
    }

    /**
     * `always` will execute whether the request succeeds or fails
     */
  , always: function (fn) {
      if (this._fulfilled || this._erred) {
        fn(this._responseArgs.resp)
      } else {
        this._completeHandlers.push(fn)
      }
      return this
    }

    /**
     * `fail` will execute when the request fails
     */
  , fail: function (fn) {
      if (this._erred) {
        fn(this._responseArgs.resp, this._responseArgs.msg, this._responseArgs.t)
      } else {
        this._errorHandlers.push(fn)
      }
      return this
    }
  , 'catch': function (fn) {
      return this.fail(fn)
    }
  }

  function reqwest(o, fn) {
    return new Reqwest(o, fn)
  }

  // normalize newline variants according to spec -> CRLF
  function normalize(s) {
    return s ? s.replace(/\r?\n/g, '\r\n') : ''
  }

  function serial(el, cb) {
    var n = el.name
      , t = el.tagName.toLowerCase()
      , optCb = function (o) {
          // IE gives value="" even where there is no value attribute
          // 'specified' ref: http://www.w3.org/TR/DOM-Level-3-Core/core.html#ID-862529273
          if (o && !o['disabled'])
            cb(n, normalize(o['attributes']['value'] && o['attributes']['value']['specified'] ? o['value'] : o['text']))
        }
      , ch, ra, val, i

    // don't serialize elements that are disabled or without a name
    if (el.disabled || !n) return

    switch (t) {
    case 'input':
      if (!/reset|button|image|file/i.test(el.type)) {
        ch = /checkbox/i.test(el.type)
        ra = /radio/i.test(el.type)
        val = el.value
        // WebKit gives us "" instead of "on" if a checkbox has no value, so correct it here
        ;(!(ch || ra) || el.checked) && cb(n, normalize(ch && val === '' ? 'on' : val))
      }
      break
    case 'textarea':
      cb(n, normalize(el.value))
      break
    case 'select':
      if (el.type.toLowerCase() === 'select-one') {
        optCb(el.selectedIndex >= 0 ? el.options[el.selectedIndex] : null)
      } else {
        for (i = 0; el.length && i < el.length; i++) {
          el.options[i].selected && optCb(el.options[i])
        }
      }
      break
    }
  }

  // collect up all form elements found from the passed argument elements all
  // the way down to child elements; pass a '<form>' or form fields.
  // called with 'this'=callback to use for serial() on each element
  function eachFormElement() {
    var cb = this
      , e, i
      , serializeSubtags = function (e, tags) {
          var i, j, fa
          for (i = 0; i < tags.length; i++) {
            fa = e[byTag](tags[i])
            for (j = 0; j < fa.length; j++) serial(fa[j], cb)
          }
        }

    for (i = 0; i < arguments.length; i++) {
      e = arguments[i]
      if (/input|select|textarea/i.test(e.tagName)) serial(e, cb)
      serializeSubtags(e, [ 'input', 'select', 'textarea' ])
    }
  }

  // standard query string style serialization
  function serializeQueryString() {
    return reqwest.toQueryString(reqwest.serializeArray.apply(null, arguments))
  }

  // { 'name': 'value', ... } style serialization
  function serializeHash() {
    var hash = {}
    eachFormElement.apply(function (name, value) {
      if (name in hash) {
        hash[name] && !isArray(hash[name]) && (hash[name] = [hash[name]])
        hash[name].push(value)
      } else hash[name] = value
    }, arguments)
    return hash
  }

  // [ { name: 'name', value: 'value' }, ... ] style serialization
  reqwest.serializeArray = function () {
    var arr = []
    eachFormElement.apply(function (name, value) {
      arr.push({name: name, value: value})
    }, arguments)
    return arr
  }

  reqwest.serialize = function () {
    if (arguments.length === 0) return ''
    var opt, fn
      , args = Array.prototype.slice.call(arguments, 0)

    opt = args.pop()
    opt && opt.nodeType && args.push(opt) && (opt = null)
    opt && (opt = opt.type)

    if (opt == 'map') fn = serializeHash
    else if (opt == 'array') fn = reqwest.serializeArray
    else fn = serializeQueryString

    return fn.apply(null, args)
  }

  reqwest.toQueryString = function (o, trad) {
    var prefix, i
      , traditional = trad || false
      , s = []
      , enc = encodeURIComponent
      , add = function (key, value) {
          // If value is a function, invoke it and return its value
          value = ('function' === typeof value) ? value() : (value == null ? '' : value)
          s[s.length] = enc(key) + '=' + enc(value)
        }
    // If an array was passed in, assume that it is an array of form elements.
    if (isArray(o)) {
      for (i = 0; o && i < o.length; i++) add(o[i]['name'], o[i]['value'])
    } else {
      // If traditional, encode the "old" way (the way 1.3.2 or older
      // did it), otherwise encode params recursively.
      for (prefix in o) {
        if (o.hasOwnProperty(prefix)) buildParams(prefix, o[prefix], traditional, add)
      }
    }

    // spaces should be + according to spec
    return s.join('&').replace(/%20/g, '+')
  }

  function buildParams(prefix, obj, traditional, add) {
    var name, i, v
      , rbracket = /\[\]$/

    if (isArray(obj)) {
      // Serialize array item.
      for (i = 0; obj && i < obj.length; i++) {
        v = obj[i]
        if (traditional || rbracket.test(prefix)) {
          // Treat each array item as a scalar.
          add(prefix, v)
        } else {
          buildParams(prefix + '[' + (typeof v === 'object' ? i : '') + ']', v, traditional, add)
        }
      }
    } else if (obj && obj.toString() === '[object Object]') {
      // Serialize object item.
      for (name in obj) {
        buildParams(prefix + '[' + name + ']', obj[name], traditional, add)
      }

    } else {
      // Serialize scalar item.
      add(prefix, obj)
    }
  }

  reqwest.getcallbackPrefix = function () {
    return callbackPrefix
  }

  // jQuery and Zepto compatibility, differences can be remapped here so you can call
  // .ajax.compat(options, callback)
  reqwest.compat = function (o, fn) {
    if (o) {
      o['type'] && (o['method'] = o['type']) && delete o['type']
      o['dataType'] && (o['type'] = o['dataType'])
      o['jsonpCallback'] && (o['jsonpCallbackName'] = o['jsonpCallback']) && delete o['jsonpCallback']
      o['jsonp'] && (o['jsonpCallback'] = o['jsonp'])
    }
    return new Reqwest(o, fn)
  }

  reqwest.ajaxSetup = function (options) {
    options = options || {}
    for (var k in options) {
      globalSetupOptions[k] = options[k]
    }
  }

  return reqwest
});

/**
 * UAParser.js v0.7.3
 * Lightweight JavaScript-based User-Agent string parser
 * https://github.com/faisalman/ua-parser-js
 * 
 * Copyright © 2012-2014 Faisal Salman <fyzlman@gmail.com>
 * Dual licensed under GPLv2 & MIT
 */
(function(window,undefined){"use strict";var LIBVERSION="0.7.3",EMPTY="",UNKNOWN="?",FUNC_TYPE="function",UNDEF_TYPE="undefined",OBJ_TYPE="object",MAJOR="major",MODEL="model",NAME="name",TYPE="type",VENDOR="vendor",VERSION="version",ARCHITECTURE="architecture",CONSOLE="console",MOBILE="mobile",TABLET="tablet",SMARTTV="smarttv",WEARABLE="wearable",EMBEDDED="embedded";var util={extend:function(regexes,extensions){for(var i in extensions){if("browser cpu device engine os".indexOf(i)!==-1&&extensions[i].length%2===0){regexes[i]=extensions[i].concat(regexes[i])}}return regexes},has:function(str1,str2){if(typeof str1==="string"){return str2.toLowerCase().indexOf(str1.toLowerCase())!==-1}},lowerize:function(str){return str.toLowerCase()}};var mapper={rgx:function(){var result,i=0,j,k,p,q,matches,match,args=arguments;while(i<args.length&&!matches){var regex=args[i],props=args[i+1];if(typeof result===UNDEF_TYPE){result={};for(p in props){q=props[p];if(typeof q===OBJ_TYPE){result[q[0]]=undefined}else{result[q]=undefined}}}j=k=0;while(j<regex.length&&!matches){matches=regex[j++].exec(this.getUA());if(!!matches){for(p=0;p<props.length;p++){match=matches[++k];q=props[p];if(typeof q===OBJ_TYPE&&q.length>0){if(q.length==2){if(typeof q[1]==FUNC_TYPE){result[q[0]]=q[1].call(this,match)}else{result[q[0]]=q[1]}}else if(q.length==3){if(typeof q[1]===FUNC_TYPE&&!(q[1].exec&&q[1].test)){result[q[0]]=match?q[1].call(this,match,q[2]):undefined}else{result[q[0]]=match?match.replace(q[1],q[2]):undefined}}else if(q.length==4){result[q[0]]=match?q[3].call(this,match.replace(q[1],q[2])):undefined}}else{result[q]=match?match:undefined}}}}i+=2}return result},str:function(str,map){for(var i in map){if(typeof map[i]===OBJ_TYPE&&map[i].length>0){for(var j=0;j<map[i].length;j++){if(util.has(map[i][j],str)){return i===UNKNOWN?undefined:i}}}else if(util.has(map[i],str)){return i===UNKNOWN?undefined:i}}return str}};var maps={browser:{oldsafari:{major:{1:["/8","/1","/3"],2:"/4","?":"/"},version:{"1.0":"/8",1.2:"/1",1.3:"/3","2.0":"/412","2.0.2":"/416","2.0.3":"/417","2.0.4":"/419","?":"/"}}},device:{amazon:{model:{"Fire Phone":["SD","KF"]}},sprint:{model:{"Evo Shift 4G":"7373KT"},vendor:{HTC:"APA",Sprint:"Sprint"}}},os:{windows:{version:{ME:"4.90","NT 3.11":"NT3.51","NT 4.0":"NT4.0",2000:"NT 5.0",XP:["NT 5.1","NT 5.2"],Vista:"NT 6.0",7:"NT 6.1",8:"NT 6.2",8.1:"NT 6.3",10:"NT 6.4",RT:"ARM"}}}};var regexes={browser:[[/(opera\smini)\/((\d+)?[\w\.-]+)/i,/(opera\s[mobiletab]+).+version\/((\d+)?[\w\.-]+)/i,/(opera).+version\/((\d+)?[\w\.]+)/i,/(opera)[\/\s]+((\d+)?[\w\.]+)/i],[NAME,VERSION,MAJOR],[/\s(opr)\/((\d+)?[\w\.]+)/i],[[NAME,"Opera"],VERSION,MAJOR],[/(kindle)\/((\d+)?[\w\.]+)/i,/(lunascape|maxthon|netfront|jasmine|blazer)[\/\s]?((\d+)?[\w\.]+)*/i,/(avant\s|iemobile|slim|baidu)(?:browser)?[\/\s]?((\d+)?[\w\.]*)/i,/(?:ms|\()(ie)\s((\d+)?[\w\.]+)/i,/(rekonq)((?:\/)[\w\.]+)*/i,/(chromium|flock|rockmelt|midori|epiphany|silk|skyfire|ovibrowser|bolt|iron)\/((\d+)?[\w\.-]+)/i],[NAME,VERSION,MAJOR],[/(trident).+rv[:\s]((\d+)?[\w\.]+).+like\sgecko/i],[[NAME,"IE"],VERSION,MAJOR],[/(yabrowser)\/((\d+)?[\w\.]+)/i],[[NAME,"Yandex"],VERSION,MAJOR],[/(comodo_dragon)\/((\d+)?[\w\.]+)/i],[[NAME,/_/g," "],VERSION,MAJOR],[/(chrome|omniweb|arora|[tizenoka]{5}\s?browser)\/v?((\d+)?[\w\.]+)/i,/(uc\s?browser|qqbrowser)[\/\s]?((\d+)?[\w\.]+)/i],[NAME,VERSION,MAJOR],[/(dolfin)\/((\d+)?[\w\.]+)/i],[[NAME,"Dolphin"],VERSION,MAJOR],[/((?:android.+)crmo|crios)\/((\d+)?[\w\.]+)/i],[[NAME,"Chrome"],VERSION,MAJOR],[/version\/((\d+)?[\w\.]+).+?mobile\/\w+\s(safari)/i],[VERSION,MAJOR,[NAME,"Mobile Safari"]],[/version\/((\d+)?[\w\.]+).+?(mobile\s?safari|safari)/i],[VERSION,MAJOR,NAME],[/webkit.+?(mobile\s?safari|safari)((\/[\w\.]+))/i],[NAME,[MAJOR,mapper.str,maps.browser.oldsafari.major],[VERSION,mapper.str,maps.browser.oldsafari.version]],[/(konqueror)\/((\d+)?[\w\.]+)/i,/(webkit|khtml)\/((\d+)?[\w\.]+)/i],[NAME,VERSION,MAJOR],[/(navigator|netscape)\/((\d+)?[\w\.-]+)/i],[[NAME,"Netscape"],VERSION,MAJOR],[/(swiftfox)/i,/(icedragon|iceweasel|camino|chimera|fennec|maemo\sbrowser|minimo|conkeror)[\/\s]?((\d+)?[\w\.\+]+)/i,/(firefox|seamonkey|k-meleon|icecat|iceape|firebird|phoenix)\/((\d+)?[\w\.-]+)/i,/(mozilla)\/((\d+)?[\w\.]+).+rv\:.+gecko\/\d+/i,/(polaris|lynx|dillo|icab|doris|amaya|w3m|netsurf)[\/\s]?((\d+)?[\w\.]+)/i,/(links)\s\(((\d+)?[\w\.]+)/i,/(gobrowser)\/?((\d+)?[\w\.]+)*/i,/(ice\s?browser)\/v?((\d+)?[\w\._]+)/i,/(mosaic)[\/\s]((\d+)?[\w\.]+)/i],[NAME,VERSION,MAJOR]],cpu:[[/(?:(amd|x(?:(?:86|64)[_-])?|wow|win)64)[;\)]/i],[[ARCHITECTURE,"amd64"]],[/(ia32(?=;))/i],[[ARCHITECTURE,util.lowerize]],[/((?:i[346]|x)86)[;\)]/i],[[ARCHITECTURE,"ia32"]],[/windows\s(ce|mobile);\sppc;/i],[[ARCHITECTURE,"arm"]],[/((?:ppc|powerpc)(?:64)?)(?:\smac|;|\))/i],[[ARCHITECTURE,/ower/,"",util.lowerize]],[/(sun4\w)[;\)]/i],[[ARCHITECTURE,"sparc"]],[/((?:avr32|ia64(?=;))|68k(?=\))|arm(?:64|(?=v\d+;))|(?=atmel\s)avr|(?:irix|mips|sparc)(?:64)?(?=;)|pa-risc)/i],[[ARCHITECTURE,util.lowerize]]],device:[[/\((ipad|playbook);[\w\s\);-]+(rim|apple)/i],[MODEL,VENDOR,[TYPE,TABLET]],[/applecoremedia\/[\w\.]+ \((ipad)/],[MODEL,[VENDOR,"Apple"],[TYPE,TABLET]],[/(apple\s{0,1}tv)/i],[[MODEL,"Apple TV"],[VENDOR,"Apple"]],[/(archos)\s(gamepad2?)/i,/(hp).+(touchpad)/i,/(kindle)\/([\w\.]+)/i,/\s(nook)[\w\s]+build\/(\w+)/i,/(dell)\s(strea[kpr\s\d]*[\dko])/i],[VENDOR,MODEL,[TYPE,TABLET]],[/(kf[A-z]+)\sbuild\/[\w\.]+.*silk\//i],[MODEL,[VENDOR,"Amazon"],[TYPE,TABLET]],[/(sd|kf)[0349hijorstuw]+\sbuild\/[\w\.]+.*silk\//i],[[MODEL,mapper.str,maps.device.amazon.model],[VENDOR,"Amazon"],[TYPE,MOBILE]],[/\((ip[honed|\s\w*]+);.+(apple)/i],[MODEL,VENDOR,[TYPE,MOBILE]],[/\((ip[honed|\s\w*]+);/i],[MODEL,[VENDOR,"Apple"],[TYPE,MOBILE]],[/(blackberry)[\s-]?(\w+)/i,/(blackberry|benq|palm(?=\-)|sonyericsson|acer|asus|dell|huawei|meizu|motorola|polytron)[\s_-]?([\w-]+)*/i,/(hp)\s([\w\s]+\w)/i,/(asus)-?(\w+)/i],[VENDOR,MODEL,[TYPE,MOBILE]],[/\(bb10;\s(\w+)/i],[MODEL,[VENDOR,"BlackBerry"],[TYPE,MOBILE]],[/android.+(transfo[prime\s]{4,10}\s\w+|eeepc|slider\s\w+|nexus 7)/i],[MODEL,[VENDOR,"Asus"],[TYPE,TABLET]],[/(sony)\s(tablet\s[ps])/i],[VENDOR,MODEL,[TYPE,TABLET]],[/\s(ouya)\s/i,/(nintendo)\s([wids3u]+)/i],[VENDOR,MODEL,[TYPE,CONSOLE]],[/android.+;\s(shield)\sbuild/i],[MODEL,[VENDOR,"Nvidia"],[TYPE,CONSOLE]],[/(playstation\s[3portablevi]+)/i],[MODEL,[VENDOR,"Sony"],[TYPE,CONSOLE]],[/(sprint\s(\w+))/i],[[VENDOR,mapper.str,maps.device.sprint.vendor],[MODEL,mapper.str,maps.device.sprint.model],[TYPE,MOBILE]],[/(lenovo)\s?(S(?:5000|6000)+(?:[-][\w+]))/i],[VENDOR,MODEL,[TYPE,TABLET]],[/(htc)[;_\s-]+([\w\s]+(?=\))|\w+)*/i,/(zte)-(\w+)*/i,/(alcatel|geeksphone|huawei|lenovo|nexian|panasonic|(?=;\s)sony)[_\s-]?([\w-]+)*/i],[VENDOR,[MODEL,/_/g," "],[TYPE,MOBILE]],[/[\s\(;](xbox(?:\sone)?)[\s\);]/i],[MODEL,[VENDOR,"Microsoft"],[TYPE,CONSOLE]],[/(kin\.[onetw]{3})/i],[[MODEL,/\./g," "],[VENDOR,"Microsoft"],[TYPE,MOBILE]],[/\s((milestone|droid(?:[2-4x]|\s(?:bionic|x2|pro|razr))?(:?\s4g)?))[\w\s]+build\//i,/(mot)[\s-]?(\w+)*/i],[[VENDOR,"Motorola"],MODEL,[TYPE,MOBILE]],[/android.+\s(mz60\d|xoom[\s2]{0,2})\sbuild\//i],[MODEL,[VENDOR,"Motorola"],[TYPE,TABLET]],[/android.+((sch-i[89]0\d|shw-m380s|gt-p\d{4}|gt-n8000|sgh-t8[56]9|nexus 10))/i,/((SM-T\w+))/i],[[VENDOR,"Samsung"],MODEL,[TYPE,TABLET]],[/((s[cgp]h-\w+|gt-\w+|galaxy\snexus|sm-n900))/i,/(sam[sung]*)[\s-]*(\w+-?[\w-]*)*/i,/sec-((sgh\w+))/i],[[VENDOR,"Samsung"],MODEL,[TYPE,MOBILE]],[/(samsung);smarttv/i],[VENDOR,MODEL,[TYPE,SMARTTV]],[/\(dtv[\);].+(aquos)/i],[MODEL,[VENDOR,"Sharp"],[TYPE,SMARTTV]],[/sie-(\w+)*/i],[MODEL,[VENDOR,"Siemens"],[TYPE,MOBILE]],[/(maemo|nokia).*(n900|lumia\s\d+)/i,/(nokia)[\s_-]?([\w-]+)*/i],[[VENDOR,"Nokia"],MODEL,[TYPE,MOBILE]],[/android\s3\.[\s\w-;]{10}(a\d{3})/i],[MODEL,[VENDOR,"Acer"],[TYPE,TABLET]],[/android\s3\.[\s\w-;]{10}(lg?)-([06cv9]{3,4})/i],[[VENDOR,"LG"],MODEL,[TYPE,TABLET]],[/(lg) netcast\.tv/i],[VENDOR,MODEL,[TYPE,SMARTTV]],[/(nexus\s[45])/i,/lg[e;\s\/-]+(\w+)*/i],[MODEL,[VENDOR,"LG"],[TYPE,MOBILE]],[/android.+(ideatab[a-z0-9\-\s]+)/i],[MODEL,[VENDOR,"Lenovo"],[TYPE,TABLET]],[/linux;.+((jolla));/i],[VENDOR,MODEL,[TYPE,MOBILE]],[/((pebble))app\/[\d\.]+\s/i],[VENDOR,MODEL,[TYPE,WEARABLE]],[/android.+;\s(glass)\s\d/i],[MODEL,[VENDOR,"Google"],[TYPE,WEARABLE]],[/(mobile|tablet);.+rv\:.+gecko\//i],[[TYPE,util.lowerize],VENDOR,MODEL]],engine:[[/(presto)\/([\w\.]+)/i,/(webkit|trident|netfront|netsurf|amaya|lynx|w3m)\/([\w\.]+)/i,/(khtml|tasman|links)[\/\s]\(?([\w\.]+)/i,/(icab)[\/\s]([23]\.[\d\.]+)/i],[NAME,VERSION],[/rv\:([\w\.]+).*(gecko)/i],[VERSION,NAME]],os:[[/microsoft\s(windows)\s(vista|xp)/i],[NAME,VERSION],[/(windows)\snt\s6\.2;\s(arm)/i,/(windows\sphone(?:\sos)*|windows\smobile|windows)[\s\/]?([ntce\d\.\s]+\w)/i],[NAME,[VERSION,mapper.str,maps.os.windows.version]],[/(win(?=3|9|n)|win\s9x\s)([nt\d\.]+)/i],[[NAME,"Windows"],[VERSION,mapper.str,maps.os.windows.version]],[/\((bb)(10);/i],[[NAME,"BlackBerry"],VERSION],[/(blackberry)\w*\/?([\w\.]+)*/i,/(tizen)[\/\s]([\w\.]+)/i,/(android|webos|palm\os|qnx|bada|rim\stablet\sos|meego|contiki)[\/\s-]?([\w\.]+)*/i,/linux;.+(sailfish);/i],[NAME,VERSION],[/(symbian\s?os|symbos|s60(?=;))[\/\s-]?([\w\.]+)*/i],[[NAME,"Symbian"],VERSION],[/\((series40);/i],[NAME],[/mozilla.+\(mobile;.+gecko.+firefox/i],[[NAME,"Firefox OS"],VERSION],[/(nintendo|playstation)\s([wids3portablevu]+)/i,/(mint)[\/\s\(]?(\w+)*/i,/(mageia|vectorlinux)[;\s]/i,/(joli|[kxln]?ubuntu|debian|[open]*suse|gentoo|arch|slackware|fedora|mandriva|centos|pclinuxos|redhat|zenwalk|linpus)[\/\s-]?([\w\.-]+)*/i,/(hurd|linux)\s?([\w\.]+)*/i,/(gnu)\s?([\w\.]+)*/i],[NAME,VERSION],[/(cros)\s[\w]+\s([\w\.]+\w)/i],[[NAME,"Chromium OS"],VERSION],[/(sunos)\s?([\w\.]+\d)*/i],[[NAME,"Solaris"],VERSION],[/\s([frentopc-]{0,4}bsd|dragonfly)\s?([\w\.]+)*/i],[NAME,VERSION],[/(ip[honead]+)(?:.*os\s*([\w]+)*\slike\smac|;\sopera)/i],[[NAME,"iOS"],[VERSION,/_/g,"."]],[/(mac\sos\sx)\s?([\w\s\.]+\w)*/i,/(macintosh|mac(?=_powerpc)\s)/i],[[NAME,"Mac OS"],[VERSION,/_/g,"."]],[/((?:open)?solaris)[\/\s-]?([\w\.]+)*/i,/(haiku)\s(\w+)/i,/(aix)\s((\d)(?=\.|\)|\s)[\w\.]*)*/i,/(plan\s9|minix|beos|os\/2|amigaos|morphos|risc\sos|openvms)/i,/(unix)\s?([\w\.]+)*/i],[NAME,VERSION]]};var UAParser=function(uastring,extensions){if(!(this instanceof UAParser)){return new UAParser(uastring,extensions).getResult()}var ua=uastring||(window&&window.navigator&&window.navigator.userAgent?window.navigator.userAgent:EMPTY);var rgxmap=extensions?util.extend(regexes,extensions):regexes;this.getBrowser=function(){return mapper.rgx.apply(this,rgxmap.browser)};this.getCPU=function(){return mapper.rgx.apply(this,rgxmap.cpu)};this.getDevice=function(){return mapper.rgx.apply(this,rgxmap.device)};this.getEngine=function(){return mapper.rgx.apply(this,rgxmap.engine)};this.getOS=function(){return mapper.rgx.apply(this,rgxmap.os)};this.getResult=function(){return{ua:this.getUA(),browser:this.getBrowser(),engine:this.getEngine(),os:this.getOS(),device:this.getDevice(),cpu:this.getCPU()}};this.getUA=function(){return ua};this.setUA=function(uastring){ua=uastring;return this};this.setUA(ua)};UAParser.VERSION=LIBVERSION;UAParser.BROWSER={NAME:NAME,MAJOR:MAJOR,VERSION:VERSION};UAParser.CPU={ARCHITECTURE:ARCHITECTURE};UAParser.DEVICE={MODEL:MODEL,VENDOR:VENDOR,TYPE:TYPE,CONSOLE:CONSOLE,MOBILE:MOBILE,SMARTTV:SMARTTV,TABLET:TABLET,WEARABLE:WEARABLE,EMBEDDED:EMBEDDED};UAParser.ENGINE={NAME:NAME,VERSION:VERSION};UAParser.OS={NAME:NAME,VERSION:VERSION};if(typeof exports!==UNDEF_TYPE){if(typeof module!==UNDEF_TYPE&&module.exports){exports=module.exports=UAParser}exports.UAParser=UAParser}else{window.UAParser=UAParser;if(typeof define===FUNC_TYPE&&define.amd){define('utils/../../vendor/ua-parser-js/dist/ua-parser.min',[],function(){return UAParser})}var $=window.jQuery||window.Zepto;if(typeof $!==UNDEF_TYPE){var parser=new UAParser;$.ua=parser.getResult();$.ua.get=function(){return parser.getUA()};$.ua.set=function(uastring){parser.setUA(uastring);var result=parser.getResult();for(var prop in result){$.ua[prop]=result[prop]}}}}})(this);
define(
'utils/environment',[
    '../../vendor/ua-parser-js/dist/ua-parser.min'
],

function(UAParser) {
    'use strict';

    var parser = new UAParser();
    var result = parser.getResult();

    result.isTouchEnabled = !!(('ontouchstart' in window) || window.DocumentTouch && document instanceof window.DocumentTouch);
    return result;
});

/* jshint -W083 */

define(
'utils/utils',[
    'uuid',
    '../../vendor/reqwest/reqwest',
    'utils/environment'
],

function(uuid, reqwest, env) {
    'use strict';

    var hasOwn = Object.prototype.hasOwnProperty;
    var toString = Object.prototype.toString;

    var isPlainObject = function isPlainObject(obj) {
        if (!obj || toString.call(obj) !== '[object Object]' || obj.nodeType || obj.setInterval) {
            return false;
        }

        var hasOwnConstructor = hasOwn.call(obj, 'constructor');
        var hasIsPropertyOfMethod = obj.constructor && obj.constructor.prototype && hasOwn.call(obj.constructor.prototype, 'isPrototypeOf');
        // Not own constructor property must be Object
        if (obj.constructor && !hasOwnConstructor && !hasIsPropertyOfMethod) {
            return false;
        }

        // Own properties are enumerated firstly, so to speed up,
        // if last one is own, then all properties are own.
        var key;

        // jscs:disable
        for (key in obj) {
        }
        // jscs:enable

        return key === undefined || hasOwn.call(obj, key);
    };

    var extend = (function() {
        var extend = function extend() {
            // jscs:disable
            var options, name, src, copy, copyIsArray, clone;
            var target = arguments[0];
            var i = 1;
            var length = arguments.length;
            var deep = false;
            // jscs:enable

            // Handle a deep copy situation
            if (typeof target === 'boolean') {
                deep = target;
                target = arguments[1] || {};
                // skip the boolean and the target
                i = 2;
            } else if (typeof target !== 'object' && typeof target !== 'function' || target === undefined) {
                target = {};
            }

            for (; i < length; ++i) {
            // Only deal with non-null/undefined values
                if ((options = arguments[i]) !== null) {
                // Extend the base object
                    for (name in options) {
                        src = target[name];
                        copy = options[name];

                        // Prevent never-ending loop
                        if (target === copy) {
                            continue;
                        }

                        // [Tomer] If we're dealing with a property with getter/setter, instead of cloning the value, define the property's getter/setter.
                        if (Object.getOwnPropertyDescriptor && Object.getOwnPropertyDescriptor(options, name) &&
                            (Object.getOwnPropertyDescriptor(options, name).get || Object.getOwnPropertyDescriptor(options, name).set)) {
                            Object.defineProperty(target, name, Object.getOwnPropertyDescriptor(options, name));
                            continue;
                        }

                        // Recurse if we're merging plain objects or arrays
                        if (deep && copy && (isPlainObject(copy) || (copyIsArray = Array.isArray(copy)))) {
                            if (copyIsArray) {
                                copyIsArray = false;
                                clone = src && Array.isArray(src) ? src : [];
                            } else {
                                clone = src && isPlainObject(src) ? src : {};
                            }

                            // Never move original objects, clone them
                            target[name] = extend(deep, clone, copy);

                            // Don't bring in undefined values
                        } else if (copy !== undefined) {
                            target[name] = copy;
                        }
                    }
                }
            }

            // Return the modified object
            return target;
        };

        return extend;
    })();

    /* Simple JavaScript Inheritance
     * By John Resig http://ejohn.org/
     * MIT Licensed.
     */
    var Class = (function() {
        var initializing = false;
        // jscs:disable
        var fnTest = /xyz/.test(function() { var xyz; xyz = 0; }) ?
                            /\b_super\b/ :
                            /.*/;
        // jscs:enable

        // The base Class implementation (does nothing)
        var Class = function() {};

        // Create a new Class that inherits from this class
        Class.extend = function extend(prop) {
            var _super = this.prototype;

            // Instantiate a base class (but only create the instance,
            // don't run the init constructor)
            initializing = true;
            var prototype = new this();
            initializing = false;

            // Copy the properties over onto the new prototype
            for (var name in prop) {
                // Check if we're overwriting an existing function
                prototype[name] =
                    (typeof prop[name] === 'function' && typeof _super[name] === 'function' && fnTest.test(prop[name])) ?
                        (function(name, fn) {
                            return function() {
                                var tmp = this._super;

                                // Add a new ._super() method that is the same method
                                // but on the super-class
                                this._super = _super[name];

                                // The method only need to be bound temporarily, so we
                                // remove it when we're done executing
                                var ret = fn.apply(this, arguments);
                                this._super = tmp;

                                return ret;
                            };
                        })(name, prop[name]) :
                        prop[name];
            }

            // The dummy class constructor
            function Class() {
                // All construction is actually done in the init method
                if (!initializing && this.init) {
                    this.init.apply(this, arguments);
                }
            }

            // Populate our constructed prototype object
            Class.prototype = prototype;

            // Enforce the constructor to be what we expect
            Class.prototype.constructor = Class;

            // And make this class extendable
            Class.extend = extend;

            return Class;
        };

        return Class;
    })();

    function createMethod(methodName) {
        return function() {
            console.warn('The "' + methodName + '" method is not available.');
        };
    }

    function get(url, type) {
        var req = {
            url: url,
            method: 'get',
            crossOrigin: true
        };

        if (type) {
            req.type = type;
        }

        return reqwest(req);
    }

    function post(url, data, type) {
        var req = {
            url: url,
            method: 'post',
            crossOrigin: true,
            data: data
        };

        if (type) {
            req.contentType = type;
        }

        return reqwest(req);
    }

    function isUrl(url) {
        return (typeof url === 'string') && (url.startsWith('http://') || url.startsWith('https://'));
    }

    /**
     * Returns a property of object by using dot notation.
     * Examples:
     *      getObjectProperty({a: {b: 'coolio'}}, 'a.b');                   // Returns 'coolio'
     *      getObjectProperty({a: {b: 'coolio'}}, 'a.b.c');                 // Returns null
     *      getObjectProperty({a: {b: 'coolio'}}, 'a.b.c', 'default');      // Returns 'default'
     */
    function getObjectProperty(obj, str, defaultVal) {
        var retVal = (defaultVal !== undefined) ? defaultVal : null;

        str = str.split('.');
        for (var i = 0; i < str.length; i++) {
            if (obj && obj.hasOwnProperty(str[i])) {
                obj = obj[str[i]];
            } else {
                obj = null;
                break;
            }
        }

        return (obj === null || obj === undefined) ? retVal : obj;
    }

    /**
     * Sets a property of object by using dot notation.
     * Returns new value.
     * Examples:
     *      setObjectProperty({a: {b: 'coolio'}}, 'a.b', 'not coolio');     // Returns 'not coolio'
     *      setObjectProperty({a: {b: 'coolio'}}, 'a.b.c', 'ya');           // Returns 'ya'
     */
    function setObjectProperty(obj, str, val) {
        var newObj = {};
        var currObj = newObj;

        str = str.split('.');
        for (var i = 0; i < str.length; i++) {
            if (i !== str.length - 1) {
                currObj[str[i]] = {};
                currObj = currObj[str[i]];
            } else {
                currObj[str[i]] = val;
            }
        }

        extend(true, obj, newObj);

        return val;
    }

    /**
    * makeWorker is a little util for generating web workers from script strings.
    * Optionally takes 'log' as second argument to log any errors.
    */
    function makeWorker(script, log) {
        var URL = window.URL || window.webkitURL;
        var Blob = window.Blob;
        var Worker = window.Worker;

        if (!URL || !Blob || !Worker) {
            if (log) {
                log.error('Workers are not supported on current environment.');
            }
            return null;
        }

        if (!script) {
            if (log) {
                log.error('Must supply script text in order to create a worker.');
            }
            return null;
        }

        var blob = new Blob([script], {type: 'text/javascript'});
        var worker = new Worker(URL.createObjectURL(blob));
        return worker;
    }

    /** Cross-browser implementation for dispatching a simple Event. */
    function dispatchSimpleEvent(target, type, bubbles, cancelable) {
        bubbles = !!bubbles;
        cancelable = !!cancelable;

        if (env.browser.name === 'IE') {
            var evt = document.createEvent('Event');
            evt.initEvent(type, bubbles, cancelable);
            target.dispatchEvent(evt);
        } else {
            target.dispatchEvent(new Event(type, {bubbles: bubbles, cancelable: cancelable}));
        }
    }

    /////////////////////////////////////////////////////////////////////////
    // Extend Basic Types
    /////////////////////////////////////////////////////////////////////////

    /** can - Check if an object defines a function. */
    if (!Object.prototype.can) {
        Object.defineProperty(Object.prototype, 'can', {
            enumerable: false,
            value: function(method) {
                return (typeof this[method] === 'function');
            }
        });
    }

    /** conforms - Check if an object conforms/implements an interface. */
    if (!Object.prototype.conforms) {
        Object.defineProperty(Object.prototype, 'conforms', {
            enumerable: false,
            value: function(interfaceObj) {
                var retVal = true;

                for (var prop in interfaceObj) {
                    if (typeof this[prop] !== typeof interfaceObj[prop]) {
                        retVal = false;
                        break;
                    }
                }

                return retVal;
            }
        });
    }

    /** Add the endsWith function to String's prototype. */
    if (typeof String.prototype.endsWith !== 'function') {
        String.prototype.endsWith = function(suffix) {
            return this.indexOf(suffix, this.length - suffix.length) !== -1;
        };
    }

    /** Add the startsWith function to String's prototype. */
    if (typeof String.prototype.startsWith !== 'function') {
        String.prototype.startsWith = function(prefix) {
            return this.indexOf(prefix) === 0;
        };
    }

    return {
        extend: extend,
        isPlainObject: isPlainObject,
        isUrl: isUrl,
        createMethod: createMethod,
        get: get,
        post: post,
        uuid: uuid.v4,
        getObjectProperty: getObjectProperty,
        setObjectProperty: setObjectProperty,
        Class: Class,
        makeWorker: makeWorker,
        dispatchSimpleEvent: dispatchSimpleEvent
    };
});

/*!
 * EventEmitter v4.2.11 - git.io/ee
 * Unlicense - http://unlicense.org/
 * Oliver Caldwell - http://oli.me.uk/
 * @preserve
 */
(function(){"use strict";function t(){}function i(t,n){for(var e=t.length;e--;)if(t[e].listener===n)return e;return-1}function n(e){return function(){return this[e].apply(this,arguments)}}var e=t.prototype,r=this,s=r.EventEmitter;e.getListeners=function(n){var r,e,t=this._getEvents();if(n instanceof RegExp){r={};for(e in t)t.hasOwnProperty(e)&&n.test(e)&&(r[e]=t[e])}else r=t[n]||(t[n]=[]);return r},e.flattenListeners=function(t){var e,n=[];for(e=0;e<t.length;e+=1)n.push(t[e].listener);return n},e.getListenersAsObject=function(n){var e,t=this.getListeners(n);return t instanceof Array&&(e={},e[n]=t),e||t},e.addListener=function(r,e){var t,n=this.getListenersAsObject(r),s="object"==typeof e;for(t in n)n.hasOwnProperty(t)&&-1===i(n[t],e)&&n[t].push(s?e:{listener:e,once:!1});return this},e.on=n("addListener"),e.addOnceListener=function(e,t){return this.addListener(e,{listener:t,once:!0})},e.once=n("addOnceListener"),e.defineEvent=function(e){return this.getListeners(e),this},e.defineEvents=function(t){for(var e=0;e<t.length;e+=1)this.defineEvent(t[e]);return this},e.removeListener=function(r,s){var n,e,t=this.getListenersAsObject(r);for(e in t)t.hasOwnProperty(e)&&(n=i(t[e],s),-1!==n&&t[e].splice(n,1));return this},e.off=n("removeListener"),e.addListeners=function(e,t){return this.manipulateListeners(!1,e,t)},e.removeListeners=function(e,t){return this.manipulateListeners(!0,e,t)},e.manipulateListeners=function(r,t,i){var e,n,s=r?this.removeListener:this.addListener,o=r?this.removeListeners:this.addListeners;if("object"!=typeof t||t instanceof RegExp)for(e=i.length;e--;)s.call(this,t,i[e]);else for(e in t)t.hasOwnProperty(e)&&(n=t[e])&&("function"==typeof n?s.call(this,e,n):o.call(this,e,n));return this},e.removeEvent=function(e){var t,r=typeof e,n=this._getEvents();if("string"===r)delete n[e];else if(e instanceof RegExp)for(t in n)n.hasOwnProperty(t)&&e.test(t)&&delete n[t];else delete this._events;return this},e.removeAllListeners=n("removeEvent"),e.emitEvent=function(r,o){var e,i,t,s,n=this.getListenersAsObject(r);for(t in n)if(n.hasOwnProperty(t))for(i=n[t].length;i--;)e=n[t][i],e.once===!0&&this.removeListener(r,e.listener),s=e.listener.apply(this,o||[]),s===this._getOnceReturnValue()&&this.removeListener(r,e.listener);return this},e.trigger=n("emitEvent"),e.emit=function(e){var t=Array.prototype.slice.call(arguments,1);return this.emitEvent(e,t)},e.setOnceReturnValue=function(e){return this._onceReturnValue=e,this},e._getOnceReturnValue=function(){return this.hasOwnProperty("_onceReturnValue")?this._onceReturnValue:!0},e._getEvents=function(){return this._events||(this._events={})},t.noConflict=function(){return r.EventEmitter=s,t},"function"==typeof define&&define.amd?define('utils/../../vendor/eventemitter/EventEmitter.min',[],function(){return t}):"object"==typeof module&&module.exports?module.exports=t:r.EventEmitter=t}).call(this);
define('utils/EventEmitter',[
    '../../vendor/eventemitter/EventEmitter.min'
],

/**
 * A wrapper for Wolfy87's EventEmitter.
 * Makes minor changes to EventEmitter's API, most notably, trigger() will take 1...N arguments.
 */
function(EE) {
    'use strict';

    var EventEmitter = function() {
        // Call super constructor
        EE.apply(this, arguments);
    };

    EventEmitter.prototype = Object.create(EE.prototype);
    EventEmitter.prototype.constructor = EventEmitter;
    EventEmitter.prototype.on = EE.prototype.on;
    EventEmitter.prototype.off = EE.prototype.off;
    EventEmitter.prototype.one = EE.prototype.once;
    EventEmitter.prototype.trigger = EE.prototype.emit;

    return EventEmitter;
});

define('error/errorMap',[
],

/**
 * A map of all error IDs used in the player and their messages.
 */
function() {
    'use strict';

    return {
        1: 'html5 video tag could not be found',
        2: 'Cannot force technology, unknown tech: {0}',
        3: 'Could not find any supported video technology',
        4: 'Unknown video format: {0}',
        5: 'Error appending ArrayBuffer to MSE SourceBuffer. {0}: {1}',
        6: 'Could not create MediaSource instance',
        7: 'MSE is not available',
        8: 'Segment "{0}" does not include "{1}" source',
        9: 'Unable to initialize worker',
        10: 'Unkown msg: {0}',
        11: 'The "{0}" factory is not available.',
        12: 'Could not create cue point - no arguments given',
        13: 'Could not create node - no arguments given',
        14: 'Cannot create node - input arguments are not valid',
        15: 'Cannot create prefetch object with input: {0}',
        16: 'Could not create segment - no arguments given',
        17: 'Cannot create segment - input arguments are not valid',
        18: 'Automatic IMC segment creation is not yet implemented',
        19: 'Unable to set critical time, value {0} is not a number',
        20: 'Cannot set critical time - object missing a "{0}" property',
        21: 'Invalid value supplied to currentNodeIndex: {0}',
        22: 'Requested currentNodeIndex is beyond bounds',
        23: 'Invalid value supplied to currenTime: {0}',
        24: 'Requested currentTime is beyond bounds',
        25: 'Could not perform seek operation',
        26: 'No video container element (or selector) provided',
        27: 'Could not retrieve container element',
        28: 'Could not find or create node: {0}',
        29: 'Could not create segment',
        30: 'Invalid argument given to updateSegment(): {0}',
        31: 'No segmentId given',
        32: 'Segment could not be found: {0}',
        33: 'Could not create node',
        34: 'Invalid argument given to updateNode(): {0}',
        35: 'No nodeId given',
        36: 'Node could not be found: {0}',
        37: 'Could not create cue point',
        38: 'Could not get node, given argument: {0}',
        39: 'Could not get node or cuePointId is not a string',
        40: 'Prefetch argument must contain a nodeId property',
        41: 'Could not get node',
        42: 'Could not get node or prefetchNodeId is not a string',
        43: 'Node does not contain a segment: {1}',
        44: 'Unknown string given to switchChannel: {0}. Expected either a numeric segment\'s zero-based index, or the string \'up\' or \'down\'',
        45: 'Invalid index given to switchChannel: {0}',
        46: 'Engine failed to switch channel',
        47: 'Could not find plugin: {0}',
        48: 'Plugin is not a function: {0}',
        49: 'Error setting SourceBuffer\'s timestampOffset. {0}: {1}',
        50: 'MSE SourceBuffer error event.',
        51: 'Error ending MediaSource stream. {0}: {1}',
        52: 'Automatic ts segment creation is not yet implemented',
        53: 'Source type {0} is not supported',
        54: 'Seek inside node not implemented yet in hls',
        55: 'Hls error building playlist',
        56: 'Hls failed init playlist in server',
        57: 'Hls failed update playlist in server',
        58: 'Hls cannot update playlist, initPlaylist should be called first',
        59: 'Hls illegal segment data',
        60: 'Hls trying to set illegal offset time {0}',
        61: 'Hls cannot switch channel, current node is not parallel or we already in critical time',
        62: 'Unable to embed SWF',
        63: 'Error loading SWF',
        64: 'Invalid EngineTech configuration, \'engines\' must be a non-empty array (or function that returns a non-empty array): {0}'
    };
});

define('error/InterludeError',[
    'error/errorMap'
],

/**
 * Class for InterludeError to be thrown or triggered (as argument in event).
 */
function(errorMap) {
    'use strict';

    function formatString(format, args) {
        return format.replace(/{(\d+)}/g, function(match, number) {
            return (typeof args[number] !== 'undefined') ?
                args[number] :
                match;
        });
    }

    function getErrorMessage(errorId, args) {
        return errorMap.hasOwnProperty(errorId) ? formatString(errorMap[errorId], args) : '';
    }

    function InterludeError(errorId) {
        this.args = Array.prototype.slice.call(arguments, 1);

        if (typeof errorId === 'number') {
            this.id = errorId;
            this.message = getErrorMessage(errorId, this.args);
        } else if (typeof errorId === 'string') {
            this.id = NaN;
            this.message = errorId;
        }
    }

    InterludeError.prototype = new Error();
    InterludeError.prototype.constructor = InterludeError;

    return InterludeError;
});

/* jshint -W083 */

define('modules/Base',[
    'utils/loggerFactory',
    'utils/utils',
    'utils/EventEmitter',
    'error/InterludeError'
],

/**
 * Base module - contains code that's common to all modules.
 * Concrete modules will extend this base.
 *
 * Example of extending base class:
 * ---------------------------------
 *
    define([
        'modules/Base'
    ],

    function(BaseModuleClass) {
        'use strict';

        var MyModule = BaseModuleClass.extend({
            // Must supply a name
            // The name will be used to get the modules settings from the general options object
            name: 'MyModule',

            // Can supply a default object (no need to nest under module name)
            // This will be merged into settings which can be overridden by user options.
            // It is encouraged that ALL possible settings be provided in the 'defaults' object.
            // That way, user can always override any setting.
            defaults: {
                a: true,
                b: {
                    okey: 'dokey'
                }
            },

            // Set the logging level of this module
            // Options are: trace, debug, info, warn, error, disable.
            // While developing the module this should be set to 'trace'.
            // When module development is done, change this to either 'warn', 'error' or 'disable'.
            logLevel: 'trace',

            // Define methods
            // These methods (myFunction, myOtherFunction) will be accessible from object instance, but not necessarily through exports
            // (you can choose to expose them via the expose() method).
            myPublicFunction: function() {
                if (this.getSetting('b.okey') === 'dokey') {
                    this.log.debug('Yay! Calling myInternalFunction...');
                    this.myInternalFunction();
                }
                return 'bye';
            },

            myInternalFunction: function() {
                this.log.debug('myInternalFunction');
            },

            // Provide constructor implementation
            // This will be called when user calls 'new MyModule()'.
            // Arguments will be passed to this constructor, except for first argument (options) which is handled automatically.
            ctor: function() {
                // Call parent ctor if needed
                this._super();

                // You can define properties with getters/setters.
                this.defineProperty('myProperty', function() {
                    this.log.debug('[myProperty] -> IN GETTER');
                    return this.getSetting('a');
                }, function(value) {
                    this.log.debug('[myProperty] -> IN SETTER');
                    return this.setSetting('a', value);
                });

                // Finally, expose API (will be available in 'exports')
                this.expose({
                    // Exposing a function
                    myPublicFunction: this.myPublicFunction,

                    // Exposing a property that was defined with 'this.defineProperty()' (getter/setter)
                    myProperty: this.exposeProperty('myProperty')
                });
            }
        });

        return MyModule;
    });
 *
 *
 */
function(loggerFactory, utils, EventEmitter, InterludeError) {
    'use strict';

    /** Base class type for Interlude modules. */
    var BaseModuleClass = utils.Class.extend({

        ////////////////////////////////////////////////////////////////////////////////
        // Instance Properties
        ////////////////////////////////////////////////////////////////////////////////

        /** Module name. Used for logger and for retrieving the settings from options. */
        name: null,

        /** Instance of logger initialized with module name. */
        log: null,

        /** Defaults - plain object. Should contain all default settings. Should not be nested under module name. */
        defaults: null,

        /** Settings object created by merging the defaults with user supplied options. */
        settings: null,

        /** Debug function for testing. */
        debug: null,

        /** Extending modules should expose their public API via the 'exports' property. */
        exports: null,

        /** Will be populated with first argument (options) passed to constructor. */
        options: null,

        /** If set to true, will throw error if this.notifyError() is invoked, but no 'error' listeners were added. Can be overridden in runtime via options. */
        throwErrors: false,

        /** Calls to functions in this array will not be auto-debugged. If instead array, value is the string '*', no function will be autodebugged. */
        excludeFuncFromAutoDebug: [],

        /** Values in this array will be concatenated with excludeFuncFromAutoDebug. */
        baseExcludeFuncFromAutoDebug: ['ctor', 'getSetting', 'setSetting', 'expose', 'defineProperty', 'exposeProperty',
         'getListenersAsObject', 'getListeners', '_getEvents', 'on', 'once', 'one', 'off', 'addListener', '_getOnceReturnValue', 'defineEvents', 'defineEvent',
         'trigger', 'emitEvent', 'addErrorListener'],

        /** The lowest level (string) where logging is enabled. Options are: trace, debug, info, warn, error, disable. */
        logLevel: 'trace',

        ////////////////////////////////////////////////////////////////////////////////
        // Instance Methods
        ////////////////////////////////////////////////////////////////////////////////

        /** Base Constructor */
        init: function(options) {
            // Execute common init code
            this.options = options;
            this.logLevel = (options && options[this.name] && options[this.name].logLevel) || this.logLevel;
            this.log = loggerFactory.createLogger(this.name);
            this.log.setLevel(this.logLevel);
            this.log.debug('init: ' + JSON.stringify((options && options[this.name]) || {}, null, '\t'));
            options = options || {};

            // Create settings object
            this.settings = utils.extend(true, {}, this.settings, this.defaults, options[this.name] || {});
            this.debug = (this.settings[this.name] && this.settings[this.name].debug) || function() {};

            // Bind all functions that are defined on this object to this instance
            var noAutoDebug = (this.baseExcludeFuncFromAutoDebug === '*' || this.excludeFuncFromAutoDebug === '*') ?
                                        '*' :
                                        this.baseExcludeFuncFromAutoDebug.concat(this.excludeFuncFromAutoDebug);
            var _this = this;
            var logIntLevel = this.log.getIntLevel();

            for (var prop in this) {
                if (typeof this[prop] === 'function') {
                    // If this function is excluded from auto debug, simply bind it
                    // Since auto debugging incurs some performance hit even when logger is off,
                    // we will not bind the auto-debugged function if logger level is above 'debug'.
                    if (logIntLevel > 1 || noAutoDebug === '*' || noAutoDebug.indexOf(prop) >= 0) {
                        this[prop] = this[prop].bind(this);
                    }

                    // Otherwise, add auto debug to function
                    else {
                        this[prop] = (function(fn, prop) {
                            var argsToStrings = function(args) {
                                var retVal = [];

                                for (var i = 0; i < args.length; i++) {
                                    switch (typeof args[i]) {
                                        case 'string':
                                            retVal.push('\'' + args[i] + '\'');
                                            break;
                                        case 'object':
                                            if (args[i] === null) {
                                                retVal.push('null');
                                            } else if (Array.isArray(args[i])) {
                                                retVal.push('[' + argsToStrings(args[i]).join(', ') + ']');
                                            } else {
                                                retVal.push(args[i]);
                                            }
                                            break;
                                        case 'undefined':
                                            retVal.push('undefined');
                                            break;
                                        default:
                                            retVal.push(args[i]);
                                            break;
                                    }
                                }

                                return retVal;
                            };

                            return function() {
                                var args = Array.prototype.slice.call(arguments, 0);
                                _this.log.debug('-> ' + prop + '(' + argsToStrings(args).join(', ') + ')');
                                return fn.apply(_this, arguments);
                            };
                        })(this[prop], prop); // jshint -W083
                    }
                }

                // If this is an object, create a clone, so as to fix cross-instance contamination
                else if (typeof this[prop] === 'object' && this[prop] !== null) {
                    if (Array.isArray(this[prop])) {
                        this[prop] = this[prop].slice(0);
                    } else {
                        this[prop] = utils.extend(true, {}, this[prop]);
                    }
                }
            }

            // Listen to window unload event in order to cleanly dispose of object
            if (window) {
                window.addEventListener('unload', this.dispose);
            }

            // Call concrete constructor, without first argument (options) which was handled by this function
            this.ctor.apply(this, Array.prototype.slice.call(arguments, 1));
        },

        /**
         * Concrete constructor to be overriden in extending classes. Will be called with arguments used in "new" expression,
         * except the first argument (options), which is handled in init function.
         * The 'options' parameter passed to constructor is accessible to 'ctor()' via this.options
         */
        ctor: function() {},

        /** Get a setting from settings object - propName should be a string using dot notation. */
        getSetting: function(propName, defaultValue) {
            return utils.getObjectProperty(this.settings, propName, defaultValue);
        },

        /** Get a setting from settings object - propName should be a string using dot notation. */
        setSetting: function(propName, value) {
            return utils.setObjectProperty(this.settings, propName, value);
        },

        /** Expose/merge an object to 'exports'. */
        expose: function(exposeObj) {
            this.exports = utils.extend(true, this.exports || {}, exposeObj);
        },

        /** Defines a property with getter and/or setter on object. */
        defineProperty: function(propName, getFn, setFn) {
            var propObj = {enumerable: true};
            if (getFn && typeof getFn === 'function') {
                propObj.get = getFn.bind(this);
            }
            if (setFn && typeof setFn === 'function') {
                propObj.set = setFn.bind(this);
            }
            Object.defineProperty(this, propName, propObj);
        },

        /** Expose a property (to 'exports') that was defined via 'this.defineProperty()'. */
        exposeProperty: function(propName, exportsName) {
            var propObj = Object.getOwnPropertyDescriptor(this, propName);

            if (propObj) {
                exportsName = exportsName || propName;
                Object.defineProperty(this.exports = this.exports || {}, exportsName, propObj);
            } else {
                this.log.warn('Could not expose property "' + propName + '", did you define it using this.defineProperty() ?');
            }

            return undefined;
        },

        /**
         * Creates an InterludeError object from arguments and triggers an 'error' event.
         * If no listeners were added for the 'error' event, and 'throwErrors' setting is true, will throw the error instead.
         */
        notifyError: function() {
            var error = null;

            if (arguments.length === 1 && arguments[0] instanceof InterludeError) {
                error = arguments[0];
            } else {
                var Ctor = InterludeError.bind.apply(InterludeError, [null].concat(Array.prototype.slice.call(arguments)));
                error = new Ctor();
            }

            if (this.getListeners('error').length) {
                this.trigger('error', error);
            } else if (this.getSetting('throwErrors', this.throwErrors)) {
                throw error;
            } else {
                this.log.error(error && error.message);
            }
        },

        /** Adds an error listener to an object, handled by this.notifyError() - will trigger error onwards. */
        addErrorListener: function(eventEmitterInstance) {
            if (eventEmitterInstance && typeof eventEmitterInstance.on === 'function') {
                eventEmitterInstance.on('error', this.notifyError);
            }
        },

        /**
         * Cleanup once object is no longer needed (this is a good place to remove listeners, terminate workers etc).
         * Will be called automatically when window unloads, or can be called manually when object is no longer needed.
         * To be overridden by extending classes (extending implementations should call 'this._super();'.)
         */
        dispose: function() {
            if (window) {
                window.removeEventListener('unload', this.dispose);
            }

            this.removeAllListeners();
        }
    });

    utils.extend(BaseModuleClass.prototype, EventEmitter.prototype);

    return BaseModuleClass;
});

define('objects/CuePoint',[
    'utils/utils'
],

/**
 * Defines class for CuePoint data object.
 */
function(utils) {
    'use strict';

    var UUID_MARKER = '<uuid>';

    var CuePoint = utils.Class.extend({
        id: UUID_MARKER,
        time: 0,
        callback: null,
        data: null
    });

    // Static (belongs to class and not instance)
    CuePoint.UUID_MARKER = UUID_MARKER;

    return CuePoint;
});

define('factories/CuePointFactory',[
    'modules/Base',
    'objects/CuePoint',
    'utils/utils'
],

/**
 * Factory for creating cue point objects.
 */
function(BaseModuleClass, CuePoint, utils) {
    'use strict';

    var CuePointFactory = BaseModuleClass.extend({
        name: 'CuePointFactory',
        logLevel: 'warn',

        createCuePoint: function() {
            var retVal = null;

            // If no arguments given, log an error
            if (!arguments.length) {
                this.log.error('Could not create cue point - no arguments given');
                this.notifyError(12);
            }

            // If only one argument given, and it's an object
            else if (arguments.length === 1 && typeof arguments[0] === 'object') {
                // If the argument is already a cue point object, simply return it
                if (arguments[0] instanceof CuePoint) {
                    retVal = arguments[0];
                }

                // Otherwise, treat this as a plain object and extend the default cue point with given object's properties
                else {
                    retVal = utils.extend(true, new CuePoint(), arguments[0]);
                }
            }

            // Otherwise, assume every argument is a property of cue point
            // Since every property of a cue point object is of a different type, we can receive them in any order and identify by type
            else {
                retVal = new CuePoint();

                for (var i = 0; i < arguments.length; i++) {
                    var arg = arguments[i];

                    switch (typeof arg) {
                        case 'function':
                            retVal.callback = arg;
                            break;
                        case 'string':
                            retVal.id = arg;
                            break;
                        case 'number':
                            retVal.time = arg;
                            break;
                        case 'object':
                            retVal.data = arg;
                            break;
                        default:
                            this.log.warn('Unknown argument: ' + arg);
                            break;
                    }
                }
            }

            // If the id string is the UUID marker, replace it with a UUID
            if (retVal && retVal.id === CuePoint.UUID_MARKER) {
                retVal.id = utils.uuid();
            }

            return retVal;
        },

        ctor: function() {
            this.expose({
                createCuePoint: this.createCuePoint
            });
        }
    });

    return CuePointFactory;
});

define('objects/Node',[
    'utils/utils',
    'utils/EventEmitter',
    'factories/CuePointFactory'
],

/**
 * Defines class for Node data object.
 */
function(utils, EventEmitter, CuePointFactory) {
    'use strict';

    var UUID_MARKER = '<uuid>';
    var cuePointFactory = new CuePointFactory();

    var Node = utils.Class.extend({
        ///////////////////////////////////////////
        // Constructor
        ///////////////////////////////////////////

        init: function() {
            var ee = new EventEmitter();

            // Define EventEmitter methods on this instance
            this.on = function(evt, callback, data) {
                if (/^timeupdate:-?\d*(\.\d*)?$/.test(evt)) {
                    var cp = cuePointFactory.createCuePoint({
                        time: parseFloat(/^timeupdate:(-?\d*(\.\d*)?)$/.exec(evt)[1]),
                        callback: callback,
                        data: data
                    });

                    this.cuePoints = this.cuePoints || [];
                    this.cuePoints.push(cp);
                    this.trigger('cuepoint.added', this, cp);

                    return cp;
                } else {
                    return ee.on.apply(ee, arguments);
                }
            }.bind(this);

            this.off = function(evt, callback) {
                if (/^timeupdate:-?\d*(\.\d*)?$/.test(evt) && this.cuePoints && this.cuePoints.length) {
                    var time = parseFloat(/^timeupdate:(-?\d*(\.\d*)?)$/.exec(evt)[1]);
                    var cp = null;

                    for (var i = 0; i < this.cuePoints.length; i++) {
                        if (this.cuePoints[i].time === time && this.cuePoints[i].callback === callback) {
                            cp = this.cuePoints.splice(i, 1);
                            break;
                        }
                    }

                    if (cp) {
                        this.trigger('cuepoint.removed', this, cp);
                    }

                    return cp;
                } else {
                    return ee.off.apply(ee, arguments);
                }
            }.bind(this);

            this.one = function(evt, callback, data) {
                if (/^timeupdate:-?\d*(\.\d*)?$/.test(evt)) {
                    var callbackWrapper;

                    callbackWrapper = function() {
                        this.off(evt, callbackWrapper);

                        if (callback && typeof callback === 'function') {
                            callback.apply(null, arguments);
                        }
                    }.bind(this);

                    return this.on(evt, callbackWrapper, data);
                } else {
                    return ee.one.apply(ee, arguments);
                }
            }.bind(this);

            this.trigger = ee.trigger.bind(ee);

            // Define a getter for the first segment in 'segments' array
            Object.defineProperty(this, 'segment', {
                enumerable: true,
                get: function() {
                    return (this.segments && this.segments.length) ?
                                        this.segments[0] :
                                        null;
                }.bind(this)
            });

            // Define the dispatched events so listeners can use regex
            ee.defineEvents([
                'cuepoint.added',
                'cuepoint.removed'
            ]);
        },

        //////////////////////////////////////////
        // Members
        //////////////////////////////////////////

        id: UUID_MARKER,            // Unique id of this node instance
        segments: null,             // Array of segment objects
        prefetch: null,             // Array of strings (node ids)
        cuePoints: null,            // Array of cue point objects
        data: null,                 // Any user defined object to be attached to the node instance

        //////////////////////////////////////////
        // Methods
        //////////////////////////////////////////

        /**
         * Returns a simple object that is safe to transfer/clone between workers.
         */
        getWorkerSafeNode: function() {
            return {
                id: this.id,
                segment: this.segment && this.segment.id,
                segments: this.segments,
                prefetch: this.prefetch
            };
        }
    });

    // Static (belongs to class and not instance)
    Node.UUID_MARKER = UUID_MARKER;

    return Node;
});

define('factories/NodeFactory',[
    'modules/Base',
    'objects/Node',
    'utils/utils'
],

/**
 * Factory for creating node objects.
 */
function(BaseModuleClass, Node, utils) {
    'use strict';

    var NodeFactory = BaseModuleClass.extend({
        name: 'NodeFactory',
        logLevel: 'warn',

        createNode: function(id, segments, prefetch, cuePoints, data) {
            var retVal = null;
            var i;

            // If no arguments given, log an error
            if (!arguments.length) {
                this.log.error('Could not create node - no arguments given');
                this.notifyError(13);
            }

            // If only one argument given (and it's an object), use the object's properties to populate node properties (or return object if already Node)
            else if (arguments.length === 1 && typeof arguments[0] === 'object') {
                if (arguments[0] instanceof Node) {
                    retVal = arguments[0];
                } else {
                    retVal = utils.extend(true, new Node(), arguments[0]);
                }
            }

            // If only one argument given (and it's a URL), create the default object with the URL
            else if (arguments.length === 1 && utils.isUrl(arguments[0])) {
                retVal = utils.extend(true, new Node(), {segments: arguments[0]});
            }

            // Otherwise, assume every argument is a property of node
            else {
                // Log error if input params are not valid
                if (typeof id !== 'string' ||
                    (typeof segment !== 'string' && typeof segment !== 'object') ||
                    (prefetch && (!Array.isArray(prefetch) && typeof prefetch !== 'string' && typeof prefetch !== 'object'))) {
                    this.log.error('Cannot create node - input arguments are not valid');
                    this.notifyError(14);
                }

                // Create output object
                else {
                    retVal = utils.extend(true, new Node(), {
                        id: id,
                        segments: segments,
                        prefetch: prefetch,
                        cuePoints: cuePoints,
                        data: data ? data : null
                    });
                }
            }

            // If the id string is the UUID marker, replace it with a UUID
            if (retVal && retVal.id === Node.UUID_MARKER) {
                retVal.id = utils.uuid();
            }

            // Populate segments with actual segment objects
            if (retVal && retVal.segments) {
                 // Force array
                if (!Array.isArray(retVal.segments)) {
                    retVal.segments = [retVal.segments];
                }

                for (i = 0; i < retVal.segments.length; i++) {
                    retVal.segments[i] = this.segmentFactory.createSegment(retVal.segments[i]);
                }
            }

            // Populate prefetch with actual prefetch objects
            if (retVal && retVal.prefetch) {
                // Force array
                if (!Array.isArray(retVal.prefetch)) {
                    retVal.prefetch = [retVal.prefetch];
                }

                for (i = 0; i < retVal.prefetch.length; i++) {
                    retVal.prefetch[i] = String(retVal.prefetch[i]);
                }
            } else if (retVal && !retVal.prefetch) {
                retVal.prefetch = [];
            }

            // Populate cue points with actual cue point objects
            if (retVal && retVal.cuePoints) {
                // Force array
                if (!Array.isArray(retVal.cuePoints)) {
                    retVal.cuePoints = [retVal.cuePoints];
                }

                for (i = 0; i < retVal.cuePoints.length; i++) {
                    retVal.cuePoints[i] = this.cuePointFactory.createCuePoint(retVal.cuePoints[i]);
                }
            } else if (retVal && !retVal.cuePoints) {
                retVal.cuePoints = [];
            }

            return retVal;
        },

        ctor: function(segmentFact, cuePointFact) {
            this.segmentFactory = segmentFact;
            this.cuePointFactory = cuePointFact;

            this.expose({
                createNode: this.createNode
            });
        }
    });

    return NodeFactory;
});

define('objects/Segment',[
    'utils/utils'
],

/**
 * Defines class for Segment data object.
 */
function(utils) {
    'use strict';

    var UUID_MARKER = '<uuid>';

    var Segment = utils.Class.extend({

        //////////////////////////////////////////
        // Members
        //////////////////////////////////////////

        id: UUID_MARKER,
        source: null,
        duration: NaN,
        data: null,

        /**
         * Returns a simple object that is safe to transfer/clone between workers.
         */
        getWorkerSafeSegment: function() {
            return {
                id: this.id,
                source: this.source,
                duration: this.duration
            };
        }
    });

    // Static (belongs to class and not instance)
    Segment.UUID_MARKER = UUID_MARKER;

    return Segment;
});

define('factories/SegmentFactory',[
    'modules/Base',
    'objects/Segment',
    'utils/utils'
],

/**
 * Factory for creating segment objects.
 */
function(BaseModuleClass, Segment, utils) {
    'use strict';

    var SegmentFactory = BaseModuleClass.extend({
        name: 'SegmentFactory',
        logLevel: 'warn',

        createSegment: function(id, source, duration, data) {
            var retVal = null;

            // If no arguments given, log an error
            if (!arguments.length) {
                this.log.error('Could not create segment - no arguments given');
                this.notifyError(16);
            }

            // If only one argument given (and it's an object), use the object's properties to populate segment properties (or return object if already Segment)
            else if (arguments.length === 1 && typeof arguments[0] === 'object') {
                if (arguments[0] instanceof Segment) {
                    retVal = arguments[0];
                } else {
                    retVal = utils.extend(true, new Segment(), arguments[0]);
                }
            }

            // If only one argument given (and it's a URL), create the default object with the URL
            else if (arguments.length === 1 && utils.isUrl(arguments[0])) {
                retVal = utils.extend(true, new Segment(), {source: arguments[0]});
            }

            // If only one argument given (and it's a string), assume it's the segment ID.
            else if (arguments.length === 1 && typeof arguments[0] === 'string') {
                retVal = utils.extend(true, new Segment(), {id: arguments[0]});
            }

            // Otherwise, assume every argument is a property of segment
            else {
                // Log error if input params are not valid
                if (typeof id !== 'string' || (typeof source !== 'string' && typeof source !== 'object') || typeof duration !== 'number') {
                    this.log.error('Cannot create segment - input arguments are not valid');
                    this.notifyError(17);
                }

                // Create output object
                else {
                    retVal = utils.extend(true, new Segment(), {
                        id: id,
                        source: source,
                        duration: duration,
                        data: data ? data : null
                    });
                }
            }

            // If the id string is the UUID marker, replace it with a UUID
            if (retVal && retVal.id === Segment.UUID_MARKER) {
                retVal.id = utils.uuid();
            }

            // If source is a single string, convert it to a source object
            if (retVal && typeof retVal.source === 'string') {
                if (retVal.source.endsWith('.mp4')) {
                    retVal.source = {mp4: {url: retVal.source,
                                           duration: duration}};
                } else if (retVal.source.endsWith('.webm')) {
                    retVal.source = {webm: {url: retVal.source,
                                            duration: duration}};
                } else if (retVal.source.endsWith('.flv')) {
                    retVal.source = {flv: {url: retVal.source,
                                            duration: duration}};
                } else if (retVal.source.endsWith('.imc')) {
                    // TODO
                    this.log.error('Automatic IMC segment creation is not yet implemented.');
                    this.notifyError(18);
                } else if (retVal.source.endsWith('.ts')) {
                    this.log.error('Automatic ts segment creation is not yet implemented.');
                    this.notifyError(52);
                } else {
                    this.log.error('Source type ' + retVal.source + ' is not supported.');
                    this.notifyError(53, retVal.source);
                }
            }

            return retVal;
        },

        ctor: function() {
            this.expose({
                createSegment: this.createSegment
            });
        }
    });

    return SegmentFactory;
});

define('modules/Repository',[
    'utils/utils',
    'modules/Base',
    'factories/CuePointFactory',
    'factories/NodeFactory',
    'factories/SegmentFactory'
],

/**
 * Repository module, responsible for storing and managing node/segment/cuepoint objects.
 */
function(utils, BaseModuleClass, CuePointFactoryClass, NodeFactoryClass, SegmentFactoryClass) {
    'use strict';

    var Repository = BaseModuleClass.extend({
        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'Repository',
        logLevel: 'warn',
        defaults: {
            exposeFactoryMethods: false
        },

        //////////////////////////////////////////////////////////////////////////
        // ----------------------------- MEMBERS ---------------------------------
        //////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////
        // Factories
        //////////////////////////////////////

        cuePointFactory: null,
        segmentFactory: null,
        nodeFactory: null,

        //////////////////////////////////////
        // Repos
        //////////////////////////////////////

        nodeMap: {},
        segmentMap: {},

        //////////////////////////////////////////////////////////////////////////
        // ---------------------------- METHODS ----------------------------------
        //////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////
        // Segment Related
        //////////////////////////////////////

        addSegment: function(segment) {
            // Handle array
            if (Array.isArray(segment) || (arguments.length > 1 && (segment = arguments))) {
                var retVal = [];

                for (var i = 0; i < segment.length; i++) {
                    retVal.push(this.addSegment(segment[i]));
                }

                return retVal;
            }

            // Handle single segment
            segment = this.segmentFactory.createSegment(segment);

            if (!segment) {
                this.log.error('Could not create segment');
                this.notifyError(29);
                return null;
            } else if (this.segmentMap.hasOwnProperty(segment.id)) {
                this.log.warn('Segment id already exists in repository - overriding previous instance.');
            }

            this.segmentMap[segment.id] = segment;
            this._notifyChange('segment.added', segment);

            return this.segmentMap[segment.id];
        },

        updateSegment: function(segment) {
            var retVal = null;

            // Handle array
            if (Array.isArray(segment) || (arguments.length > 1 && (segment = arguments))) {
                retVal = [];

                for (var i = 0; i < segment.length; i++) {
                    retVal.push(this.updateSegment(segment[i]));
                }

                return retVal;
            }

            // Handle single segment
            if (!segment || typeof segment !== 'object' || !segment.id) {
                this.log.error('Invalid argument given to updateSegment(): ' + JSON.stringify(segment, null, '\t'));
                this.notifyError(30, JSON.stringify(segment, null, '\t'));
                return null;
            } else if (!this.segmentMap.hasOwnProperty(segment.id)) {
                this.log.warn('Could not find segment with id: "' + segment.id + '", adding segment instead.');
                retVal = this.addSegment(segment);
            } else {
                retVal = utils.extend(true, this.segmentMap[segment.id], segment);
                this._notifyChange('segment.updated', retVal);
            }

            return retVal;
        },

        hasSegment: function(segmentId) {
            if (typeof segmentId !== 'string' && segmentId && segmentId.id) {
                segmentId = segmentId.id;
            }
            return this.segmentMap.hasOwnProperty(segmentId);
        },

        getSegment: function(segmentId) {
            var retVal = null;

            // Handle special case 'all'
            if (segmentId === 'all') {
                retVal = [];
                for (var prop in this.segmentMap) {
                    if (this.segmentMap.hasOwnProperty(prop)) {
                        retVal.push(this.segmentMap[prop]);
                    }
                }
                return retVal;
            }

            // Handle array
            if (Array.isArray(segmentId) || (arguments.length > 1 && (segmentId = arguments))) {
                retVal = [];

                for (var i = 0; i < segmentId.length; i++) {
                    retVal.push(this.getSegment(segmentId[i]));
                }

                return retVal;
            }

            // Handle single segment
            if (!segmentId) {
                this.log.error('No segmentId given');
                this.notifyError(31);
            } else if (typeof segmentId !== 'string' && segmentId.id) {
                segmentId = segmentId.id;
            }

            if (segmentId && !this.segmentMap.hasOwnProperty(segmentId)) {
                this.log.error('Segment could not be found: ' + segmentId);
                this.notifyError(32, segmentId);
            } else {
                retVal = this.segmentMap[segmentId];
            }

            return retVal;
        },

        removeSegment: function(segment) {
            // Handle special case 'all'
            if (segment === 'all') {
                this.segmentMap = {};
                return null;
            }

            // Handle array
            if (Array.isArray(segment) || (arguments.length > 1 && (segment = arguments))) {
                for (var i = 0; i < segment.length; i++) {
                    this.removeSegment(segment[i]);
                }
                return null;
            }

            // Handle single segment
            segment = this.getSegment(segment);

            if (segment) {
                delete this.segmentMap[segment.id];
                this._notifyChange('segment.removed', segment);
            }

            return null;
        },

        //////////////////////////////////////
        // Node Related
        //////////////////////////////////////

        addNode: function(node) {
            var retVal = null;
            var i;

            // Handle array
            if (Array.isArray(node) || (arguments.length > 1 && (node = arguments))) {
                retVal = [];

                for (i = 0; i < node.length; i++) {
                    retVal.push(this.addNode(node[i]));
                }

                return retVal;
            }

            // Handle single node
            node = this.nodeFactory.createNode(node);

            if (!node) {
                this.log.error('Could not create node');
                this.notifyError(33);
                return null;
            } else if (this.nodeMap.hasOwnProperty(node.id)) {
                this.log.warn('Node id already exists in repository - overriding previous instance.');
            }

            // Handle node's segments
            if (node.segments) {
                for (i = 0; i < node.segments.length; i++) {
                    if (!this.segmentMap[node.segments[i].id]) {
                        this.addSegment(node.segments[i]);
                    }
                    node.segments[i] = this.segmentMap[node.segments[i].id];
                }
            }

            // Add/override node in map
            retVal = this.nodeMap[node.id] = node;
            this._notifyChange('node.added', node);

            this._addNodeListeners(node);

            return retVal;
        },

        updateNode: function(node) {
            var retVal = null;
            var i;

            // Handle array
            if (Array.isArray(node) || (arguments.length > 1 && (node = arguments))) {
                retVal = [];

                for (i = 0; i < node.length; i++) {
                    retVal.push(this.updateNode(node[i]));
                }

                return retVal;
            }

            // Handle single node
            if (!node || typeof node !== 'object' || !node.id) {
                this.log.error('Invalid argument given to updateNode(): ' + JSON.stringify(node, null, '\t'));
                this.notifyError(34, JSON.stringify(node, null, '\t'));
                return null;
            } else if (!this.nodeMap.hasOwnProperty(node.id)) {
                this.log.warn('Could not find node with id: "' + node.id + '", adding node instead.');
                retVal = this.addNode(node);
            } else {
                retVal = utils.extend(true, this.nodeMap[node.id], node);
                this._notifyChange('node.updated', retVal);
            }

            // Handle node's segments
            if (retVal && retVal.segments) {
                for (i = 0; i < retVal.segments.length; i++) {
                    if (!this.segmentMap[retVal.segments[i].id]) {
                        this.addSegment(retVal.segments[i]);
                    }
                    retVal.segments[i] = this.segmentMap[retVal.segments[i].id];
                }
            }

            return retVal;
        },

        hasNode: function(nodeId) {
            if (typeof nodeId !== 'string' && nodeId && nodeId.id) {
                nodeId = nodeId.id;
            }
            return this.nodeMap.hasOwnProperty(nodeId);
        },

        getNode: function(nodeId) {
            var retVal = null;

            // Handle special case 'all'
            if (nodeId === 'all') {
                retVal = [];
                for (var prop in this.nodeMap) {
                    if (this.nodeMap.hasOwnProperty(prop)) {
                        retVal.push(this.nodeMap[prop]);
                    }
                }
                return retVal;
            }

            // Handle array
            if (Array.isArray(nodeId) || (arguments.length > 1 && (nodeId = arguments))) {
                retVal = [];

                for (var i = 0; i < nodeId.length; i++) {
                    retVal.push(this.getNode(nodeId[i]));
                }

                return retVal;
            }

            // Handle single node
            if (!nodeId) {
                this.log.error('No nodeId given');
                this.notifyError(35);
            } else if (typeof nodeId !== 'string' && nodeId.id) {
                nodeId = nodeId.id;
            }

            if (nodeId && !this.nodeMap.hasOwnProperty(nodeId)) {
                this.log.error('Node could not be found: ' + nodeId);
                this.notifyError(36, nodeId);
            } else {
                retVal = this.nodeMap[nodeId];
            }

            return retVal;
        },

        removeNode: function(node) {
            var nodeKeys;
            var i;

            // Handle special case 'all'
            if (node === 'all') {
                nodeKeys = Object.keys(this.nodeMap);

                for (i = 0; i < nodeKeys.length; i++) {
                    this.removeNode(nodeKeys[i]);
                }

                return null;
            }

            // Handle array
            if (Array.isArray(node) || (arguments.length > 1 && (node = arguments))) {
                for (i = 0; i < node.length; i++) {
                    this.removeNode(node[i]);
                }
                return null;
            }

            // Handle single node
            node = this.getNode(node);

            if (node) {
                delete this.nodeMap[node.id];
                this._removeNodeListeners(node);
                this._notifyChange('node.removed', node);
            }

            return null;
        },

        //////////////////////////////////////
        // Misc Helpers
        //////////////////////////////////////

        reset: function() {
            this.removeNode('all');
            this.removeSegment('all');
        },

        addPrefetch: function(nodeId, prefetchNodeId) {
            if (typeof prefetchNodeId !== 'string' && typeof prefetchNodeId.id === 'string') {
                prefetchNodeId = prefetchNodeId.id;
            }

            var node = this.getNode(nodeId);
            if (!node) {
                this.log.error('Could not get node');
                this.notifyError(41);
                return null;
            }

            if (this._hasPrefetch(node, prefetchNodeId)) {
                this.log.warn('Node "' + node.id + '" already contains prefetch (child) with id: ' + prefetchNodeId);
            } else {
                node.prefetch.push(prefetchNodeId);
                this._notifyChange('prefetch.added', node, prefetchNodeId);
            }

            return node;
        },

        removePrefetch: function(nodeId, prefetchNodeId) {
            var node = this.getNode(nodeId);

            if (typeof prefetchNodeId !== 'string' && typeof prefetchNodeId.id === 'string') {
                prefetchNodeId = prefetchNodeId.id;
            }

            if (!node || typeof prefetchNodeId !== 'string') {
                this.log.error('Could not get node or prefetchNodeId is not a string');
                this.notifyError(42);
                return null;
            }

            for (var i = 0; i < node.prefetch.length; i++) {
                if (node.prefetch[i] === prefetchNodeId) {
                    break;
                }
            }

            if (i < node.prefetch.length) {
                this.log.debug('Successfully removed prefetch rule (child): ' + JSON.stringify(node.prefetch[i], null, '\t'));
                var prefetchArr = node.prefetch.splice(i, 1);
                this._notifyChange('prefetch.removed', node, prefetchArr[0]);
            } else {
                this.log.warn('Could not find prefetch: ' + prefetchNodeId);
            }

            return node;
        },

        //////////////////////////////////////
        // Internal Helpers
        //////////////////////////////////////

        _notifyChange: function(change) {
            this.trigger('repository.changed');

            if (change) {
                this.trigger.apply(this, ['repository.' + change].concat(Array.prototype.slice.call(arguments, 1)));
            }
        },

        _hasCuePoint: function(node, cp) {
            var retVal = false;

            for (var i = 0; i < node.cuePoints.length; i++) {
                if (cp.id === node.cuePoints[i].id) {
                    retVal = true;
                    break;
                }
            }

            return retVal;
        },

        _hasPrefetch: function(node, prefetchNodeId) {
            return node && node.prefetch && node.prefetch.length && node.prefetch.indexOf(prefetchNodeId) !== -1;
        },

        _onCuePointAdded: function(node, cp) {
            this._notifyChange('cuepoint.added', node, cp);
        },

        _onCuePointRemoved: function(node, cp) {
            this._notifyChange('cuepoint.removed', node, cp);
        },

        _addNodeListeners: function(node) {
            node.on('cuepoint.added', this._onCuePointAdded);
            node.on('cuepoint.removed', this._onCuePointRemoved);
        },

        _removeNodeListeners: function(node) {
            node.off('cuepoint.added', this._onCuePointAdded);
            node.off('cuepoint.removed', this._onCuePointRemoved);
        },

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- CONSTRUCTOR --------------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function() {
            // Create factories
            this.cuePointFactory = new CuePointFactoryClass(this.options);
            this.segmentFactory = new SegmentFactoryClass(this.options);
            this.nodeFactory = new NodeFactoryClass(this.options, this.segmentFactory, this.cuePointFactory);

            // Add error listener for submodules
            this.addErrorListener(this.cuePointFactory);
            this.addErrorListener(this.segmentFactory);
            this.addErrorListener(this.nodeFactory);

            // Expose public methods
            this.expose({
                addSegment: this.addSegment,
                updateSegment: this.updateSegment,
                getSegment: this.getSegment,
                removeSegment: this.removeSegment,
                hasSegment: this.hasSegment,

                addNode: this.addNode,
                updateNode: this.updateNode,
                getNode: this.getNode,
                removeNode: this.removeNode,
                hasNode: this.hasNode,

                addPrefetch: this.addPrefetch,
                removePrefetch: this.removePrefetch,
                reset: this.reset
            });

            // Also export factory methods if needed (configurable)
            if (this.getSetting('exposeFactoryMethods', false)) {
                this.expose(this.cuePointFactory.exports);
                this.expose(this.segmentFactory.exports);
                this.expose(this.nodeFactory.exports);
            }

            // Define the events that the Repository object fires
            // This allows listeners to use regex
            this.defineEvents([
                'repository.segment.added',         // Callback args: (segment)
                'repository.segment.removed',       // Callback args: (segment)
                'repository.segment.updated',       // Callback args: (segment)
                'repository.node.added',            // Callback args: (node)
                'repository.node.removed',          // Callback args: (node)
                'repository.node.updated',          // Callback args: (node)
                'repository.cuepoint.added',        // Callback args: (node, cuepoint)
                'repository.cuepoint.removed',      // Callback args: (node, cuepoint)
                'repository.prefetch.added',        // Callback args: (node, prefetch)
                'repository.prefetch.removed'       // Callback args: (node, prefetch)
            ]);
        }
    });

    return Repository;
});

define('modules/Controller',[
    'modules/Base'
],

/**
 * The main controller class, for controlling/orchestrating the entire player.
 */
function(BaseModuleClass) {
    'use strict';

    var Controller = BaseModuleClass.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'Controller',
        logLevel: 'warn',
        excludeFuncFromAutoDebug: ['triggerApiEvent', 'nodeDuration', 'getCurrentTime', 'getDuration'],
        defaults: {
            /** Should video be paused after 'ended' event was dispatched? */
            pauseAfterEnded: true,

            /** Should engine end the stream after last node in playlist reached critical time and no next node was pushed? */
            endStreamAfterLastCritical: false,

            /**
             * The critical time in seconds before end of node.
             * Could either be an object with a value for each videoTech, a number, or a function that accepts no arguments and returns a number.
             */
            criticalTime: {
                flash: 1.5,
                mse: 1.5,
                hls: 2.5
            },

            floatingPointNumDigits: 10
        },

        //////////////////////////////////////////////////////////////////////////
        // ----------------------------- MEMBERS ---------------------------------
        //////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////
        // Module Instances
        //////////////////////////////////////

        playlist: null,
        repository: null,
        engine: null,
        video: null,
        api: null,
        cuepoints: null,
        fullScreen: null,

        //////////////////////////////////////
        // Misc
        //////////////////////////////////////

        _duration: 0,
        _currentNodeIndex: 0,
        _playbackOffset: 0,
        _currentChannelIndex: 0,
        _nextChannelIndex: NaN,

        //////////////////////////////////////////////////////////////////////////
        // ----------------------------- METHODS ---------------------------------
        //////////////////////////////////////////////////////////////////////////

        triggerApiEvent: function(eventname, args) {
            if (eventname !== 'timeupdate') {
                this.log.debug('Dispatching API event: ' + eventname);
            }

            args = args || [];

            // If args are not an array, turn them into an array
            if (!Array.isArray(args)) {
                args = Array.prototype.slice.call(args, 0);
            }

            // Trigger the event from API
            this.api.trigger.apply(this.api, [eventname].concat(args));
        },

        addVideoListeners: function() {
            this.video.on(this.api.events.timeupdate,       function() { this.triggerApiEvent(this.api.events.timeupdate, [this.currentTime]); }.bind(this));
            this.video.on(this.api.events.volumechange,     function() { this.triggerApiEvent(this.api.events.volumechange, arguments); }.bind(this));
            this.video.on(this.api.events.loadeddata,       function() { this.triggerApiEvent(this.api.events.loadeddata, arguments); }.bind(this));
            this.video.on(this.api.events.playing,          function() { this.triggerApiEvent(this.api.events.playing, arguments); }.bind(this));
            this.video.on(this.api.events.waiting,          function() { this.triggerApiEvent(this.api.events.waiting, arguments); }.bind(this));
            this.video.on(this.api.events.play,             function() { this.triggerApiEvent(this.api.events.play, arguments); }.bind(this));
            this.video.on(this.api.events.pause,            function() { this.triggerApiEvent(this.api.events.pause, arguments); }.bind(this));
            this.video.on(this.api.events.ended,            function() { this.triggerApiEvent(this.api.events.ended, arguments); }.bind(this));
        },

        addFullScreenListeners: function() {
            this.fullScreen.on(this.api.events.fullscreenchange, function() { this.triggerApiEvent(this.api.events.fullscreenchange, arguments); }.bind(this));
        },

        appendNodeForEnginePlayback: function(nodeId) {
            this.engine.appendNode(nodeId);
        },

        addPlaylistListeners: function() {
            // All playlist events should trigger the playlist changed listener
            this.playlist.on(/playlist\..*/, this.onPlaylistChanged);

            // Handle node pushed to playlist
            this.playlist.on('playlist.push', function(node, index) {
                this.appendNodeForEnginePlayback(node.id);
                this.calcDurations(true);
                this.triggerApiEvent(this.api.events.playlistpush, [node, index]);
            }.bind(this));

            // Handle playlist reset
            this.playlist.on('playlist.reset', function() {
                this.engine.reset().done(function() {
                    this.triggerApiEvent(this.api.events.timeupdate, [this.currentTime]);
                    this.triggerApiEvent(this.api.events.playlistreset);
                }.bind(this));
                this._currentNodeIndex = 0;
                this._playbackOffset = 0;
                this._currentChannelIndex = 0;
                this._nextChannelIndex = NaN;
                this.calcDurations(true);
            }.bind(this));
        },

        addRepositoryListeners: function() {
            // If any segment is updated, while playlist has some nodes, it could be that duration was changed.
            // Update the calculated duration
            this.repository.on('repository.segment.updated', function() {
                if (this.playlist.nodes.length) {
                    this.calcDurations();
                }
            }.bind(this));
        },

        addCuePointsListeners: function() {
            this.cuepoints.on('nodestart', function(node, playlistIndex) {
                this._currentNodeIndex = playlistIndex;
                this._currentChannelIndex = 0;
                this._nextChannelIndex = NaN;
                this.triggerApiEvent(this.api.events.nodestart, [node, playlistIndex]);
            }.bind(this));

            this.cuepoints.on('nodeend', function(node, playlistIndex) {
                this.triggerApiEvent(this.api.events.nodeend, [node, playlistIndex]);

                // If node ended and no next node has been pushed into playlist, trigger the 'ended' event as well.
                if (playlistIndex === this.playlist.nodes.length - 1) {
                    this.triggerApiEvent(this.api.events.ended);

                    // Pause video after ended
                    if (this.getSetting('pauseAfterEnded', false)) {
                        this.video.pause();
                    }
                }

            }.bind(this));

            this.cuepoints.on('nodecritical', function(node, playlistIndex) {
                // Only fire the 'nodecritical' event if next node has not yet been appended.
                if (playlistIndex === this.playlist.nodes.length - 1) {
                    this.triggerApiEvent(this.api.events.nodecritical, [node, playlistIndex]);

                    // If no node was pushed after 'nodecritical' was fired, perform the end stream operation
                    if (playlistIndex === this.playlist.nodes.length - 1 && this.getSetting('endStreamAfterLastCritical', false)) {
                        this.engine.endStream();
                    }
                }
            }.bind(this));

            this.cuepoints.on('waiting', function() {
                this.triggerApiEvent(this.api.events.waiting);
            }.bind(this));

            this.cuepoints.on('playing', function() {
                this.triggerApiEvent(this.api.events.playing);
            }.bind(this));
        },

        onPlaylistChanged: function() {
            this.calcDurations();
        },

        nodeDuration: function(node) {
            return (node && node.segment) ? node.segment.duration : 0;
        },

        calcDurationOfNodeArray: function(nodes) {
            var retVal = 0;
            var segDuration = 0;

            for (var i = 0; i < nodes.length; i++) {
                segDuration = this.nodeDuration(nodes[i]);
                retVal += isNaN(segDuration) ? 0 : segDuration;
            }

            return retVal;
        },

        calcDurations: function(suppressEvent) {
            this._duration = this.playlist.calcPlaylistDuration();

            if (!suppressEvent) {
                this.triggerApiEvent(this.api.events.durationchange, [this.duration]);
            }
        },

        switchChannel: function(index) {
            // Validate input
            if (typeof index === 'string' && index !== 'up' && index !== 'down') {
                this.log.error('Unexpected input: ' + index);
                this.notifyError(44, index);
                return;
            } else if (typeof index === 'number' && (index < 0 || index >= this.currentNode.segments.length)) {
                this.log.error('Unexpected input: ' + index);
                this.notifyError(45, index);
                return;
            }

            var expectedChannelIndex = this._nextChannelIndex ? this._nextChannelIndex : this._currentChannelIndex;
            // Handle 'up', 'down'
            if (typeof index === 'string') {
                index = (index === 'up') ?
                            (expectedChannelIndex + 1) % this.currentNode.segments.length :
                            ((expectedChannelIndex + this.currentNode.segments.length) - 1) % this.currentNode.segments.length;
            }

            // Handle numeric index
            else {
                index = Math.floor(index);
            }

            // Ignore switch channel request if we're already on this channel
            if (index === expectedChannelIndex) {
                this.log.warn('Ignoring switch channel request, we\'re already on channel ' + index);
                return;
            }

            // Notify we're switching channels
            this.triggerApiEvent(this.api.events.channelswitching, [expectedChannelIndex, index]);

            // Perform switch channel operation in engine
            this.engine.switchChannel(index).done(function(switchObject) {
                // Success!
                var timeInNode = (switchObject && switchObject.timeInNode) ? switchObject.timeInNode : 0;
                var absoluteTime = this.currentNodeTimeOffset + timeInNode;

                this._nextChannelIndex = index;

                this.cuepoints.addTempCuePoint(absoluteTime, function() {
                    var prevChannel = this._currentChannelIndex;
                    this._currentChannelIndex = index;
                    this._nextChannelIndex = NaN;
                    this.triggerApiEvent(this.api.events.channelswitched, [prevChannel, index]);
                }.bind(this));
            }.bind(this),
            function(e) {
                // Failed to switch channels
                this.log.error('Engine failed to switch channel');
                this.notifyError(46, e);
            }.bind(this));
        },

        setCriticalTime: function() {
            var criticalTime = this.getSetting('criticalTime', 3.0);

            var applyCriticalTime = function(seconds) {
                if (typeof seconds !== 'number') {
                    this.log.error('Unable to set critical time, value ' + seconds + ' is not a number.');
                    this.notifyError(19, seconds);
                }
                seconds = Math.abs(seconds);
                this.cuepoints.setSetting('internalPerNode.nodecritical.time', (0 - seconds));
            }.bind(this);

            switch (typeof criticalTime) {
                case 'number':
                    applyCriticalTime(criticalTime);
                    break;
                case 'object':
                    if (!criticalTime || !criticalTime[this.engine.videoTech()]) {
                        this.log.error('Cannot set critical time - object missing a "' + this.engine.videoTech() + '" property.');
                        this.notifyError(20, this.engine.videoTech());
                    } else {
                        applyCriticalTime(criticalTime[this.engine.videoTech()]);
                    }
                    break;
                case 'function':
                    applyCriticalTime(criticalTime());
                    break;
                default:
                    break;
            }
        },

        getCriticalTime: function() {
            return 0 - this.cuepoints.getSetting('internalPerNode.nodecritical.time');
        },

        getDuration: function() {
            return this._duration;
        },

        getCurrentNode: function() {
            return (this._currentNodeIndex < this.playlist.nodes.length) ?
                                this.playlist.nodes[this._currentNodeIndex] :
                                null;
        },

        getCurrentNodeIndex: function() {
            return this._currentNodeIndex;
        },

        setCurrentNodeIndex: function(index) {
            if (index < 0 || isNaN(index) || typeof index !== 'number') {
                this.log.error('Invalid value supplied to currentNodeIndex: ' + index);
                this.notifyError(21, index);
                return;
            }

            if (index >= this.playlist.nodes.length) {
                this.log.error('Requested currentNodeIndex is beyond playlist');
                this.notifyError(22);
            } else {
                this.currentTime = this.calcDurationOfNodeArray(this.playlist.nodes.slice(0, index));
            }
        },

        getCurrentTime: function() {
            return this._playbackOffset + this.video.currentTime();
        },

        setCurrentTime: function(time) {
            if (time < 0 || isNaN(time) || typeof time !== 'number') {
                this.log.error('Invalid value supplied to currenTime: ' + time);
                this.notifyError(23, time);
                return;
            }

            // If trying to seek past (duration - criticalTime), and not to start of last node, trigger error.
            var fpDelta = this.getSetting('floatingPointNumDigits');

            if (parseFloat(time.toFixed(fpDelta)) >= parseFloat((this._duration - this.getCriticalTime()).toFixed(fpDelta)) && this.playlist.length && parseFloat(time.toFixed(fpDelta)) > parseFloat((this._duration - this.nodeDuration(this.playlist.nodes[this.playlist.length - 1])).toFixed(fpDelta))) {
                this.log.error('Requested currentTime is beyond playlist');
                this.notifyError(24);
            } else {
                var currNode = this.playlist.nodes[this._currentNodeIndex];
                var timeInCurrNode = this.getCurrentTime() - this.getCurrentNodeTimeOffset();
                var node;
                var nodeIndex;
                var nodeTimeOffset = 0;
                var timeInNode;

                // Find the node and index at 'time'
                for (nodeIndex = 0; nodeIndex < this.playlist.nodes.length; nodeIndex++) {
                    if (time >= nodeTimeOffset && time < nodeTimeOffset + this.nodeDuration(this.playlist.nodes[nodeIndex])) {
                        node = this.playlist.nodes[nodeIndex];
                        break;
                    }

                    nodeTimeOffset += this.nodeDuration(this.playlist.nodes[nodeIndex]);
                }

                timeInNode = time - nodeTimeOffset;

                // If requested time is less than CRITICAL_TIME before end of node, seek to next node.
                // except for the case where node is shorter than CRITICAL_TIME.
                if (this.nodeDuration(node) - timeInNode <= this.getCriticalTime() && this.nodeDuration(node) > this.getCriticalTime()) {
                    nodeTimeOffset += this.nodeDuration(node);
                    timeInNode = 0;
                    nodeIndex++;
                    node = this.playlist.nodes[nodeIndex];
                }

                // If this is a seek backwards and there are future nodes already in playlist, maintain an array of future nodes
                // so we can push them for engine playback if seek is successful.
                var futureNodes = [];
                if (nodeIndex < this.playlist.nodes.length - 1) {
                    for (var i = nodeIndex + 1; i < this.playlist.nodes.length; i++) {
                        futureNodes.push(this.playlist.nodes[i]);
                    }
                }

                this.triggerApiEvent(this.api.events.seeking);
                this.engine.seek(node.id, timeInNode).done(function(seekObject) {
                    var onSeekDelay = function() {
                        this.log.info('Successfully seeked video');

                        // If this is a seek backwards and there are future nodes already in playlist, append nodes for playback
                        for (var i = 0; i < futureNodes.length; i++) {
                            this.appendNodeForEnginePlayback(futureNodes[i].id);
                        }

                        // Set the playbackOffset time
                        this._playbackOffset = nodeTimeOffset;

                        // For any engine where seek does not interrupt current playback (currently, only HLS engine),
                        // subtract currentTime from playback offset
                        if (seekObject && seekObject.seekedTimeDelay) {
                            this._playbackOffset -= this.video.currentTime();
                        }

                        // Set the current node index
                        this._currentNodeIndex = nodeIndex;
                        this._currentChannelIndex = 0;
                        this._nextChannelIndex = NaN;

                        // Let the world know that the seek operation was a great success!
                        this.triggerApiEvent(this.api.events.seeked, [this._playbackOffset + timeInNode, nodeIndex, nodeTimeOffset]);
                    }.bind(this);

                    // if the engine return seekedTimeDelay (delay for the time it seeked) we will handle the seek at the delay time, otherwise we'll handle it immediately
                    if (seekObject && seekObject.seekedTimeDelay) {
                        var seekCuePoint = timeInCurrNode + seekObject.seekedTimeDelay;
                        currNode.one('timeupdate:' + seekCuePoint, function() {
                            // Bug fix - perform onSeekDelay in timeout in order to break out of stack.
                            // Otherwise, we will be performing an operation that will reset the cuepoints from within a cuepoint callback.
                            // This will tamper with the cue points array in mid iteration, which is a big NO NO
                            setTimeout(onSeekDelay, 0);
                        }.bind(this));
                    }
                    else {
                        onSeekDelay();
                    }
                }.bind(this), function() {
                    // Oh no, something went terribly wrong...
                    this.log.error('Could not perform seek operation.');
                    this.notifyError(25);
                }.bind(this));
            }
        },

        getCurrentNodeTimeOffset: function() {
            return this.calcDurationOfNodeArray(this.playlist.nodes.slice(0, this.currentNodeIndex));
        },

        getCurrentChannel: function() {
            return this._nextChannelIndex ? this._nextChannelIndex : this._currentChannelIndex;
        },

        //////////////////////////////////////////////////////////////////////////
        // --------------------- CONSTRUCTOR / DESTRUCTOR ------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function(playlist, repository, engine, api, cuepoints, fullScreen) {
            // Keep references
            this.playlist = playlist;
            this.repository = repository;
            this.engine = engine;
            this.video = engine.video;
            this.api = api;
            this.cuepoints = cuepoints;
            this.fullScreen = fullScreen;

            // Add listeners
            this.addPlaylistListeners();
            this.addRepositoryListeners();
            this.addCuePointsListeners();
            this.addVideoListeners();
            this.addFullScreenListeners();

            // Handle critical time setting
            this.setCriticalTime();

            // Define properties
            this.defineProperty('duration', function() {
                return this.getDuration();
            });
            this.defineProperty('currentNode', function() {
                return this.getCurrentNode();
            });
            this.defineProperty('currentNodeIndex', function() {
                return this.getCurrentNodeIndex();
            }, function(value) {
                this.setCurrentNodeIndex(value);
            });
            this.defineProperty('currentTime', function() {
                return this.getCurrentTime();
            }, function(value) {
                this.setCurrentTime(value);
            });
            this.defineProperty('currentNodeTimeOffset', function() {
                return this.getCurrentNodeTimeOffset();
            });
            this.defineProperty('currentChannel', function() {
                return this.getCurrentChannel();
            });

            // Expose Public API
            this.expose({
                duration: this.exposeProperty('duration'),

                currentTime: this.exposeProperty('currentTime'),

                currentNode: this.exposeProperty('currentNode'),

                currentNodeIndex: this.exposeProperty('currentNodeIndex'),

                currentChannel: this.exposeProperty('currentChannel'),

                switchChannel: this.switchChannel,

                videoTech: this.engine.videoTech
            });
        }
    });

    return Controller;
});

/*jshint -W083 */

define(
'modules/CuePoints',[
    'modules/Base',
    'utils/utils'
],

/**
 * Class for handling cue points.
 */
function(BaseModuleClass, utils) {
    'use strict';

    /* Utility function for sorting the cue points array */
    function sortFunc(a, b) {
        return a.time - b.time;
    }

    /**
     * Utility function to calculate the absolute time of a cuepoint.
     * Returns NaN if negative cue point time and segment duration not known.
     */
    function calcCuePointAbsoluteTime(offset, cuepoint, node) {
        var retVal;

        // Support negative cue point times
        // If cue point time is negative, that means seconds before end of node
        if (cuepoint.time < 0) {
            // If we do not yet know the segment's duration, we cannot calculate the absoulte time
            if (isNaN(node.segment.duration)) {
                retVal = NaN;
            } else {
                retVal = offset + node.segment.duration + cuepoint.time;
            }
        } else {
            retVal = offset + cuepoint.time;
        }

        return retVal;
    }

    var CuePoints = BaseModuleClass.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'CuePoints',
        logLevel: 'warn',
        excludeFuncFromAutoDebug: ['onTimeUpdate', 'dispatchCuePoints'],
        defaults: {
            /** Internal cue points to be fired as events for each node in playlist. */
            internalPerNode: {
                nodestart: {
                    time: 0
                },
                nodeend: {
                    time: -0.001
                },
                nodecritical: {
                    time: -3
                }
            },

            /** currentTime delta (in seconds) from total duration that could be considered as end of playback. */
            endDelta: 0.250,

            /** Interval for checking current time (in seconds). */
            interval: 0.016,

            /** currentTimestamp delta (in seconds) from prevTimestamp for buffering indication. */
            bufferingDelta: 0.100,

            /** How long after we detect buffering should we trigger the 'waiting' event (in case we're still in buffering) - in seconds. */
            triggerWaitingDelay: 0.075,

            /** How long after we detect buffering should we trigger the 'waiting' event (in case we're in a post-seeking state) - in seconds. */
            triggerWaitingDelayPostSeeking: 0.5,

            /** The duration after a 'seeking' event is triggered, to be considered 'post-seeking' state - in seconds. */
            postSeekingDuration: 1.0
        },

        //////////////////////////////////////////////////////////////////////////
        // ----------------------------- MEMBERS ---------------------------------
        //////////////////////////////////////////////////////////////////////////

        core: null,
        events: null,
        repository: null,
        playlist: null,
        prevCuePointWrapper: null,
        nodeIndexOffset: 0,
        currTimestampOffset: 0,
        intervalRef: null,
        waiting: false,
        cuePointsArray: null,
        postSeeking: false,
        postSeekingTimeoutRef: null,
        triggeredIds: {},

        //////////////////////////////////////////////////////////////////////////
        // ----------------------------- METHODS ---------------------------------
        //////////////////////////////////////////////////////////////////////////

        resetCuePoints: function() {
            var currNode = null;
            var currCuePoint = null;
            var currTimestampOffset = this.currTimestampOffset;
            var absoluteCuePointTime = 0;
            var internalPerNode = this.getSetting('internalPerNode', {});
            var internalCpId;

            this.cuePointsArray = [];

            for (var i = this.nodeIndexOffset; i < this.playlist.nodes.length; i++) {
                currNode = this.playlist.nodes[i];

                // If playback is already past currNode, we can continue to next node in playlist.
                if (!isNaN(currNode.segment.duration) && this.prevCuePointWrapper && currTimestampOffset + currNode.segment.duration < this.prevCuePointWrapper.time) {
                    currTimestampOffset += currNode.segment.duration;
                    this.currTimestampOffset = currTimestampOffset;
                    this.nodeIndexOffset++;
                    continue;
                }

                // Handle node's internal cuepoints
                for (var prop in internalPerNode) {
                    internalCpId = prop + '_' + currNode.id + '_' + i;
                    currCuePoint = internalPerNode[prop];
                    absoluteCuePointTime = calcCuePointAbsoluteTime(currTimestampOffset, currCuePoint, currNode);

                    // If we couldn't calculate the absolute time of this cue point, continue to next one
                    if (isNaN(absoluteCuePointTime)) {
                        continue;
                    }

                    // If playback is already past this cue point, continue to next cue point
                    if (this.prevCuePointWrapper &&
                            (absoluteCuePointTime < this.prevCuePointWrapper.time ||
                                    (absoluteCuePointTime === this.prevCuePointWrapper.time && this.triggeredIds[internalCpId]))) {
                        continue;
                    }

                    // Push cue point to array and define a callback that simply triggers an event
                    this.cuePointsArray.push({time: absoluteCuePointTime, cuePoint: utils.extend({id: internalCpId}, currCuePoint, {callback: (function(_this, eventName, playlistIndex, node) {
                        return function() {
                            _this.trigger(eventName, node, playlistIndex);
                            node.trigger(eventName, node, playlistIndex);
                        };
                    })(this, prop, i, currNode)})});
                }

                // Handle node's cue points
                for (var j = 0; j < currNode.cuePoints.length; j++) {
                    currCuePoint = currNode.cuePoints[j];
                    absoluteCuePointTime = calcCuePointAbsoluteTime(currTimestampOffset, currCuePoint, currNode);

                    // If we couldn't calculate the absolute time of this cue point, continue to next one
                    if (isNaN(absoluteCuePointTime)) {
                        continue;
                    }

                    // If playback is already past this cue point, continue to next cue point
                    if (this.prevCuePointWrapper &&
                            (absoluteCuePointTime < this.prevCuePointWrapper.time ||
                                    (absoluteCuePointTime === this.prevCuePointWrapper.time && this.triggeredIds[currCuePoint.id]))) {
                        continue;
                    }

                    this.cuePointsArray.push({time: absoluteCuePointTime, cuePoint: currCuePoint});
                }

                if (isNaN(currNode.segment.duration)) {
                    break;
                }

                currTimestampOffset += currNode.segment.duration;
            }

            this.cuePointsArray.sort(sortFunc);
        },

        /** Will add a temporary cuepoint with absolute time. For internal use by Controller. */
        addTempCuePoint: function(time, callback) {
            this.cuePointsArray.push({time: time, cuePoint: {callback: callback}});
            this.cuePointsArray.sort(sortFunc);
        },

        onPlaylistChanged: function() {
            if (!this.playlist.nodes.length) {
                this.prevCuePointWrapper = null;
                this.waiting = false;
                this.triggeredIds = {};
                this.nodeIndexOffset = 0;
                this.currTimestampOffset = 0;
            }

            this.resetCuePoints();
        },

        onSegmentUpdated: function() {
            this.resetCuePoints();
        },

        onCuePointsChanged: function() {
            this.resetCuePoints();
        },

        dispatchCuePoints: function(upToTimestamp) {
            var cuePointWrapper = this.cuePointsArray.length ? this.cuePointsArray[0] : null;

            while (cuePointWrapper && cuePointWrapper.time <= upToTimestamp) {
                if (cuePointWrapper.cuePoint.callback) {
                    this.log.debug('Dispatching cue point at: ' + cuePointWrapper.time);
                    cuePointWrapper.cuePoint.callback(cuePointWrapper.cuePoint);
                    this.prevCuePointWrapper = cuePointWrapper;
                    this.triggeredIds[cuePointWrapper.cuePoint.id] = true;
                }
                this.cuePointsArray.shift();
                cuePointWrapper = this.cuePointsArray.length ? this.cuePointsArray[0] : null;
            }
        },

        startInterval: function() {
            if (!this.intervalRef) {
                this.intervalRef = setInterval(this.onTimeUpdate, this.getSetting('interval') * 1000);
                this.waiting = false;
            }
        },

        stopInterval: function() {
            if (this.intervalRef) {
                clearInterval(this.intervalRef);
                this.intervalRef = null;
            }
        },

        onPlaying: function() {
            this.startInterval();
        },

        onPause: function() {
            this.stopInterval();
        },

        onEnded: function() {
            this.stopInterval();
            this.triggeredIds = {};
        },

        onSeeking: function() {
            if (this.core.exports.videoTech() !== 'hls') { // in hls, when seeking the video continue so we need to keep the interval
                this.stopInterval();
                this.triggeredIds = {};

                // Set postSeeking state to true for configured duration,
                // afterwhich reset it to false
                this.postSeeking = true;
                clearTimeout(this.postSeekingTimeoutRef);
                this.postSeekingTimeoutRef = setTimeout(function() {
                    this.postSeeking = false;
                }.bind(this), this.getSetting('postSeekingDuration') * 1000);
            }

        },

        onSeeked: function(time, index, offset) {
            // After seek is complete, we'll reset the cue points after setting the current offsets (index/timestamp etc).
            this.nodeIndexOffset = index;
            this.currTimestampOffset = offset;
            this.prevCuePointWrapper = {time: offset, cuePoint: {id: 'seeked'}};

            // Reset the cue points
            this.resetCuePoints();
        },

        onTimeUpdate: function() {
            var currentTime = this.core.exports.currentTime;
            var currentTimestamp = Date.now();
            var playlistDuration;

            this.dispatchCuePoints(currentTime);

            // If this is the end, dispatch remaining cue points
            if (currentTime === this.prevCurrentTime && Math.abs((playlistDuration = this.playlist.calcPlaylistDuration()) - this.prevCurrentTime) <= this.getSetting('endDelta')) {
                this.dispatchCuePoints(playlistDuration);
            } else if (currentTime === this.prevCurrentTime && this.prevTimestamp + (this.getSetting('bufferingDelta') * 1000) <= currentTimestamp && !this.waiting) {
                setTimeout(function() {
                    if (this.waiting) {
                        this.trigger('waiting');
                    }
                }.bind(this), (this.postSeeking ? this.getSetting('triggerWaitingDelayPostSeeking') :  this.getSetting('triggerWaitingDelay')) * 1000);
                this.waiting = true;
            } else if (currentTime !== this.prevCurrentTime) {
                this.prevTimestamp = currentTimestamp;
                if (this.waiting) {
                    this.waiting = false;
                    this.trigger('playing');
                }
            }

            this.prevCurrentTime = currentTime;
        },

        //////////////////////////////////////////////////////////////////////////
        // -------------------- CONSTRUCTOR / DESTRUCTOR -------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function(core, repo, playlist) {
            // Keep references to module instances
            this.core = core;
            this.events = core.exports.events;
            this.repository = repo;
            this.playlist = playlist;

            // Add event listeners
            this.core.on(this.events.playing, this.onPlaying);
            this.core.on(this.events.pause, this.onPause);
            this.core.on(this.events.seeked, this.onSeeked);
            this.core.on(this.events.ended, this.onEnded);
            this.core.on(this.events.seeking, this.onSeeking);
            this.playlist.on(/.*/, this.onPlaylistChanged);
            this.repository.on('repository.segment.updated', this.onSegmentUpdated);
            this.repository.on(/repository.cuepoint\..*/, this.onCuePointsChanged);
        },

        dispose: function() {
            this._super();

            // Clear interval
            this.stopInterval();

            // Remove event listeners
            this.core.off(this.events.playing, this.onPlaying);
            this.core.off(this.events.pause, this.onPause);
            this.core.off(this.events.seeked, this.onSeeked);
            this.core.off(this.events.seeking, this.onSeeking);
            this.core.off(this.events.ended, this.onEnded);
            this.playlist.off(/.*/, this.onPlaylistChanged);
            this.repository.off('repository.segment.updated', this.onSegmentUpdated);
            this.repository.off(/repository.cuepoint\..*/, this.onCuePointsChanged);
        }
    });

    return CuePoints;
});

define(
'modules/Playlist',[
    'modules/Base'
],

function(BaseModuleClass) {
    'use strict';

    var Playlist = BaseModuleClass.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'Playlist',
        logLevel: 'warn',

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ MEMBERS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        repository: null,
        nodes: [],

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ METHODS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        push: function(nodeId) {
            var node;

            // Handle pushing multiple nodes
            if (arguments.length > 1) {
                for (var i = 0; i < arguments.length; i++) {
                    this.push(arguments[i]);
                }

                return this.nodes.length;
            }

            // Handle pushing a single node
            node = null;
            if (this.repository.hasNode(nodeId)) {
                node = this.repository.getNode(nodeId);
            } else {
                node = this.repository.addNode(nodeId);
            }

            // Couldn't find node
            if (!node) {
                this.log.error('Could not find or create node: ' + nodeId);
                this.notifyError(28, nodeId);
            }

            // Node does not have a segment
            else if (!node.segment) {
                this.log.error('Node does not contain a segment: ' + nodeId);
                this.notifyError(43, node, nodeId);
            }

            // Success! Let's append node to playlist array
            else {
                this.nodes.push(node);
                this.exports[this.nodes.length - 1] = this.nodes[this.nodes.length - 1];
                this.trigger('playlist.push', node, this.nodes.length - 1);
            }

            return this.nodes.length;
        },

        reset: function() {
            for (var i = 0; i < this.nodes.length; i++) {
                delete this.exports[i];
            }
            this.nodes = [];
            this.trigger('playlist.reset');
        },

        /** Internal Metod */
        calcPlaylistDuration: function() {
            var retVal = 0;
            var segDuration = 0;

            for (var i = 0; i < this.nodes.length; i++) {
                segDuration = this.nodes[i].segment.duration;
                retVal += isNaN(segDuration) ? 0 : segDuration;
            }

            return retVal;
        },

        //////////////////////////////////////////////////////////////////////////
        // ---------------------------- CONSTRUCTOR ------------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function(repository) {
            this.repository = repository;

            // Define the getter 'length'
            this.defineProperty('length', function() {
                return this.nodes.length;
            });

            // Define the events that the Playlist object fires
            // This allows listeners to use regex
            this.defineEvents([
                'playlist.push',            // Callback args: (node)
                'playlist.reset'            // No callback args
            ]);

            // Public API
            this.expose({
                /**
                 * Push a node (or nodes) into the playlist array.
                 * Returns the new length of playlist array.
                 */
                push: this.push,

                reset: this.reset,

                length: this.exposeProperty('length')
            });
        }
    });

    return Playlist;
});

define('modules/FullScreen',[
    'modules/Base'
],

/**
 * Returns a class that exports the Full Screen API.
 * Instantiate using: new FullScreen(options, el);
 */
function(BaseModuleClass) {
    var FullScreen = BaseModuleClass.extend({

        /////////////////////////////////////////////////////////////
        //                       MODULE CONFIG
        /////////////////////////////////////////////////////////////

        name: 'FullScreen',
        logLevel: 'warn',

        /////////////////////////////////////////////////////////////
        //                      MEMBERS
        /////////////////////////////////////////////////////////////

        /** Map of events fired by this class. */
        events: {},

        /** An HTML DOM element that contains the player. This is the element that will go to fullscreen. */
        element: null,

        isCurrentlyInFullscreen: false,

        /////////////////////////////////////////////////////////////
        //                      METHODS
        /////////////////////////////////////////////////////////////

        requestFullscreen: function() {
            if (!this.element) {
                this.log.warn('Cannot perform requestFullscreen since element is null.');
                return;
            }

            if (this.element.requestFullscreen) {
                this.element.requestFullscreen();
            } else if (this.element.msRequestFullscreen) {
                this.element.msRequestFullscreen();
            } else if (this.element.mozRequestFullScreen) {
                this.element.mozRequestFullScreen();
            } else if (this.element.webkitRequestFullscreen) {
                this.element.webkitRequestFullscreen();
            } else {
                this.log.warn('requestFullscreen is not supported in current environment.');
            }
        },

        exitFullscreen: function() {
            if (!this.element) {
                this.log.warn('Cannot perform exitFullscreen since element is null.');
                return;
            }

            if (document && document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document && document.msExitFullscreen) {
                document.msExitFullscreen();
            } else if (document && document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document && document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            } else {
                this.log.warn('exitFullscreen is not supported in current environment.');
            }
        },

        isFullscreenSupported: function() {
            return !!(this.element &&
                (this.element.requestFullscreen || this.element.msRequestFullscreen || this.element.mozRequestFullScreen || this.element.webkitRequestFullscreen)
            );
        },

        onFullscreenChange: function() {
            var isFullscreen = !!(document &&
                    (document.fullscreenElement || document.fullscreen || document.mozFullScreen || document.webkitIsFullScreen || document.msFullscreenElement));
            var currentFullscreenElement = document &&
                    (document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement);

            if (currentFullscreenElement === this.element && currentFullscreenElement !== null) {
                this.isCurrentlyInFullscreen = true;
            }

            if (this.isCurrentlyInFullscreen) {
                if (!isFullscreen) {
                    this.isCurrentlyInFullscreen = false;
                }

                this.trigger(this.events.fullscreenchange, isFullscreen);
            }
        },

        /////////////////////////////////////////////////////////////
        //                      CONSTRUCTOR
        /////////////////////////////////////////////////////////////

        ctor: function(core, el) {
            this.element = el;

            // Populate the events object
            this.events.fullscreenchange = core.events.fullscreenchange;

            // Add event listeners
            if (document) {
                document.addEventListener('fullscreenchange', this.onFullscreenChange);
                document.addEventListener('webkitfullscreenchange', this.onFullscreenChange);
                document.addEventListener('mozfullscreenchange', this.onFullscreenChange);
                document.addEventListener('MSFullscreenChange', this.onFullscreenChange);
            }

            // This instance triggers the following events:
            this.defineEvents([
                this.events.fullscreenchange
            ]);

            // Expose public API
            this.expose({
                isFullscreenSupported: this.isFullscreenSupported,
                requestFullscreen: this.requestFullscreen,
                exitFullscreen: this.exitFullscreen
            });
        },

        dispose: function() {
            this._super();

            // Remove event listeners
            if (document) {
                document.removeEventListener('fullscreenchange', this.onFullscreenChange);
                document.removeEventListener('webkitfullscreenchange', this.onFullscreenChange);
                document.removeEventListener('mozfullscreenchange', this.onFullscreenChange);
                document.removeEventListener('MSFullscreenChange', this.onFullscreenChange);
            }
        }
    });

    return FullScreen;
});

define('engine/mse/formats/MseFormat',[
    'modules/Base'
],

/**
* This module defines a single method - 'detect()'.
* The 'detect()' method will detect the video format to be used by MSE.
*/
function(BaseModuleClass) {
    'use strict';

    function pushSupportedFormat(supportedFormatsArray, format)
    {
        supportedFormatsArray = supportedFormatsArray || [];
        supportedFormatsArray.push(format);
        return supportedFormatsArray;
    }

    var MseFormat = BaseModuleClass.extend({
        name: 'MseFormat',
        logLevel: 'warn',

        /**
        * Keys are the different video container formats currently available.
        * Values are objects that contain properties for the video format.
        */
        formats: {
            /*ts: {
                source: 'ts',
                mimeType: 'video/mp2t',
                mimeTypeAndCodecs: 'video/mp2t; codecs="avc1.42E01E, mp4a.40.2"'
            },*/

            mp4: {
                source: 'mp4',
                mimeType: 'video/mp4',
                mimeTypeAndCodecs: 'video/mp4; codecs="avc1.42E01E, mp4a.40.2"'
            },

            webm: {
                source: 'webm',
                mimeType: 'video/webm',
                mimeTypeAndCodecs: 'video/webm; codecs="vp8, vorbis"'
            }
        },

        /**
        * Detect the recommended technology in runtime based on environment and/or options param object.
        *
        * @param {Object} options - Use the key 'forceFormat' to force use of a certain video format (current valid values are: 'webm' or 'mp4').
        * Returns an array of objects containing following String properties: 'source', 'mimeType' and 'mimeTypeAndCodecs' (the array is sorted by preference of format).
        */
        detect: function() {
            var retVal = null;
            var forceFormat;

            // Handle forceFormat
            forceFormat = this.getSetting('forceFormat', false);
            if (forceFormat && this.formats[forceFormat]) {
                this.log.info('Forcing video format: ' + forceFormat);
                retVal = pushSupportedFormat(retVal, this.formats[forceFormat]);
            } else if (forceFormat) {
                this.log.error('Unknown video format: ' + forceFormat);
                this.notifyError(4, forceFormat);
            }

            // Detect video format based on environment
            else if (window && window.hasOwnProperty('MediaSource')) {
                for (var key in this.formats) {
                    if (window.MediaSource.isTypeSupported(this.formats[key].mimeTypeAndCodecs)) {
                        retVal = pushSupportedFormat(retVal, this.formats[key]);
                        this.log.info('Detected supported format: ' + key);
                    }
                }
            }

            // MSE is not supported...
            else {
                this.log.info('MSE is not supported!');
            }

            // MSE exists, but no supported formats were found
            if (retVal === null || retVal.length <= 0) {
                this.log.info('No supported MSE formats found!');
            }

            return retVal;
        }
    });

    return MseFormat;
});

(function(root) {
define("swfobject", [], function() {
  return (function() {
/*!    SWFObject v2.3.20130521 <http://github.com/swfobject/swfobject>
    is released under the MIT License <http://www.opensource.org/licenses/mit-license.php>
*/
var swfobject=function(){var D="undefined",r="object",T="Shockwave Flash",Z="ShockwaveFlash.ShockwaveFlash",q="application/x-shockwave-flash",S="SWFObjectExprInst",x="onreadystatechange",Q=window,h=document,t=navigator,V=false,X=[],o=[],P=[],K=[],I,p,E,B,L=false,a=false,m,G,j=true,l=false,O=function(){var ad=typeof h.getElementById!=D&&typeof h.getElementsByTagName!=D&&typeof h.createElement!=D,ak=t.userAgent.toLowerCase(),ab=t.platform.toLowerCase(),ah=ab?/win/.test(ab):/win/.test(ak),af=ab?/mac/.test(ab):/mac/.test(ak),ai=/webkit/.test(ak)?parseFloat(ak.replace(/^.*webkit\/(\d+(\.\d+)?).*$/,"$1")):false,aa=t.appName==="Microsoft Internet Explorer",aj=[0,0,0],ae=null;if(typeof t.plugins!=D&&typeof t.plugins[T]==r){ae=t.plugins[T].description;if(ae&&(typeof t.mimeTypes!=D&&t.mimeTypes[q]&&t.mimeTypes[q].enabledPlugin)){V=true;aa=false;ae=ae.replace(/^.*\s+(\S+\s+\S+$)/,"$1");aj[0]=n(ae.replace(/^(.*)\..*$/,"$1"));aj[1]=n(ae.replace(/^.*\.(.*)\s.*$/,"$1"));aj[2]=/[a-zA-Z]/.test(ae)?n(ae.replace(/^.*[a-zA-Z]+(.*)$/,"$1")):0}}else{if(typeof Q.ActiveXObject!=D){try{var ag=new ActiveXObject(Z);if(ag){ae=ag.GetVariable("$version");if(ae){aa=true;ae=ae.split(" ")[1].split(",");aj=[n(ae[0]),n(ae[1]),n(ae[2])]}}}catch(ac){}}}return{w3:ad,pv:aj,wk:ai,ie:aa,win:ah,mac:af}}(),i=function(){if(!O.w3){return}if((typeof h.readyState!=D&&(h.readyState==="complete"||h.readyState==="interactive"))||(typeof h.readyState==D&&(h.getElementsByTagName("body")[0]||h.body))){f()}if(!L){if(typeof h.addEventListener!=D){h.addEventListener("DOMContentLoaded",f,false)}if(O.ie){h.attachEvent(x,function aa(){if(h.readyState=="complete"){h.detachEvent(x,aa);f()}});if(Q==top){(function ac(){if(L){return}try{h.documentElement.doScroll("left")}catch(ad){setTimeout(ac,0);return}f()}())}}if(O.wk){(function ab(){if(L){return}if(!/loaded|complete/.test(h.readyState)){setTimeout(ab,0);return}f()}())}}}();function f(){if(L||!document.getElementsByTagName("body")[0]){return}try{var ac,ad=C("span");ad.style.display="none";ac=h.getElementsByTagName("body")[0].appendChild(ad);ac.parentNode.removeChild(ac);ac=null;ad=null}catch(ae){return}L=true;var aa=X.length;for(var ab=0;ab<aa;ab++){X[ab]()}}function M(aa){if(L){aa()}else{X[X.length]=aa}}function s(ab){if(typeof Q.addEventListener!=D){Q.addEventListener("load",ab,false)}else{if(typeof h.addEventListener!=D){h.addEventListener("load",ab,false)}else{if(typeof Q.attachEvent!=D){g(Q,"onload",ab)}else{if(typeof Q.onload=="function"){var aa=Q.onload;Q.onload=function(){aa();ab()}}else{Q.onload=ab}}}}}function Y(){var aa=h.getElementsByTagName("body")[0];var ae=C(r);ae.setAttribute("style","visibility: hidden;");ae.setAttribute("type",q);var ad=aa.appendChild(ae);if(ad){var ac=0;(function ab(){if(typeof ad.GetVariable!=D){try{var ag=ad.GetVariable("$version");if(ag){ag=ag.split(" ")[1].split(",");O.pv=[n(ag[0]),n(ag[1]),n(ag[2])]}}catch(af){O.pv=[8,0,0]}}else{if(ac<10){ac++;setTimeout(ab,10);return}}aa.removeChild(ae);ad=null;H()}())}else{H()}}function H(){var aj=o.length;if(aj>0){for(var ai=0;ai<aj;ai++){var ab=o[ai].id;var ae=o[ai].callbackFn;var ad={success:false,id:ab};if(O.pv[0]>0){var ah=c(ab);if(ah){if(F(o[ai].swfVersion)&&!(O.wk&&O.wk<312)){w(ab,true);if(ae){ad.success=true;ad.ref=z(ab);ad.id=ab;ae(ad)}}else{if(o[ai].expressInstall&&A()){var al={};al.data=o[ai].expressInstall;al.width=ah.getAttribute("width")||"0";al.height=ah.getAttribute("height")||"0";if(ah.getAttribute("class")){al.styleclass=ah.getAttribute("class")}if(ah.getAttribute("align")){al.align=ah.getAttribute("align")}var ak={};var aa=ah.getElementsByTagName("param");var af=aa.length;for(var ag=0;ag<af;ag++){if(aa[ag].getAttribute("name").toLowerCase()!="movie"){ak[aa[ag].getAttribute("name")]=aa[ag].getAttribute("value")}}R(al,ak,ab,ae)}else{b(ah);if(ae){ae(ad)}}}}}else{w(ab,true);if(ae){var ac=z(ab);if(ac&&typeof ac.SetVariable!=D){ad.success=true;ad.ref=ac;ad.id=ac.id}ae(ad)}}}}}X[0]=function(){if(V){Y()}else{H()}};function z(ac){var aa=null,ab=c(ac);if(ab&&ab.nodeName.toUpperCase()==="OBJECT"){if(typeof ab.SetVariable!==D){aa=ab}else{aa=ab.getElementsByTagName(r)[0]||ab}}return aa}function A(){return !a&&F("6.0.65")&&(O.win||O.mac)&&!(O.wk&&O.wk<312)}function R(ad,ae,aa,ac){var ah=c(aa);aa=W(aa);a=true;E=ac||null;B={success:false,id:aa};if(ah){if(ah.nodeName.toUpperCase()=="OBJECT"){I=J(ah);p=null}else{I=ah;p=aa}ad.id=S;if(typeof ad.width==D||(!/%$/.test(ad.width)&&n(ad.width)<310)){ad.width="310"}if(typeof ad.height==D||(!/%$/.test(ad.height)&&n(ad.height)<137)){ad.height="137"}var ag=O.ie?"ActiveX":"PlugIn",af="MMredirectURL="+encodeURIComponent(Q.location.toString().replace(/&/g,"%26"))+"&MMplayerType="+ag+"&MMdoctitle="+encodeURIComponent(h.title.slice(0,47)+" - Flash Player Installation");if(typeof ae.flashvars!=D){ae.flashvars+="&"+af}else{ae.flashvars=af}if(O.ie&&ah.readyState!=4){var ab=C("div");
aa+="SWFObjectNew";ab.setAttribute("id",aa);ah.parentNode.insertBefore(ab,ah);ah.style.display="none";y(ah)}u(ad,ae,aa)}}function b(ab){if(O.ie&&ab.readyState!=4){ab.style.display="none";var aa=C("div");ab.parentNode.insertBefore(aa,ab);aa.parentNode.replaceChild(J(ab),aa);y(ab)}else{ab.parentNode.replaceChild(J(ab),ab)}}function J(af){var ae=C("div");if(O.win&&O.ie){ae.innerHTML=af.innerHTML}else{var ab=af.getElementsByTagName(r)[0];if(ab){var ag=ab.childNodes;if(ag){var aa=ag.length;for(var ad=0;ad<aa;ad++){if(!(ag[ad].nodeType==1&&ag[ad].nodeName=="PARAM")&&!(ag[ad].nodeType==8)){ae.appendChild(ag[ad].cloneNode(true))}}}}}return ae}function k(aa,ab){var ac=C("div");ac.innerHTML="<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'><param name='movie' value='"+aa+"'>"+ab+"</object>";return ac.firstChild}function u(ai,ag,ab){var aa,ad=c(ab);ab=W(ab);if(O.wk&&O.wk<312){return aa}if(ad){var ac=(O.ie)?C("div"):C(r),af,ah,ae;if(typeof ai.id==D){ai.id=ab}for(ae in ag){if(ag.hasOwnProperty(ae)&&ae.toLowerCase()!=="movie"){e(ac,ae,ag[ae])}}if(O.ie){ac=k(ai.data,ac.innerHTML)}for(af in ai){if(ai.hasOwnProperty(af)){ah=af.toLowerCase();if(ah==="styleclass"){ac.setAttribute("class",ai[af])}else{if(ah!=="classid"&&ah!=="data"){ac.setAttribute(af,ai[af])}}}}if(O.ie){P[P.length]=ai.id}else{ac.setAttribute("type",q);ac.setAttribute("data",ai.data)}ad.parentNode.replaceChild(ac,ad);aa=ac}return aa}function e(ac,aa,ab){var ad=C("param");ad.setAttribute("name",aa);ad.setAttribute("value",ab);ac.appendChild(ad)}function y(ac){var ab=c(ac);if(ab&&ab.nodeName.toUpperCase()=="OBJECT"){if(O.ie){ab.style.display="none";(function aa(){if(ab.readyState==4){for(var ad in ab){if(typeof ab[ad]=="function"){ab[ad]=null}}ab.parentNode.removeChild(ab)}else{setTimeout(aa,10)}}())}else{ab.parentNode.removeChild(ab)}}}function U(aa){return(aa&&aa.nodeType&&aa.nodeType===1)}function W(aa){return(U(aa))?aa.id:aa}function c(ac){if(U(ac)){return ac}var aa=null;try{aa=h.getElementById(ac)}catch(ab){}return aa}function C(aa){return h.createElement(aa)}function n(aa){return parseInt(aa,10)}function g(ac,aa,ab){ac.attachEvent(aa,ab);K[K.length]=[ac,aa,ab]}function F(ac){ac+="";var ab=O.pv,aa=ac.split(".");aa[0]=n(aa[0]);aa[1]=n(aa[1])||0;aa[2]=n(aa[2])||0;return(ab[0]>aa[0]||(ab[0]==aa[0]&&ab[1]>aa[1])||(ab[0]==aa[0]&&ab[1]==aa[1]&&ab[2]>=aa[2]))?true:false}function v(af,ab,ag,ae){var ad=h.getElementsByTagName("head")[0];if(!ad){return}var aa=(typeof ag=="string")?ag:"screen";if(ae){m=null;G=null}if(!m||G!=aa){var ac=C("style");ac.setAttribute("type","text/css");ac.setAttribute("media",aa);m=ad.appendChild(ac);if(O.ie&&typeof h.styleSheets!=D&&h.styleSheets.length>0){m=h.styleSheets[h.styleSheets.length-1]}G=aa}if(m){if(typeof m.addRule!=D){m.addRule(af,ab)}else{if(typeof h.createTextNode!=D){m.appendChild(h.createTextNode(af+" {"+ab+"}"))}}}}function w(ad,aa){if(!j){return}var ab=aa?"visible":"hidden",ac=c(ad);if(L&&ac){ac.style.visibility=ab}else{if(typeof ad==="string"){v("#"+ad,"visibility:"+ab)}}}function N(ab){var ac=/[\\\"<>\.;]/;var aa=ac.exec(ab)!=null;return aa&&typeof encodeURIComponent!=D?encodeURIComponent(ab):ab}var d=function(){if(O.ie){window.attachEvent("onunload",function(){var af=K.length;for(var ae=0;ae<af;ae++){K[ae][0].detachEvent(K[ae][1],K[ae][2])}var ac=P.length;for(var ad=0;ad<ac;ad++){y(P[ad])}for(var ab in O){O[ab]=null}O=null;for(var aa in swfobject){swfobject[aa]=null}swfobject=null})}}();return{registerObject:function(ae,aa,ad,ac){if(O.w3&&ae&&aa){var ab={};ab.id=ae;ab.swfVersion=aa;ab.expressInstall=ad;ab.callbackFn=ac;o[o.length]=ab;w(ae,false)}else{if(ac){ac({success:false,id:ae})}}},getObjectById:function(aa){if(O.w3){return z(aa)}},embedSWF:function(af,al,ai,ak,ab,ae,ad,ah,aj,ag){var ac=W(al),aa={success:false,id:ac};if(O.w3&&!(O.wk&&O.wk<312)&&af&&al&&ai&&ak&&ab){w(ac,false);M(function(){ai+="";ak+="";var an={};if(aj&&typeof aj===r){for(var aq in aj){an[aq]=aj[aq]}}an.data=af;an.width=ai;an.height=ak;var ar={};if(ah&&typeof ah===r){for(var ao in ah){ar[ao]=ah[ao]}}if(ad&&typeof ad===r){for(var am in ad){if(ad.hasOwnProperty(am)){var ap=(l)?encodeURIComponent(am):am,at=(l)?encodeURIComponent(ad[am]):ad[am];if(typeof ar.flashvars!=D){ar.flashvars+="&"+ap+"="+at}else{ar.flashvars=ap+"="+at}}}}if(F(ab)){var au=u(an,ar,al);if(an.id==ac){w(ac,true)}aa.success=true;aa.ref=au;aa.id=au.id}else{if(ae&&A()){an.data=ae;R(an,ar,al,ag);return}else{w(ac,true)}}if(ag){ag(aa)}})}else{if(ag){ag(aa)}}},switchOffAutoHideShow:function(){j=false},enableUriEncoding:function(aa){l=(typeof aa===D)?true:aa},ua:O,getFlashPlayerVersion:function(){return{major:O.pv[0],minor:O.pv[1],release:O.pv[2]}},hasFlashPlayerVersion:F,createSWF:function(ac,ab,aa){if(O.w3){return u(ac,ab,aa)}else{return undefined}},showExpressInstall:function(ac,ad,aa,ab){if(O.w3&&A()){R(ac,ad,aa,ab)}},removeSWF:function(aa){if(O.w3){y(aa)}},createCSS:function(ad,ac,ab,aa){if(O.w3){v(ad,ac,ab,aa)}},addDomLoadEvent:M,addLoadEvent:s,getQueryParamValue:function(ad){var ac=h.location.search||h.location.hash;
if(ac){if(/\?/.test(ac)){ac=ac.split("?")[1]}if(ad==null){return N(ac)}var ab=ac.split("&");for(var aa=0;aa<ab.length;aa++){if(ab[aa].substring(0,ab[aa].indexOf("="))==ad){return N(ab[aa].substring((ab[aa].indexOf("=")+1)))}}}return""},expressInstallCallback:function(){if(a){var aa=c(S);if(aa&&I){aa.parentNode.replaceChild(I,aa);if(p){w(p,true);if(O.ie){I.style.display="block"}}if(E){E(B)}}a=false}},version:"2.3"}}();

return root.swfobject = swfobject;
  }).apply(root, arguments);
});
}(this));

define('engine/flash/FlashConfig',[
    'modules/Base',
    'swfobject'
],

function(BaseModuleClass, swfobject) {
    'use strict';

    var FlashConfig = BaseModuleClass.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'FlashConfig',
        logLevel: 'warn',
        defaults: {
            /** Minimum flash player version required. */
            MIN_FLASH_VERSION: '11.4.0',

            /** URL (preferably absolute) of flash engine SWF. */
            SWF_URL: 'https://d3425luerwqydx.cloudfront.net/players/flashengine/0.0.13/flashEngine.swf'
            // SWF_URL: 'http://localhost/flashengine/flashEngine.swf'
        },

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ METHODS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        isFlashSupported: function() {
            return swfobject.hasFlashPlayerVersion(this.getSetting('MIN_FLASH_VERSION'));
        },

        //////////////////////////////////////////////////////////////////////////
        // ---------------------------- CONSTRUCTOR ------------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function() {
            this.defineProperty('MIN_FLASH_VERSION', function() {
                return this.getSetting('MIN_FLASH_VERSION');
            });

            this.defineProperty('SWF_URL', function() {
                return this.getSetting('SWF_URL');
            });

            this.expose({
                MIN_FLASH_VERSION: this.exposeProperty('MIN_FLASH_VERSION'),
                SWF_URL: this.exposeProperty('SWF_URL'),
                isFlashSupported: this.isFlashSupported
            });
        }
    });

    return FlashConfig;
});

define(
'engine/EngineTech',[
    'modules/Base',
    'engine/mse/formats/MseFormat',
    'engine/flash/FlashConfig',
    'utils/environment'
],

/*
* This module defines the 'detect()' method,
* which detects the best technology to use and returns an Object that can be used as input param for VideoTechFactory.
*/
function(BaseModuleClass, MseFormat, FlashConfig, env) {
    'use strict';

    var EngineTech = BaseModuleClass.extend({

        ////////////////////////////////////////////////////////////
        // MODULE CONFIG
        ////////////////////////////////////////////////////////////

        name: 'EngineTech',
        logLevel: 'warn',
        defaults: {
            /** String. Optionally force a specific technology. Currently available techs: 'flash', 'mse' or 'hls'. */
            forceTech: false,

            /**
             * Array of strings, or a function that returns an array of strings.
             * Techs to detect by descending priority.
             * Currently available techs: 'flash', 'mse' and 'hls'.
             */
            techs: function() {
                if (env.browser.name === 'Safari') {
                    return ['flash', 'hls', 'mse'];
                }

                if (env.browser.name === 'Chrome') {
                    return ['mse', 'flash', 'hls'];
                }

                return ['flash', 'mse', 'hls'];
            }
        },

        ////////////////////////////////////////////////////////////
        // MEMBERS
        ////////////////////////////////////////////////////////////

        mseFormats: null,
        flashConfig: null,

        ////////////////////////////////////////////////////////////
        // METHODS
        ////////////////////////////////////////////////////////////

        isTechSupported: function(tech) {
            var retVal = false;

            switch (tech.toLowerCase()) {
                case 'flash':
                    retVal = this.flashConfig && this.flashConfig.isFlashSupported();
                    break;
                case 'mse':
                    retVal = this.mseFormats && this.mseFormats.length >= 1;
                    break;
                case 'hls':
                    retVal = window &&
                            window.document &&
                            window.document.createElement('video').canPlayType('application/vnd.apple.mpegURL') !== '';
                    break;
                default:
                    this.log.warn('Unknown tech: ' + tech);
                    break;
            }

            return retVal;
        },

        getTechObject: function(tech) {
            var retVal = null;

            switch (tech.toLowerCase()) {
                case 'flash':
                    retVal = {
                        videoTech: 'flash',
                        flashConfig: this.flashConfig
                    };
                    break;
                case 'mse':
                    retVal = {
                        videoTech: 'mse',
                        formats: this.mseFormats
                    };
                    break;
                case 'hls':
                    retVal = {
                        videoTech: 'hls'
                    };
                    break;
                default:
                    this.log.warn('Unknown tech: ' + tech);
                    break;
            }

            return retVal;
        },

        ////////////////////////////////////////////////////////////
        // PUBLIC METHODS
        ////////////////////////////////////////////////////////////

        /*
        * Detect the recommended technology in runtime based on environment and/or options param object.
        * Returns an Object that can be used as the param for VideoTechFactory.
        * If no supported technology is detected, logs error and returns null.
        */
        detect: function() {
            var retVal = null;
            var forceTech;

            // Handle 'forceTech' setting.
            ///////////////////////////////

            forceTech = this.getSetting('forceTech', false);
            if (forceTech) {
                retVal = this.getTechObject(forceTech);
                if (retVal) {
                    this.log.info('Forcing tech: ' + forceTech);
                } else {
                    this.log.error('Cannot force technology, unknown tech: ' + forceTech);
                    this.notifyError(2, forceTech);
                }
            }

            // Detect technology based on environment
            //////////////////////////////////////////

            else {
                var techs = this.getSetting('techs');

                // Allow techs to be a function
                if (typeof techs === 'function') {
                    techs = techs();
                }

                // Validate techs
                if (!techs || !Array.isArray(techs) || !techs.length) {
                    this.log.error('Invalid EngineTech configuration, \'techs\' must be a non-empty array (or function that returns a non-empty array): ' + techs);
                    this.notifyError(64, techs);
                    return null;
                }

                // Iterate over techs (by descending priority).
                for (var i = 0; i < techs.length; i++) {
                    if (this.isTechSupported(techs[i])) {
                        retVal = this.getTechObject(techs[i]);
                        this.log.info('Detected tech: ' + techs[i]);
                        break;
                    }
                }

                // If no supported tech was found, throw error
                if (!retVal) {
                    this.log.error('Could not find any supported video technology.');
                    this.notifyError(3);
                }
            }

            return retVal;
        },

        ////////////////////////////////////////////////////////////
        // CONSTRUCTOR
        ////////////////////////////////////////////////////////////

        ctor: function() {
            // Init members
            this.mseFormats = new MseFormat(this.options).detect();
            this.flashConfig = new FlashConfig(this.options).exports;

            // Expose public API
            this.expose({
                detect: this.detect
            });
        }
    });

    return EngineTech;
});

define(
'engine/BaseFactory',[
    'modules/Base'
],

/**
 * BaseFactory is a base class for concrete technology factories. (currently HlsFactory and MseFactory).
 */
function(BaseModuleClass) {
    'use strict';

    var BaseFactory = BaseModuleClass.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'BaseFactory',
        logLevel: 'warn',

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ MEMBERS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        el: null,
        core: null,
        repo: null,

        //////////////////////////////////////////////////////////////////////////
        // ------------------------- PRIVATE METHODS -----------------------------
        //////////////////////////////////////////////////////////////////////////

        /** To be overriden in extending classes. */
        _createEngine: function() { return null; },

        //////////////////////////////////////////////////////////////////////////
        // ------------------------- PUBLIC METHODS ------------------------------
        //////////////////////////////////////////////////////////////////////////

        makeEngine: function() {
            return this._createEngine();
        },

        //////////////////////////////////////////////////////////////////////////
        // ---------------------------- CONSTRUCTOR ------------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function(el, core, repo) {
            this.el = el;
            this.core = core;
            this.repo = repo;

            // Export Public API
            this.expose({
                makeEngine: this.makeEngine
            });
        }
    });

    return BaseFactory;
});

/* jshint unused:false */

define(
'engine/BaseEngine',[
    'modules/Base'
],

/**
* BaseEngine - Base module for different concrete engine implementation
*/
function(BaseModuleClass) {
    'use strict';

    var BaseEngine = BaseModuleClass.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'BaseEngine',
        logLevel: 'warn',

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ MEMBERS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        api: null,
        events: null,
        environment: null,
        repository: null,

        /** To be populated by extending classes. This MUST adhere to the BaseVideo interface. */
        video: null,

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ METHODS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        /** To be overridden in extending classes. */
        appendNode: function(nodeId) {},

        /** To be overridden in extending classes. */
        reset: function() {},

        /** To be overridden in extending classes. End the video stream (if such an operation is relevant to specific engine). */
        endStream: function() {},

        /** To be overridden in extending classes. */
        switchChannel: function(channelIndex) {},

        /** To be overridden in extending classes. */
        seek: function(nodeId, toTimeInNode) {},

        /** To be overridden in extending classes. */
        videoTech: function() { return 'unknown'; },

        //////////////////////////////////////////////////////////////////////////
        // ---------------------------- CONSTRUCTOR ------------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function(core, repo) {
            this.api = core;
            this.repository = repo;
            this.events = core.events;
            this.environment = core.environment;

            /* Public API */
            this.expose({

                /**
                * Append relevent node the playing.
                *
                * @param {string} nodeId - The unique ID of the repository node to append.
                *
                */
                appendNode: this.appendNode,

                /**
                * Reset engine data - revert to initial state
                */
                reset: this.reset,

                switchChannel: this.switchChannel,

                seek: this.seek,

                videoTech: this.videoTech
            });
        }
    });

    return BaseEngine;
});

define('engine/BaseVideo',[
    'modules/Base'
],

/**
 * A base class for concrete implementations of Video module.
 */
function(BaseModuleClass) {
    'use strict';

    var BaseVideo = BaseModuleClass.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'BaseVideo',
        logLevel: 'warn',

        //////////////////////////////////////////////////////////////////////////
        // ----------------------------- MEMBERS ---------------------------------
        //////////////////////////////////////////////////////////////////////////

        api: null,
        events: null,

        //////////////////////////////////////////////////////////////////////////
        // ----------------------------- METHODS ---------------------------------
        //////////////////////////////////////////////////////////////////////////

        /** To be overridden in extending classes. */
        play: function() {},

        /** To be overridden in extending classes. */
        pause: function() {},

        /** To be overridden in extending classes. */
        currentTime: function() {},

        //////////////////////////////////////////////////////////////////////////
        // --------------------------- CONSTRUCTOR -------------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function(api) {
            this.api = api;
            this.events = api.events;

            /** Extending classes should trigger the following events. */
            this.defineEvents([
                this.events.timeupdate,
                this.events.volumechange,
                this.events.loadeddata,
                this.events.playing,
                this.events.waiting,
                this.events.play,
                this.events.pause,
                this.events.ended
            ]);

            /**
             * Extending classes should define the following properties:
             *
             * 'volume' - Getter/Setter
             * 'muted' - Getter/Setter
             */

            this.expose({
                play: this.play,
                pause: this.pause,
                volume: this.exposeProperty('volume'),
                muted: this.exposeProperty('muted')
            });
        }
    });

    return BaseVideo;
});

define('modules/Html5Video',[
    'modules/Base'
],

function(BaseModuleClass) {
    'use strict';

    var Html5Video = BaseModuleClass.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'Html5Video',
        logLevel: 'warn',
        defaults: {
            videoStyle: {
                width: '100%',
                height: '100%'
            }
        },

        //////////////////////////////////////////////////////////////////////////
        // ----------------------------- METHODS ---------------------------------
        //////////////////////////////////////////////////////////////////////////

        /**
         * Parses the input el argument (whether it be a selector string, or a container elememt), creates and returns the <video> element to be used by player.
         * In case of error, it will be logged and null returned.
         * oldElement is an optional parameter, if it was given and it one of the container childs, it will be replaced by the new <video> element.
         *
         * @param {*} container
         * @param oldElement (optional)
         * @returns {HTMLVideoElement}
         */
        createVideoElement: function(container, oldElement) {
            var retVal = null;

            // If container is valid, create and append <video> element
            if (container && container.appendChild) {
                retVal = document.createElement('video');
                var videoStyle = this.getSetting('videoStyle', null);
                for (var prop in videoStyle) {
                    retVal.style[prop] = videoStyle[prop];
                }
                if (oldElement && oldElement.parentElement === container) {
                    container.replaceChild(retVal, oldElement);
                }
                else {
                    container.appendChild(retVal);
                }
            } else {
                this.log.error('Could not retrieve container element.');
                this.notifyError(27);
            }

            return retVal;
        },

        makeHtml5Video: function(containerElement, oldElement) {
            var container = containerElement;
            if (!container) {
                return null;
            }

            var element = this.createVideoElement(container, oldElement);
            if (!element) {
                return null;
            }

            this.log.info('Successfully created video element.');

            return {
                getElement: function getElement() {
                    return element;
                },
                play: function play() {
                    element.play();
                },
                pause: function pause() {
                    element.pause();
                },
                paused: function paused() {
                    return element.paused;
                },
                stop: function stop() {
                    element.pause();
                },
                currentTime: function currentTime() {
                    return element.currentTime;
                },
                setCurrentTime: function setCurrentTime(time) {
                    element.currentTime = time;
                },
                duration: function duration() {
                    return element.duration || 0.0;
                },
                buffered: function buffered() {
                    return element.buffered;
                },
                volume: function volume() {
                    return element.volume;
                },
                setVolume: function setVolume(volume) {
                    element.volume = volume;
                },
                muted: function muted() {
                    return element.muted;
                },
                setMuted: function setMuted(muted) {
                    element.muted = muted;
                },
                currentSrc: function currentSrc() {
                    return element.currentSrc;
                },
                setSrc: function setSrc(src) {
                    if (src) {
                        element.src = src;
                    } else {
                        element.removeAttribute('src');
                    }
                },
                load: function load() {
                    element.load();
                },
                error: function error() {
                    return element.error;
                },
                seeking: function seeking() {
                    return element.seeking;
                },
                ended: function ended() {
                    return element.ended;
                },
                on: function on(event, callback) {
                    element.addEventListener(event, callback, false);
                },
                off: function off(event, callback) {
                    element.removeEventListener(event, callback);
                },
                networkState: function networkState() {
                    return element.networkState;
                },
                readyState: function readyState() {
                    return element.readyState;
                },
                NETWORK_EMPTY: element.NETWORK_EMPTY,
                NETWORK_IDLE: element.NETWORK_IDLE,
                NETWORK_LOADING: element.NETWORK_LOADING,
                NETWORK_NO_SOURCE: element.NETWORK_NO_SOURCE,
                HAVE_NOTHING: element.HAVE_NOTHING,
                HAVE_METADATA: element.HAVE_METADATA,
                HAVE_CURRENT_DATA: element.HAVE_CURRENT_DATA,
                HAVE_FUTURE_DATA: element.HAVE_FUTURE_DATA,
                HAVE_ENOUGH_DATA: element.HAVE_ENOUGH_DATA,
                MEDIA_ERR_ABORTED: element.MEDIA_ERR_ABORTED,
                MEDIA_ERR_NETWORK: element.MEDIA_ERR_NETWORK,
                MEDIA_ERR_DECODE: element.MEDIA_ERR_DECODE,
                MEDIA_ERR_SRC_NOT_SUPPORTED: element.MEDIA_ERR_SRC_NOT_SUPPORTED
            };
        }
    });

    return Html5Video;
});

define(
'engine/hls/HlsVideo',[
    'engine/BaseVideo',
    'modules/Html5Video'
],

/**
 * Concrete basic implementation for the Video module of HLS tech (extends the BaseVideo module).
 */
function(BaseVideo, Html5Video) {
    'use strict';

    var HlsVideo = BaseVideo.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'HlsVideo',
        logLevel: 'warn',
        excludeFuncFromAutoDebug: [],

        //////////////////////////////////////////////////////////////////////////
        // ----------------------------- MEMBERS ---------------------------------
        //////////////////////////////////////////////////////////////////////////

        api: null,
        events: null,
        htmlVideo: null,
        firstPlay: null,
        endStream: null,
        url: null,
        loadedmetadata: null,
        firstPlayshouldBePlaying: false,

        //////////////////////////////////////////////////////////////////////////
        // ------------------------- NATIVE LISTENERS ----------------------------
        //////////////////////////////////////////////////////////////////////////

        onAbort: function() {},
        onError: function() {},
        onCanPlayThrough: function() {
            this.loadedmetadata = false;
            var paused = this.htmlVideo.paused();
            var readyState = this.htmlVideo.readyState();
            if (paused && readyState === this.htmlVideo.HAVE_ENOUGH_DATA) {
                this.htmlVideo.play();
            }
        },

        onPlaying: function() {
            this.trigger(this.events.playing);
        },

        onWaiting: function() {
            this.trigger(this.events.waiting);
            this.htmlVideo.pause();
        },

        onEnded: function() {
            this.trigger(this.events.ended);
        },

        onTimeUpdate: function() {
            this.trigger(this.events.timeupdate, this.htmlVideo.currentTime());
        },

        onPlay: function() {
            this.trigger(this.events.play);
        },

        onPause: function() {
            this.trigger(this.events.pause);
        },

        onVolumeChange: function() {
            this.trigger(this.events.volumechange, this.htmlVideo.volume());
        },

        onProgress: function() {},
        onStalled: function() {},
        onSuspend: function() {},
        onEmptied: function() {},

        onCanPlay: function() {
            this.loadedmetadata = false;
        },

        onLoadedData: function() {
            this.loadedmetadata = true;
            setTimeout(function() {
                if (this.loadedmetadata) {
                    this.log.debug('reload video element');
                    this.htmlVideo.load();
                }
            }.bind(this), 3000);
            this.trigger(this.events.loadeddata);
        },

        onLoadedMetaData: function() {},

        onSeeking: function() {},

        onSeeked: function() {},

        onAPISeeking: function() {},

        onAPISeeked: function() {},

        //////////////////////////////////////////////////////////////////////////
        // ----------------------------- METHODS ---------------------------------
        //////////////////////////////////////////////////////////////////////////

        play: function() {
            if (this.firstPlay) {
                if (this.url) {
                    this.firstPlay = false;
                    this.htmlVideo.setSrc(this.url);
                    this.htmlVideo.load();
                    this.htmlVideo.play();
                } else {
                    this.firstPlayShouldBePlaying = true;
                    this.log.warn('url was not set yet.');
                }
            } else {
                if (this.endStream) {
                    this.log.debug('ignore play - we dont have more to play');
                } else {
                    var paused = this.htmlVideo.paused();
                    if (paused) {
                        this.htmlVideo.play();
                    } else {
                        this.log.warn('video is not paused. ignore request.');
                    }
                }
            }
        },

        pause: function() {
            var paused = this.htmlVideo.paused();
            if (!paused) {
                this.htmlVideo.pause();
            } else {
                this.log.warn('video is paused. ignore request.');
            }
        },

        currentTime: function() {
            return this.htmlVideo.currentTime();
        },

        initSessionInfo: function(url) {
            this.url = url;
        },

        reset: function() {
            this.firstPlay = true;
            this.endStream = true;
            this.loadedmetadata = false;
            this.firstPlayShouldBePlaying = false;
            var oldUrl = this.url;
            this.url = null;

            /* jshint ignore:start */
            this.htmlVideo && this.removeHtmlVideoListeners();
            /* jshint ignore:end */

            var dummyUrl = oldUrl ? oldUrl + '&dummy=true' : '';
            this.htmlVideo.setSrc(dummyUrl);
            this.htmlVideo.load();

            // re-create html5Video
            var vidElm = this.htmlVideo.getElement();
            var container = vidElm.parentElement;
            delete this.htmlVideo;

            this.htmlVideo = new Html5Video(this.options).makeHtml5Video(container, vidElm);
            this.addErrorListener(this.htmlVideo);
            this.addHtmlVideoListeners();
        },

        addHtmlVideoListeners: function() {
            this.htmlVideo.on('abort', this.onAbort);
            this.htmlVideo.on('error', this.onError);
            this.htmlVideo.on('canplaythrough', this.onCanPlayThrough);
            this.htmlVideo.on('playing', this.onPlaying);
            this.htmlVideo.on('waiting', this.onWaiting);
            this.htmlVideo.on('ended', this.onEnded);
            this.htmlVideo.on('timeupdate', this.onTimeUpdate);
            this.htmlVideo.on('play', this.onPlay);
            this.htmlVideo.on('pause', this.onPause);
            this.htmlVideo.on('volumechange', this.onVolumeChange);
            this.htmlVideo.on('progress', this.onProgress);
            this.htmlVideo.on('stalled', this.onStalled);
            this.htmlVideo.on('suspend', this.onSuspend);
            this.htmlVideo.on('emptied', this.onEmptied);
            this.htmlVideo.on('canplay', this.onCanPlay);
            this.htmlVideo.on('loadeddata', this.onLoadedData);
            this.htmlVideo.on('loadedmetadata', this.onLoadedMetaData);
            this.htmlVideo.on('seeking', this.onSeeking);
            this.htmlVideo.on('seeked', this.onSeeked);
        },

        removeHtmlVideoListeners: function() {
            this.htmlVideo.off('abort', this.onAbort);
            this.htmlVideo.off('error', this.onError);
            this.htmlVideo.off('canplaythrough', this.onCanPlayThrough);
            this.htmlVideo.off('playing', this.onPlaying);
            this.htmlVideo.off('waiting', this.onWaiting);
            this.htmlVideo.off('ended', this.onEnded);
            this.htmlVideo.off('timeupdate', this.onTimeUpdate);
            this.htmlVideo.off('play', this.onPlay);
            this.htmlVideo.off('pause', this.onPause);
            this.htmlVideo.off('volumechange', this.onVolumeChange);
            this.htmlVideo.off('progress', this.onProgress);
            this.htmlVideo.off('stalled', this.onStalled);
            this.htmlVideo.off('suspend', this.onSuspend);
            this.htmlVideo.off('emptied', this.onEmptied);
            this.htmlVideo.off('canplay', this.onCanPlay);
            this.htmlVideo.off('loadeddata', this.onLoadedData);
            this.htmlVideo.off('loadedmetadata', this.onLoadedMetaData);
            this.htmlVideo.off('seeking', this.onSeeking);
            this.htmlVideo.off('seeked', this.onSeeked);
        },

        //////////////////////////////////////////////////////////////////////////
        // --------------------------- CONSTRUCTOR -------------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function(api, el) {
            // Keep references
            this.api = api;
            this.events = api.events;
            this.htmlVideo = new Html5Video(this.options).makeHtml5Video(el);

            // Add error listeners to submodules
            this.addErrorListener(this.htmlVideo);

            this.firstPlay = true;
            this.endStream = true;
            this.loadedmetadata = false;

            // Define properties
            this.defineProperty('volume', function() {
                return this.htmlVideo.getElement().volume;
            }, function(value) {
                this.htmlVideo.getElement().volume = value;
            });

            this.defineProperty('muted', function() {
                return this.htmlVideo.getElement().muted;
            }, function(value) {
                this.htmlVideo.getElement().muted = value;
            });

            // Add listeners
            this.addHtmlVideoListeners();

            // Call base ctor - exposes public API.
            this._super.apply(this, arguments);
        },

        dispose: function() {
            this._super();

            // Remove listeners
            this.removeHtmlVideoListeners();
        }
    });

    return HlsVideo;
});

define(
'engine/hls/HlsPlaylistBuilder',[
    'modules/Base'
],

function(BaseModuleClass) {
    'use strict';

    var HlsPlaylistBuilder = BaseModuleClass.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'HlsPlaylistBuilder',
        logLevel: 'warn',

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ MEMBERS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ METHODS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        buildInitialPlaylist: function() {
            return this.createHeader(0);
        },

        buildPlaylist: function(streamList, startIndex, endIndex) {
            return this.createStrForPlaylist(streamList, startIndex, endIndex);
        },

        buildEndPlaylist: function(streamList, startIndex, endIndex) {
            var retVal = '';
            retVal += this.buildPlaylist(streamList, startIndex, endIndex);
            retVal += this.createEndList();
            return retVal;
        },

        createStrForPlaylist: function(streamList, startIndex, endIndex) {
            var retVal = '';
            var strBytes = null;
            retVal += this.createHeader(startIndex);
            strBytes = this.createBytes(streamList, startIndex, endIndex);
            if (strBytes === null) {
                return null;
            } else {
                retVal += strBytes;
                return retVal;
            }
        },

        createHeader: function(index) {
            return '#EXTM3U\n#EXT-X-VERSION:4\n#EXT-X-MEDIA-SEQUENCE:' + index + '\n#EXT-X-TARGETDURATION:1\n';
        },

        createEndList: function() {
            return '#EXT-X-ENDLIST\n';
        },

        createBytes: function(streamList, startIndex, endIndex) {
            var i;
            var bytes;
            var url;
            var duration;
            var offset;
            var length;
            var bytesStr;
            var retVal = '';
            // validate input
            if (!streamList || startIndex < 0 || endIndex < 0 || startIndex > endIndex || endIndex > streamList.length) {
                return null;
            } else {
                for (i = startIndex ; i < endIndex ; i++) {
                    if (!this.isValidData(streamList[i])) {
                        return null;
                    } else {
                        bytes = streamList[i].bytesData;
                        url = bytes.url;
                        duration = bytes.byterange.duration;
                        offset = bytes.byterange.offset;
                        length = bytes.byterange.length;
                        bytesStr = '#EXT-INF:' + duration + ',\n#EXT-X-BYTERANGE:' + length + '@' + offset + '\n' + url + '\n';
                        retVal += bytesStr;
                    }
                }
                return retVal;
            }
        },

        isValidData: function(data) {
            var bytes = data.bytesData;
            if (bytes && bytes.url && bytes.byterange && bytes.byterange.hasOwnProperty('duration') && bytes.byterange.hasOwnProperty('offset') && bytes.byterange.hasOwnProperty('length')) {
                return true;
            }
            return false;
        },

        //////////////////////////////////////////////////////////////////////////
        // ---------------------------- CONSTRUCTOR ------------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function() {},

        dispose: function() {}
    });

    return HlsPlaylistBuilder;
});

define('engine/hls/awsRegionsMap',[], function() {
    'use strict';
    return {
			'regions': {
				'ap-northeast-1': {
					'server': 'http://lsg.ap-southeast-2.interlude.fm',
					'continent': 'Asia Pacific',
					'countries': {
						'AP': {
							'name': 'Asia/Pacific Region'
						},
						'CN': {
							'name': 'China'
						},
						'FM': {
							'name': 'Micronesia, Federated States of'
						},		
						'GU': {
							'name': 'Guam'
						},	
						'JP': {
							'name': 'Japan'
						},		
						'KP': {
							'name': 'Korea, Democratic Peoples Republic of'
						},		
						'KR': {
							'name': 'Korea, Republic of'
						},	
						'MH': {
							'name': 'Marshall Islands'
						},								
						'MN': {
							'name': 'Mongolia'
						},		
						'MP': {
							'name': 'Northern Mariana Islands'
						},	
						'PW': {
							'name': 'Palau'
						},		
						'RU': {
							'name': 'Russian Federation'
						},		
						'TW': {
							'name': 'Taiwan'
						},	
						'UM': {
							'name': 'United States Minor Outlying Islands'
						}								
					}
				},
				'ap-southeast-1': {
					'server': 'http://lsg.ap-southeast-2.interlude.fm',
					'continent': 'Asia Pacific',
					'countries': {
						'AE': {
							'name': 'United Arab Emirates'
						},
						'AF': {
							'name': 'Afghanistan'
						},				
						'BD': {
							'name': 'Bangladesh'
						},				
						'BN': {
							'name': 'Brunei Darussalam'
						},				
						'BT': {
							'name': 'Bhutan'
						},				
						'CC': {
							'name': 'Cocos (Keeling) Islands'
						},				
						'CX': {
							'name': 'Christmas Island'
						},				
						'HK': {
							'name': 'Hong Kong'
						},	
						'ID': {
							'name': 'Indonesia'
						},
						'IN': {
							'name': 'India'
						},				
						'IO': {
							'name': 'British Indian Ocean Territory'
						},				
						'KG': {
							'name': 'Kyrgyzstan'
						},				
						'KH': {
							'name': 'Cambodia'
						},				
						'KM': {
							'name': 'Comoros'
						},		
						'LA': {
							'name': 'Lao Peoples Democratic Republic'
						},				
						'LK': {
							'name': 'HSri Lanka'
						},
						'LS': {
							'name': 'Lesotho'
						},		
						'MG': {
							'name': 'Madagascar'
						},				
						'MM': {
							'name': 'Myanmar'
						},	
						'MO': {
							'name': 'Macao'
						},		
						'MU': {
							'name': 'Mauritius'
						},				
						'MV': {
							'name': 'Maldives'
						},		
						'MW': {
							'name': 'Malawi'
						},	
						'MY': {
							'name': 'Malaysia'
						},		
						'MZ': {
							'name': 'Mozambique'
						},				
						'NP': {
							'name': 'Nepal'
						},	
						'OM': {
							'name': 'Oman'
						},		
						'PH': {
							'name': 'Philippines'
						},				
						'PK': {
							'name': 'Pakistan'
						},
						'RE': {
							'name': 'Reunion'
						},				
						'SC': {
							'name': 'Seychelles'
						},	
						'SG': {
							'name': 'Singapore'
						},		
						'SO': {
							'name': 'Somalia'
						},				
						'SZ': {
							'name': 'Swaziland'
						},				
						'TF': {
							'name': 'French Southern Territories'
						},		
						'TH': {
							'name': 'Thailand'
						},				
						'TJ': {
							'name': 'Tajikistan'
						},		
						'TL': {
							'name': 'Timor-Leste'
						},		
						'TZ': {
							'name': 'Tanzania, United Republic of'
						},				
						'VN': {
							'name': 'Vietnam'
						},				
						'YE': {
							'name': 'Yemen'
						},		
						'YT': {
							'name': 'Mayotte'
						},				
						'ZA': {
							'name': 'South Africa'
						}
					}
				},
				'ap-southeast-2': {
					'server': 'http://lsg.ap-southeast-2.interlude.fm',
					'continent': 'Asia Pacific',
					'countries': {
						'AQ' : {
							'name': 'Antarctica'
						},
						'AS' : {
							'name': 'American Samoa'
						},		
						'AU' : {
							'name': 'Australia'
						},
						'CK' : {
							'name': 'Cook Islands'
						},	
						'FJ' : {
							'name': 'Fiji'
						},
						'HM' : {
							'name': 'Heard Island and McDonald Islands'
						},		
						'KI' : {
							'name': 'Kiribati'
						},
						'NC' : {
							'name': 'New Caledonia'
						},	
						'NF' : {
							'name': 'Norfolk Island'
						},		
						'NR' : {
							'name': 'Nauru'
						},
						'NU' : {
							'name': 'NNiue'
						},	
						'NZ' : {
							'name': 'new Zealand'
						},		
						'PG' : {
							'name': 'Papua New Guinea'
						},
						'SB' : {
							'name': 'Solomon Islands'
						},							
						'TK' : {
							'name': 'Tokelau'
						},
						'TO' : {
							'name': 'Tonga'
						},	
						'TV' : {
							'name': 'Tuvalu'
						},		
						'VU' : {
							'name': 'Vanuatu'
						},
						'WF' : {
							'name': 'Wallis and Futuna'
						},	
						'WS' : {
							'name': 'Samoa'
						}
					}
				},	
				'eu-central-1': {
					'server': 'http://lsg.eu-west-1.interlude.fm',
					'continent': 'EU',
					'countries': {}
				},	
				'eu-west-1': {
					'server': 'http://lsg.eu-west-1.interlude.fm',
					'continent': 'EU',
					'countries': {
						'AD': {
							'name': 'Andorra'
						},	
						'AL': {
							'name': 'Albania'
						},	
						'AM': {
							'name': 'Armenia'
						},	
						'AO': {
							'name': 'Angola'
						},					
						'AT': {
							'name': 'Austria'
						},	
						'AX': {
							'name': 'Aland Islands'
						},	
						'AZ': {
							'name': 'Azerbaijan'
						},	
						'BA': {
							'name': 'Bosnia and Herzegovina'
						},	
						'BE': {
							'name': 'Belgium'
						},	
						'BF': {
							'name': 'Burkina Faso'
						},	
						'BG': {
							'name': 'Bulgaria'
						},	
						'BH': {
							'name': 'Bahrain'
						},	
						'BI': {
							'name': 'Burundi'
						},	
						'BJ': {
							'name': 'Benin'
						},	
						'BW': {
							'name': 'Botswana'
						},	
						'BY': {
							'name': 'Belarus'
						},	
						'CD': {
							'name': 'Congo, The Democratic Republic of the'
						},	
						'CF': {
							'name': 'Central African Republic'
						},	
						'CG': {
							'name': 'Congo'
						},	
						'CH': {
							'name': 'Switzerland'
						},	
						'CI': {
							'name': 'Cote dIvoire'
						},	
						'CM': {
							'name': 'Cameroon'
						},	
						'CV': {
							'name': 'Cape Verde'
						},	
						'CY': {
							'name': 'Cyprus'
						},	
						'CZ': {
							'name': 'Czech Republic'
						},	
						'DE': {
							'name': 'Germany'
						},	
						'DJ': {
							'name': 'Djibouti'
						},	
						'DK': {
							'name': 'Denmark'
						},	
						'DZ': {
							'name': 'Algeria'
						},	
						'EE': {
							'name': 'Estonia'
						},	
						'EG': {
							'name': 'Egypt'
						},	
						'EH': {
							'name': 'Western Sahara'
						},	
						'ER': {
							'name': 'Eritrea'
						},	
						'ES': {
							'name': 'Spain'
						},	
						'ET': {
							'name': 'Ethiopia'
						},	
						'EU': {
							'name': 'Europe'
						},	
						'FI': {
							'name': 'Finland'
						},	
						'FO': {
							'name': 'Faroe Islands'
						},	
						'FR': {
							'name': 'France'
						},	
						'GA': {
							'name': 'Gabon'
						},	
						'GB': {
							'name': 'United Kingdom'
						},	
						'GD': {
							'name': 'Grenada'
						},	
						'GE': {
							'name': 'Georgia'
						},	
						'GG': {
							'name': 'Guernsey'
						},	
						'GH': {
							'name': 'Ghana'
						},					
						'GI': {
							'name': 'Gibraltar'
						},	
						'GL': {
							'name': 'Greenland'
						},	
						'GM': {
							'name': 'Gambia'
						},	
						'GN': {
							'name': 'Guinea'
						},	
						'GQ': {
							'name': 'Equatorial Guinea'
						},
						'GR': {
							'name': 'Greece'
						},	
						'GW': {
							'name': 'Guinea-Bissau'
						},	
						'HR': {
							'name': 'Croatia'
						},	
						'HU': {
							'name': 'Hungary'
						},	
						'IE': {
							'name': 'Ireland'
						},
						'IL': {
							'name': 'Israel'
						},	
						'IM': {
							'name': 'Isle of Man'
						},	
						'IQ': {
							'name': 'Iraq'
						},	
						'IR': {
							'name': 'Iran, Islamic Republic of'
						},	
						'IS': {
							'name': 'Iceland'
						},				
						'IT': {
							'name': 'Italy'
						},	
						'JE': {
							'name': 'Jersey'
						},	
						'JO': {
							'name': 'Jordan'
						},	
						'KE': {
							'name': 'Kenya'
						},	
						'KW': {
							'name': 'Kuwait'
						},	
						'KZ': {
							'name': 'Kazakhstan'
						},	
						'LB': {
							'name': 'Lebanon'
						},	
						'LI': {
							'name': 'Liechtenstein'
						},	
						'LR': {
							'name': 'Liberia'
						},	
						'LT': {
							'name': 'Lithuania'
						},	
						'LU': {
							'name': 'Luxembourg'
						},	
						'LV': {
							'name': 'Latvia'
						},	
						'LY': {
							'name': 'Libyan Arab Jamahiriya'
						},	
						'MA': {
							'name': 'Monaco'
						},	
						'MD': {
							'name': 'Moldova, Republic of'
						},	
						'ME': {
							'name': 'Montenegro'
						},	
						'MK': {
							'name': 'Macedonia'
						},					
						'ML': {
							'name': 'Mali'
						},	
						'MR': {
							'name': 'Mauritania'
						},	
						'MT': {
							'name': 'Malta'
						},	
						'NA': {
							'name': 'Namibia'
						},	
						'NE': {
							'name': 'Niger'
						},	
						'NG': {
							'name': 'Nigeria'
						},	
						'NL': {
							'name': 'Netherlands'
						},	
						'NO': {
							'name': 'Norway'
						},					
						'PL': {
							'name': 'Poland'
						},	
						'PS': {
							'name': 'Palestinian Territory'
						},	
						'PT': {
							'name': 'Portugal'
						},	
						'QA': {
							'name': 'Qatar'
						},	
						'RO': {
							'name': 'Romania'
						},	
						'RS': {
							'name': 'Serbia'
						},					
						'RW': {
							'name': 'Rwanda'
						},	
						'SA': {
							'name': 'Saudi Arabia'
						},	
						'SD': {
							'name': 'Sudan'
						},	
						'SE': {
							'name': 'Sweden'
						},	
						'SI': {
							'name': 'Slovenia'
						},	
						'SJ': {
							'name': 'Svalbard and Jan Mayen'
						},					
						'SK': {
							'name': 'Slovakia'
						},	
						'SL': {
							'name': 'Sierra Leone'
						},	
						'SM': {
							'name': 'San Marino'
						},	
						'SN': {
							'name': 'Senegal'
						},					
						'ST': {
							'name': 'Sao Tome and Principe'
						},	
						'SY': {
							'name': 'Syrian Arab Republic'
						},	
						'TD': {
							'name': 'Chad'
						},	
						'TG': {
							'name': 'Togo'
						},	
						'TM': {
							'name': 'Turkmenistan'
						},					
						'TN': {
							'name': 'Tunisia'
						},	
						'TR': {
							'name': 'Turkey'
						},	
						'UA': {
							'name': 'Ukraine'
						},	
						'UG': {
							'name': 'Uganda'
						},	
						'UZ': {
							'name': 'Uzbekistan'
						},	
						'VA': {
							'name': 'Holy See (Vatican City State)'
						},	
						'ZM': {
							'name': 'Zambia'
						},	
						'ZW': {
							'name': 'Zimbabwe'
						}
					}
				},		
				'sa-east-1': {
					'server': 'http://lsg-us-west.interlude.fm',
					'continent': 'South America',
					'countries': {
						'AR': {
							'name': 'Argentina'
						},
						'BO': {
							'name': 'Bolivia'
						},		
						'BR': {
							'name': 'Brazil'
						},
						'BV': {
							'name': 'Bouvet Island'
						},	
						'CL': {
							'name': 'Chile'
						},
						'CO': {
							'name': 'Colombia'
						},		
						'EC': {
							'name': 'Ecuador'
						},
						'FK': {
							'name': 'Falkland Islands (Malvinas)'
						},				
						'GF': {
							'name': 'French Guiana'
						},
						'GS': {
							'name': 'South Georgia and the South Sandwich Islands'
						},		
						'GY': {
							'name': 'Guyana'
						},
						'PE': {
							'name': 'Peru'
						},	
						'PY': {
							'name': 'Paraguay'
						},
						'SH': {
							'name': 'Saint Helena'
						},		
						'SR': {
							'name': 'Suriname'
						},
						'UY': {
							'name': 'Uruguay'
						},	
						'VE': {
							'name': 'Venezuela'
						}
					}
				},	
				'us-east-1': {
					'server': 'http://lsg-us-west.interlude.fm',
					'continent': 'US East',
					'countries': {	
						'AG': {
							'name': 'Antigua and Barbuda'
						},
						'AI': {
							'name': 'Anguilla'
						},				
						'AN': {
							'name': 'Netherlands Antilles'
						},
						'AW': {
							'name': 'Aruba'
						},	
						'BB': {
							'name': 'Barbados'
						},
						'BM': {
							'name': 'Bermuda'
						},	
						'BS': {
							'name': 'Bahamas'
						},
						'BZ': {
							'name': 'Belize'
						},	
						'CR': {
							'name': 'Costa Rica'
						},
						'CU': {
							'name': 'Cuba'
						},
						'DM': {
							'name': 'Dominica'
						},	
						'DO': {
							'name': 'Dominican Republic'
						},
						'GD': {
							'name': 'Grenada'
						},	
						'GP': {
							'name': 'Guadeloupe'
						},				
						'GT': {
							'name': 'Guatemala'
						},
						'HN': {
							'name': 'Honduras'
						},
						'HT': {
							'name': 'Haiti'
						},	
						'JM': {
							'name': 'Jamaica'
						},
						'KN': {
							'name': 'Saint Kitts and Nevis'
						},	
						'LC': {
							'name': 'Saint Lucia'
						},
						'MQ': {
							'name': 'Martinique'
						},	
						'MS': {
							'name': 'Montserrat'
						},
						'NI': {
							'name': 'Nicaragua'
						},	
						'PA': {
							'name': 'Panama'
						},	
						'PM': {
							'name': 'Saint Pierre and Miquelon'
						},
						'PR': {
							'name': 'Puerto Rico'
						},	
						'SV': {
							'name': 'El Salvador'
						},
						'TC': {
							'name': 'Turks and Caicos Islands'
						},	
						'TT': {
							'name': 'Trinidad and Tobago'
						},	
						'US': {
							'name': 'United States'
						},	
						'VC': {
							'name': 'Saint Vincent and the Grenadines'
						},
						'VG': {
							'name': 'Virgin Islands, British'
						},	
						'VI': {
							'name': 'Virgin Islands, U.S.'
						},	
						'AL': {
							'name': 'Alabama'
						},
						'AR': {
							'name': 'Arkansas'
						},			
						'CT': {
							'name': 'Connecticut'
						},	
						'DC': {
							'name': 'District of Columbia'
						},	
						'DE': {
							'name': 'Delaware'
						},	
						'FL': {
							'name': 'Florida'
						},	
						'GA': {
							'name': 'Georgia'
						},					
						'IA': {
							'name': 'Iowa'
						},			
						'IN': {
							'name': 'Indiana'
						},	
						'KS': {
							'name': 'Kansas'
						},	
						'KY': {
							'name': 'Kentucky'
						},	
						'LA': {
							'name': 'Louisiana'
						},					
						'MA': {
							'name': 'Massachusetts'
						},	
						'MD': {
							'name': 'Maryland'
						},	
						'ME': {
							'name': 'Maine'
						},	
						'MI': {
							'name': 'Michigan'
						},
						'NC': {
							'name': 'North Carolina'
						},	
						'NH': {
							'name': 'New Hampshire'
						},					
						'NJ': {
							'name': 'New Jersey'
						},		
						'NY': {
							'name': 'New York'
						},	
						'OH': {
							'name': 'Ohio'
						},
						'OK': {
							'name': 'Oklahoma'
						},		
						'RI': {
							'name': 'Rhode Island'
						},	
						'SC': {
							'name': 'South Carolina'
						},	
						'TN': {
							'name': 'Tennessee'
						},	
						'TX': {
							'name': 'Texas'
						},
						'VA': {
							'name': 'Virginia'
						},	
						'VT': {
							'name': 'Vermont'
						},	
						'WI': {
							'name': 'Wisconsin'
						},	
						'WV': {
							'name': 'West Virginia'
						},
						'MN': {
							'name': 'Minnesota'
						},	
						'MO': {
							'name': 'Missouri'
						}
					}
				},
				'us-west-1': {
					'server': 'http://lsg-us-west.interlude.fm',
					'continent': 'US West',
					'countries': {
						'MX': {
							'name': 'Mexico'
						},
						'PF': {
							'name': 'French Polynesia'
						},
						'PN': {
							'name': 'Pitcairn'
						},
						'AZ': {
							'name': 'Arizona'
						},
						'CA': {
							'name': 'California'
						},
						'CO': {
							'name': 'Colorado'
						},
						'MT': {
							'name': 'Montana'
						},
						'ND': {
							'name': 'North Dakota'
						},
						'NE': {
							'name': 'Nebraska'
						},
						'NM': {
							'name': 'New Mexico'
						},
						'NV': {
							'name': 'Nevada'
						},								
						'SD': {
							'name': 'South Dakota'
						},
						'UT': {
							'name': 'Utah'
						},
						'WY': {
							'name': 'Wyoming'
						}
					}
				},	
				'us-west-2': {
					'server': 'http://lsg-us-west.interlude.fm',
					'continent': 'US West',
					'countries': {
						'CA': {
							'name': 'Canada'
						},
						'AK': {
							'name': 'Alaska'
						},
						'HI': {
							'name': 'Hawaii'
						},
						'OR': {
							'name': 'Oregon'
						},
						'WA': {
							'name': 'Washington'
						}
					}
				}	
			}
	};
});

/** @license MIT License (c) copyright 2010-2014 original author or authors */
/** @author Brian Cavalier */
/** @author John Hann */

/*global process,document,setTimeout,clearTimeout,MutationObserver,WebKitMutationObserver*/
(function(define) { 'use strict';
define('when/lib/env',['require'],function(require) {
	/*jshint maxcomplexity:6*/

	// Sniff "best" async scheduling option
	// Prefer process.nextTick or MutationObserver, then check for
	// setTimeout, and finally vertx, since its the only env that doesn't
	// have setTimeout

	var MutationObs;
	var capturedSetTimeout = typeof setTimeout !== 'undefined' && setTimeout;

	// Default env
	var setTimer = function(f, ms) { return setTimeout(f, ms); };
	var clearTimer = function(t) { return clearTimeout(t); };
	var asap = function (f) { return capturedSetTimeout(f, 0); };

	// Detect specific env
	if (isNode()) { // Node
		asap = function (f) { return process.nextTick(f); };

	} else if (MutationObs = hasMutationObserver()) { // Modern browser
		asap = initMutationObserver(MutationObs);

	} else if (!capturedSetTimeout) { // vert.x
		var vertxRequire = require;
		var vertx = vertxRequire('vertx');
		setTimer = function (f, ms) { return vertx.setTimer(ms, f); };
		clearTimer = vertx.cancelTimer;
		asap = vertx.runOnLoop || vertx.runOnContext;
	}

	return {
		setTimer: setTimer,
		clearTimer: clearTimer,
		asap: asap
	};

	function isNode () {
		return typeof process !== 'undefined' && process !== null &&
			typeof process.nextTick === 'function';
	}

	function hasMutationObserver () {
		return (typeof MutationObserver === 'function' && MutationObserver) ||
			(typeof WebKitMutationObserver === 'function' && WebKitMutationObserver);
	}

	function initMutationObserver(MutationObserver) {
		var scheduled;
		var node = document.createTextNode('');
		var o = new MutationObserver(run);
		o.observe(node, { characterData: true });

		function run() {
			var f = scheduled;
			scheduled = void 0;
			f();
		}

		var i = 0;
		return function (f) {
			scheduled = f;
			node.data = (i ^= 1);
		};
	}
});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(require); }));

/** @license MIT License (c) copyright 2010-2014 original author or authors */
/** @author Brian Cavalier */
/** @author John Hann */

(function(define) { 'use strict';
define('when/lib/TimeoutError',[],function() {

	/**
	 * Custom error type for promises rejected by promise.timeout
	 * @param {string} message
	 * @constructor
	 */
	function TimeoutError (message) {
		Error.call(this);
		this.message = message;
		this.name = TimeoutError.name;
		if (typeof Error.captureStackTrace === 'function') {
			Error.captureStackTrace(this, TimeoutError);
		}
	}

	TimeoutError.prototype = Object.create(Error.prototype);
	TimeoutError.prototype.constructor = TimeoutError;

	return TimeoutError;
});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));
/** @license MIT License (c) copyright 2010-2014 original author or authors */
/** @author Brian Cavalier */
/** @author John Hann */

(function(define) { 'use strict';
define('when/lib/decorators/timed',['require','../env','../TimeoutError'],function(require) {

	var env = require('../env');
	var TimeoutError = require('../TimeoutError');

	function setTimeout(f, ms, x, y) {
		return env.setTimer(function() {
			f(x, y, ms);
		}, ms);
	}

	return function timed(Promise) {
		/**
		 * Return a new promise whose fulfillment value is revealed only
		 * after ms milliseconds
		 * @param {number} ms milliseconds
		 * @returns {Promise}
		 */
		Promise.prototype.delay = function(ms) {
			var p = this._beget();
			this._handler.fold(handleDelay, ms, void 0, p._handler);
			return p;
		};

		function handleDelay(ms, x, h) {
			setTimeout(resolveDelay, ms, x, h);
		}

		function resolveDelay(x, h) {
			h.resolve(x);
		}

		/**
		 * Return a new promise that rejects after ms milliseconds unless
		 * this promise fulfills earlier, in which case the returned promise
		 * fulfills with the same value.
		 * @param {number} ms milliseconds
		 * @param {Error|*=} reason optional rejection reason to use, defaults
		 *   to a TimeoutError if not provided
		 * @returns {Promise}
		 */
		Promise.prototype.timeout = function(ms, reason) {
			var p = this._beget();
			var h = p._handler;

			var t = setTimeout(onTimeout, ms, reason, p._handler);

			this._handler.visit(h,
				function onFulfill(x) {
					env.clearTimer(t);
					this.resolve(x); // this = h
				},
				function onReject(x) {
					env.clearTimer(t);
					this.reject(x); // this = h
				},
				h.notify);

			return p;
		};

		function onTimeout(reason, h, ms) {
			var e = typeof reason === 'undefined'
				? new TimeoutError('timed out after ' + ms + 'ms')
				: reason;
			h.reject(e);
		}

		return Promise;
	};

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(require); }));

/** @license MIT License (c) copyright 2010-2014 original author or authors */
/** @author Brian Cavalier */
/** @author John Hann */

(function(define) { 'use strict';
define('when/lib/state',[],function() {

	return {
		pending: toPendingState,
		fulfilled: toFulfilledState,
		rejected: toRejectedState,
		inspect: inspect
	};

	function toPendingState() {
		return { state: 'pending' };
	}

	function toRejectedState(e) {
		return { state: 'rejected', reason: e };
	}

	function toFulfilledState(x) {
		return { state: 'fulfilled', value: x };
	}

	function inspect(handler) {
		var state = handler.state();
		return state === 0 ? toPendingState()
			 : state > 0   ? toFulfilledState(handler.value)
			               : toRejectedState(handler.value);
	}

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

/** @license MIT License (c) copyright 2010-2014 original author or authors */
/** @author Brian Cavalier */
/** @author John Hann */

(function(define) { 'use strict';
define('when/lib/apply',[],function() {

	makeApply.tryCatchResolve = tryCatchResolve;

	return makeApply;

	function makeApply(Promise, call) {
		if(arguments.length < 2) {
			call = tryCatchResolve;
		}

		return apply;

		function apply(f, thisArg, args) {
			var p = Promise._defer();
			var l = args.length;
			var params = new Array(l);
			callAndResolve({ f:f, thisArg:thisArg, args:args, params:params, i:l-1, call:call }, p._handler);

			return p;
		}

		function callAndResolve(c, h) {
			if(c.i < 0) {
				return call(c.f, c.thisArg, c.params, h);
			}

			var handler = Promise._handler(c.args[c.i]);
			handler.fold(callAndResolveNext, c, void 0, h);
		}

		function callAndResolveNext(c, x, h) {
			c.params[c.i] = x;
			c.i -= 1;
			callAndResolve(c, h);
		}
	}

	function tryCatchResolve(f, thisArg, args, resolver) {
		try {
			resolver.resolve(f.apply(thisArg, args));
		} catch(e) {
			resolver.reject(e);
		}
	}

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));



/** @license MIT License (c) copyright 2010-2014 original author or authors */
/** @author Brian Cavalier */
/** @author John Hann */

(function(define) { 'use strict';
define('when/lib/decorators/array',['require','../state','../apply'],function(require) {

	var state = require('../state');
	var applier = require('../apply');

	return function array(Promise) {

		var applyFold = applier(Promise);
		var toPromise = Promise.resolve;
		var all = Promise.all;

		var ar = Array.prototype.reduce;
		var arr = Array.prototype.reduceRight;
		var slice = Array.prototype.slice;

		// Additional array combinators

		Promise.any = any;
		Promise.some = some;
		Promise.settle = settle;

		Promise.map = map;
		Promise.filter = filter;
		Promise.reduce = reduce;
		Promise.reduceRight = reduceRight;

		/**
		 * When this promise fulfills with an array, do
		 * onFulfilled.apply(void 0, array)
		 * @param {function} onFulfilled function to apply
		 * @returns {Promise} promise for the result of applying onFulfilled
		 */
		Promise.prototype.spread = function(onFulfilled) {
			return this.then(all).then(function(array) {
				return onFulfilled.apply(this, array);
			});
		};

		return Promise;

		/**
		 * One-winner competitive race.
		 * Return a promise that will fulfill when one of the promises
		 * in the input array fulfills, or will reject when all promises
		 * have rejected.
		 * @param {array} promises
		 * @returns {Promise} promise for the first fulfilled value
		 */
		function any(promises) {
			var p = Promise._defer();
			var resolver = p._handler;
			var l = promises.length>>>0;

			var pending = l;
			var errors = [];

			for (var h, x, i = 0; i < l; ++i) {
				x = promises[i];
				if(x === void 0 && !(i in promises)) {
					--pending;
					continue;
				}

				h = Promise._handler(x);
				if(h.state() > 0) {
					resolver.become(h);
					Promise._visitRemaining(promises, i, h);
					break;
				} else {
					h.visit(resolver, handleFulfill, handleReject);
				}
			}

			if(pending === 0) {
				resolver.reject(new RangeError('any(): array must not be empty'));
			}

			return p;

			function handleFulfill(x) {
				/*jshint validthis:true*/
				errors = null;
				this.resolve(x); // this === resolver
			}

			function handleReject(e) {
				/*jshint validthis:true*/
				if(this.resolved) { // this === resolver
					return;
				}

				errors.push(e);
				if(--pending === 0) {
					this.reject(errors);
				}
			}
		}

		/**
		 * N-winner competitive race
		 * Return a promise that will fulfill when n input promises have
		 * fulfilled, or will reject when it becomes impossible for n
		 * input promises to fulfill (ie when promises.length - n + 1
		 * have rejected)
		 * @param {array} promises
		 * @param {number} n
		 * @returns {Promise} promise for the earliest n fulfillment values
		 *
		 * @deprecated
		 */
		function some(promises, n) {
			/*jshint maxcomplexity:7*/
			var p = Promise._defer();
			var resolver = p._handler;

			var results = [];
			var errors = [];

			var l = promises.length>>>0;
			var nFulfill = 0;
			var nReject;
			var x, i; // reused in both for() loops

			// First pass: count actual array items
			for(i=0; i<l; ++i) {
				x = promises[i];
				if(x === void 0 && !(i in promises)) {
					continue;
				}
				++nFulfill;
			}

			// Compute actual goals
			n = Math.max(n, 0);
			nReject = (nFulfill - n + 1);
			nFulfill = Math.min(n, nFulfill);

			if(n > nFulfill) {
				resolver.reject(new RangeError('some(): array must contain at least '
				+ n + ' item(s), but had ' + nFulfill));
			} else if(nFulfill === 0) {
				resolver.resolve(results);
			}

			// Second pass: observe each array item, make progress toward goals
			for(i=0; i<l; ++i) {
				x = promises[i];
				if(x === void 0 && !(i in promises)) {
					continue;
				}

				Promise._handler(x).visit(resolver, fulfill, reject, resolver.notify);
			}

			return p;

			function fulfill(x) {
				/*jshint validthis:true*/
				if(this.resolved) { // this === resolver
					return;
				}

				results.push(x);
				if(--nFulfill === 0) {
					errors = null;
					this.resolve(results);
				}
			}

			function reject(e) {
				/*jshint validthis:true*/
				if(this.resolved) { // this === resolver
					return;
				}

				errors.push(e);
				if(--nReject === 0) {
					results = null;
					this.reject(errors);
				}
			}
		}

		/**
		 * Apply f to the value of each promise in a list of promises
		 * and return a new list containing the results.
		 * @param {array} promises
		 * @param {function(x:*, index:Number):*} f mapping function
		 * @returns {Promise}
		 */
		function map(promises, f) {
			return Promise._traverse(f, promises);
		}

		/**
		 * Filter the provided array of promises using the provided predicate.  Input may
		 * contain promises and values
		 * @param {Array} promises array of promises and values
		 * @param {function(x:*, index:Number):boolean} predicate filtering predicate.
		 *  Must return truthy (or promise for truthy) for items to retain.
		 * @returns {Promise} promise that will fulfill with an array containing all items
		 *  for which predicate returned truthy.
		 */
		function filter(promises, predicate) {
			var a = slice.call(promises);
			return Promise._traverse(predicate, a).then(function(keep) {
				return filterSync(a, keep);
			});
		}

		function filterSync(promises, keep) {
			// Safe because we know all promises have fulfilled if we've made it this far
			var l = keep.length;
			var filtered = new Array(l);
			for(var i=0, j=0; i<l; ++i) {
				if(keep[i]) {
					filtered[j++] = Promise._handler(promises[i]).value;
				}
			}
			filtered.length = j;
			return filtered;

		}

		/**
		 * Return a promise that will always fulfill with an array containing
		 * the outcome states of all input promises.  The returned promise
		 * will never reject.
		 * @param {Array} promises
		 * @returns {Promise} promise for array of settled state descriptors
		 */
		function settle(promises) {
			return all(promises.map(settleOne));
		}

		function settleOne(p) {
			var h = Promise._handler(p);
			if(h.state() === 0) {
				return toPromise(p).then(state.fulfilled, state.rejected);
			}

			h._unreport();
			return state.inspect(h);
		}

		/**
		 * Traditional reduce function, similar to `Array.prototype.reduce()`, but
		 * input may contain promises and/or values, and reduceFunc
		 * may return either a value or a promise, *and* initialValue may
		 * be a promise for the starting value.
		 * @param {Array|Promise} promises array or promise for an array of anything,
		 *      may contain a mix of promises and values.
		 * @param {function(accumulated:*, x:*, index:Number):*} f reduce function
		 * @returns {Promise} that will resolve to the final reduced value
		 */
		function reduce(promises, f /*, initialValue */) {
			return arguments.length > 2 ? ar.call(promises, liftCombine(f), arguments[2])
					: ar.call(promises, liftCombine(f));
		}

		/**
		 * Traditional reduce function, similar to `Array.prototype.reduceRight()`, but
		 * input may contain promises and/or values, and reduceFunc
		 * may return either a value or a promise, *and* initialValue may
		 * be a promise for the starting value.
		 * @param {Array|Promise} promises array or promise for an array of anything,
		 *      may contain a mix of promises and values.
		 * @param {function(accumulated:*, x:*, index:Number):*} f reduce function
		 * @returns {Promise} that will resolve to the final reduced value
		 */
		function reduceRight(promises, f /*, initialValue */) {
			return arguments.length > 2 ? arr.call(promises, liftCombine(f), arguments[2])
					: arr.call(promises, liftCombine(f));
		}

		function liftCombine(f) {
			return function(z, x, i) {
				return applyFold(f, void 0, [z,x,i]);
			};
		}
	};

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(require); }));

/** @license MIT License (c) copyright 2010-2014 original author or authors */
/** @author Brian Cavalier */
/** @author John Hann */

(function(define) { 'use strict';
define('when/lib/decorators/flow',[],function() {

	return function flow(Promise) {

		var resolve = Promise.resolve;
		var reject = Promise.reject;
		var origCatch = Promise.prototype['catch'];

		/**
		 * Handle the ultimate fulfillment value or rejection reason, and assume
		 * responsibility for all errors.  If an error propagates out of result
		 * or handleFatalError, it will be rethrown to the host, resulting in a
		 * loud stack track on most platforms and a crash on some.
		 * @param {function?} onResult
		 * @param {function?} onError
		 * @returns {undefined}
		 */
		Promise.prototype.done = function(onResult, onError) {
			this._handler.visit(this._handler.receiver, onResult, onError);
		};

		/**
		 * Add Error-type and predicate matching to catch.  Examples:
		 * promise.catch(TypeError, handleTypeError)
		 *   .catch(predicate, handleMatchedErrors)
		 *   .catch(handleRemainingErrors)
		 * @param onRejected
		 * @returns {*}
		 */
		Promise.prototype['catch'] = Promise.prototype.otherwise = function(onRejected) {
			if (arguments.length < 2) {
				return origCatch.call(this, onRejected);
			}

			if(typeof onRejected !== 'function') {
				return this.ensure(rejectInvalidPredicate);
			}

			return origCatch.call(this, createCatchFilter(arguments[1], onRejected));
		};

		/**
		 * Wraps the provided catch handler, so that it will only be called
		 * if the predicate evaluates truthy
		 * @param {?function} handler
		 * @param {function} predicate
		 * @returns {function} conditional catch handler
		 */
		function createCatchFilter(handler, predicate) {
			return function(e) {
				return evaluatePredicate(e, predicate)
					? handler.call(this, e)
					: reject(e);
			};
		}

		/**
		 * Ensures that onFulfilledOrRejected will be called regardless of whether
		 * this promise is fulfilled or rejected.  onFulfilledOrRejected WILL NOT
		 * receive the promises' value or reason.  Any returned value will be disregarded.
		 * onFulfilledOrRejected may throw or return a rejected promise to signal
		 * an additional error.
		 * @param {function} handler handler to be called regardless of
		 *  fulfillment or rejection
		 * @returns {Promise}
		 */
		Promise.prototype['finally'] = Promise.prototype.ensure = function(handler) {
			if(typeof handler !== 'function') {
				return this;
			}

			return this.then(function(x) {
				return runSideEffect(handler, this, identity, x);
			}, function(e) {
				return runSideEffect(handler, this, reject, e);
			});
		};

		function runSideEffect (handler, thisArg, propagate, value) {
			var result = handler.call(thisArg);
			return maybeThenable(result)
				? propagateValue(result, propagate, value)
				: propagate(value);
		}

		function propagateValue (result, propagate, x) {
			return resolve(result).then(function () {
				return propagate(x);
			});
		}

		/**
		 * Recover from a failure by returning a defaultValue.  If defaultValue
		 * is a promise, it's fulfillment value will be used.  If defaultValue is
		 * a promise that rejects, the returned promise will reject with the
		 * same reason.
		 * @param {*} defaultValue
		 * @returns {Promise} new promise
		 */
		Promise.prototype['else'] = Promise.prototype.orElse = function(defaultValue) {
			return this.then(void 0, function() {
				return defaultValue;
			});
		};

		/**
		 * Shortcut for .then(function() { return value; })
		 * @param  {*} value
		 * @return {Promise} a promise that:
		 *  - is fulfilled if value is not a promise, or
		 *  - if value is a promise, will fulfill with its value, or reject
		 *    with its reason.
		 */
		Promise.prototype['yield'] = function(value) {
			return this.then(function() {
				return value;
			});
		};

		/**
		 * Runs a side effect when this promise fulfills, without changing the
		 * fulfillment value.
		 * @param {function} onFulfilledSideEffect
		 * @returns {Promise}
		 */
		Promise.prototype.tap = function(onFulfilledSideEffect) {
			return this.then(onFulfilledSideEffect)['yield'](this);
		};

		return Promise;
	};

	function rejectInvalidPredicate() {
		throw new TypeError('catch predicate must be a function');
	}

	function evaluatePredicate(e, predicate) {
		return isError(predicate) ? e instanceof predicate : predicate(e);
	}

	function isError(predicate) {
		return predicate === Error
			|| (predicate != null && predicate.prototype instanceof Error);
	}

	function maybeThenable(x) {
		return (typeof x === 'object' || typeof x === 'function') && x !== null;
	}

	function identity(x) {
		return x;
	}

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

/** @license MIT License (c) copyright 2010-2014 original author or authors */
/** @author Brian Cavalier */
/** @author John Hann */
/** @author Jeff Escalante */

(function(define) { 'use strict';
define('when/lib/decorators/fold',[],function() {

	return function fold(Promise) {

		Promise.prototype.fold = function(f, z) {
			var promise = this._beget();

			this._handler.fold(function(z, x, to) {
				Promise._handler(z).fold(function(x, z, to) {
					to.resolve(f.call(this, z, x));
				}, x, this, to);
			}, z, promise._handler.receiver, promise._handler);

			return promise;
		};

		return Promise;
	};

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

/** @license MIT License (c) copyright 2010-2014 original author or authors */
/** @author Brian Cavalier */
/** @author John Hann */

(function(define) { 'use strict';
define('when/lib/decorators/inspect',['require','../state'],function(require) {

	var inspect = require('../state').inspect;

	return function inspection(Promise) {

		Promise.prototype.inspect = function() {
			return inspect(Promise._handler(this));
		};

		return Promise;
	};

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(require); }));

/** @license MIT License (c) copyright 2010-2014 original author or authors */
/** @author Brian Cavalier */
/** @author John Hann */

(function(define) { 'use strict';
define('when/lib/decorators/iterate',[],function() {

	return function generate(Promise) {

		var resolve = Promise.resolve;

		Promise.iterate = iterate;
		Promise.unfold = unfold;

		return Promise;

		/**
		 * @deprecated Use github.com/cujojs/most streams and most.iterate
		 * Generate a (potentially infinite) stream of promised values:
		 * x, f(x), f(f(x)), etc. until condition(x) returns true
		 * @param {function} f function to generate a new x from the previous x
		 * @param {function} condition function that, given the current x, returns
		 *  truthy when the iterate should stop
		 * @param {function} handler function to handle the value produced by f
		 * @param {*|Promise} x starting value, may be a promise
		 * @return {Promise} the result of the last call to f before
		 *  condition returns true
		 */
		function iterate(f, condition, handler, x) {
			return unfold(function(x) {
				return [x, f(x)];
			}, condition, handler, x);
		}

		/**
		 * @deprecated Use github.com/cujojs/most streams and most.unfold
		 * Generate a (potentially infinite) stream of promised values
		 * by applying handler(generator(seed)) iteratively until
		 * condition(seed) returns true.
		 * @param {function} unspool function that generates a [value, newSeed]
		 *  given a seed.
		 * @param {function} condition function that, given the current seed, returns
		 *  truthy when the unfold should stop
		 * @param {function} handler function to handle the value produced by unspool
		 * @param x {*|Promise} starting value, may be a promise
		 * @return {Promise} the result of the last value produced by unspool before
		 *  condition returns true
		 */
		function unfold(unspool, condition, handler, x) {
			return resolve(x).then(function(seed) {
				return resolve(condition(seed)).then(function(done) {
					return done ? seed : resolve(unspool(seed)).spread(next);
				});
			});

			function next(item, newSeed) {
				return resolve(handler(item)).then(function() {
					return unfold(unspool, condition, handler, newSeed);
				});
			}
		}
	};

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

/** @license MIT License (c) copyright 2010-2014 original author or authors */
/** @author Brian Cavalier */
/** @author John Hann */

(function(define) { 'use strict';
define('when/lib/decorators/progress',[],function() {

	return function progress(Promise) {

		/**
		 * @deprecated
		 * Register a progress handler for this promise
		 * @param {function} onProgress
		 * @returns {Promise}
		 */
		Promise.prototype.progress = function(onProgress) {
			return this.then(void 0, void 0, onProgress);
		};

		return Promise;
	};

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

/** @license MIT License (c) copyright 2010-2014 original author or authors */
/** @author Brian Cavalier */
/** @author John Hann */

(function(define) { 'use strict';
define('when/lib/decorators/with',[],function() {

	return function addWith(Promise) {
		/**
		 * Returns a promise whose handlers will be called with `this` set to
		 * the supplied receiver.  Subsequent promises derived from the
		 * returned promise will also have their handlers called with receiver
		 * as `this`. Calling `with` with undefined or no arguments will return
		 * a promise whose handlers will again be called in the usual Promises/A+
		 * way (no `this`) thus safely undoing any previous `with` in the
		 * promise chain.
		 *
		 * WARNING: Promises returned from `with`/`withThis` are NOT Promises/A+
		 * compliant, specifically violating 2.2.5 (http://promisesaplus.com/#point-41)
		 *
		 * @param {object} receiver `this` value for all handlers attached to
		 *  the returned promise.
		 * @returns {Promise}
		 */
		Promise.prototype['with'] = Promise.prototype.withThis = function(receiver) {
			var p = this._beget();
			var child = p._handler;
			child.receiver = receiver;
			this._handler.chain(child, receiver);
			return p;
		};

		return Promise;
	};

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));


/** @license MIT License (c) copyright 2010-2014 original author or authors */
/** @author Brian Cavalier */
/** @author John Hann */

(function(define) { 'use strict';
define('when/lib/format',[],function() {

	return {
		formatError: formatError,
		formatObject: formatObject,
		tryStringify: tryStringify
	};

	/**
	 * Format an error into a string.  If e is an Error and has a stack property,
	 * it's returned.  Otherwise, e is formatted using formatObject, with a
	 * warning added about e not being a proper Error.
	 * @param {*} e
	 * @returns {String} formatted string, suitable for output to developers
	 */
	function formatError(e) {
		var s = typeof e === 'object' && e !== null && e.stack ? e.stack : formatObject(e);
		return e instanceof Error ? s : s + ' (WARNING: non-Error used)';
	}

	/**
	 * Format an object, detecting "plain" objects and running them through
	 * JSON.stringify if possible.
	 * @param {Object} o
	 * @returns {string}
	 */
	function formatObject(o) {
		var s = String(o);
		if(s === '[object Object]' && typeof JSON !== 'undefined') {
			s = tryStringify(o, s);
		}
		return s;
	}

	/**
	 * Try to return the result of JSON.stringify(x).  If that fails, return
	 * defaultValue
	 * @param {*} x
	 * @param {*} defaultValue
	 * @returns {String|*} JSON.stringify(x) or defaultValue
	 */
	function tryStringify(x, defaultValue) {
		try {
			return JSON.stringify(x);
		} catch(e) {
			return defaultValue;
		}
	}

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

/** @license MIT License (c) copyright 2010-2014 original author or authors */
/** @author Brian Cavalier */
/** @author John Hann */

(function(define) { 'use strict';
define('when/lib/decorators/unhandledRejection',['require','../env','../format'],function(require) {

	var setTimer = require('../env').setTimer;
	var format = require('../format');

	return function unhandledRejection(Promise) {

		var logError = noop;
		var logInfo = noop;
		var localConsole;

		if(typeof console !== 'undefined') {
			// Alias console to prevent things like uglify's drop_console option from
			// removing console.log/error. Unhandled rejections fall into the same
			// category as uncaught exceptions, and build tools shouldn't silence them.
			localConsole = console;
			logError = typeof localConsole.error !== 'undefined'
				? function (e) { localConsole.error(e); }
				: function (e) { localConsole.log(e); };

			logInfo = typeof localConsole.info !== 'undefined'
				? function (e) { localConsole.info(e); }
				: function (e) { localConsole.log(e); };
		}

		Promise.onPotentiallyUnhandledRejection = function(rejection) {
			enqueue(report, rejection);
		};

		Promise.onPotentiallyUnhandledRejectionHandled = function(rejection) {
			enqueue(unreport, rejection);
		};

		Promise.onFatalRejection = function(rejection) {
			enqueue(throwit, rejection.value);
		};

		var tasks = [];
		var reported = [];
		var running = null;

		function report(r) {
			if(!r.handled) {
				reported.push(r);
				logError('Potentially unhandled rejection [' + r.id + '] ' + format.formatError(r.value));
			}
		}

		function unreport(r) {
			var i = reported.indexOf(r);
			if(i >= 0) {
				reported.splice(i, 1);
				logInfo('Handled previous rejection [' + r.id + '] ' + format.formatObject(r.value));
			}
		}

		function enqueue(f, x) {
			tasks.push(f, x);
			if(running === null) {
				running = setTimer(flush, 0);
			}
		}

		function flush() {
			running = null;
			while(tasks.length > 0) {
				tasks.shift()(tasks.shift());
			}
		}

		return Promise;
	};

	function throwit(e) {
		throw e;
	}

	function noop() {}

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(require); }));

/** @license MIT License (c) copyright 2010-2014 original author or authors */
/** @author Brian Cavalier */
/** @author John Hann */

(function(define) { 'use strict';
define('when/lib/makePromise',[],function() {

	return function makePromise(environment) {

		var tasks = environment.scheduler;
		var emitRejection = initEmitRejection();

		var objectCreate = Object.create ||
			function(proto) {
				function Child() {}
				Child.prototype = proto;
				return new Child();
			};

		/**
		 * Create a promise whose fate is determined by resolver
		 * @constructor
		 * @returns {Promise} promise
		 * @name Promise
		 */
		function Promise(resolver, handler) {
			this._handler = resolver === Handler ? handler : init(resolver);
		}

		/**
		 * Run the supplied resolver
		 * @param resolver
		 * @returns {Pending}
		 */
		function init(resolver) {
			var handler = new Pending();

			try {
				resolver(promiseResolve, promiseReject, promiseNotify);
			} catch (e) {
				promiseReject(e);
			}

			return handler;

			/**
			 * Transition from pre-resolution state to post-resolution state, notifying
			 * all listeners of the ultimate fulfillment or rejection
			 * @param {*} x resolution value
			 */
			function promiseResolve (x) {
				handler.resolve(x);
			}
			/**
			 * Reject this promise with reason, which will be used verbatim
			 * @param {Error|*} reason rejection reason, strongly suggested
			 *   to be an Error type
			 */
			function promiseReject (reason) {
				handler.reject(reason);
			}

			/**
			 * @deprecated
			 * Issue a progress event, notifying all progress listeners
			 * @param {*} x progress event payload to pass to all listeners
			 */
			function promiseNotify (x) {
				handler.notify(x);
			}
		}

		// Creation

		Promise.resolve = resolve;
		Promise.reject = reject;
		Promise.never = never;

		Promise._defer = defer;
		Promise._handler = getHandler;

		/**
		 * Returns a trusted promise. If x is already a trusted promise, it is
		 * returned, otherwise returns a new trusted Promise which follows x.
		 * @param  {*} x
		 * @return {Promise} promise
		 */
		function resolve(x) {
			return isPromise(x) ? x
				: new Promise(Handler, new Async(getHandler(x)));
		}

		/**
		 * Return a reject promise with x as its reason (x is used verbatim)
		 * @param {*} x
		 * @returns {Promise} rejected promise
		 */
		function reject(x) {
			return new Promise(Handler, new Async(new Rejected(x)));
		}

		/**
		 * Return a promise that remains pending forever
		 * @returns {Promise} forever-pending promise.
		 */
		function never() {
			return foreverPendingPromise; // Should be frozen
		}

		/**
		 * Creates an internal {promise, resolver} pair
		 * @private
		 * @returns {Promise}
		 */
		function defer() {
			return new Promise(Handler, new Pending());
		}

		// Transformation and flow control

		/**
		 * Transform this promise's fulfillment value, returning a new Promise
		 * for the transformed result.  If the promise cannot be fulfilled, onRejected
		 * is called with the reason.  onProgress *may* be called with updates toward
		 * this promise's fulfillment.
		 * @param {function=} onFulfilled fulfillment handler
		 * @param {function=} onRejected rejection handler
		 * @param {function=} onProgress @deprecated progress handler
		 * @return {Promise} new promise
		 */
		Promise.prototype.then = function(onFulfilled, onRejected, onProgress) {
			var parent = this._handler;
			var state = parent.join().state();

			if ((typeof onFulfilled !== 'function' && state > 0) ||
				(typeof onRejected !== 'function' && state < 0)) {
				// Short circuit: value will not change, simply share handler
				return new this.constructor(Handler, parent);
			}

			var p = this._beget();
			var child = p._handler;

			parent.chain(child, parent.receiver, onFulfilled, onRejected, onProgress);

			return p;
		};

		/**
		 * If this promise cannot be fulfilled due to an error, call onRejected to
		 * handle the error. Shortcut for .then(undefined, onRejected)
		 * @param {function?} onRejected
		 * @return {Promise}
		 */
		Promise.prototype['catch'] = function(onRejected) {
			return this.then(void 0, onRejected);
		};

		/**
		 * Creates a new, pending promise of the same type as this promise
		 * @private
		 * @returns {Promise}
		 */
		Promise.prototype._beget = function() {
			return begetFrom(this._handler, this.constructor);
		};

		function begetFrom(parent, Promise) {
			var child = new Pending(parent.receiver, parent.join().context);
			return new Promise(Handler, child);
		}

		// Array combinators

		Promise.all = all;
		Promise.race = race;
		Promise._traverse = traverse;

		/**
		 * Return a promise that will fulfill when all promises in the
		 * input array have fulfilled, or will reject when one of the
		 * promises rejects.
		 * @param {array} promises array of promises
		 * @returns {Promise} promise for array of fulfillment values
		 */
		function all(promises) {
			return traverseWith(snd, null, promises);
		}

		/**
		 * Array<Promise<X>> -> Promise<Array<f(X)>>
		 * @private
		 * @param {function} f function to apply to each promise's value
		 * @param {Array} promises array of promises
		 * @returns {Promise} promise for transformed values
		 */
		function traverse(f, promises) {
			return traverseWith(tryCatch2, f, promises);
		}

		function traverseWith(tryMap, f, promises) {
			var handler = typeof f === 'function' ? mapAt : settleAt;

			var resolver = new Pending();
			var pending = promises.length >>> 0;
			var results = new Array(pending);

			for (var i = 0, x; i < promises.length && !resolver.resolved; ++i) {
				x = promises[i];

				if (x === void 0 && !(i in promises)) {
					--pending;
					continue;
				}

				traverseAt(promises, handler, i, x, resolver);
			}

			if(pending === 0) {
				resolver.become(new Fulfilled(results));
			}

			return new Promise(Handler, resolver);

			function mapAt(i, x, resolver) {
				if(!resolver.resolved) {
					traverseAt(promises, settleAt, i, tryMap(f, x, i), resolver);
				}
			}

			function settleAt(i, x, resolver) {
				results[i] = x;
				if(--pending === 0) {
					resolver.become(new Fulfilled(results));
				}
			}
		}

		function traverseAt(promises, handler, i, x, resolver) {
			if (maybeThenable(x)) {
				var h = getHandlerMaybeThenable(x);
				var s = h.state();

				if (s === 0) {
					h.fold(handler, i, void 0, resolver);
				} else if (s > 0) {
					handler(i, h.value, resolver);
				} else {
					resolver.become(h);
					visitRemaining(promises, i+1, h);
				}
			} else {
				handler(i, x, resolver);
			}
		}

		Promise._visitRemaining = visitRemaining;
		function visitRemaining(promises, start, handler) {
			for(var i=start; i<promises.length; ++i) {
				markAsHandled(getHandler(promises[i]), handler);
			}
		}

		function markAsHandled(h, handler) {
			if(h === handler) {
				return;
			}

			var s = h.state();
			if(s === 0) {
				h.visit(h, void 0, h._unreport);
			} else if(s < 0) {
				h._unreport();
			}
		}

		/**
		 * Fulfill-reject competitive race. Return a promise that will settle
		 * to the same state as the earliest input promise to settle.
		 *
		 * WARNING: The ES6 Promise spec requires that race()ing an empty array
		 * must return a promise that is pending forever.  This implementation
		 * returns a singleton forever-pending promise, the same singleton that is
		 * returned by Promise.never(), thus can be checked with ===
		 *
		 * @param {array} promises array of promises to race
		 * @returns {Promise} if input is non-empty, a promise that will settle
		 * to the same outcome as the earliest input promise to settle. if empty
		 * is empty, returns a promise that will never settle.
		 */
		function race(promises) {
			if(typeof promises !== 'object' || promises === null) {
				return reject(new TypeError('non-iterable passed to race()'));
			}

			// Sigh, race([]) is untestable unless we return *something*
			// that is recognizable without calling .then() on it.
			return promises.length === 0 ? never()
				 : promises.length === 1 ? resolve(promises[0])
				 : runRace(promises);
		}

		function runRace(promises) {
			var resolver = new Pending();
			var i, x, h;
			for(i=0; i<promises.length; ++i) {
				x = promises[i];
				if (x === void 0 && !(i in promises)) {
					continue;
				}

				h = getHandler(x);
				if(h.state() !== 0) {
					resolver.become(h);
					visitRemaining(promises, i+1, h);
					break;
				} else {
					h.visit(resolver, resolver.resolve, resolver.reject);
				}
			}
			return new Promise(Handler, resolver);
		}

		// Promise internals
		// Below this, everything is @private

		/**
		 * Get an appropriate handler for x, without checking for cycles
		 * @param {*} x
		 * @returns {object} handler
		 */
		function getHandler(x) {
			if(isPromise(x)) {
				return x._handler.join();
			}
			return maybeThenable(x) ? getHandlerUntrusted(x) : new Fulfilled(x);
		}

		/**
		 * Get a handler for thenable x.
		 * NOTE: You must only call this if maybeThenable(x) == true
		 * @param {object|function|Promise} x
		 * @returns {object} handler
		 */
		function getHandlerMaybeThenable(x) {
			return isPromise(x) ? x._handler.join() : getHandlerUntrusted(x);
		}

		/**
		 * Get a handler for potentially untrusted thenable x
		 * @param {*} x
		 * @returns {object} handler
		 */
		function getHandlerUntrusted(x) {
			try {
				var untrustedThen = x.then;
				return typeof untrustedThen === 'function'
					? new Thenable(untrustedThen, x)
					: new Fulfilled(x);
			} catch(e) {
				return new Rejected(e);
			}
		}

		/**
		 * Handler for a promise that is pending forever
		 * @constructor
		 */
		function Handler() {}

		Handler.prototype.when
			= Handler.prototype.become
			= Handler.prototype.notify // deprecated
			= Handler.prototype.fail
			= Handler.prototype._unreport
			= Handler.prototype._report
			= noop;

		Handler.prototype._state = 0;

		Handler.prototype.state = function() {
			return this._state;
		};

		/**
		 * Recursively collapse handler chain to find the handler
		 * nearest to the fully resolved value.
		 * @returns {object} handler nearest the fully resolved value
		 */
		Handler.prototype.join = function() {
			var h = this;
			while(h.handler !== void 0) {
				h = h.handler;
			}
			return h;
		};

		Handler.prototype.chain = function(to, receiver, fulfilled, rejected, progress) {
			this.when({
				resolver: to,
				receiver: receiver,
				fulfilled: fulfilled,
				rejected: rejected,
				progress: progress
			});
		};

		Handler.prototype.visit = function(receiver, fulfilled, rejected, progress) {
			this.chain(failIfRejected, receiver, fulfilled, rejected, progress);
		};

		Handler.prototype.fold = function(f, z, c, to) {
			this.when(new Fold(f, z, c, to));
		};

		/**
		 * Handler that invokes fail() on any handler it becomes
		 * @constructor
		 */
		function FailIfRejected() {}

		inherit(Handler, FailIfRejected);

		FailIfRejected.prototype.become = function(h) {
			h.fail();
		};

		var failIfRejected = new FailIfRejected();

		/**
		 * Handler that manages a queue of consumers waiting on a pending promise
		 * @constructor
		 */
		function Pending(receiver, inheritedContext) {
			Promise.createContext(this, inheritedContext);

			this.consumers = void 0;
			this.receiver = receiver;
			this.handler = void 0;
			this.resolved = false;
		}

		inherit(Handler, Pending);

		Pending.prototype._state = 0;

		Pending.prototype.resolve = function(x) {
			this.become(getHandler(x));
		};

		Pending.prototype.reject = function(x) {
			if(this.resolved) {
				return;
			}

			this.become(new Rejected(x));
		};

		Pending.prototype.join = function() {
			if (!this.resolved) {
				return this;
			}

			var h = this;

			while (h.handler !== void 0) {
				h = h.handler;
				if (h === this) {
					return this.handler = cycle();
				}
			}

			return h;
		};

		Pending.prototype.run = function() {
			var q = this.consumers;
			var handler = this.handler;
			this.handler = this.handler.join();
			this.consumers = void 0;

			for (var i = 0; i < q.length; ++i) {
				handler.when(q[i]);
			}
		};

		Pending.prototype.become = function(handler) {
			if(this.resolved) {
				return;
			}

			this.resolved = true;
			this.handler = handler;
			if(this.consumers !== void 0) {
				tasks.enqueue(this);
			}

			if(this.context !== void 0) {
				handler._report(this.context);
			}
		};

		Pending.prototype.when = function(continuation) {
			if(this.resolved) {
				tasks.enqueue(new ContinuationTask(continuation, this.handler));
			} else {
				if(this.consumers === void 0) {
					this.consumers = [continuation];
				} else {
					this.consumers.push(continuation);
				}
			}
		};

		/**
		 * @deprecated
		 */
		Pending.prototype.notify = function(x) {
			if(!this.resolved) {
				tasks.enqueue(new ProgressTask(x, this));
			}
		};

		Pending.prototype.fail = function(context) {
			var c = typeof context === 'undefined' ? this.context : context;
			this.resolved && this.handler.join().fail(c);
		};

		Pending.prototype._report = function(context) {
			this.resolved && this.handler.join()._report(context);
		};

		Pending.prototype._unreport = function() {
			this.resolved && this.handler.join()._unreport();
		};

		/**
		 * Wrap another handler and force it into a future stack
		 * @param {object} handler
		 * @constructor
		 */
		function Async(handler) {
			this.handler = handler;
		}

		inherit(Handler, Async);

		Async.prototype.when = function(continuation) {
			tasks.enqueue(new ContinuationTask(continuation, this));
		};

		Async.prototype._report = function(context) {
			this.join()._report(context);
		};

		Async.prototype._unreport = function() {
			this.join()._unreport();
		};

		/**
		 * Handler that wraps an untrusted thenable and assimilates it in a future stack
		 * @param {function} then
		 * @param {{then: function}} thenable
		 * @constructor
		 */
		function Thenable(then, thenable) {
			Pending.call(this);
			tasks.enqueue(new AssimilateTask(then, thenable, this));
		}

		inherit(Pending, Thenable);

		/**
		 * Handler for a fulfilled promise
		 * @param {*} x fulfillment value
		 * @constructor
		 */
		function Fulfilled(x) {
			Promise.createContext(this);
			this.value = x;
		}

		inherit(Handler, Fulfilled);

		Fulfilled.prototype._state = 1;

		Fulfilled.prototype.fold = function(f, z, c, to) {
			runContinuation3(f, z, this, c, to);
		};

		Fulfilled.prototype.when = function(cont) {
			runContinuation1(cont.fulfilled, this, cont.receiver, cont.resolver);
		};

		var errorId = 0;

		/**
		 * Handler for a rejected promise
		 * @param {*} x rejection reason
		 * @constructor
		 */
		function Rejected(x) {
			Promise.createContext(this);

			this.id = ++errorId;
			this.value = x;
			this.handled = false;
			this.reported = false;

			this._report();
		}

		inherit(Handler, Rejected);

		Rejected.prototype._state = -1;

		Rejected.prototype.fold = function(f, z, c, to) {
			to.become(this);
		};

		Rejected.prototype.when = function(cont) {
			if(typeof cont.rejected === 'function') {
				this._unreport();
			}
			runContinuation1(cont.rejected, this, cont.receiver, cont.resolver);
		};

		Rejected.prototype._report = function(context) {
			tasks.afterQueue(new ReportTask(this, context));
		};

		Rejected.prototype._unreport = function() {
			if(this.handled) {
				return;
			}
			this.handled = true;
			tasks.afterQueue(new UnreportTask(this));
		};

		Rejected.prototype.fail = function(context) {
			this.reported = true;
			emitRejection('unhandledRejection', this);
			Promise.onFatalRejection(this, context === void 0 ? this.context : context);
		};

		function ReportTask(rejection, context) {
			this.rejection = rejection;
			this.context = context;
		}

		ReportTask.prototype.run = function() {
			if(!this.rejection.handled && !this.rejection.reported) {
				this.rejection.reported = true;
				emitRejection('unhandledRejection', this.rejection) ||
					Promise.onPotentiallyUnhandledRejection(this.rejection, this.context);
			}
		};

		function UnreportTask(rejection) {
			this.rejection = rejection;
		}

		UnreportTask.prototype.run = function() {
			if(this.rejection.reported) {
				emitRejection('rejectionHandled', this.rejection) ||
					Promise.onPotentiallyUnhandledRejectionHandled(this.rejection);
			}
		};

		// Unhandled rejection hooks
		// By default, everything is a noop

		Promise.createContext
			= Promise.enterContext
			= Promise.exitContext
			= Promise.onPotentiallyUnhandledRejection
			= Promise.onPotentiallyUnhandledRejectionHandled
			= Promise.onFatalRejection
			= noop;

		// Errors and singletons

		var foreverPendingHandler = new Handler();
		var foreverPendingPromise = new Promise(Handler, foreverPendingHandler);

		function cycle() {
			return new Rejected(new TypeError('Promise cycle'));
		}

		// Task runners

		/**
		 * Run a single consumer
		 * @constructor
		 */
		function ContinuationTask(continuation, handler) {
			this.continuation = continuation;
			this.handler = handler;
		}

		ContinuationTask.prototype.run = function() {
			this.handler.join().when(this.continuation);
		};

		/**
		 * Run a queue of progress handlers
		 * @constructor
		 */
		function ProgressTask(value, handler) {
			this.handler = handler;
			this.value = value;
		}

		ProgressTask.prototype.run = function() {
			var q = this.handler.consumers;
			if(q === void 0) {
				return;
			}

			for (var c, i = 0; i < q.length; ++i) {
				c = q[i];
				runNotify(c.progress, this.value, this.handler, c.receiver, c.resolver);
			}
		};

		/**
		 * Assimilate a thenable, sending it's value to resolver
		 * @param {function} then
		 * @param {object|function} thenable
		 * @param {object} resolver
		 * @constructor
		 */
		function AssimilateTask(then, thenable, resolver) {
			this._then = then;
			this.thenable = thenable;
			this.resolver = resolver;
		}

		AssimilateTask.prototype.run = function() {
			var h = this.resolver;
			tryAssimilate(this._then, this.thenable, _resolve, _reject, _notify);

			function _resolve(x) { h.resolve(x); }
			function _reject(x)  { h.reject(x); }
			function _notify(x)  { h.notify(x); }
		};

		function tryAssimilate(then, thenable, resolve, reject, notify) {
			try {
				then.call(thenable, resolve, reject, notify);
			} catch (e) {
				reject(e);
			}
		}

		/**
		 * Fold a handler value with z
		 * @constructor
		 */
		function Fold(f, z, c, to) {
			this.f = f; this.z = z; this.c = c; this.to = to;
			this.resolver = failIfRejected;
			this.receiver = this;
		}

		Fold.prototype.fulfilled = function(x) {
			this.f.call(this.c, this.z, x, this.to);
		};

		Fold.prototype.rejected = function(x) {
			this.to.reject(x);
		};

		Fold.prototype.progress = function(x) {
			this.to.notify(x);
		};

		// Other helpers

		/**
		 * @param {*} x
		 * @returns {boolean} true iff x is a trusted Promise
		 */
		function isPromise(x) {
			return x instanceof Promise;
		}

		/**
		 * Test just enough to rule out primitives, in order to take faster
		 * paths in some code
		 * @param {*} x
		 * @returns {boolean} false iff x is guaranteed *not* to be a thenable
		 */
		function maybeThenable(x) {
			return (typeof x === 'object' || typeof x === 'function') && x !== null;
		}

		function runContinuation1(f, h, receiver, next) {
			if(typeof f !== 'function') {
				return next.become(h);
			}

			Promise.enterContext(h);
			tryCatchReject(f, h.value, receiver, next);
			Promise.exitContext();
		}

		function runContinuation3(f, x, h, receiver, next) {
			if(typeof f !== 'function') {
				return next.become(h);
			}

			Promise.enterContext(h);
			tryCatchReject3(f, x, h.value, receiver, next);
			Promise.exitContext();
		}

		/**
		 * @deprecated
		 */
		function runNotify(f, x, h, receiver, next) {
			if(typeof f !== 'function') {
				return next.notify(x);
			}

			Promise.enterContext(h);
			tryCatchReturn(f, x, receiver, next);
			Promise.exitContext();
		}

		function tryCatch2(f, a, b) {
			try {
				return f(a, b);
			} catch(e) {
				return reject(e);
			}
		}

		/**
		 * Return f.call(thisArg, x), or if it throws return a rejected promise for
		 * the thrown exception
		 */
		function tryCatchReject(f, x, thisArg, next) {
			try {
				next.become(getHandler(f.call(thisArg, x)));
			} catch(e) {
				next.become(new Rejected(e));
			}
		}

		/**
		 * Same as above, but includes the extra argument parameter.
		 */
		function tryCatchReject3(f, x, y, thisArg, next) {
			try {
				f.call(thisArg, x, y, next);
			} catch(e) {
				next.become(new Rejected(e));
			}
		}

		/**
		 * @deprecated
		 * Return f.call(thisArg, x), or if it throws, *return* the exception
		 */
		function tryCatchReturn(f, x, thisArg, next) {
			try {
				next.notify(f.call(thisArg, x));
			} catch(e) {
				next.notify(e);
			}
		}

		function inherit(Parent, Child) {
			Child.prototype = objectCreate(Parent.prototype);
			Child.prototype.constructor = Child;
		}

		function snd(x, y) {
			return y;
		}

		function noop() {}

		function initEmitRejection() {
			/*global process, self, CustomEvent*/
			if(typeof process !== 'undefined' && process !== null
				&& typeof process.emit === 'function') {
				// Returning falsy here means to call the default
				// onPotentiallyUnhandledRejection API.  This is safe even in
				// browserify since process.emit always returns falsy in browserify:
				// https://github.com/defunctzombie/node-process/blob/master/browser.js#L40-L46
				return function(type, rejection) {
					return type === 'unhandledRejection'
						? process.emit(type, rejection.value, rejection)
						: process.emit(type, rejection);
				};
			} else if(typeof self !== 'undefined' && typeof CustomEvent === 'function') {
				return (function(noop, self, CustomEvent) {
					var hasCustomEvent = false;
					try {
						var ev = new CustomEvent('unhandledRejection');
						hasCustomEvent = ev instanceof CustomEvent;
					} catch (e) {}

					return !hasCustomEvent ? noop : function(type, rejection) {
						var ev = new CustomEvent(type, {
							detail: {
								reason: rejection.value,
								key: rejection
							},
							bubbles: false,
							cancelable: true
						});

						return !self.dispatchEvent(ev);
					};
				}(noop, self, CustomEvent));
			}

			return noop;
		}

		return Promise;
	};
});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

/** @license MIT License (c) copyright 2010-2014 original author or authors */
/** @author Brian Cavalier */
/** @author John Hann */

(function(define) { 'use strict';
define('when/lib/Scheduler',[],function() {

	// Credit to Twisol (https://github.com/Twisol) for suggesting
	// this type of extensible queue + trampoline approach for next-tick conflation.

	/**
	 * Async task scheduler
	 * @param {function} async function to schedule a single async function
	 * @constructor
	 */
	function Scheduler(async) {
		this._async = async;
		this._running = false;

		this._queue = this;
		this._queueLen = 0;
		this._afterQueue = {};
		this._afterQueueLen = 0;

		var self = this;
		this.drain = function() {
			self._drain();
		};
	}

	/**
	 * Enqueue a task
	 * @param {{ run:function }} task
	 */
	Scheduler.prototype.enqueue = function(task) {
		this._queue[this._queueLen++] = task;
		this.run();
	};

	/**
	 * Enqueue a task to run after the main task queue
	 * @param {{ run:function }} task
	 */
	Scheduler.prototype.afterQueue = function(task) {
		this._afterQueue[this._afterQueueLen++] = task;
		this.run();
	};

	Scheduler.prototype.run = function() {
		if (!this._running) {
			this._running = true;
			this._async(this.drain);
		}
	};

	/**
	 * Drain the handler queue entirely, and then the after queue
	 */
	Scheduler.prototype._drain = function() {
		var i = 0;
		for (; i < this._queueLen; ++i) {
			this._queue[i].run();
			this._queue[i] = void 0;
		}

		this._queueLen = 0;
		this._running = false;

		for (i = 0; i < this._afterQueueLen; ++i) {
			this._afterQueue[i].run();
			this._afterQueue[i] = void 0;
		}

		this._afterQueueLen = 0;
	};

	return Scheduler;

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

/** @license MIT License (c) copyright 2010-2014 original author or authors */
/** @author Brian Cavalier */
/** @author John Hann */

(function(define) { 'use strict';
define('when/lib/Promise',['require','./makePromise','./Scheduler','./env'],function (require) {

	var makePromise = require('./makePromise');
	var Scheduler = require('./Scheduler');
	var async = require('./env').asap;

	return makePromise({
		scheduler: new Scheduler(async)
	});

});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });

/** @license MIT License (c) copyright 2010-2014 original author or authors */

/**
 * Promises/A+ and when() implementation
 * when is part of the cujoJS family of libraries (http://cujojs.com/)
 * @author Brian Cavalier
 * @author John Hann
 * @version 3.7.2
 */
(function(define) { 'use strict';
define('when/when',['require','./lib/decorators/timed','./lib/decorators/array','./lib/decorators/flow','./lib/decorators/fold','./lib/decorators/inspect','./lib/decorators/iterate','./lib/decorators/progress','./lib/decorators/with','./lib/decorators/unhandledRejection','./lib/TimeoutError','./lib/Promise','./lib/apply'],function (require) {

	var timed = require('./lib/decorators/timed');
	var array = require('./lib/decorators/array');
	var flow = require('./lib/decorators/flow');
	var fold = require('./lib/decorators/fold');
	var inspect = require('./lib/decorators/inspect');
	var generate = require('./lib/decorators/iterate');
	var progress = require('./lib/decorators/progress');
	var withThis = require('./lib/decorators/with');
	var unhandledRejection = require('./lib/decorators/unhandledRejection');
	var TimeoutError = require('./lib/TimeoutError');

	var Promise = [array, flow, fold, generate, progress,
		inspect, withThis, timed, unhandledRejection]
		.reduce(function(Promise, feature) {
			return feature(Promise);
		}, require('./lib/Promise'));

	var apply = require('./lib/apply')(Promise);

	// Public API

	when.promise     = promise;              // Create a pending promise
	when.resolve     = Promise.resolve;      // Create a resolved promise
	when.reject      = Promise.reject;       // Create a rejected promise

	when.lift        = lift;                 // lift a function to return promises
	when['try']      = attempt;              // call a function and return a promise
	when.attempt     = attempt;              // alias for when.try

	when.iterate     = Promise.iterate;      // DEPRECATED (use cujojs/most streams) Generate a stream of promises
	when.unfold      = Promise.unfold;       // DEPRECATED (use cujojs/most streams) Generate a stream of promises

	when.join        = join;                 // Join 2 or more promises

	when.all         = all;                  // Resolve a list of promises
	when.settle      = settle;               // Settle a list of promises

	when.any         = lift(Promise.any);    // One-winner race
	when.some        = lift(Promise.some);   // Multi-winner race
	when.race        = lift(Promise.race);   // First-to-settle race

	when.map         = map;                  // Array.map() for promises
	when.filter      = filter;               // Array.filter() for promises
	when.reduce      = lift(Promise.reduce);       // Array.reduce() for promises
	when.reduceRight = lift(Promise.reduceRight);  // Array.reduceRight() for promises

	when.isPromiseLike = isPromiseLike;      // Is something promise-like, aka thenable

	when.Promise     = Promise;              // Promise constructor
	when.defer       = defer;                // Create a {promise, resolve, reject} tuple

	// Error types

	when.TimeoutError = TimeoutError;

	/**
	 * Get a trusted promise for x, or by transforming x with onFulfilled
	 *
	 * @param {*} x
	 * @param {function?} onFulfilled callback to be called when x is
	 *   successfully fulfilled.  If promiseOrValue is an immediate value, callback
	 *   will be invoked immediately.
	 * @param {function?} onRejected callback to be called when x is
	 *   rejected.
	 * @param {function?} onProgress callback to be called when progress updates
	 *   are issued for x. @deprecated
	 * @returns {Promise} a new promise that will fulfill with the return
	 *   value of callback or errback or the completion value of promiseOrValue if
	 *   callback and/or errback is not supplied.
	 */
	function when(x, onFulfilled, onRejected, onProgress) {
		var p = Promise.resolve(x);
		if (arguments.length < 2) {
			return p;
		}

		return p.then(onFulfilled, onRejected, onProgress);
	}

	/**
	 * Creates a new promise whose fate is determined by resolver.
	 * @param {function} resolver function(resolve, reject, notify)
	 * @returns {Promise} promise whose fate is determine by resolver
	 */
	function promise(resolver) {
		return new Promise(resolver);
	}

	/**
	 * Lift the supplied function, creating a version of f that returns
	 * promises, and accepts promises as arguments.
	 * @param {function} f
	 * @returns {Function} version of f that returns promises
	 */
	function lift(f) {
		return function() {
			for(var i=0, l=arguments.length, a=new Array(l); i<l; ++i) {
				a[i] = arguments[i];
			}
			return apply(f, this, a);
		};
	}

	/**
	 * Call f in a future turn, with the supplied args, and return a promise
	 * for the result.
	 * @param {function} f
	 * @returns {Promise}
	 */
	function attempt(f /*, args... */) {
		/*jshint validthis:true */
		for(var i=0, l=arguments.length-1, a=new Array(l); i<l; ++i) {
			a[i] = arguments[i+1];
		}
		return apply(f, this, a);
	}

	/**
	 * Creates a {promise, resolver} pair, either or both of which
	 * may be given out safely to consumers.
	 * @return {{promise: Promise, resolve: function, reject: function, notify: function}}
	 */
	function defer() {
		return new Deferred();
	}

	function Deferred() {
		var p = Promise._defer();

		function resolve(x) { p._handler.resolve(x); }
		function reject(x) { p._handler.reject(x); }
		function notify(x) { p._handler.notify(x); }

		this.promise = p;
		this.resolve = resolve;
		this.reject = reject;
		this.notify = notify;
		this.resolver = { resolve: resolve, reject: reject, notify: notify };
	}

	/**
	 * Determines if x is promise-like, i.e. a thenable object
	 * NOTE: Will return true for *any thenable object*, and isn't truly
	 * safe, since it may attempt to access the `then` property of x (i.e.
	 *  clever/malicious getters may do weird things)
	 * @param {*} x anything
	 * @returns {boolean} true if x is promise-like
	 */
	function isPromiseLike(x) {
		return x && typeof x.then === 'function';
	}

	/**
	 * Return a promise that will resolve only once all the supplied arguments
	 * have resolved. The resolution value of the returned promise will be an array
	 * containing the resolution values of each of the arguments.
	 * @param {...*} arguments may be a mix of promises and values
	 * @returns {Promise}
	 */
	function join(/* ...promises */) {
		return Promise.all(arguments);
	}

	/**
	 * Return a promise that will fulfill once all input promises have
	 * fulfilled, or reject when any one input promise rejects.
	 * @param {array|Promise} promises array (or promise for an array) of promises
	 * @returns {Promise}
	 */
	function all(promises) {
		return when(promises, Promise.all);
	}

	/**
	 * Return a promise that will always fulfill with an array containing
	 * the outcome states of all input promises.  The returned promise
	 * will only reject if `promises` itself is a rejected promise.
	 * @param {array|Promise} promises array (or promise for an array) of promises
	 * @returns {Promise} promise for array of settled state descriptors
	 */
	function settle(promises) {
		return when(promises, Promise.settle);
	}

	/**
	 * Promise-aware array map function, similar to `Array.prototype.map()`,
	 * but input array may contain promises or values.
	 * @param {Array|Promise} promises array of anything, may contain promises and values
	 * @param {function(x:*, index:Number):*} mapFunc map function which may
	 *  return a promise or value
	 * @returns {Promise} promise that will fulfill with an array of mapped values
	 *  or reject if any input promise rejects.
	 */
	function map(promises, mapFunc) {
		return when(promises, function(promises) {
			return Promise.map(promises, mapFunc);
		});
	}

	/**
	 * Filter the provided array of promises using the provided predicate.  Input may
	 * contain promises and values
	 * @param {Array|Promise} promises array of promises and values
	 * @param {function(x:*, index:Number):boolean} predicate filtering predicate.
	 *  Must return truthy (or promise for truthy) for items to retain.
	 * @returns {Promise} promise that will fulfill with an array containing all items
	 *  for which predicate returned truthy.
	 */
	function filter(promises, predicate) {
		return when(promises, function(promises) {
			return Promise.filter(promises, predicate);
		});
	}

	return when;
});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });

define('when', ['when/when'], function (main) { return main; });

define(
'engine/hls/HlsPlaylistSender',[
    'modules/Base',
    'engine/hls/awsRegionsMap',
    'utils/utils',
    'when'
],

function(BaseModuleClass, awsRegionsMap, utils, when) {
    'use strict';

    var HlsPlaylistSender = BaseModuleClass.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'HlsPlaylistSender',
        logLevel: 'warn',
        defaults: {
            server: 'https://lsg-us-west.interlude.fm',
            embeddedSDKServer: 'http://127.0.0.1:3141',
            geoip: 'https://www.telize.com/geoip',
            dynamicServer: true // If true, the server will be choose according to the client continent, otherwise it will be the default server
        },

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ MEMBERS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        regionsMap: null,
        serverDef: null,
        server: null,
        sessionId: null,
        embeddedSDK: false,
        embeddedSDKServer: null,

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ METHODS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        initPlaylist: function(playlist) {
            var d = when.defer();
            // we init only after serverDef was resolved - which mean we choosed server.
            this.serverDef.done(function() {

                // EmbeddedSDK
                if (this.embeddedSDK) {
                    window.InterludePlayer.playlist = playlist;
                    var url = this.server + '/playlist';
                    d.resolve(url);
                    return d.promise;
                }

                ///TODO: dont like those nasted callbacks!!
                utils.post(this.server + '/init')
                    .then(function(sessionId) {
                        this.sessionId = sessionId;
                        this.sendPlaylist(playlist)
                            .done(function() {
                                var url = this.server + '/playlist?sessionId=' + this.sessionId;
                                d.resolve(url);
                            }.bind(this), function() {
                                this.log.error('Hls failed update playlist in server');
                                this.notifyError(57);
                                d.reject();
                            }.bind(this));
                    }.bind(this))
                    .fail(function(err) {
                        this.log.error('Hls failed init playlist in server');
                        this.notifyError(56, err);
                        d.reject();
                    }.bind(this));
            }.bind(this));

            return d.promise;
        },

        sendPlaylist: function(playlist) {
            var d = when.defer();

            // EmbeddedSDK: just set a global variable
            if (this.embeddedSDK) {
                window.InterludePlayer.playlist = playlist;
                d.resolve();
                return d.promise;
            }

            // check that we have a session ID
            if (!this.sessionId) {
                this.log.error('Hls cannot update playlist, initPlaylist should be called first');
                this.notifyError(58);
                d.reject();
                return d.promise;
            }

            // send the playlist
            utils.post(this.server + '/update?sessionId=' + this.sessionId, playlist)
                .then(function() {
                    this.log.debug('success update playlist in server');
                    d.resolve();
                }.bind(this)).fail(function() {
                    d.reject();
                }.bind(this));

            return d.promise;
        },

        disposePlaylist: function() {
            if (this.sessionId) {
                utils.post(this.server + '/dispose?sessionId=' + this.sessionId);
            }
        },

        chooseServer: function() {
            var d = when.defer();

            // if this project is served within an embedded SDK:
            if (this.embeddedSDK) {
                this.server = this.getSetting('embeddedSDKServer');
                var programPlaylistRoute = 'http://127.0.0.1:3141/add_route?route=%2Fplaylist&javascript=window.InterludePlayer.playlist%3B';
                utils.get(programPlaylistRoute)
                     .then(function() { d.resolve();})
                     .fail(function() { d.resolve();});
                return d.promise;
            }

            // if this project uses dynamic servers:
            if (!this.getSetting('dynamicServer')) {
                this.server = this.getSetting('server');
                d.resolve();
                return d.promise;
            }

            /* jshint ignore:start */
            // jscs:disable
            // if we have country_code in the query string we use it instead the info from geoip
            var countryCode = unescape(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + escape('country_code').replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
            if (countryCode) {
                var server = this.countryToServer(countryCode);
                this.server = server ? server : this.getSetting('server');
                this.log.debug('choosen server is ' + this.server);
                d.resolve();
            }
            else {
                utils.post(this.getSetting('geoip'))
                    .then(function(geoInfo) {
                        if (geoInfo && geoInfo.country_code) {
                            var server = this.countryToServer(geoInfo.country_code);
                            this.server = server ? server : this.getSetting('server');
                            this.log.debug('choosen server is ' + this.server);
                            d.resolve();
                        } else {
                            this.server = this.getSetting('server');
                            d.resolve();
                        }
                    }.bind(this))
                    .fail(function() {
                        this.server = this.getSetting('server');
                        d.resolve();
                    }.bind(this)); 
            }

            // jscs:enable
            /* jshint ignore:end */

            return d.promise;
        },

        countryToServer: function(countryCode) {
            var region;
            var regions = this.regionsMap.regions;
            for (var regionKey in regions) {
                region = regions[regionKey];
                if (region.countries.hasOwnProperty(countryCode)) {
                    return region.server;
                }
            }
            return null;
        },

        //////////////////////////////////////////////////////////////////////////
        // ---------------------------- CONSTRUCTOR ------------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function() {

            // parse the query string
            var oGetVars = {};
            if (location.search.length > 1) {
                for (var aItKey, nKeyId = 0, aCouples = location.search.substr(1).split('&'); nKeyId < aCouples.length; nKeyId++) {
                    aItKey = aCouples[nKeyId].split('=');
                    oGetVars[decodeURIComponent(aItKey[0])] = aItKey.length > 1 ? decodeURIComponent(aItKey[1]) : '';
                }
            }

            // check if we are inside and Embedded SDK
            if (oGetVars.player_env && oGetVars.player_env === 'embedded_sdk') { // jshint ignore:line 
               this.embeddedSDK = true;
            }

            // choose a server
            this.regionsMap = awsRegionsMap;
            this.serverDef = this.chooseServer();
        },

        dispose: function() {
            this.disposePlaylist();
        }
    });

    return HlsPlaylistSender;
});

define(
'engine/hls/stream/HlsStream',[
    'modules/Base',
    'engine/hls/HlsPlaylistBuilder',
    'engine/hls/HlsPlaylistSender',
    'utils/utils',
    'when'
],

function(BaseModuleClass, HlsPlaylistBuilder, HlsPlaylistSender, utils, when) {
    'use strict';

    var HlsStream = BaseModuleClass.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'HlsStream',
        logLevel: 'warn',
        defaults: {
            startBuffer: 3,
            bufferLimit: 2.5,
            endDelta: 0.250
        },

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ MEMBERS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        htmlVideo: null,
        hlsVideo: null,
        events: null,
        streamBytesGenerators: null,
        totalTime: NaN,
        streamList: null,
        playlistBuilder: null,
        playlistSender: null,
        playingWindow: null,
        bufferLimit: NaN,
        currentIndex: { // indexes for getting more bytes
            main: NaN, // the index in streamBytesGenerator
            sub: NaN // the index in the sepecific bytesGenerator
        },
        url: null,
        inInitPlaylist: false, // indicate if we in the middle of init playlist for the first time.

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ METHODS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        initStreamData: function() {
            this.streamList = [];
            this.streamBytesGenerators = [];
            this.totalTime = 0;
            this.currentIndex.main = 0;
            this.currentIndex.sub = 0;
            this.initPlayingWindow();
        },

        push: function(bytesGen) {
            var d = when.defer();

            var execPush = function() {
                var duration = bytesGen.totalDuration;
                var offset = this.totalTime;
                this.totalTime += duration;
                bytesGen.offset = offset;
                this.streamBytesGenerators.push(bytesGen);
                this.hlsVideo.endStream = false;
                if (this.streamBytesGenerators.length == 1) {
                    this.hlsVideo.initSessionInfo(this.url);
                    this.firstUpdate()
                        .done(function() {
                            if (this.hlsVideo.firstPlayShouldBePlaying) {
                                this.hlsVideo.play();
                            }
                            d.resolve();
                        }.bind(this), function() {
                            d.reject();
                        });
                }
                else {
                    d.resolve();
                }
            }.bind(this);

            if (!this.url) {
                if (!this.inInitPlaylist) {
                    this.inInitPlaylist = true;
                    var playlist = this.playlistBuilder.buildInitialPlaylist();
                    this.playlistSender.initPlaylist(playlist)
                        .done(function(url) {
                            this.url = url;
                            this.inInitPlaylist = false;
                            execPush();
                        }.bind(this), function() {
                            d.reject();
                        }.bind(this));
                }
                else {
                    // the delay is needed to make sure we run execPush for the first node before second node (which can be happened if initPlaylist didn't return yet)
                    var pushInDelay = function() {
                        if (this.inInitPlaylist) {
                            setTimeout(pushInDelay.bind(this), 1000);
                        }
                        else {
                            execPush();
                        }
                    }.bind(this);
                    setTimeout(pushInDelay.bind(this), 1000);
                }
            }
            else {
                execPush();
            }

            return d.promise;
        },

        switchChannel: function(channelIndex) {
            var currIndex = this.currentIndex.main;
            var bytesGen = this.streamBytesGenerators[currIndex];
            if (bytesGen && bytesGen.setChannel) {
                var timeInNode = this.playingWindow.endTime - bytesGen.offset;
                bytesGen.setChannel(channelIndex);
                return timeInNode;
            }
            else {
                this.log.error('Hls cannot switch channel, current node is not parallel or we already in critical time');
                this.notifyError(61);
                return null;
            }
        },

        flush: function() {
            var currIndex = this.currentIndex.main;
            // if we are not playing the last node right now we should remove suffix
            if (!this.isLastBytesGen(currIndex)) {
                this.removeSuffixFrom(currIndex + 1);
                this.streamBytesGenerators = this.streamBytesGenerators.slice(0, currIndex + 1);
            }
            // update totalTime according to current endTime
            this.totalTime = this.playingWindow.endTime;

            // update indexes
            this.currentIndex.sub = this.streamBytesGenerators[currIndex].length;
            this.hlsVideo.endStream = true;
            return this.totalTime; // return the time that we already gave to playlist
        },

        initPlayingWindow: function() {
            var playingWindow = {
                startIndex: 0,
                endIndex: 0,
                startTime: 0,
                endTime: 0
            };
            this.playingWindow = playingWindow;
        },

        firstUpdate: function() {
            var d = when.defer();
            this.updateStream(0, this.getSetting('startBuffer'));
            var playlist = this.playlistBuilder.buildPlaylist(this.streamList, this.playingWindow.startIndex, this.playingWindow.endIndex);
            if (playlist === null) {
                this.log.error('Hls error building playlist');
                this.notifyError(55);
                d.reject();
            } else {
                this.playlistSender.sendPlaylist(playlist)
                    .done(function() {
                        d.resolve();
                    }.bind(this), function() {
                        d.reject();
                    }.bind(this));
            }

            return d.promise;
        },

        onTimeUpdate: function() {
            var currTime = this.htmlVideo.currentTime();
            if (this.isTimeEnded(currTime)) {
                this.hlsVideo.endStream = true;
            }

            var desirableEndTime = currTime + this.bufferLimit;
            if (desirableEndTime > this.playingWindow.endTime) {
                this.updateStream(currTime, desirableEndTime);
                this.log.debug('startIndex: ' + this.playingWindow.startIndex + ' endIndex: ' + this.playingWindow.endIndex + ' startTime: ' + this.playingWindow.startTime + ' endTime ' + this.playingWindow.endTime);
                var playlist = this.playlistBuilder.buildPlaylist(this.streamList, this.playingWindow.startIndex, this.playingWindow.endIndex);
                if (playlist === null) {
                    this.log.error('Hls error building playlist');
                    this.notifyError(55);
                } else {
                    this.playlistSender.sendPlaylist(playlist);
                }
            }
            else {
                this.log.debug('no need to add bytes');
            }
        },

        updateStream: function(currTime, desirableEndTime) {
            this.addBytesToStream(desirableEndTime);
            this.removeBytesFromStream(currTime);
        },

        // add bytes to stream from the streamBytesGenerators.
        // update playing window and indexs accordingly
        addBytesToStream: function(desirableEndTime) {
            var nextEndTime = this.playingWindow.endTime;
            var nextEndIndex = this.playingWindow.endIndex;
            var mainIndex = this.currentIndex.main;
            var subIndex = this.currentIndex.sub;
            var currentBytesGen = this.streamBytesGenerators[mainIndex];
            var bytes;
            while (nextEndTime < desirableEndTime) {
                bytes = currentBytesGen.getBytes(subIndex);
                if (bytes !== null) {
                    nextEndTime = bytes.endTime;
                    this.streamList.push(bytes);
                    nextEndIndex++;
                    subIndex++;
                }
                else { // no more bytes from current bytesGen
                    if (!this.isLastBytesGen(mainIndex)) {
                        mainIndex++;
                        currentBytesGen = this.streamBytesGenerators[mainIndex];
                        subIndex = 0;
                    }
                    else { // no more bytes at all
                        this.log.debug('no more bytes to add');
                        break;
                    }
                }
            }

            this.playingWindow.endTime = nextEndTime;
            this.playingWindow.endIndex = nextEndIndex;
            this.currentIndex.main = mainIndex;
            this.currentIndex.sub = subIndex;
        },

        removeBytesFromStream: function(currTime) {
            var endIndex = this.playingWindow.endIndex;
            var nextStartTime = this.playingWindow.startTime;
            var nextStartIndex = this.playingWindow.startIndex;

            while (nextStartTime < currTime && nextStartIndex < endIndex) {
                nextStartTime = this.streamList[nextStartIndex].endTime;
                nextStartIndex++;
            }
            this.playingWindow.startIndex = nextStartIndex;
            this.playingWindow.startTime = nextStartTime;
        },

        isLastBytesGen: function(index) {
            return (index + 1) === this.streamBytesGenerators.length;
        },

        isTimeEnded: function(currTime) {
            return Math.abs(this.totalTime - currTime) <= this.getSetting('endDelta');
        },

        removeSuffixFrom: function(index) {
            for (var i = index ; i < this.streamBytesGenerators.length ; i++) {
                delete this.streamBytesGenerators[i];
            }
        },

        //////////////////////////////////////////////////////////////////////////
        // ---------------------------- CONSTRUCTOR ------------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function(core, hlsVideo) {
            this.events = core.events;
            this.hlsVideo = hlsVideo;
            this.htmlVideo = hlsVideo.htmlVideo;
            this.bufferLimit = this.getSetting('bufferLimit');
            this.initStreamData();
            this.playlistBuilder = new HlsPlaylistBuilder(this.options);
            this.playlistSender = new HlsPlaylistSender(this.options);
            this.htmlVideo.on(this.events.timeupdate, this.onTimeUpdate);
        },

        dispose: function() {
            this.htmlVideo.off(this.events.timeupdate, this.onTimeUpdate);

            /* jshint ignore:start */
            this.playlistSender && this.playlistSender.dispose() && delete this.playlistSender;
            this.playlistBuilder && this.playlistBuilder.dispose() && delete this.playlistBuilder;
            /* jshint ignore:end */

            for (var i = 0 ; i < this.streamBytesGenerators.length ; i++) {
                delete this.streamBytesGenerators[i];
            }
        }
    });

    return HlsStream;
});

define(
'engine/hls/stream/IOS8HlsStream',[
    'engine/hls/stream/HlsStream'
],

function(HlsStream) {
    'use strict';

    var IOS8HlsStream = HlsStream.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'IOS8HlsStream',
        logLevel: 'warn',
        defaults: {
            startBuffer: 3,
            bufferLimit: 2.5,
            endDelta: 0.250,
            cutPlaylistbufferLimit: 1.5 // limit buffer time in seconds for cutting the playlist
        },

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ MEMBERS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ METHODS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        //-- override HlsStream functionailty --//
        updateStream: function(currTime, desirableEndTime) {
            this.addBytesToStream(desirableEndTime);
            var currBuffer = this.calcCurrentBuffer(currTime);

            // we want to cut the prefix of the playlist only if the current buffer time larger than cutPlaylistbufferLimit property
            this.log.debug('currBuffer: ' + currBuffer);
            if (currBuffer > this.getSetting('cutPlaylistbufferLimit')) {
                this.removeBytesFromStream(currTime);
            }
        },

        calcCurrentBuffer: function(currTime) {
            var buffered = this.htmlVideo.buffered();
            var bufferedEnd = buffered.length > 0 ? buffered.end(0) : 0;
            var currBuffer = bufferedEnd === 0 ? 0 : (bufferedEnd - currTime).toFixed(3);
            return currBuffer;
        },

        //////////////////////////////////////////////////////////////////////////
        // ---------------------------- CONSTRUCTOR ------------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function(core, hlsVideo) {
            this._super(core, hlsVideo);
        },

        dispose: function() {
            this._super();
        }
    });

    return IOS8HlsStream;
});

define(
'engine/hls/factories/HlsStreamFactory',[
    'modules/Base',
    'engine/hls/stream/HlsStream',
    'engine/hls/stream/IOS8HlsStream'
],

function(BaseModuleClass, HlsStream, IOS8HlsStream) {
    'use strict';

    var HlsStreamFactory = BaseModuleClass.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'HlsStreamFactory',
        logLevel: 'warn',

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ MEMBERS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ METHODS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        createStream: function(core, hlsVideo) {
            var os = core.environment.os;
            var isIOS8 = os.name === 'iOS' && os.version[0].split('.')[0] === '8';
            if (isIOS8) {
                return new IOS8HlsStream(this.options, core, hlsVideo);
            }
            else {
                return new HlsStream(this.options, core, hlsVideo);
            }
        },

        //////////////////////////////////////////////////////////////////////////
        // ---------------------------- CONSTRUCTOR ------------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function() {},

        dispose: function() {}

    });

    return HlsStreamFactory;
});

define(
'engine/hls/stream/BaseBytesGenerator',[
    'modules/Base'
],

function(BaseModuleClass) {
    'use strict';

    var BaseBytesGenerator = BaseModuleClass.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'BaseBytesGenerator',
        logLevel: 'warn',

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ MEMBERS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        offset: NaN,
        totalDuration: NaN,
        length: NaN,
        bytesQueue: null,

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ METHODS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        getBytes: function(index) {
            if (index >= this.length) {
                return null;
            }
            else {
                return this.bytesQueue[index];
            }
        },

        //////////////////////////////////////////////////////////////////////////
        // ---------------------------- CONSTRUCTOR ------------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function(bytes, duration) {
            this.bytesQueue = bytes;
            this.totalDuration = duration;
            this.offset = 0;
            this.length = this.bytesQueue.length;
            // define offset getter / setter
            this.defineProperty('offset', function() {
                return this.getSetting('offset');
            }, function(value) {
                if (value < 0) {
                    this.log.error('Hls trying to set illegal offset time ' + value);
                    this.notifyError(60, value);
                } else {
                    this.updateTimes(value);
                    return this.setSetting('offset', value);
                }
            });
        },

        dispose: function() {}
    });

    return BaseBytesGenerator;
});

define(
'engine/hls/stream/RegularBytesGenerator',[
    'engine/hls/stream/BaseBytesGenerator'
],

function(BaseBytesGenerator) {
    'use strict';

    var RegularBytesGenerator = BaseBytesGenerator.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'RegularBytesGenerator',
        logLevel: 'warn',

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ MEMBERS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        updateTimes: function(offset) {
            var len = this.bytesQueue.length;
            var i;
            for (i = 0; i < len ; i++) {
                this.bytesQueue[i].endTime += offset;
            }
        },

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ METHODS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////
        // ---------------------------- CONSTRUCTOR ------------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function(bytes, duration) {
            this._super(bytes, duration);
        },

        dispose: function() {}
    });

    return RegularBytesGenerator;
});

define(
'engine/hls/stream/ParallelBytesGenerator',[
    'engine/hls/stream/BaseBytesGenerator'
],

function(BaseBytesGenerator) {
    'use strict';

    var ParallelBytesGenerator = BaseBytesGenerator.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'ParallelBytesGenerator',
        logLevel: 'warn',

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ MEMBERS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        queues: null,

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ METHODS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        setChannel: function(channelId) {
            var index = this.channelToQue(channelId);
            if (index < 0) {
                this.log.warn('illegal channelId ' + channelId);
            }
            else {
                this.bytesQueue = this.queues[index];
            }
        },

        channelToQue: function(channelId) {
            // for now assume channelId is just number according to array , so return it. maybe in the future we'll have names and need to have map from id to number
            if (channelId < 0 || channelId >= this.queues.length) {
                return -1;
            }
            else {
                return channelId;
            }
        },

        updateTimes: function(offset) {
            var numQueues = this.queues.length;
            var currQue;
            var len;
            var q;
            var i;
            for (q = 0; q < numQueues ; q++) {
                currQue = this.queues[q];
                len = currQue.length;
                for (i = 0; i < len ; i++) {
                    currQue[i].endTime += offset;
                }
            }
        },

        //////////////////////////////////////////////////////////////////////////
        // ---------------------------- CONSTRUCTOR ------------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function(bytesArr, duration) {
            this.queues = bytesArr;
            this._super(this.queues[0], duration);
        },

        dispose: function() {}
    });

    return ParallelBytesGenerator;
});

define(
'engine/hls/factories/BytesGeneratorFactory',[
    'modules/Base',
    'engine/hls/stream/RegularBytesGenerator',
    'engine/hls/stream/ParallelBytesGenerator'
],

function(BaseModuleClass, RegularBytesGenerator, ParallelBytesGenerator) {
    'use strict';

    var BytesGeneratorFactory = BaseModuleClass.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'BytesGeneratorFactory',
        logLevel: 'warn',

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ MEMBERS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ METHODS --------------------------------
        //////////////////////////////////////////////////////////////////////////
        // create bytesGenerator object according to type of node (regular / parallel... )
        createBytesGenerator: function(node) {
            var bytesGenerator;
            var segments = node.segments;
            var bytes;

            if (segments.length === 1) {
                bytes = this.createBytes(segments[0]);
                if (!bytes) {
                    return null;
                }
                else {
                    bytesGenerator = new RegularBytesGenerator(this.options, bytes, segments[0].source.ts.duration);
                }
            }
            else { // parallel
                var parallelBytes = [];
                var len = segments.length;
                var i;
                for (i = 0 ; i < len ; i++) {
                    bytes = this.createBytes(segments[i]);
                    if (!bytes) {
                        return null;
                    }
                    else {
                        parallelBytes.push(bytes);
                    }
                }
                if (!this.isValidDurations(segments)) {
                    return null;
                }
                else {
                    bytesGenerator = new ParallelBytesGenerator(this.options, parallelBytes, segments[0].source.ts.duration);
                }
            }
            return bytesGenerator;
        },

        createBytes: function(segment) {
            // validate segment format
            if (!segment.source || !segment.source.ts || !segment.source.ts.byterange || !segment.source.ts.url || !segment.source.ts.duration) {
                this.log.error('Hls illegal segment data');
                this.notifyError(59);
                return null;
            }
            var byterange = segment.source.ts.byterange;
            var length = byterange.length;
            var url = segment.source.ts.url;
            var bytes = [];
            var time = 0;
            var i;
            for (i = 0 ; i < length ; i++) {
                time += byterange[i].duration;
                bytes.push({
                    endTime: time,
                    bytesData: {
                        url: url,
                        byterange: byterange[i]
                    }
                });
            }

            return bytes;
        },

        /* return true if duration of all segments are equal, false otherwise */
        isValidDurations: function(segments) {
            var len = segments.length;
            var i;
            var duration = segments[0].source.ts.duration;
            for (i = 1 ; i < len ; i++) {
                if (segments[i].source.ts.duration !== duration) {
                    return false;
                }
            }
            return true;
        },

        //////////////////////////////////////////////////////////////////////////
        // ---------------------------- CONSTRUCTOR ------------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function() {
        },

        dispose: function() {}

    });

    return BytesGeneratorFactory;
});

define(
'engine/hls/HlsEngine',[
    'engine/BaseEngine',
    'engine/hls/HlsVideo',
    'engine/hls/factories/HlsStreamFactory',
    'engine/hls/factories/BytesGeneratorFactory',
    'utils/utils',
    'when'
],

function(BaseEngine, HlsVideo, HlsStreamFactory, BytesGeneratorFactory, utils, when) {
    'use strict';

    var HlsEngine = BaseEngine.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'HlsEngine',
        logLevel: 'warn',

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ MEMBERS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        streamer: null,
        bytesGenFactory: null,
        hlsStreamFactory: null,

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ METHODS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        appendNode: function(nodeId) {
            var d = when.defer();
            // Validate input
            if (!nodeId || !this.repository || !this.repository.hasNode(nodeId)) {
                d.reject();
            }
            // Valid node
            else {
                var node = this.repository.getNode(nodeId);
                var byteGen = this.bytesGenFactory.createBytesGenerator(node);
                if (!byteGen) {
                    d.reject();
                }
                else {
                    this.streamer.push(byteGen)
                        .done(function() {
                            d.resolve(nodeId);
                        }.bind(this), function() {
                            d.reject();
                        }.bind(this));
                }
            }

            return d.promise;
        },

        reset: function() {
            var d = when.defer();

            var onPaused = function() {
                this.video.htmlVideo.off(this.events.pause, onPaused);
                this.reCreateObjects();
                d.resolve();
            }.bind(this);

            this.video.htmlVideo.on(this.events.pause, onPaused);

            if (!this.video.htmlVideo.paused()) {
                this.video.htmlVideo.pause();
            } else {
                onPaused();
            }

            return d.promise;
        },

        switchChannel: function(channelIndex) {
            var d = when.defer();
            var timeInNode = this.streamer.switchChannel(channelIndex);
            if (timeInNode) {
                d.resolve({timeInNode: timeInNode});
            }
            else {
                d.reject();
            }
            return d.promise;
        },

        seek: function(nodeId, toTimeInNode) {
            var d = when.defer();
            if (toTimeInNode !== 0) {
                this.log.error('Seek inside node not implemented yet in hls');
                this.notifyError(54);
                d.reject();
            }
            else {
                var endSeekTime = this.streamer.flush();
                var currTime = this.video.htmlVideo.currentTime();
                var delayTime = endSeekTime - currTime;
                this.appendNode(nodeId)
                    .done(function() {
                        d.resolve({seekedTimeDelay: delayTime});
                    }.bind(this), function() {
                        d.reject();
                    });
            }

            return d.promise;
        },

        videoTech: function() {
            return 'hls';
        },

        reCreateObjects: function() {
            /* jshint ignore:start */

            //will re-build  streamer and reset video (which re-build htmlVideo)
            this.streamer && this.streamer.dispose() && delete this.streamer;

            /* jshint ignore:end */
            this.video.reset();
            this.streamer = this.hlsStreamFactory.createStream(this.api, this.video);
        },

        //////////////////////////////////////////////////////////////////////////
        // ---------------------------- CONSTRUCTOR ------------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function(el, core, repo) {
            this._super(core, repo);

            // create video
            this.video = new HlsVideo(this.options, this.api, el);

            // Add error listeners to submodules
            this.addErrorListener(this.video);

            this.bytesGenFactory = new BytesGeneratorFactory(this.options);
            this.hlsStreamFactory = new HlsStreamFactory(this.options);
            this.streamer = this.hlsStreamFactory.createStream(this.api, this.video);
        },

        dispose: function() {}
    });

    return HlsEngine;
});

define(
'engine/hls/HlsFactory',[
    'engine/BaseFactory',
    'engine/hls/HlsEngine'
],

function(BaseFactory, HlsEngine) {
    'use strict';

    var HlsFactory = BaseFactory.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'HlsFactory',

        //////////////////////////////////////////////////////////////////////////
        // ------------------------- PRIVATE METHODS -----------------------------
        //////////////////////////////////////////////////////////////////////////

        /** Overrides base implementation. */

        _createEngine: function() {
            return new HlsEngine(this.options, this.el, this.core, this.repo);
        }
    });

    return HlsFactory;
});

define(
'engine/mse/MseVideo',[
    'engine/BaseVideo',
    'modules/Html5Video',
    'utils/utils'
],

/**
 * Concrete implementation for the Video module of MSE tech (extends the BaseVideo module).
 */
function(BaseVideo, Html5Video, utils) {
    'use strict';

    var MseVideo = BaseVideo.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'MseVideo',
        logLevel: 'warn',
        excludeFuncFromAutoDebug: ['onTimeUpdate', 'currentTime', 'onProgress'],

        //////////////////////////////////////////////////////////////////////////
        // ----------------------------- MEMBERS ---------------------------------
        //////////////////////////////////////////////////////////////////////////

        api: null,
        events: null,
        htmlVideo: null,
        shouldBePlaying: false,
        playing: false,
        forcingPlay: false,

        //////////////////////////////////////////////////////////////////////////
        // ------------------------- NATIVE LISTENERS ----------------------------
        //////////////////////////////////////////////////////////////////////////

        onTimeUpdate: function() {
            this.trigger(this.events.timeupdate, this.htmlVideo.currentTime());
        },

        onVolumeChange: function() {
            this.trigger(this.events.volumechange, this.htmlVideo.volume());
        },

        onPause: function() {
            this.shouldBePlaying = false;
            this.playing = false;

            this.trigger(this.events.pause);
        },

        onPlay: function() {
            this.shouldBePlaying = true;

            if (this.forcingPlay) {
                this.forcingPlay = false;
                return;
            }

            this.trigger(this.events.play);
        },

        onPlaying: function() {
            this.playing = true;

            this.trigger(this.events.playing);
        },

        onWaiting: function() {
            // Only trigger waiting event if we're currently in mid playback
            if (this.playing) {
                this.trigger(this.events.waiting);
            }
        },

        onLoadedData: function() {
            this.trigger(this.events.loadeddata);
        },

        onSeeking: function() {

        },

        onSeeked: function() {

        },

        onEnded: function() {
            this.trigger(this.events.ended);
        },

        onProgress: function() {

        },

        onLoadedMetaData: function() {

        },

        onCanPlay: function() {

        },

        onCanPlayThrough: function() {
            if (this.shouldBePlaying && !this.playing) {
                this.forcingPlay = true;
                this.play();
            }
        },

        onAPISeeking: function() {
            this.playing = false;
        },

        onAPISeeked: function() {

        },

        //////////////////////////////////////////////////////////////////////////
        // ----------------------------- METHODS ---------------------------------
        //////////////////////////////////////////////////////////////////////////

        _createHtmlVideo: function(el) {
            var htmlVideo = new Html5Video(this.options).makeHtml5Video(el);

            htmlVideo = htmlVideo ? utils.extend(htmlVideo, {
                load: function load() {
                    utils.dispatchSimpleEvent(htmlVideo.getElement(), 'loadstart');
                    utils.dispatchSimpleEvent(htmlVideo.getElement(), 'durationchange');
                    utils.dispatchSimpleEvent(htmlVideo.getElement(), 'loadedmetadata');
                    utils.dispatchSimpleEvent(htmlVideo.getElement(), 'loadeddata');
                    utils.dispatchSimpleEvent(htmlVideo.getElement(), 'progress');
                    utils.dispatchSimpleEvent(htmlVideo.getElement(), 'canplay');
                    utils.dispatchSimpleEvent(htmlVideo.getElement(), 'canplaythrough');
                }
            }) : null;

            return htmlVideo;
        },

        play: function() {
            this.shouldBePlaying = true; this.htmlVideo.play();
        },

        pause: function() {
            this.shouldBePlaying = false; this.htmlVideo.pause();
        },

        currentTime: function() { return this.htmlVideo.currentTime(); },

        //////////////////////////////////////////////////////////////////////////
        // --------------------------- CONSTRUCTOR -------------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function(api, el) {
            // Keep references
            this.api = api;
            this.events = api.events;
            this.htmlVideo = this._createHtmlVideo(el);

            // Add error listeners to submodules
            this.addErrorListener(this.htmlVideo);

            // Define properties
            this.defineProperty('volume', function() {
                return this.htmlVideo.getElement().volume;
            }, function(value) {
                this.htmlVideo.getElement().volume = value;
            });

            this.defineProperty('muted', function() {
                return this.htmlVideo.getElement().muted;
            }, function(value) {
                this.htmlVideo.getElement().muted = value;
            });

            // Add listeners
            this.htmlVideo.on('timeupdate', this.onTimeUpdate);
            this.htmlVideo.on('volumechange', this.onVolumeChange);
            this.htmlVideo.on('pause', this.onPause);
            this.htmlVideo.on('play', this.onPlay);
            this.htmlVideo.on('playing', this.onPlaying);
            this.htmlVideo.on('waiting', this.onWaiting);
            this.htmlVideo.on('seeking', this.onSeeking);
            this.htmlVideo.on('seeked', this.onSeeked);
            this.htmlVideo.on('ended', this.onEnded);
            this.htmlVideo.on('progress', this.onProgress);
            this.htmlVideo.on('loadeddata', this.onLoadedData);
            this.htmlVideo.on('loadedmetadata', this.onLoadedMetaData);
            this.htmlVideo.on('canplay', this.onCanPlay);
            this.htmlVideo.on('canplaythrough', this.onCanPlayThrough);
            this.api.on(this.events.seeking, this.onAPISeeking);
            this.api.on(this.events.seeked, this.onAPISeeked);

            // Call base ctor - exposes public API.
            this._super.apply(this, arguments);
        },

        dispose: function() {
            this._super();

            // Remove listeners
            this.htmlVideo.off('timeupdate', this.onTimeUpdate);
            this.htmlVideo.off('volumechange', this.onVolumeChange);
            this.htmlVideo.off('pause', this.onPause);
            this.htmlVideo.off('play', this.onPlay);
            this.htmlVideo.off('playing', this.onPlaying);
            this.htmlVideo.off('waiting', this.onWaiting);
            this.htmlVideo.off('seeking', this.onSeeking);
            this.htmlVideo.off('seeked', this.onSeeked);
            this.htmlVideo.off('ended', this.onEnded);
            this.htmlVideo.off('progress', this.onProgress);
            this.htmlVideo.off('loadeddata', this.onLoadedData);
            this.htmlVideo.off('loadedmetadata', this.onLoadedMetaData);
            this.htmlVideo.off('canplay', this.onCanPlay);
            this.htmlVideo.off('canplaythrough', this.onCanPlayThrough);
            this.api.off(this.events.seeking, this.onAPISeeking);
            this.api.off(this.events.seeked, this.onAPISeeked);
        }
    });

    return MseVideo;
});

define('engine/mse/workers/WorkerMsg',[
],

/**
* This module defines a generic Worker message structure.
*/
function() {
    'use strict';

    function WorkerMsg(worker, name, data, arrayBuffer) {
        var msg = {
            name: name,
            data: data,
            arrayBuffer: arrayBuffer
        };

        this.post = function() {
            if (msg.arrayBuffer) {
                worker.postMessage(msg, [msg.arrayBuffer]);
            } else {
                worker.postMessage(msg);
            }
        };
    }

    return WorkerMsg;
});

/**
 * @license RequireJS text 2.0.14 Copyright (c) 2010-2014, The Dojo Foundation All Rights Reserved.
 * Available via the MIT or new BSD license.
 * see: http://github.com/requirejs/text for details
 */
/*jslint regexp: true */
/*global require, XMLHttpRequest, ActiveXObject,
  define, window, process, Packages,
  java, location, Components, FileUtils */

define('text',['module'], function (module) {
    'use strict';

    var text, fs, Cc, Ci, xpcIsWindows,
        progIds = ['Msxml2.XMLHTTP', 'Microsoft.XMLHTTP', 'Msxml2.XMLHTTP.4.0'],
        xmlRegExp = /^\s*<\?xml(\s)+version=[\'\"](\d)*.(\d)*[\'\"](\s)*\?>/im,
        bodyRegExp = /<body[^>]*>\s*([\s\S]+)\s*<\/body>/im,
        hasLocation = typeof location !== 'undefined' && location.href,
        defaultProtocol = hasLocation && location.protocol && location.protocol.replace(/\:/, ''),
        defaultHostName = hasLocation && location.hostname,
        defaultPort = hasLocation && (location.port || undefined),
        buildMap = {},
        masterConfig = (module.config && module.config()) || {};

    text = {
        version: '2.0.14',

        strip: function (content) {
            //Strips <?xml ...?> declarations so that external SVG and XML
            //documents can be added to a document without worry. Also, if the string
            //is an HTML document, only the part inside the body tag is returned.
            if (content) {
                content = content.replace(xmlRegExp, "");
                var matches = content.match(bodyRegExp);
                if (matches) {
                    content = matches[1];
                }
            } else {
                content = "";
            }
            return content;
        },

        jsEscape: function (content) {
            return content.replace(/(['\\])/g, '\\$1')
                .replace(/[\f]/g, "\\f")
                .replace(/[\b]/g, "\\b")
                .replace(/[\n]/g, "\\n")
                .replace(/[\t]/g, "\\t")
                .replace(/[\r]/g, "\\r")
                .replace(/[\u2028]/g, "\\u2028")
                .replace(/[\u2029]/g, "\\u2029");
        },

        createXhr: masterConfig.createXhr || function () {
            //Would love to dump the ActiveX crap in here. Need IE 6 to die first.
            var xhr, i, progId;
            if (typeof XMLHttpRequest !== "undefined") {
                return new XMLHttpRequest();
            } else if (typeof ActiveXObject !== "undefined") {
                for (i = 0; i < 3; i += 1) {
                    progId = progIds[i];
                    try {
                        xhr = new ActiveXObject(progId);
                    } catch (e) {}

                    if (xhr) {
                        progIds = [progId];  // so faster next time
                        break;
                    }
                }
            }

            return xhr;
        },

        /**
         * Parses a resource name into its component parts. Resource names
         * look like: module/name.ext!strip, where the !strip part is
         * optional.
         * @param {String} name the resource name
         * @returns {Object} with properties "moduleName", "ext" and "strip"
         * where strip is a boolean.
         */
        parseName: function (name) {
            var modName, ext, temp,
                strip = false,
                index = name.lastIndexOf("."),
                isRelative = name.indexOf('./') === 0 ||
                             name.indexOf('../') === 0;

            if (index !== -1 && (!isRelative || index > 1)) {
                modName = name.substring(0, index);
                ext = name.substring(index + 1);
            } else {
                modName = name;
            }

            temp = ext || modName;
            index = temp.indexOf("!");
            if (index !== -1) {
                //Pull off the strip arg.
                strip = temp.substring(index + 1) === "strip";
                temp = temp.substring(0, index);
                if (ext) {
                    ext = temp;
                } else {
                    modName = temp;
                }
            }

            return {
                moduleName: modName,
                ext: ext,
                strip: strip
            };
        },

        xdRegExp: /^((\w+)\:)?\/\/([^\/\\]+)/,

        /**
         * Is an URL on another domain. Only works for browser use, returns
         * false in non-browser environments. Only used to know if an
         * optimized .js version of a text resource should be loaded
         * instead.
         * @param {String} url
         * @returns Boolean
         */
        useXhr: function (url, protocol, hostname, port) {
            var uProtocol, uHostName, uPort,
                match = text.xdRegExp.exec(url);
            if (!match) {
                return true;
            }
            uProtocol = match[2];
            uHostName = match[3];

            uHostName = uHostName.split(':');
            uPort = uHostName[1];
            uHostName = uHostName[0];

            return (!uProtocol || uProtocol === protocol) &&
                   (!uHostName || uHostName.toLowerCase() === hostname.toLowerCase()) &&
                   ((!uPort && !uHostName) || uPort === port);
        },

        finishLoad: function (name, strip, content, onLoad) {
            content = strip ? text.strip(content) : content;
            if (masterConfig.isBuild) {
                buildMap[name] = content;
            }
            onLoad(content);
        },

        load: function (name, req, onLoad, config) {
            //Name has format: some.module.filext!strip
            //The strip part is optional.
            //if strip is present, then that means only get the string contents
            //inside a body tag in an HTML string. For XML/SVG content it means
            //removing the <?xml ...?> declarations so the content can be inserted
            //into the current doc without problems.

            // Do not bother with the work if a build and text will
            // not be inlined.
            if (config && config.isBuild && !config.inlineText) {
                onLoad();
                return;
            }

            masterConfig.isBuild = config && config.isBuild;

            var parsed = text.parseName(name),
                nonStripName = parsed.moduleName +
                    (parsed.ext ? '.' + parsed.ext : ''),
                url = req.toUrl(nonStripName),
                useXhr = (masterConfig.useXhr) ||
                         text.useXhr;

            // Do not load if it is an empty: url
            if (url.indexOf('empty:') === 0) {
                onLoad();
                return;
            }

            //Load the text. Use XHR if possible and in a browser.
            if (!hasLocation || useXhr(url, defaultProtocol, defaultHostName, defaultPort)) {
                text.get(url, function (content) {
                    text.finishLoad(name, parsed.strip, content, onLoad);
                }, function (err) {
                    if (onLoad.error) {
                        onLoad.error(err);
                    }
                });
            } else {
                //Need to fetch the resource across domains. Assume
                //the resource has been optimized into a JS module. Fetch
                //by the module name + extension, but do not include the
                //!strip part to avoid file system issues.
                req([nonStripName], function (content) {
                    text.finishLoad(parsed.moduleName + '.' + parsed.ext,
                                    parsed.strip, content, onLoad);
                });
            }
        },

        write: function (pluginName, moduleName, write, config) {
            if (buildMap.hasOwnProperty(moduleName)) {
                var content = text.jsEscape(buildMap[moduleName]);
                write.asModule(pluginName + "!" + moduleName,
                               "define(function () { return '" +
                                   content +
                               "';});\n");
            }
        },

        writeFile: function (pluginName, moduleName, req, write, config) {
            var parsed = text.parseName(moduleName),
                extPart = parsed.ext ? '.' + parsed.ext : '',
                nonStripName = parsed.moduleName + extPart,
                //Use a '.js' file name so that it indicates it is a
                //script that can be loaded across domains.
                fileName = req.toUrl(parsed.moduleName + extPart) + '.js';

            //Leverage own load() method to load plugin value, but only
            //write out values that do not have the strip argument,
            //to avoid any potential issues with ! in file names.
            text.load(nonStripName, req, function (value) {
                //Use own write() method to construct full module value.
                //But need to create shell that translates writeFile's
                //write() to the right interface.
                var textWrite = function (contents) {
                    return write(fileName, contents);
                };
                textWrite.asModule = function (moduleName, contents) {
                    return write.asModule(moduleName, fileName, contents);
                };

                text.write(pluginName, nonStripName, textWrite, config);
            }, config);
        }
    };

    if (masterConfig.env === 'node' || (!masterConfig.env &&
            typeof process !== "undefined" &&
            process.versions &&
            !!process.versions.node &&
            !process.versions['node-webkit'] &&
            !process.versions['atom-shell'])) {
        //Using special require.nodeRequire, something added by r.js.
        fs = require.nodeRequire('fs');

        text.get = function (url, callback, errback) {
            try {
                var file = fs.readFileSync(url, 'utf8');
                //Remove BOM (Byte Mark Order) from utf8 files if it is there.
                if (file[0] === '\uFEFF') {
                    file = file.substring(1);
                }
                callback(file);
            } catch (e) {
                if (errback) {
                    errback(e);
                }
            }
        };
    } else if (masterConfig.env === 'xhr' || (!masterConfig.env &&
            text.createXhr())) {
        text.get = function (url, callback, errback, headers) {
            var xhr = text.createXhr(), header;
            xhr.open('GET', url, true);

            //Allow plugins direct access to xhr headers
            if (headers) {
                for (header in headers) {
                    if (headers.hasOwnProperty(header)) {
                        xhr.setRequestHeader(header.toLowerCase(), headers[header]);
                    }
                }
            }

            //Allow overrides specified in config
            if (masterConfig.onXhr) {
                masterConfig.onXhr(xhr, url);
            }

            xhr.onreadystatechange = function (evt) {
                var status, err;
                //Do not explicitly handle errors, those should be
                //visible via console output in the browser.
                if (xhr.readyState === 4) {
                    status = xhr.status || 0;
                    if (status > 399 && status < 600) {
                        //An http 4xx or 5xx error. Signal an error.
                        err = new Error(url + ' HTTP status: ' + status);
                        err.xhr = xhr;
                        if (errback) {
                            errback(err);
                        }
                    } else {
                        callback(xhr.responseText);
                    }

                    if (masterConfig.onXhrComplete) {
                        masterConfig.onXhrComplete(xhr, url);
                    }
                }
            };
            xhr.send(null);
        };
    } else if (masterConfig.env === 'rhino' || (!masterConfig.env &&
            typeof Packages !== 'undefined' && typeof java !== 'undefined')) {
        //Why Java, why is this so awkward?
        text.get = function (url, callback) {
            var stringBuffer, line,
                encoding = "utf-8",
                file = new java.io.File(url),
                lineSeparator = java.lang.System.getProperty("line.separator"),
                input = new java.io.BufferedReader(new java.io.InputStreamReader(new java.io.FileInputStream(file), encoding)),
                content = '';
            try {
                stringBuffer = new java.lang.StringBuffer();
                line = input.readLine();

                // Byte Order Mark (BOM) - The Unicode Standard, version 3.0, page 324
                // http://www.unicode.org/faq/utf_bom.html

                // Note that when we use utf-8, the BOM should appear as "EF BB BF", but it doesn't due to this bug in the JDK:
                // http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4508058
                if (line && line.length() && line.charAt(0) === 0xfeff) {
                    // Eat the BOM, since we've already found the encoding on this file,
                    // and we plan to concatenating this buffer with others; the BOM should
                    // only appear at the top of a file.
                    line = line.substring(1);
                }

                if (line !== null) {
                    stringBuffer.append(line);
                }

                while ((line = input.readLine()) !== null) {
                    stringBuffer.append(lineSeparator);
                    stringBuffer.append(line);
                }
                //Make sure we return a JavaScript string and not a Java string.
                content = String(stringBuffer.toString()); //String
            } finally {
                input.close();
            }
            callback(content);
        };
    } else if (masterConfig.env === 'xpconnect' || (!masterConfig.env &&
            typeof Components !== 'undefined' && Components.classes &&
            Components.interfaces)) {
        //Avert your gaze!
        Cc = Components.classes;
        Ci = Components.interfaces;
        Components.utils['import']('resource://gre/modules/FileUtils.jsm');
        xpcIsWindows = ('@mozilla.org/windows-registry-key;1' in Cc);

        text.get = function (url, callback) {
            var inStream, convertStream, fileObj,
                readData = {};

            if (xpcIsWindows) {
                url = url.replace(/\//g, '\\');
            }

            fileObj = new FileUtils.File(url);

            //XPCOM, you so crazy
            try {
                inStream = Cc['@mozilla.org/network/file-input-stream;1']
                           .createInstance(Ci.nsIFileInputStream);
                inStream.init(fileObj, 1, 0, false);

                convertStream = Cc['@mozilla.org/intl/converter-input-stream;1']
                                .createInstance(Ci.nsIConverterInputStream);
                convertStream.init(inStream, "utf-8", inStream.available(),
                Ci.nsIConverterInputStream.DEFAULT_REPLACEMENT_CHARACTER);

                convertStream.readString(inStream.available(), readData);
                convertStream.close();
                inStream.close();
                callback(readData.value);
            } catch (e) {
                throw new Error((fileObj && fileObj.path || '') + ': ' + e);
            }
        };
    }
    return text;
});


define('text!engine/../../tmp/mseWorker.min.js',[],function () { return '(function () {/**\n * @license almond 0.3.0 Copyright (c) 2011-2014, The Dojo Foundation All Rights Reserved.\n * Available via the MIT or new BSD license.\n * see: http://github.com/jrburke/almond for details\n */\n//Going sloppy to avoid \'use strict\' string cost, but strict practices should\n//be followed.\n/*jslint sloppy: true */\n/*global setTimeout: false */\n\nvar requirejs, require, define;\n(function (undef) {\n    var main, req, makeMap, handlers,\n        defined = {},\n        waiting = {},\n        config = {},\n        defining = {},\n        hasOwn = Object.prototype.hasOwnProperty,\n        aps = [].slice,\n        jsSuffixRegExp = /\\.js$/;\n\n    function hasProp(obj, prop) {\n        return hasOwn.call(obj, prop);\n    }\n\n    /**\n     * Given a relative module name, like ./something, normalize it to\n     * a real name that can be mapped to a path.\n     * @param {String} name the relative name\n     * @param {String} baseName a real name that the name arg is relative\n     * to.\n     * @returns {String} normalized name\n     */\n    function normalize(name, baseName) {\n        var nameParts, nameSegment, mapValue, foundMap, lastIndex,\n            foundI, foundStarMap, starI, i, j, part,\n            baseParts = baseName && baseName.split("/"),\n            map = config.map,\n            starMap = (map && map[\'*\']) || {};\n\n        //Adjust any relative paths.\n        if (name && name.charAt(0) === ".") {\n            //If have a base name, try to normalize against it,\n            //otherwise, assume it is a top-level require that will\n            //be relative to baseUrl in the end.\n            if (baseName) {\n                //Convert baseName to array, and lop off the last part,\n                //so that . matches that "directory" and not name of the baseName\'s\n                //module. For instance, baseName of "one/two/three", maps to\n                //"one/two/three.js", but we want the directory, "one/two" for\n                //this normalization.\n                baseParts = baseParts.slice(0, baseParts.length - 1);\n                name = name.split(\'/\');\n                lastIndex = name.length - 1;\n\n                // Node .js allowance:\n                if (config.nodeIdCompat && jsSuffixRegExp.test(name[lastIndex])) {\n                    name[lastIndex] = name[lastIndex].replace(jsSuffixRegExp, \'\');\n                }\n\n                name = baseParts.concat(name);\n\n                //start trimDots\n                for (i = 0; i < name.length; i += 1) {\n                    part = name[i];\n                    if (part === ".") {\n                        name.splice(i, 1);\n                        i -= 1;\n                    } else if (part === "..") {\n                        if (i === 1 && (name[2] === \'..\' || name[0] === \'..\')) {\n                            //End of the line. Keep at least one non-dot\n                            //path segment at the front so it can be mapped\n                            //correctly to disk. Otherwise, there is likely\n                            //no path mapping for a path starting with \'..\'.\n                            //This can still fail, but catches the most reasonable\n                            //uses of ..\n                            break;\n                        } else if (i > 0) {\n                            name.splice(i - 1, 2);\n                            i -= 2;\n                        }\n                    }\n                }\n                //end trimDots\n\n                name = name.join("/");\n            } else if (name.indexOf(\'./\') === 0) {\n                // No baseName, so this is ID is resolved relative\n                // to baseUrl, pull off the leading dot.\n                name = name.substring(2);\n            }\n        }\n\n        //Apply map config if available.\n        if ((baseParts || starMap) && map) {\n            nameParts = name.split(\'/\');\n\n            for (i = nameParts.length; i > 0; i -= 1) {\n                nameSegment = nameParts.slice(0, i).join("/");\n\n                if (baseParts) {\n                    //Find the longest baseName segment match in the config.\n                    //So, do joins on the biggest to smallest lengths of baseParts.\n                    for (j = baseParts.length; j > 0; j -= 1) {\n                        mapValue = map[baseParts.slice(0, j).join(\'/\')];\n\n                        //baseName segment has  config, find if it has one for\n                        //this name.\n                        if (mapValue) {\n                            mapValue = mapValue[nameSegment];\n                            if (mapValue) {\n                                //Match, update name to the new value.\n                                foundMap = mapValue;\n                                foundI = i;\n                                break;\n                            }\n                        }\n                    }\n                }\n\n                if (foundMap) {\n                    break;\n                }\n\n                //Check for a star map match, but just hold on to it,\n                //if there is a shorter segment match later in a matching\n                //config, then favor over this star map.\n                if (!foundStarMap && starMap && starMap[nameSegment]) {\n                    foundStarMap = starMap[nameSegment];\n                    starI = i;\n                }\n            }\n\n            if (!foundMap && foundStarMap) {\n                foundMap = foundStarMap;\n                foundI = starI;\n            }\n\n            if (foundMap) {\n                nameParts.splice(0, foundI, foundMap);\n                name = nameParts.join(\'/\');\n            }\n        }\n\n        return name;\n    }\n\n    function makeRequire(relName, forceSync) {\n        return function () {\n            //A version of a require function that passes a moduleName\n            //value for items that may need to\n            //look up paths relative to the moduleName\n            var args = aps.call(arguments, 0);\n\n            //If first arg is not require(\'string\'), and there is only\n            //one arg, it is the array form without a callback. Insert\n            //a null so that the following concat is correct.\n            if (typeof args[0] !== \'string\' && args.length === 1) {\n                args.push(null);\n            }\n            return req.apply(undef, args.concat([relName, forceSync]));\n        };\n    }\n\n    function makeNormalize(relName) {\n        return function (name) {\n            return normalize(name, relName);\n        };\n    }\n\n    function makeLoad(depName) {\n        return function (value) {\n            defined[depName] = value;\n        };\n    }\n\n    function callDep(name) {\n        if (hasProp(waiting, name)) {\n            var args = waiting[name];\n            delete waiting[name];\n            defining[name] = true;\n            main.apply(undef, args);\n        }\n\n        if (!hasProp(defined, name) && !hasProp(defining, name)) {\n            throw new Error(\'No \' + name);\n        }\n        return defined[name];\n    }\n\n    //Turns a plugin!resource to [plugin, resource]\n    //with the plugin being undefined if the name\n    //did not have a plugin prefix.\n    function splitPrefix(name) {\n        var prefix,\n            index = name ? name.indexOf(\'!\') : -1;\n        if (index > -1) {\n            prefix = name.substring(0, index);\n            name = name.substring(index + 1, name.length);\n        }\n        return [prefix, name];\n    }\n\n    /**\n     * Makes a name map, normalizing the name, and using a plugin\n     * for normalization if necessary. Grabs a ref to plugin\n     * too, as an optimization.\n     */\n    makeMap = function (name, relName) {\n        var plugin,\n            parts = splitPrefix(name),\n            prefix = parts[0];\n\n        name = parts[1];\n\n        if (prefix) {\n            prefix = normalize(prefix, relName);\n            plugin = callDep(prefix);\n        }\n\n        //Normalize according\n        if (prefix) {\n            if (plugin && plugin.normalize) {\n                name = plugin.normalize(name, makeNormalize(relName));\n            } else {\n                name = normalize(name, relName);\n            }\n        } else {\n            name = normalize(name, relName);\n            parts = splitPrefix(name);\n            prefix = parts[0];\n            name = parts[1];\n            if (prefix) {\n                plugin = callDep(prefix);\n            }\n        }\n\n        //Using ridiculous property names for space reasons\n        return {\n            f: prefix ? prefix + \'!\' + name : name, //fullName\n            n: name,\n            pr: prefix,\n            p: plugin\n        };\n    };\n\n    function makeConfig(name) {\n        return function () {\n            return (config && config.config && config.config[name]) || {};\n        };\n    }\n\n    handlers = {\n        require: function (name) {\n            return makeRequire(name);\n        },\n        exports: function (name) {\n            var e = defined[name];\n            if (typeof e !== \'undefined\') {\n                return e;\n            } else {\n                return (defined[name] = {});\n            }\n        },\n        module: function (name) {\n            return {\n                id: name,\n                uri: \'\',\n                exports: defined[name],\n                config: makeConfig(name)\n            };\n        }\n    };\n\n    main = function (name, deps, callback, relName) {\n        var cjsModule, depName, ret, map, i,\n            args = [],\n            callbackType = typeof callback,\n            usingExports;\n\n        //Use name if no relName\n        relName = relName || name;\n\n        //Call the callback to define the module, if necessary.\n        if (callbackType === \'undefined\' || callbackType === \'function\') {\n            //Pull out the defined dependencies and pass the ordered\n            //values to the callback.\n            //Default to [require, exports, module] if no deps\n            deps = !deps.length && callback.length ? [\'require\', \'exports\', \'module\'] : deps;\n            for (i = 0; i < deps.length; i += 1) {\n                map = makeMap(deps[i], relName);\n                depName = map.f;\n\n                //Fast path CommonJS standard dependencies.\n                if (depName === "require") {\n                    args[i] = handlers.require(name);\n                } else if (depName === "exports") {\n                    //CommonJS module spec 1.1\n                    args[i] = handlers.exports(name);\n                    usingExports = true;\n                } else if (depName === "module") {\n                    //CommonJS module spec 1.1\n                    cjsModule = args[i] = handlers.module(name);\n                } else if (hasProp(defined, depName) ||\n                           hasProp(waiting, depName) ||\n                           hasProp(defining, depName)) {\n                    args[i] = callDep(depName);\n                } else if (map.p) {\n                    map.p.load(map.n, makeRequire(relName, true), makeLoad(depName), {});\n                    args[i] = defined[depName];\n                } else {\n                    throw new Error(name + \' missing \' + depName);\n                }\n            }\n\n            ret = callback ? callback.apply(defined[name], args) : undefined;\n\n            if (name) {\n                //If setting exports via "module" is in play,\n                //favor that over return value and exports. After that,\n                //favor a non-undefined return value over exports use.\n                if (cjsModule && cjsModule.exports !== undef &&\n                        cjsModule.exports !== defined[name]) {\n                    defined[name] = cjsModule.exports;\n                } else if (ret !== undef || !usingExports) {\n                    //Use the return value from the function.\n                    defined[name] = ret;\n                }\n            }\n        } else if (name) {\n            //May just be an object definition for the module. Only\n            //worry about defining if have a module name.\n            defined[name] = callback;\n        }\n    };\n\n    requirejs = require = req = function (deps, callback, relName, forceSync, alt) {\n        if (typeof deps === "string") {\n            if (handlers[deps]) {\n                //callback in this case is really relName\n                return handlers[deps](callback);\n            }\n            //Just return the module wanted. In this scenario, the\n            //deps arg is the module name, and second arg (if passed)\n            //is just the relName.\n            //Normalize module name, if it contains . or ..\n            return callDep(makeMap(deps, callback).f);\n        } else if (!deps.splice) {\n            //deps is a config object, not an array.\n            config = deps;\n            if (config.deps) {\n                req(config.deps, config.callback);\n            }\n            if (!callback) {\n                return;\n            }\n\n            if (callback.splice) {\n                //callback is an array, which means it is a dependency list.\n                //Adjust args if there are dependencies\n                deps = callback;\n                callback = relName;\n                relName = null;\n            } else {\n                deps = undef;\n            }\n        }\n\n        //Support require([\'a\'])\n        callback = callback || function () {};\n\n        //If relName is a function, it is an errback handler,\n        //so remove it.\n        if (typeof relName === \'function\') {\n            relName = forceSync;\n            forceSync = alt;\n        }\n\n        //Simulate async callback;\n        if (forceSync) {\n            main(undef, deps, callback, relName);\n        } else {\n            //Using a non-zero value because of concern for what old browsers\n            //do, and latest browsers "upgrade" to 4 if lower value is used:\n            //http://www.whatwg.org/specs/web-apps/current-work/multipage/timers.html#dom-windowtimers-settimeout:\n            //If want a value immediately, use require(\'id\') instead -- something\n            //that works in almond on the global level, but not guaranteed and\n            //unlikely to work in other AMD implementations.\n            setTimeout(function () {\n                main(undef, deps, callback, relName);\n            }, 4);\n        }\n\n        return req;\n    };\n\n    /**\n     * Just drops the config on the floor, but returns req in case\n     * the config return value is used.\n     */\n    req.config = function (cfg) {\n        return req(cfg);\n    };\n\n    /**\n     * Expose module registry for debugging and tooling\n     */\n    requirejs._defined = defined;\n\n    define = function (name, deps, callback) {\n\n        //This module may not have dependencies\n        if (!deps.splice) {\n            //deps is not an array, so probably means\n            //an object literal or factory function for\n            //the value. Adjust args.\n            callback = deps;\n            deps = [];\n        }\n\n        if (!hasProp(defined, name) && !hasProp(waiting, name)) {\n            waiting[name] = [name, deps, callback];\n        }\n    };\n\n    define.amd = {\n        jQuery: true\n    };\n}());\n\ndefine("../node_modules/almond/almond", function(){});\n\n/*! loglevel - v1.2.0 - https://github.com/pimterry/loglevel - (c) 2014 Tim Perry - licensed MIT */\n!function(a,b){"object"==typeof module&&module.exports&&"function"==typeof require?module.exports=b():"function"==typeof define&&"object"==typeof define.amd?define(\'utils/../../vendor/loglevel/dist/loglevel.min\',b):a.log=b()}(this,function(){function a(a){return typeof console===i?!1:void 0!==console[a]?b(console,a):void 0!==console.log?b(console,"log"):h}function b(a,b){var c=a[b];if("function"==typeof c.bind)return c.bind(a);try{return Function.prototype.bind.call(c,a)}catch(d){return function(){return Function.prototype.apply.apply(c,[a,arguments])}}}function c(a,b){return function(){typeof console!==i&&(d(b),g[a].apply(g,arguments))}}function d(a){for(var b=0;b<j.length;b++){var c=j[b];g[c]=a>b?h:g.methodFactory(c,a)}}function e(a){var b=(j[a]||"silent").toUpperCase();try{return void(window.localStorage.loglevel=b)}catch(c){}try{window.document.cookie="loglevel="+b+";"}catch(c){}}function f(){var a;try{a=window.localStorage.loglevel}catch(b){}if(typeof a===i)try{a=/loglevel=([^;]+)/.exec(window.document.cookie)[1]}catch(b){}void 0===g.levels[a]&&(a="WARN"),g.setLevel(g.levels[a])}var g={},h=function(){},i="undefined",j=["trace","debug","info","warn","error"];g.levels={TRACE:0,DEBUG:1,INFO:2,WARN:3,ERROR:4,SILENT:5},g.methodFactory=function(b,d){return a(b)||c(b,d)},g.setLevel=function(a){if("string"==typeof a&&void 0!==g.levels[a.toUpperCase()]&&(a=g.levels[a.toUpperCase()]),!("number"==typeof a&&a>=0&&a<=g.levels.SILENT))throw"log.setLevel() called with invalid level: "+a;return e(a),d(a),typeof console===i&&a<g.levels.SILENT?"No console available for logging":void 0},g.enableAll=function(){g.setLevel(g.levels.TRACE)},g.disableAll=function(){g.setLevel(g.levels.SILENT)};var k=typeof window!==i?window.log:void 0;return g.noConflict=function(){return typeof window!==i&&window.log===g&&(window.log=k),g},f(),g});\ndefine(\n\'utils/loggerFactory\',[\n    \'../../vendor/loglevel/dist/loglevel.min\'\n],\nfunction(log) {\n    \'use strict\';\n\n    return {\n\n        /**\n         * @alias player.loggerFactory.createLogger\n         * @memberof! player.loggerFactory#\n         * @method player.loggerFactory.createLogger\n         * @param {string} name\n         */\n        createLogger: function createLogger(name) {\n            var prefix = \'[\' + name + \'] \';\n            var enabled = true;\n            var level = 0;\n            var levelStringToInt = function(str) {\n                    var retVal = 0;\n\n                    if (typeof str !== \'string\') {\n                        return retVal;\n                    }\n\n                    switch (str.toLowerCase().trim()) {\n                        case \'enable\':\n                        case \'enabled\':\n                        case \'trace\':\n                            retVal = 0;\n                            break;\n                        case \'debug\':\n                            retVal = 1;\n                            break;\n                        case \'info\':\n                            retVal = 2;\n                            break;\n                        case \'warn\':\n                            retVal = 3;\n                            break;\n                        case \'error\':\n                            retVal = 4;\n                            break;\n                        case \'disable\':\n                        case \'disabled\':\n                            retVal = 5;\n                            break;\n                        default:\n                            break;\n                    }\n\n                    return retVal;\n                };\n\n            return {\n                trace: function trace(msg) {\n                    arguments[0] = prefix + msg;\n                    if (enabled && level <= 0) {\n                        log.trace.apply(null, arguments);\n                    }\n                },\n                debug: function debug(msg) {\n                    arguments[0] = prefix + msg;\n                    if (enabled && level <= 1) {\n                        log.debug.apply(null, arguments);\n                    }\n                },\n                info: function info(msg) {\n                    arguments[0] = prefix + msg;\n                    if (enabled && level <= 2) {\n                        log.info.apply(null, arguments);\n                    }\n                },\n                warn: function warn(msg) {\n                    arguments[0] = prefix + msg;\n                    if (enabled && level <= 3) {\n                        log.warn.apply(null, arguments);\n                    }\n                },\n                error: function error(msg) {\n                    arguments[0] = prefix + msg;\n                    if (enabled && level <= 4) {\n                        log.error.apply(null, arguments);\n                    }\n                },\n                /* Simple implementation for setting the level of only this instance. */\n                setLevel: function setLevel(levelStr) {\n                    level = levelStringToInt(levelStr);\n                },\n                /* Getter for int level. */\n                getIntLevel: function getIntLevel() {\n                    return level;\n                },\n                /* Disable logging only for this instnace. */\n                disable: function disable() {\n                    enabled = false;\n                },\n                /* Enable logging only for this instance. Affected by global state of logging. */\n                enable: function enable() {\n                    enabled = true;\n                }\n            };\n        },\n\n        /**\n         * @alias player.loggerFactory.enableAll\n         * @memberof! player.loggerFactory#\n         * @method player.loggerFactory.enableAll\n         */\n        enableAll: function enableAll() {\n            log.enableAll();\n        },\n\n        /**\n         * @alias player.loggerFactory.disableAll\n         * @memberof! player.loggerFactory#\n         * @method player.loggerFactory.disableAll\n         */\n        disableAll: function disableAll() {\n            log.disableAll();\n        },\n\n        /**\n         * @alias player.loggerFactory.setLevel\n         * @memberof! player.loggerFactory#\n         * @method player.loggerFactory.setLevel\n         * @param {string} level\n         */\n        setLevel: function setLevel(level) {\n            log.setLevel(level);\n        }\n    };\n});\n\n/* jshint ignore:start */\n// jscs:disable\ndefine(\'mseUtils/mseUtils\',[\n],\n\n/**\n* Redefine the src/utils/utils module without the \'when\' dependancy.\n* This is used from within the mseWorker context (since \'when\' is not used in worker, using this utils module instead reduces compiled size of Worker)\n*/\nfunction() {\n    \'use strict\';\n\n    var gist = (function() {\n\n        var _class2type = {};\n\n        var _type = function( obj ) {\n            return obj == null ?\n                String( obj ) :\n                _class2type[ toString.call(obj) ] || "object";\n        };\n\n        var _isWindow = function( obj ) {\n            return obj != null && obj == obj.window;\n        };\n\n        var _isFunction = function(target){\n            return toString.call(target) === "[object Function]";\n        };\n\n        var _isArray =  Array.isArray || function( obj ) {\n            return _type(obj) === "array";\n        };\n\n        var _isPlainObject = function( obj ) {\n            // Must be an Object.\n            // Because of IE, we also have to check the presence of the constructor property.\n            // Make sure that DOM nodes and window objects don\'t pass through, as well\n            if ( !obj || _type(obj) !== "object" || obj.nodeType || _isWindow( obj ) ) {\n                return false;\n            }\n\n            try {\n                // Not own constructor property must be Object\n                if ( obj.constructor &&\n                    !hasOwn.call(obj, "constructor") &&\n                    !hasOwn.call(obj.constructor.prototype, "isPrototypeOf") ) {\n                    return false;\n                }\n            } catch ( e ) {\n                // IE8,9 Will throw exceptions on certain host objects #9897\n                return false;\n            }\n\n            // Own properties are enumerated firstly, so to speed up,\n            // if last one is own, then all properties are own.\n\n            var key;\n            for ( key in obj ) {}\n\n            return key === undefined || hasOwn.call( obj, key );\n        };\n\n        var _extend = function() {\n            var options, name, src, copy, copyIsArray, clone,\n                target = arguments[0] || {},\n                i = 1,\n                length = arguments.length,\n                deep = false;\n\n            // Handle a deep copy situation\n            if ( typeof target === "boolean" ) {\n                deep = target;\n                target = arguments[1] || {};\n                // skip the boolean and the target\n                i = 2;\n            }\n\n            // Handle case when target is a string or something (possible in deep copy)\n            if ( typeof target !== "object" && !_isFunction(target) ) {\n                target = {};\n            }\n\n            if ( length === i ) {\n                target = this;\n                --i;\n            }\n\n            for ( ; i < length; i++ ) {\n                // Only deal with non-null/undefined values\n                if ( (options = arguments[ i ]) != null ) {\n                    // Extend the base object\n                    for ( name in options ) {\n                        src = target[ name ];\n                        copy = options[ name ];\n\n                        // Prevent never-ending loop\n                        if ( target === copy ) {\n                            continue;\n                        }\n\n                        // Recurse if we\'re merging plain objects or arrays\n                        if ( deep && copy && ( _isPlainObject(copy) || (copyIsArray = _isArray(copy)) ) ) {\n                            if ( copyIsArray ) {\n                                copyIsArray = false;\n                                clone = src && _isArray(src) ? src : [];\n                            } else {\n                                clone = src && _isPlainObject(src) ? src : {};\n                            }\n\n                            // Never move original objects, clone them\n                            target[ name ] = _extend( deep, clone, copy );\n\n                            // Don\'t bring in undefined values\n                        } else if ( copy !== undefined ) {\n                            target[ name ] = copy;\n                        }\n                    }\n                }\n            }\n\n            // Return the modified object\n            return target;\n        };\n\n        return {\n            class2type: _class2type,\n            type: _type,\n            isWindow: _isWindow,\n            isFunction: _isFunction,\n            isArray: _isArray,\n            isPlainObject: _isPlainObject,\n            extend: _extend\n        };\n    }());\n\n    function createMethod(methodName) {\n        return function() {\n            console.warn(\'The "\' + methodName + \'" method is not available.\');\n        };\n    }\n\n    return {\n        isWindow: gist.isWindow,\n        isFunction: gist.isFunction,\n        isArray: gist.isArray,\n        isPlainObject: gist.isPlainObject,\n        extend: gist.extend,\n        createMethod: createMethod\n    };\n});\ndefine(\'mseUtils\', [\'mseUtils/mseUtils\'], function (main) { return main; });\n\ndefine(\'engine/mse/workers/WorkerMsg\',[\n],\n\n/**\n* This module defines a generic Worker message structure.\n*/\nfunction() {\n    \'use strict\';\n\n    function WorkerMsg(worker, name, data, arrayBuffer) {\n        var msg = {\n            name: name,\n            data: data,\n            arrayBuffer: arrayBuffer\n        };\n\n        this.post = function() {\n            if (msg.arrayBuffer) {\n                worker.postMessage(msg, [msg.arrayBuffer]);\n            } else {\n                worker.postMessage(msg);\n            }\n        };\n    }\n\n    return WorkerMsg;\n});\n\ndefine(\'engine/mse/utils/extensions\',[\n],\n\n/**\n* This module defines some extensions for basic types.\n* Simply include it early in loading hierarchy for it to take effect.\n*/\nfunction() {\n    \'use strict\';\n\n    /** Check if an object defines a function. */\n    Object.defineProperty(Object.prototype, \'can\', {\n        enumerable: false,\n        value: function(method) {\n            return (typeof this[method] === \'function\');\n        }\n    });\n\n    /** Check if an object conforms/implements an interface. */\n    Object.defineProperty(Object.prototype, \'conforms\', {\n        enumerable: false,\n        value: function(interfaceObj) {\n            var retVal = true;\n            var prop;\n\n            for (prop in interfaceObj) {\n                if (typeof this[prop] !== typeof interfaceObj[prop]) {\n                    retVal = false;\n                    break;\n                }\n            }\n\n            return retVal;\n        }\n    });\n\n    /** Add the endsWith function to String\'s prototype. */\n    if (typeof String.prototype.endsWith !== \'function\') {\n        String.prototype.endsWith = function(suffix) {\n            return this.indexOf(suffix, this.length - suffix.length) !== -1;\n        };\n    }\n\n    /** Add the startsWith function to String\'s prototype. */\n    if (typeof String.prototype.startsWith !== \'function\') {\n        String.prototype.startsWith = function(prefix) {\n            return this.indexOf(prefix) === 0;\n        };\n    }\n});\n\ndefine(\'engine/mse/interfaces/IDisposable\',[\n    \'mseUtils\'\n],\n\n/**\n* Disposable interface.\n*/\nfunction(utils) {\n    \'use strict\';\n\n    var createMethod = utils.createMethod;\n\n    return {\n        dispose: createMethod(\'dispose\')\n    };\n});\n\ndefine(\'engine/mse/interfaces/ILoadingManager\',[\n    \'mseUtils\',\n    \'engine/mse/interfaces/IDisposable\'\n],\n\n/**\n* ILoadingManager interface.\n*/\nfunction(utils, IDisposable) {\n    \'use strict\';\n\n    var ILoadingManager = {\n    };\n\n    return utils.extend(ILoadingManager, IDisposable);\n});\n\n/*!\n * EventEmitter v4.2.11 - git.io/ee\n * Unlicense - http://unlicense.org/\n * Oliver Caldwell - http://oli.me.uk/\n * @preserve\n */\n(function(){"use strict";function t(){}function i(t,n){for(var e=t.length;e--;)if(t[e].listener===n)return e;return-1}function n(e){return function(){return this[e].apply(this,arguments)}}var e=t.prototype,r=this,s=r.EventEmitter;e.getListeners=function(n){var r,e,t=this._getEvents();if(n instanceof RegExp){r={};for(e in t)t.hasOwnProperty(e)&&n.test(e)&&(r[e]=t[e])}else r=t[n]||(t[n]=[]);return r},e.flattenListeners=function(t){var e,n=[];for(e=0;e<t.length;e+=1)n.push(t[e].listener);return n},e.getListenersAsObject=function(n){var e,t=this.getListeners(n);return t instanceof Array&&(e={},e[n]=t),e||t},e.addListener=function(r,e){var t,n=this.getListenersAsObject(r),s="object"==typeof e;for(t in n)n.hasOwnProperty(t)&&-1===i(n[t],e)&&n[t].push(s?e:{listener:e,once:!1});return this},e.on=n("addListener"),e.addOnceListener=function(e,t){return this.addListener(e,{listener:t,once:!0})},e.once=n("addOnceListener"),e.defineEvent=function(e){return this.getListeners(e),this},e.defineEvents=function(t){for(var e=0;e<t.length;e+=1)this.defineEvent(t[e]);return this},e.removeListener=function(r,s){var n,e,t=this.getListenersAsObject(r);for(e in t)t.hasOwnProperty(e)&&(n=i(t[e],s),-1!==n&&t[e].splice(n,1));return this},e.off=n("removeListener"),e.addListeners=function(e,t){return this.manipulateListeners(!1,e,t)},e.removeListeners=function(e,t){return this.manipulateListeners(!0,e,t)},e.manipulateListeners=function(r,t,i){var e,n,s=r?this.removeListener:this.addListener,o=r?this.removeListeners:this.addListeners;if("object"!=typeof t||t instanceof RegExp)for(e=i.length;e--;)s.call(this,t,i[e]);else for(e in t)t.hasOwnProperty(e)&&(n=t[e])&&("function"==typeof n?s.call(this,e,n):o.call(this,e,n));return this},e.removeEvent=function(e){var t,r=typeof e,n=this._getEvents();if("string"===r)delete n[e];else if(e instanceof RegExp)for(t in n)n.hasOwnProperty(t)&&e.test(t)&&delete n[t];else delete this._events;return this},e.removeAllListeners=n("removeEvent"),e.emitEvent=function(r,o){var e,i,t,s,n=this.getListenersAsObject(r);for(t in n)if(n.hasOwnProperty(t))for(i=n[t].length;i--;)e=n[t][i],e.once===!0&&this.removeListener(r,e.listener),s=e.listener.apply(this,o||[]),s===this._getOnceReturnValue()&&this.removeListener(r,e.listener);return this},e.trigger=n("emitEvent"),e.emit=function(e){var t=Array.prototype.slice.call(arguments,1);return this.emitEvent(e,t)},e.setOnceReturnValue=function(e){return this._onceReturnValue=e,this},e._getOnceReturnValue=function(){return this.hasOwnProperty("_onceReturnValue")?this._onceReturnValue:!0},e._getEvents=function(){return this._events||(this._events={})},t.noConflict=function(){return r.EventEmitter=s,t},"function"==typeof define&&define.amd?define(\'utils/../../vendor/eventemitter/EventEmitter.min\',[],function(){return t}):"object"==typeof module&&module.exports?module.exports=t:r.EventEmitter=t}).call(this);\ndefine(\'utils/EventEmitter\',[\n    \'../../vendor/eventemitter/EventEmitter.min\'\n],\n\n/**\n * A wrapper for Wolfy87\'s EventEmitter.\n * Makes minor changes to EventEmitter\'s API, most notably, trigger() will take 1...N arguments.\n */\nfunction(EE) {\n    \'use strict\';\n\n    var EventEmitter = function() {\n        // Call super constructor\n        EE.apply(this, arguments);\n    };\n\n    EventEmitter.prototype = Object.create(EE.prototype);\n    EventEmitter.prototype.constructor = EventEmitter;\n    EventEmitter.prototype.on = EE.prototype.on;\n    EventEmitter.prototype.off = EE.prototype.off;\n    EventEmitter.prototype.one = EE.prototype.once;\n    EventEmitter.prototype.trigger = EE.prototype.emit;\n\n    return EventEmitter;\n});\n\ndefine(\'engine/mse/interfaces/IParsingManager\',[\n    \'mseUtils\',\n    \'engine/mse/interfaces/IDisposable\',\n    \'utils/EventEmitter\'\n],\n\n/**\n* IParsingManager interface.\n*/\nfunction(utils, IDisposable, IEventEmitter) {\n    \'use strict\';\n\n    var IParsingManager = {\n        /*\n            The parsing manager is expected to trigger the following events:\n\n            - setTimestampOffset    (arguments: newTimestampOffset)\n            - dataAvailable         (arguments: arrayBuffer, data)\n            - segmentMetadata       (arguments: segmentId, segmentMetadata)\n            - requestTimeUpdate     (arguments: N/A)\n            - channelSwitched       (arguments: timestampOffset)\n        */\n    };\n\n    return utils.extend(IParsingManager, IDisposable, IEventEmitter);\n});\n\ndefine(\'engine/mse/interfaces/IMemoryManager\',[\n    \'mseUtils\',\n    \'engine/mse/interfaces/IDisposable\'\n],\n\n/**\n* ILoadingManager interface.\n*/\nfunction(utils, IDisposable) {\n    \'use strict\';\n\n    var IMemoryManager = {\n    };\n\n    return utils.extend(IMemoryManager, IDisposable);\n});\n\ndefine(\'engine/mse/controllers/BaseManager\',[\n    \'utils/loggerFactory\',\n    \'mseUtils\',\n    \'utils/EventEmitter\'\n],\n\n/*\n *\n */\nfunction(loggerFactory, utils, EventEmitter) {\n    \'use strict\';\n\n    var log = loggerFactory.createLogger(\'BaseManager\');\n    log.setLevel(\'trace\');\n\n    /////////////////////////////////////////////////////////////////////////////\n    // Constructor\n    /////////////////////////////////////////////////////////////////////////////\n\n    var BaseManager = function(iSettings) {\n        this.binaryStore = iSettings.binaryStore;\n        this.workerState = iSettings.workerState;\n        this.currentlyHandling = {};\n\n        // Autobinding\n        for (var key in this) {\n            if (typeof this[key] === \'function\') {\n                this[key] = this[key].bind(this);\n            }\n        }\n\n        // Add listeners\n        this.addWorkerStateListeners();\n        this.on(\'handler.complete\', this.onHandlerComplete);\n    };\n\n    /////////////////////////////////////////////////////////////////////////////\n    // Methods to be overriden by extending classes\n    /////////////////////////////////////////////////////////////////////////////\n\n    BaseManager.prototype._pauseNodeHandling = function(nodeId) { // jshint ignore:line\n    };\n\n    BaseManager.prototype._isNodeHandled = function(nodeId) { // jshint ignore:line\n    };\n\n    BaseManager.prototype._doHandleNode = function(nodeId) { // jshint ignore:line\n    };\n\n    BaseManager.prototype._dispose = function() {\n    };\n\n    /////////////////////////////////////////////////////////////////////////////\n    // Methods\n    /////////////////////////////////////////////////////////////////////////////\n\n    BaseManager.prototype.onHandlerComplete = function(nodeId) {\n        delete this.currentlyHandling[nodeId];\n\n        // If playlist is entirely handled, begin/resume handling graph\n        if (this.isPlaylistHandled()) {\n            this.handleChildren(nodeId);\n        }\n        // Otherwise, let\'s handle the next not-yet-handled node in playlist\n        else {\n            this.handleNextPlaylistNode();\n        }\n    };\n\n    BaseManager.prototype.isNodeHandled = function(nodeId) {\n        return this._isNodeHandled(nodeId);\n    };\n\n    BaseManager.prototype.handleNode = function(nodeId) {\n        this.currentlyHandling[nodeId] = true;\n        return this._doHandleNode(nodeId);\n    };\n\n    BaseManager.prototype.handleChildren = function(nodeId) {\n        var children = this.workerState.nodeIdToChildren(nodeId);\n        for (var i = 0; i < children.length; i++) {\n            this.handleNode(children[i]);\n        }\n    };\n\n    BaseManager.prototype.nextUnhandledPlaylistNode = function() {\n        var retVal = null;\n\n        for (var i = this.workerState.appendingIndex; i < this.workerState.playlist.length; i++) {\n            if (!this.isNodeHandled(this.workerState.playlist[i])) {\n                retVal = this.workerState.playlist[i];\n                break;\n            }\n        }\n\n        return retVal;\n    };\n\n    BaseManager.prototype.isPlaylistHandled = function() {\n        return this.nextUnhandledPlaylistNode() === null;\n    };\n\n    BaseManager.prototype.handleNextPlaylistNode = function() {\n        var nodeId = this.nextUnhandledPlaylistNode();\n        if (nodeId === null) {\n            return false;\n        }\n\n        this.handleNode(nodeId);\n        return true;\n    };\n\n    BaseManager.prototype.getUnhandledDescendantsOfFirstOrder = function(nodeId) {\n        var retVal = {};\n        var visited = {};\n\n        var recursion = function(currNodeId) {\n            var children = this.workerState.nodeIdToChildren(currNodeId);\n            var child;\n\n            for (var i = 0; i < children.length; i++) {\n                child = children[i];\n                if (!visited[child]) {\n                    visited[child] = true;\n                    if (!this.isNodeHandled(child)) {\n                        retVal[child] = true;\n                    } else {\n                        recursion(child);\n                    }\n                }\n            }\n        }.bind(this);\n\n        recursion(nodeId);\n\n        return retVal;\n    };\n\n    BaseManager.prototype.pauseAllCurrentlyHandling = function() {\n        for (var nodeId in this.currentlyHandling) {\n            this._pauseNodeHandling(nodeId);\n        }\n    };\n\n    BaseManager.prototype.resumeAllCurrentlyHandling = function() {\n        for (var nodeId in this.currentlyHandling) {\n            this.handleNode(nodeId);\n        }\n    };\n\n    BaseManager.prototype.resetAllCurrentlyHandling = function() {\n        if (!this.isPlaylistHandled()) {\n            this.currentlyHandling = {};\n            this.currentlyHandling[this.nextUnhandledPlaylistNode()] = true;\n        } else if (this.workerState.playlist.length) {\n            this.currentlyHandling = this.getUnhandledDescendantsOfFirstOrder(this.workerState.playlist[this.workerState.playlist.length - 1]);\n        } else {\n            this.currentlyHandling = {};\n        }\n    };\n\n    BaseManager.prototype.restartHandling = function() {\n        this.pauseAllCurrentlyHandling();\n        this.resetAllCurrentlyHandling();\n        this.resumeAllCurrentlyHandling();\n    };\n\n    BaseManager.prototype.onPlaylistAppend = function() {\n        this.restartHandling();\n    };\n\n    BaseManager.prototype.onPlaylistReset = function() {\n        this.restartHandling();\n    };\n\n    BaseManager.prototype.onRepoUpdated = function() {\n        this.restartHandling();\n    };\n\n    BaseManager.prototype.onWorkerStateDispose = function() {\n        this.removeWorkerStateListeners();\n    };\n\n    BaseManager.prototype.addWorkerStateListeners = function() {\n        if (this.workerState) {\n            this.workerState.on(\'workerstate.repo.updated\', this.onRepoUpdated);\n            this.workerState.on(\'workerstate.playlist.reset\', this.onPlaylistReset);\n            this.workerState.on(\'workerstate.playlist.append\', this.onPlaylistAppend);\n            this.workerState.on(\'workerstate.dispose\', this.onWorkerStateDispose);\n        }\n    };\n\n    BaseManager.prototype.removeWorkerStateListeners = function() {\n        if (this.workerState) {\n            this.workerState.off(\'workerstate.repo.updated\', this.onRepoUpdated);\n            this.workerState.off(\'workerstate.playlist.reset\', this.onPlaylistReset);\n            this.workerState.off(\'workerstate.playlist.append\', this.onPlaylistAppend);\n            this.workerState.off(\'workerstate.dispose\', this.onWorkerStateDispose);\n        }\n    };\n\n    BaseManager.prototype.dispose = function() {\n        this._dispose();\n        this.removeWorkerStateListeners();\n        this.off(\'handler.complete\', this.onHandlerComplete);\n    };\n\n    // Add the EventEmitter functionality\n    utils.extend(BaseManager.prototype, EventEmitter.prototype);\n\n    return BaseManager;\n});\n\ndefine(\'engine/mse/controllers/LoadingManager\',[\n    \'utils/loggerFactory\',\n    \'engine/mse/controllers/BaseManager\'\n],\n\n/**\n* This module handles the video loading logic of a project.\n*/\nfunction(loggerFactory, BaseManager) {\n    \'use strict\';\n\n    var log = loggerFactory.createLogger(\'LoadingManager\');\n    log.setLevel(\'warn\');\n\n    function LoadingManager() {\n        BaseManager.apply(this, arguments);\n\n        // Add listeners\n        this.workerState.on(\'workerstate.appendingIndexIncremented\', this.onAppendingIndexIncremented);\n    }\n\n    // Extend the base class SimpleImcSegmentParser\n    LoadingManager.prototype = Object.create(BaseManager.prototype);\n    LoadingManager.prototype.constructor = LoadingManager;\n\n    LoadingManager.prototype._pauseNodeHandling = function(nodeId) {\n        log.debug(\'[\' + nodeId + \'] Pausing load\');\n        this.binaryStore.getByNodeId(nodeId).pause();\n    };\n\n    LoadingManager.prototype._isNodeHandled = function(nodeId) {\n        return this.binaryStore.getByNodeId(nodeId).completed;\n    };\n\n    LoadingManager.prototype._doHandleNode = function(nodeId) {\n        var binLoader = this.binaryStore.getByNodeId(nodeId);\n\n        if (!binLoader.loadStarted) {\n            log.debug(\'[\' + nodeId + \'] Starting load\');\n            binLoader.one(\'complete\', function() {\n                log.debug(\'[\' + nodeId + \'] Load complete\');\n                this.trigger(\'handler.complete\', nodeId);\n            }.bind(this));\n            binLoader.load();\n        } else if (binLoader.paused) {\n            log.debug(\'[\' + nodeId + \'] Resuming load\');\n            binLoader.resume();\n        }\n    };\n\n    LoadingManager.prototype._dispose = function() {\n        // Remove listeners\n        if (this.workerState) {\n            this.workerState.off(\'workerstate.appendingIndexIncremented\', this.onAppendingIndexIncremented);\n        }\n    };\n\n    LoadingManager.prototype.onAppendingIndexIncremented = function() {\n        this.restartHandling();\n    };\n\n    return LoadingManager;\n});\n\n!function(a){var b=this;"object"==typeof exports?module.exports=a(b):"function"==typeof define&&define.amd?define(\'jdataview/jdataview\',[],function(){return a(b)}):b.jDataView=a(b)}(function(a){"use strict";function b(a,b){return"object"!=typeof a||null===a?!1:a.constructor===b||Object.prototype.toString.call(a)==="[object "+b.name+"]"}function c(a,c){return!c&&b(a,Array)?a:Array.prototype.slice.call(a)}function d(a,b){return void 0!==a?a:b}function e(a,c,f,g){if(e.is(a)){var h=a.slice(c,c+f);return h._littleEndian=d(g,h._littleEndian),h}if(!e.is(this))return new e(a,c,f,g);if(this.buffer=a=e.wrapBuffer(a),this._isArrayBuffer=j.ArrayBuffer&&b(a,ArrayBuffer),this._isPixelData=!0&&j.PixelData&&b(a,CanvasPixelArray),this._isDataView=j.DataView&&this._isArrayBuffer,this._isNodeBuffer=!1,!this._isArrayBuffer&&!this._isPixelData&&!b(a,Array))throw new TypeError("jDataView buffer has an incompatible type");this._littleEndian=!!g;var i="byteLength"in a?a.byteLength:a.length;this.byteOffset=c=d(c,0),this.byteLength=f=d(f,i-c),this._offset=this._bitOffset=0,this._isDataView?this._view=new DataView(a,c,f):this._checkBounds(c,f,i),this._engineAction=this._isDataView?this._dataViewAction:this._isArrayBuffer?this._arrayBufferAction:this._arrayAction}function f(a){for(var b=j.ArrayBuffer?Uint8Array:Array,c=new b(a.length),d=0,e=a.length;e>d;d++)c[d]=255&a.charCodeAt(d);return c}function g(a){return a>=0&&31>a?1<<a:g[a]||(g[a]=Math.pow(2,a))}function h(a,b){this.lo=a,this.hi=b}function i(){h.apply(this,arguments)}var j={NodeBuffer:!1,DataView:"DataView"in a,ArrayBuffer:"ArrayBuffer"in a,PixelData:!0&&"CanvasPixelArray"in a&&!("Uint8ClampedArray"in a)&&"document"in a},k=a.TextEncoder,l=a.TextDecoder;if(j.PixelData)var m=document.createElement("canvas").getContext("2d"),n=function(a,b){var c=m.createImageData((a+3)/4,1).data;if(c.byteLength=a,void 0!==b)for(var d=0;a>d;d++)c[d]=b[d];return c};var o={Int8:1,Int16:2,Int32:4,Uint8:1,Uint16:2,Uint32:4,Float32:4,Float64:8};e.wrapBuffer=function(a){switch(typeof a){case"number":if(j.ArrayBuffer)a=new Uint8Array(a).buffer;else if(j.PixelData)a=n(a);else{a=new Array(a);for(var d=0;d<a.length;d++)a[d]=0}return a;case"string":a=f(a);default:return"length"in a&&!(j.ArrayBuffer&&b(a,ArrayBuffer)||j.PixelData&&b(a,CanvasPixelArray))&&(j.ArrayBuffer?b(a,ArrayBuffer)||(a=new Uint8Array(a).buffer,b(a,ArrayBuffer)||(a=new Uint8Array(c(a,!0)).buffer)):a=j.PixelData?n(a.length,a):c(a)),a}},e.is=function(a){return a&&a.jDataView},e.from=function(){return new e(arguments)},e.Uint64=h,h.prototype={valueOf:function(){return this.lo+g(32)*this.hi},toString:function(){return Number.prototype.toString.apply(this.valueOf(),arguments)}},h.fromNumber=function(a){var b=Math.floor(a/g(32)),c=a-b*g(32);return new h(c,b)},e.Int64=i,i.prototype="create"in Object?Object.create(h.prototype):new h,i.prototype.valueOf=function(){return this.hi<g(31)?h.prototype.valueOf.apply(this,arguments):-(g(32)-this.lo+g(32)*(g(32)-1-this.hi))},i.fromNumber=function(a){var b,c;if(a>=0){var d=h.fromNumber(a);b=d.lo,c=d.hi}else c=Math.floor(a/g(32)),b=a-c*g(32),c+=g(32);return new i(b,c)};var p=e.prototype={compatibility:j,jDataView:!0,_checkBounds:function(a,b,c){if("number"!=typeof a)throw new TypeError("Offset is not a number.");if("number"!=typeof b)throw new TypeError("Size is not a number.");if(0>b)throw new RangeError("Length is negative.");if(0>a||a+b>d(c,this.byteLength))throw new RangeError("Offsets are out of bounds.")},_action:function(a,b,c,e,f){return this._engineAction(a,b,d(c,this._offset),d(e,this._littleEndian),f)},_dataViewAction:function(a,b,c,d,e){return this._offset=c+o[a],b?this._view["get"+a](c,d):this._view["set"+a](c,e,d)},_arrayBufferAction:function(b,c,e,f,g){var h,i=o[b],j=a[b+"Array"];if(f=d(f,this._littleEndian),1===i||(this.byteOffset+e)%i===0&&f)return h=new j(this.buffer,this.byteOffset+e,1),this._offset=e+i,c?h[0]:h[0]=g;var k=new Uint8Array(c?this.getBytes(i,e,f,!0):i);return h=new j(k.buffer,0,1),c?h[0]:(h[0]=g,void this._setBytes(e,k,f))},_arrayAction:function(a,b,c,d,e){return b?this["_get"+a](c,d):this["_set"+a](c,e,d)},_getBytes:function(a,b,e){e=d(e,this._littleEndian),b=d(b,this._offset),a=d(a,this.byteLength-b),this._checkBounds(b,a),b+=this.byteOffset,this._offset=b-this.byteOffset+a;var f=this._isArrayBuffer?new Uint8Array(this.buffer,b,a):(this.buffer.slice||Array.prototype.slice).call(this.buffer,b,b+a);return e||1>=a?f:c(f).reverse()},getBytes:function(a,b,e,f){var g=this._getBytes(a,b,d(e,!0));return f?c(g):g},_setBytes:function(a,b,e){var f=b.length;if(0!==f){if(e=d(e,this._littleEndian),a=d(a,this._offset),this._checkBounds(a,f),!e&&f>1&&(b=c(b,!0).reverse()),a+=this.byteOffset,this._isArrayBuffer)new Uint8Array(this.buffer,a,f).set(b);else for(var g=0;f>g;g++)this.buffer[a+g]=b[g];this._offset=a-this.byteOffset+f}},setBytes:function(a,b,c){this._setBytes(a,b,d(c,!0))},getString:function(a,b,c){var d=this._getBytes(a,b,!0);if(c="utf8"===c?"utf-8":c||"binary",l&&"binary"!==c)return new l(c).decode(this._isArrayBuffer?d:new Uint8Array(d));var e="";a=d.length;for(var f=0;a>f;f++)e+=String.fromCharCode(d[f]);return"utf-8"===c&&(e=decodeURIComponent(escape(e))),e},setString:function(a,b,c){c="utf8"===c?"utf-8":c||"binary";var d;k&&"binary"!==c?d=new k(c).encode(b):("utf-8"===c&&(b=unescape(encodeURIComponent(b))),d=f(b)),this._setBytes(a,d,!0)},getChar:function(a){return this.getString(1,a)},setChar:function(a,b){this.setString(a,b)},tell:function(){return this._offset},seek:function(a){return this._checkBounds(a,0),this._offset=a},skip:function(a){return this.seek(this._offset+a)},slice:function(a,b,c){function f(a,b){return 0>a?a+b:a}return a=f(a,this.byteLength),b=f(d(b,this.byteLength),this.byteLength),c?new e(this.getBytes(b-a,a,!0,!0),void 0,void 0,this._littleEndian):new e(this.buffer,this.byteOffset+a,b-a,this._littleEndian)},alignBy:function(a){return this._bitOffset=0,1!==d(a,1)?this.skip(a-(this._offset%a||a)):this._offset},_getFloat64:function(a,b){var c=this._getBytes(8,a,b),d=1-2*(c[7]>>7),e=((c[7]<<1&255)<<3|c[6]>>4)-1023,f=(15&c[6])*g(48)+c[5]*g(40)+c[4]*g(32)+c[3]*g(24)+c[2]*g(16)+c[1]*g(8)+c[0];return 1024===e?0!==f?0/0:1/0*d:-1023===e?d*f*g(-1074):d*(1+f*g(-52))*g(e)},_getFloat32:function(a,b){var c=this._getBytes(4,a,b),d=1-2*(c[3]>>7),e=(c[3]<<1&255|c[2]>>7)-127,f=(127&c[2])<<16|c[1]<<8|c[0];return 128===e?0!==f?0/0:1/0*d:-127===e?d*f*g(-149):d*(1+f*g(-23))*g(e)},_get64:function(a,b,c){c=d(c,this._littleEndian),b=d(b,this._offset);for(var e=c?[0,4]:[4,0],f=0;2>f;f++)e[f]=this.getUint32(b+e[f],c);return this._offset=b+8,new a(e[0],e[1])},getInt64:function(a,b){return this._get64(i,a,b)},getUint64:function(a,b){return this._get64(h,a,b)},_getInt32:function(a,b){var c=this._getBytes(4,a,b);return c[3]<<24|c[2]<<16|c[1]<<8|c[0]},_getUint32:function(a,b){return this._getInt32(a,b)>>>0},_getInt16:function(a,b){return this._getUint16(a,b)<<16>>16},_getUint16:function(a,b){var c=this._getBytes(2,a,b);return c[1]<<8|c[0]},_getInt8:function(a){return this._getUint8(a)<<24>>24},_getUint8:function(a){return this._getBytes(1,a)[0]},_getBitRangeData:function(a,b){var c=(d(b,this._offset)<<3)+this._bitOffset,e=c+a,f=c>>>3,g=e+7>>>3,h=this._getBytes(g-f,f,!0),i=0;(this._bitOffset=7&e)&&(this._bitOffset-=8);for(var j=0,k=h.length;k>j;j++)i=i<<8|h[j];return{start:f,bytes:h,wideValue:i}},getSigned:function(a,b){var c=32-a;return this.getUnsigned(a,b)<<c>>c},getUnsigned:function(a,b){var c=this._getBitRangeData(a,b).wideValue>>>-this._bitOffset;return 32>a?c&~(-1<<a):c},_setBinaryFloat:function(a,b,c,d,e){var f,h,i=0>b?1:0,j=~(-1<<d-1),k=1-j;0>b&&(b=-b),0===b?(f=0,h=0):isNaN(b)?(f=2*j+1,h=1):1/0===b?(f=2*j+1,h=0):(f=Math.floor(Math.log(b)/Math.LN2),f>=k&&j>=f?(h=Math.floor((b*g(-f)-1)*g(c)),f+=j):(h=Math.floor(b/g(k-c)),f=0));for(var l=[];c>=8;)l.push(h%256),h=Math.floor(h/256),c-=8;for(f=f<<c|h,d+=c;d>=8;)l.push(255&f),f>>>=8,d-=8;l.push(i<<d|f),this._setBytes(a,l,e)},_setFloat32:function(a,b,c){this._setBinaryFloat(a,b,23,8,c)},_setFloat64:function(a,b,c){this._setBinaryFloat(a,b,52,11,c)},_set64:function(a,b,c,e){"object"!=typeof c&&(c=a.fromNumber(c)),e=d(e,this._littleEndian),b=d(b,this._offset);var f=e?{lo:0,hi:4}:{lo:4,hi:0};for(var g in f)this.setUint32(b+f[g],c[g],e);this._offset=b+8},setInt64:function(a,b,c){this._set64(i,a,b,c)},setUint64:function(a,b,c){this._set64(h,a,b,c)},_setUint32:function(a,b,c){this._setBytes(a,[255&b,b>>>8&255,b>>>16&255,b>>>24],c)},_setUint16:function(a,b,c){this._setBytes(a,[255&b,b>>>8&255],c)},_setUint8:function(a,b){this._setBytes(a,[255&b])},setUnsigned:function(a,b,c){var d=this._getBitRangeData(c,a),e=d.wideValue,f=d.bytes;e&=~(~(-1<<c)<<-this._bitOffset),e|=(32>c?b&~(-1<<c):b)<<-this._bitOffset;for(var g=f.length-1;g>=0;g--)f[g]=255&e,e>>>=8;this._setBytes(d.start,f,!0)}};for(var q in o)!function(a){p["get"+a]=function(b,c){return this._action(a,!0,b,c)},p["set"+a]=function(b,c,d){this._action(a,!1,b,d,c)}}(q);p._setInt32=p._setUint32,p._setInt16=p._setUint16,p._setInt8=p._setUint8,p.setSigned=p.setUnsigned;for(var r in p)"set"===r.slice(0,3)&&!function(a){p["write"+a]=function(){Array.prototype.unshift.call(arguments,void 0),this["set"+a].apply(this,arguments)}}(r.slice(3));return e});\n//# sourceMappingURL=jdataview.js.map;\ndefine(\'jdataview\', [\'jdataview/jdataview\'], function (main) { return main; });\n\n!function(a){var b=this;"object"==typeof exports?module.exports=a(b,require("jdataview")):"function"==typeof define&&define.amd?define(\'jbinary/jbinary\',["jdataview"],function(c){return a(b,c)}):b.jBinary=a(b,b.jDataView)}(function(a,b){"use strict";function c(a,b){return b&&a instanceof b}function d(a){for(var b=1,c=arguments.length;c>b;++b){var d=arguments[b];for(var e in d)void 0!==d[e]&&(a[e]=d[e])}return a}function e(a){return arguments[0]=m(a),d.apply(null,arguments)}function f(a,b,d){return c(d,Function)?d.call(a,b.contexts[0]):d}function g(a){return function(){var b=arguments,d=b.length-1,e=a.length-1,f=b[d];if(b.length=e+1,!c(f,Function)){var g=this;return new l(function(c,d){b[e]=function(a,b){return a?d(a):c(b)},a.apply(g,b)})}b[d]=void 0,b[e]=f,a.apply(this,b)}}function h(a,d){return c(a,h)?a.as(d):(c(a,b)||(a=new b(a,void 0,void 0,d?d["jBinary.littleEndian"]:void 0)),c(this,h)?(this.view=a,this.view.seek(0),this.contexts=[],this.as(d,!0)):new h(a,d))}function i(a){return e(i.prototype,a)}function j(a){return e(j.prototype,a,{createProperty:function(){var b=(a.createProperty||j.prototype.createProperty).apply(this,arguments);return b.getBaseType&&(b.baseType=b.binary.getType(b.getBaseType(b.binary.contexts[0]))),b}})}var k=a.document;"atob"in a&&"btoa"in a||!function(){function b(a){var b,c,e,f,g,h;for(e=a.length,c=0,b="";e>c;){if(f=255&a.charCodeAt(c++),c==e){b+=d.charAt(f>>2),b+=d.charAt((3&f)<<4),b+="==";break}if(g=a.charCodeAt(c++),c==e){b+=d.charAt(f>>2),b+=d.charAt((3&f)<<4|(240&g)>>4),b+=d.charAt((15&g)<<2),b+="=";break}h=a.charCodeAt(c++),b+=d.charAt(f>>2),b+=d.charAt((3&f)<<4|(240&g)>>4),b+=d.charAt((15&g)<<2|(192&h)>>6),b+=d.charAt(63&h)}return b}function c(a){var b,c,d,f,g,h,i;for(h=a.length,g=0,i="";h>g;){do b=e[255&a.charCodeAt(g++)];while(h>g&&-1==b);if(-1==b)break;do c=e[255&a.charCodeAt(g++)];while(h>g&&-1==c);if(-1==c)break;i+=String.fromCharCode(b<<2|(48&c)>>4);do{if(d=255&a.charCodeAt(g++),61==d)return i;d=e[d]}while(h>g&&-1==d);if(-1==d)break;i+=String.fromCharCode((15&c)<<4|(60&d)>>2);do{if(f=255&a.charCodeAt(g++),61==f)return i;f=e[f]}while(h>g&&-1==f);if(-1==f)break;i+=String.fromCharCode((3&d)<<6|f)}return i}var d="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",e=[-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,62,-1,-1,-1,63,52,53,54,55,56,57,58,59,60,61,-1,-1,-1,-1,-1,-1,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,-1,-1,-1,-1,-1,-1,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,-1,-1,-1,-1,-1];a.btoa||(a.btoa=b),a.atob||(a.atob=c)}();var l=a.Promise||function(a){this.then=a},m=Object.create;m||(m=function(a){var b=function(){};return b.prototype=a,new b});var n=h.prototype,o=n.typeSet={};n.toValue=function(a){return f(this,this,a)},n._named=function(a,b,c){return a.displayName=b+" @ "+(void 0!==c?c:this.view.tell()),a};var p=Object.defineProperty;if(p)try{p({},"x",{})}catch(q){p=void 0}else p=function(a,b,c,d){d&&(a[b]=c.value)};var r="jBinary.Cache",s=0;n._getCached=function(a,b,c){if(a.hasOwnProperty(this.cacheKey))return a[this.cacheKey];var d=b.call(this,a);return p(a,this.cacheKey,{value:d},c),d},n.getContext=function(a){switch(typeof a){case"undefined":a=0;case"number":return this.contexts[a];case"string":return this.getContext(function(b){return a in b});case"function":for(var b=0,c=this.contexts.length;c>b;b++){var d=this.contexts[b];if(a.call(this,d))return d}}},n.inContext=function(a,b){this.contexts.unshift(a);var c=b.call(this);return this.contexts.shift(),c},i.prototype={inherit:function(a,b){function c(a,b){var c=f[a];c&&(d||(d=e(f)),b.call(d,c),d[a]=null)}var d,f=this;return c("params",function(b){for(var c=0,d=b.length;d>c;c++)this[b[c]]=a[c]}),c("setParams",function(b){b.apply(this,a)}),c("typeParams",function(a){for(var c=0,d=a.length;d>c;c++){var e=a[c],f=this[e];f&&(this[e]=b(f))}}),c("resolve",function(a){a.call(this,b)}),d||f},createProperty:function(a){return e(this,{binary:a,view:a.view})},toValue:function(a,b){return b!==!1&&"string"==typeof a?this.binary.getContext(a)[a]:f(this,this.binary,a)}},h.Type=i,j.prototype=e(i.prototype,{setParams:function(){this.baseType&&(this.typeParams=["baseType"].concat(this.typeParams||[]))},baseRead:function(){return this.binary.read(this.baseType)},baseWrite:function(a){return this.binary.write(this.baseType,a)}}),d(j.prototype,{read:j.prototype.baseRead,write:j.prototype.baseWrite}),h.Template=j,n.as=function(a,b){var c=b?this:e(this);return a=a||o,c.typeSet=a===o||o.isPrototypeOf(a)?a:e(o,a),c.cacheKey=r,c.cacheKey=c._getCached(a,function(){return r+"."+ ++s},!0),c},n.seek=function(a,b){if(a=this.toValue(a),void 0!==b){var c=this.view.tell();this.view.seek(a);var d=b.call(this);return this.view.seek(c),d}return this.view.seek(a)},n.tell=function(){return this.view.tell()},n.skip=function(a,b){return this.seek(this.tell()+this.toValue(a),b)},n.slice=function(a,b,c){return new h(this.view.slice(a,b,c),this.typeSet)},n._getType=function(a,b){switch(typeof a){case"string":if(!(a in this.typeSet))throw new ReferenceError("Unknown type: "+a);return this._getType(this.typeSet[a],b);case"number":return this._getType(o.bitfield,[a]);case"object":if(c(a,i)){var d=this;return a.inherit(b||[],function(a){return d.getType(a)})}return c(a,Array)?this._getCached(a,function(a){return this.getType(a[0],a.slice(1))},!0):this._getCached(a,function(a){return this.getType(o.object,[a])},!1)}},n.getType=function(a,b){var d=this._getType(a,b);return d&&!c(a,i)&&(d.name="object"==typeof a?c(a,Array)?a[0]+"("+a.slice(1).join(", ")+")":"object":String(a)),d},n._action=function(a,b,c){if(void 0!==a){a=this.getType(a);var d=this._named(function(){return c.call(this,a.createProperty(this),this.contexts[0])},"["+a.name+"]",b);return void 0!==b?this.seek(b,d):d.call(this)}},n.read=function(a,b){return this._action(a,b,function(a,b){return a.read(b)})},n.readAll=function(){return this.read("jBinary.all",0)},n.write=function(a,b,c){return this._action(a,c,function(a,c){var d=this.tell();return a.write(b,c),this.tell()-d})},n.writeAll=function(a){return this.write("jBinary.all",a,0)},function(a,b){for(var c=0,d=b.length;d>c;c++){var f=b[c];o[f.toLowerCase()]=e(a,{dataType:f})}}(i({params:["littleEndian"],read:function(){return this.view["get"+this.dataType](void 0,this.littleEndian)},write:function(a){this.view["write"+this.dataType](a,this.littleEndian)}}),["Uint8","Uint16","Uint32","Uint64","Int8","Int16","Int32","Int64","Float32","Float64","Char"]),d(o,{"byte":o.uint8,"float":o.float32,"double":o.float64}),o.array=j({params:["baseType","length"],read:function(){var a=this.toValue(this.length);if(this.baseType===o.uint8)return this.view.getBytes(a,void 0,!0,!0);var b;if(void 0!==a){b=new Array(a);for(var c=0;a>c;c++)b[c]=this.baseRead()}else{var d=this.view.byteLength;for(b=[];this.binary.tell()<d;)b.push(this.baseRead())}return b},write:function(a){if(this.baseType===o.uint8)return this.view.writeBytes(a);for(var b=0,c=a.length;c>b;b++)this.baseWrite(a[b])}}),o.binary=j({params:["length","typeSet"],read:function(){var a=this.binary.tell(),b=this.binary.skip(this.toValue(this.length)),c=this.view.slice(a,b);return new h(c,this.typeSet)},write:function(a){this.binary.write("blob",a.read("blob",0))}}),o.bitfield=i({params:["bitSize"],read:function(){return this.view.getUnsigned(this.bitSize)},write:function(a){this.view.writeUnsigned(a,this.bitSize)}}),o.blob=i({params:["length"],read:function(){return this.view.getBytes(this.toValue(this.length))},write:function(a){this.view.writeBytes(a,!0)}}),o["const"]=j({params:["baseType","value","strict"],read:function(){var a=this.baseRead();if(this.strict&&a!==this.value){if(c(this.strict,Function))return this.strict(a);throw new TypeError("Unexpected value ("+a+" !== "+this.value+").")}return a},write:function(a){this.baseWrite(this.strict||void 0===a?this.value:a)}}),o["enum"]=j({params:["baseType","matches"],setParams:function(a,b){this.backMatches={};for(var c in b)this.backMatches[b[c]]=c},read:function(){var a=this.baseRead();return a in this.matches?this.matches[a]:a},write:function(a){this.baseWrite(a in this.backMatches?this.backMatches[a]:a)}}),o.extend=i({setParams:function(){this.parts=arguments},resolve:function(a){for(var b=this.parts,c=b.length,d=new Array(c),e=0;c>e;e++)d[e]=a(b[e]);this.parts=d},read:function(){var a=this.parts,b=this.binary.read(a[0]);return this.binary.inContext(b,function(){for(var c=1,e=a.length;e>c;c++)d(b,this.read(a[c]))}),b},write:function(a){var b=this.parts;this.binary.inContext(a,function(){for(var c=0,d=b.length;d>c;c++)this.write(b[c],a)})}}),o["if"]=j({params:["condition","trueType","falseType"],typeParams:["trueType","falseType"],getBaseType:function(){return this.toValue(this.condition)?this.trueType:this.falseType}}),o.if_not=o.ifNot=j({setParams:function(a,b,c){this.baseType=["if",a,c,b]}}),o.lazy=j({marker:"jBinary.Lazy",params:["innerType","length"],getBaseType:function(){return["binary",this.length,this.binary.typeSet]},read:function(){var a=function(b){return 0===arguments.length?"value"in a?a.value:a.value=a.binary.read(a.innerType):d(a,{wasChanged:!0,value:b}).value};return a[this.marker]=!0,d(a,{binary:d(this.baseRead(),{contexts:this.binary.contexts.slice()}),innerType:this.innerType})},write:function(a){a.wasChanged||!a[this.marker]?this.binary.write(this.innerType,a()):this.baseWrite(a.binary)}}),o.object=i({params:["structure","proto"],resolve:function(a){var b={};for(var d in this.structure)b[d]=c(this.structure[d],Function)?this.structure[d]:a(this.structure[d]);this.structure=b},read:function(){var a=this,b=this.structure,d=this.proto?e(this.proto):{};return this.binary.inContext(d,function(){for(var e in b)this._named(function(){var f=c(b[e],Function)?b[e].call(a,d):this.read(b[e]);void 0!==f&&(d[e]=f)},e).call(this)}),d},write:function(a){var b=this,d=this.structure;this.binary.inContext(a,function(){for(var e in d)this._named(function(){c(d[e],Function)?a[e]=d[e].call(b,a):this.write(d[e],a[e])},e).call(this)})}}),o.skip=i({params:["length"],read:function(){this.view.skip(this.toValue(this.length))},write:function(){this.read()}}),o.string=j({params:["length","encoding"],read:function(){return this.view.getString(this.toValue(this.length),void 0,this.encoding)},write:function(a){this.view.writeString(a,this.encoding)}}),o.string0=i({params:["length","encoding"],read:function(){var a=this.view,b=this.length;if(void 0===b){var c,d=a.tell(),e=0;for(b=a.byteLength-d;b>e&&(c=a.getUint8());)e++;var f=a.getString(e,d,this.encoding);return b>e&&a.skip(1),f}return a.getString(b,void 0,this.encoding).replace(/\\0.*$/,"")},write:function(a){var b=this.view,c=void 0===this.length?1:this.length-a.length;b.writeString(a,void 0,this.encoding),c>0&&(b.writeUint8(0),b.skip(c-1))}});h.loadData=g(function(b,d){var e;if(c(b,a.Blob)){var f;if("FileReader"in a)f=new FileReader,f.onload=f.onerror=function(){d(this.error,this.result)},f.readAsArrayBuffer(b);else{f=new FileReaderSync;var g,h;try{h=f.readAsArrayBuffer(b)}catch(i){g=i}finally{d(g,h)}}}else{if("string"!=typeof b)d(new TypeError("Unsupported source type."));else if(e=b.match(/^data:(.+?)(;base64)?,(.*)$/))try{var j=e[2],k=e[3];d(null,(j?atob:decodeURIComponent)(k))}catch(i){d(i)}else if("XMLHttpRequest"in a){var l=new XMLHttpRequest;l.open("GET",b,!0),"responseType"in l?l.responseType="arraybuffer":"overrideMimeType"in l?l.overrideMimeType("text/plain; charset=x-user-defined"):l.setRequestHeader("Accept-Charset","x-user-defined"),"onload"in l||(l.onreadystatechange=function(){4===this.readyState&&this.onload()});var m=function(a){d(new Error(a))};l.onload=function(){return 0!==this.status&&200!==this.status?m("HTTP Error #"+this.status+": "+this.statusText):("response"in this||(this.response=new VBArray(this.responseBody).toArray()),void d(null,this.response))},l.onerror=function(){m("Network error.")},l.send(null)}else d(new TypeError("Unsupported source type."))}}),h.load=g(function(a,b,c){var d=h.loadData(a);h.load.getTypeSet(a,b,function(a){d.then(function(b){c(null,new h(b,a))},c)})}),h.load.getTypeSet=function(a,b,c){c(b)},n._toURI="URL"in a&&"createObjectURL"in URL?function(a){var b=this.seek(0,function(){return this.view.getBytes()});return URL.createObjectURL(new Blob([b],{type:a}))}:function(a){var b=this.seek(0,function(){return this.view.getString(void 0,void 0,"binary")});return"data:"+a+";base64,"+btoa(b)},n._mimeType=function(a){return a||this.typeSet["jBinary.mimeType"]||"application/octet-stream"},n.toURI=function(a){return this._toURI(this._mimeType(a))};if(k){var t=h.downloader=k.createElement("a");t.style.display="none"}return n.saveAs=g(function(a,b,c){if("string"==typeof a){"msSaveBlob"in navigator?navigator.msSaveBlob(new Blob([this.read("blob",0)],{type:this._mimeType(b)}),a):k?(t.parentNode||k.body.appendChild(t),t.href=this.toURI(b),t.download=a,t.click(),t.href=t.download=""):c(new TypeError("Saving from Web Worker is not supported.")),c()}else c(new TypeError("Unsupported storage type."))}),h});\n//# sourceMappingURL=jbinary.js.map;\ndefine(\'jbinary\', [\'jbinary/jbinary\'], function (main) { return main; });\n\n/*jslint bitwise: true */\n\ndefine(\'engine/mse/formats/webm/ebmlutils\',[\n    \'jbinary\'\n],\n\n/**\n* This module defines some utility functions for use with EBML parsing.\n*/\nfunction(JBinary) {\n    \'use strict\';\n\n    var dateMillisecondsOffset = new Date(\'2001-01-01 00:00:00 UTC\') - new Date(0);\n\n    return {\n\n        /**\n        * Turns an array of bytes into its EBML style hex string representation.\n        * For example, for input array [26, 69, 223, 163] this function will return the string: \'[1A][45][DF][A3]\'\n        *\n        * @param {Array} iByteArray - Array of bytes to be converted to string.\n        * @returns {String} - EBML style hex string representation of input bytes. If unsuccessful, returns input param.\n        */\n        byteArrayToHexString: function(iByteArray) {\n            var retVal = iByteArray;\n\n            if (this.isByteArray(iByteArray))\n            {\n                retVal = \'\';\n\n                for (var i = 0; i < iByteArray.length; i++) {\n                    var currVal = iByteArray[i].toString(16).toUpperCase();\n                    if (currVal.length == 1) {\n                        currVal = \'0\' + currVal;\n                    }\n                    retVal += \'[\' + currVal + \']\';\n                }\n            }\n\n            return retVal;\n        },\n\n        /**\n        * The reverse of byteArrayToHexString.\n        * Turns an EBML style hex string to its corresponding byte array.\n        * For example, for input string \'[1A][45][DF][A3]\' this function will return the array: [26, 69, 223, 163]\n        *\n        * @param {String} iHexString - EBML style hex string representation of bytes.\n        * @returns {Array} - Array of bytes. If unsuccessful, returns input param.\n        */\n        hexStringToByteArray: function(iHexString) {\n            var retVal = iHexString;\n\n            if (this.isHexString(iHexString)) {\n                retVal = [];\n                var i = 1;\n\n                while (i < iHexString.length) {\n                    retVal = retVal.concat(parseInt(iHexString.substr(i, 2), 16));\n                    i += 4;\n                }\n            }\n\n            return retVal;\n        },\n\n        /**\n        * Checks whether input param is indeed an EBML style hex string.\n        *\n        * @param {String} iHexString - EBML style hex string representation of bytes.\n        * @returns {Boolean} - True if input param is an EBML hex string, false otherwise.\n        */\n        isHexString: function(iHexString) {\n            var retVal = (typeof iHexString === \'string\');\n\n            if (retVal) {\n                for (var i = 0; i < iHexString.length; i++) {\n                    if (i % 4 === 0) {\n                        if (iHexString.charAt(i) != \'[\') {\n                            retVal = false;\n                            break;\n                        }\n                    } else if (i % 4 === 1 || i % 4 === 2) {\n                        if (!(/^[0-9A-F]$/i.test(iHexString.charAt(i)))) {\n                            retVal = false;\n                            break;\n                        }\n                    } else if (i % 4 === 3) {\n                        if (iHexString.charAt(i) != \']\') {\n                            retVal = false;\n                            break;\n                        }\n                    }\n                }\n            }\n\n            return retVal;\n        },\n\n        /**\n        * Checks whether input param is a valid array of bytes.\n        *\n        * @param {Array} iByteArray - Array of bytes.\n        * @returns {Boolean} - True if input param is an array of bytes, false otherwise.\n        */\n        isByteArray: function(iByteArray) {\n            var retVal = (typeof iByteArray === \'object\') && (iByteArray.length);\n\n            if (retVal) {\n                for (var i = 0; i < iByteArray.length; i++) {\n                    if (iByteArray[i] > 255 || iByteArray < 0) {\n                        retVal = false;\n                        break;\n                    }\n                }\n            }\n\n            return retVal;\n        },\n\n        /**\n        * Get the first byte of an ElementID or DataSize and return the length.\n        *\n        * @param {Integer} iByte - The first byte of ElementID or DataSize.\n        * @returns {Integer} - Length of object (in octets). -1 if not successful.\n        */\n        byteToLength: function(iByte) {\n            var retVal = -1;\n\n            if (iByte & 0x80) {\n                retVal = 1;\n            } else if (iByte & 0x40) {\n                retVal = 2;\n            } else if (iByte & 0x20) {\n                retVal = 3;\n            } else if (iByte & 0x10) {\n                retVal = 4;\n            } else if (iByte & 0x08) {\n                retVal = 5;\n            } else if (iByte & 0x04) {\n                retVal = 6;\n            } else if (iByte & 0x02) {\n                retVal = 7;\n            } else if (iByte & 0x01) {\n                retVal = 8;\n            }\n\n            return retVal;\n        },\n\n        /**\n        * Convert a byte array of variable length (1-8 bytes) to a uint64.\n        * If the resulting integer fits within the first 32-bits, will return a native integer.\n        *\n        * @param {Array} iByteArray - Array of bytes to be converted.\n        * @returns {Integer|Uint64} - Converted unsigned int. -1 if not successful.\n        */\n        byteArrayToUint64: function(iByteArray) {\n            // Validate input (if not successful, return -1)\n            if (!iByteArray ||\n                !iByteArray.length ||\n                iByteArray.length < 0 ||\n                iByteArray.length > 8 ||\n                !this.isByteArray(iByteArray))\n            {\n                return -1;\n            }\n\n            var retVal = -1;\n\n            // Apply zero padding, so we would always have 8 bytes (uint64)\n            for (var i = iByteArray.length; i < 8; i++) {\n                iByteArray = [0].concat(iByteArray);\n            }\n\n            // Read the 64-bit unsigned int\n            retVal = new JBinary(iByteArray).read(\'uint64\');\n\n            // If the bottom 32bits are enough to represent this number, return a native 32-bit int.\n            if (retVal && retVal.hi === 0) {\n                retVal = retVal.valueOf();\n            }\n\n            return retVal;\n        },\n\n        /**\n        * Gets an unsigned integer as input, and returns the number of bytes it takes to represent it (1-8 bytes).\n        * Will throw error if value cannot be represented by up to 64 bits.\n        *\n        * @param {Integer|Uint64} iNumber - An unsigned integer.\n        * @returns {Integer} - Number of bytes it would take to represent the number.\n        */\n        getDataSizeByUintValue: function(iNumber) {\n            if (iNumber < 0) {\n                throw new Error(\'EBMLUtils::Uint64ToByteArray - Integer must be positive, given \' + iNumber);\n            }\n\n            var retVal = -1;\n\n            if (iNumber <= (Math.pow(2, 8) - 1)) {\n                retVal = 1;\n            } else if (iNumber <= (Math.pow(2, 16) - 1)) {\n                retVal = 2;\n            } else if (iNumber <= (Math.pow(2, 24) - 1)) {\n                retVal = 3;\n            } else if (iNumber <= (Math.pow(2, 32) - 1)) {\n                retVal = 4;\n            } else if (iNumber <= (Math.pow(2, 40) - 1)) {\n                retVal = 5;\n            } else if (iNumber <= (Math.pow(2, 48) - 1)) {\n                retVal = 6;\n            } else if (iNumber <= (Math.pow(2, 56) - 1)) {\n                retVal = 7;\n            } else if (iNumber <= (Math.pow(2, 64) - 1)) {\n                retVal = 8;\n            } else {\n                throw new Error(\'EBMLUtils::Number cannot be represented by 64 bits.\');\n            }\n\n            return retVal;\n        },\n\n        /**\n        * Convert an unsigned integer to a byte array of variable length (1-8 bytes).\n        *\n        * @param {Integer|Uint64} iNumber - An unsigned integer to be serialized into byte array.\n        * @param {Integer} iDataSize - Length of returned byte array (1-8 bytes). Optional.\n        * @returns {Array} - Array of bytes (length iDataSize).\n        */\n        Uint64ToByteArray: function(iNumber, iDataSize) {\n            // Validate input\n            if (iNumber < 0) {\n                throw new Error(\'EBMLUtils::Uint64ToByteArray - Integer must be positive, given \' + iNumber);\n            }\n\n            if (iDataSize < 1 || iDataSize > 8) {\n                throw new Error(\'EBMLUtils::DataSize for UInt64 must be in the range 1-8 bytes, DataSize given \' + iDataSize);\n            }\n\n            // If no DataSize given, select the best DataSize according to iNumber value.\n            if (!iDataSize) {\n                iDataSize = this.getDataSizeByUintValue(iNumber);\n            }\n\n            // Check that iDataSize is enough bytes to represent the requested integer\n            if (iNumber > (Math.pow(2, (iDataSize * 8)) - 1)) {\n                throw new Error(\'EBMLUtils::DataSize for UInt64 is not enough bytes to represent the requested integer. DataSize = \' + iDataSize + \', Integer = \' + iNumber);\n            }\n\n            var bin = new JBinary(8);\n            bin.write(\'uint64\', iNumber);\n            bin.seek(8 - iDataSize);\n            return bin.read([\'array\', \'uint8\', iDataSize]);\n        },\n\n        /**\n        * Convert a byte array of variable length (1-8 bytes) to a int64.\n        * If the resulting integer fits within the first 32-bits, will return a native integer.\n        *\n        * @param {Array} iByteArray - Array of bytes to be converted.\n        * @returns {Integer|Int64} - Converted signed int. Null if not successful.\n        */\n        byteArrayToInt64: function(iByteArray) {\n            // Validate input (if not successful, return -1)\n            if (!iByteArray ||\n                !iByteArray.length ||\n                iByteArray.length < 0 ||\n                iByteArray.length > 8 ||\n                !this.isByteArray(iByteArray))\n            {\n                return null;\n            }\n\n            var retVal = null;\n\n            // Apply zero or 1 padding, so we would always have 8 bytes (int64)\n            var padding = (iByteArray[0] & 0x80) ? 0xFF : 0x00;\n            for (var i = iByteArray.length; i < 8; i++) {\n                iByteArray = [padding].concat(iByteArray);\n            }\n\n            // Read the 64-bit unsigned int\n            retVal = new JBinary(iByteArray).read(\'int64\');\n\n            // If the bottom 32bits are enough to represent this number, return a native 32-bit int.\n            if (retVal && (retVal.hi === 0 || retVal.hi === 4294967295)) {\n                retVal = retVal.valueOf();\n            }\n\n            return retVal;\n        },\n\n        /**\n        * Gets a signed integer as input, and returns the number of bytes it takes to represent it (1-8 bytes).\n        * Will throw error if value cannot be represented by up to 64 bits.\n        *\n        * @param {Integer|Int64} iNumber - A signed integer.\n        * @returns {Integer} - Number of bytes it would take to represent the number.\n        */\n        getDataSizeByIntValue: function(iNumber) {\n            var retVal = -1;\n\n            if (iNumber <= (Math.pow(2, 7) - 1) && iNumber >= 0 - (Math.pow(2, 7))) {\n                retVal = 1;\n            } else if (iNumber <= (Math.pow(2, 15) - 1) && iNumber >= 0 - (Math.pow(2, 15))) {\n                retVal = 2;\n            } else if (iNumber <= (Math.pow(2, 23) - 1) && iNumber >= 0 - (Math.pow(2, 23))) {\n                retVal = 3;\n            } else if (iNumber <= (Math.pow(2, 31) - 1) && iNumber >= 0 - (Math.pow(2, 31))) {\n                retVal = 4;\n            } else if (iNumber <= (Math.pow(2, 39) - 1) && iNumber >= 0 - (Math.pow(2, 39))) {\n                retVal = 5;\n            } else if (iNumber <= (Math.pow(2, 47) - 1) && iNumber >= 0 - (Math.pow(2, 47))) {\n                retVal = 6;\n            } else if (iNumber <= (Math.pow(2, 55) - 1) && iNumber >= 0 - (Math.pow(2, 55))) {\n                retVal = 7;\n            } else if (iNumber <= (Math.pow(2, 63) - 1) && iNumber >= 0 - (Math.pow(2, 63))) {\n                retVal = 8;\n            } else {\n                throw new Error(\'EBMLUtils::Number cannot be represented by 64 bits.\');\n            }\n\n            return retVal;\n        },\n\n        /**\n        * Convert a signed integer to a byte array of variable length (1-8 bytes).\n        *\n        * @param {Integer|Int64} iNumber - A signed integer to be serialized into byte array.\n        * @param {Integer} iDataSize - Length of returned byte array (1-8 bytes). Optional.\n        * @returns {Array} - Array of bytes (length iDataSize).\n        */\n        Int64ToByteArray: function(iNumber, iDataSize) {\n            // Validate input\n            if (iDataSize < 1 || iDataSize > 8) {\n                throw new Error(\'EBMLUtils::DataSize for Int64 must be in the range 1-8 bytes, DataSize given \' + iDataSize);\n            }\n\n            // If no DataSize given, select the best DataSize according to iNumber value.\n            if (!iDataSize) {\n                iDataSize = this.getDataSizeByIntValue(iNumber);\n            }\n\n            // Check that iDataSize is enough bytes to represent the requested integer\n            if (iNumber > (Math.pow(2, ((iDataSize * 8) - 1)) - 1) || iNumber < 0 - (Math.pow(2, ((iDataSize * 8) - 1)))) {\n                throw new Error(\'EBMLUtils::DataSize for Int64 is not enough bytes to represent the requested integer. DataSize = \' + iDataSize + \', Integer = \' + iNumber);\n            }\n\n            var bin = new JBinary(8);\n            bin.write(\'int64\', iNumber);\n            bin.seek(8 - iDataSize);\n            return bin.read([\'array\', \'uint8\', iDataSize]);\n        },\n\n        /**\n        * Gets a positive integer (or -1 for Unknown) as input, and returns an array of bytes that represent the DataSize field.\n        *\n        * @param {Integer|Uint64} iNumber - A positive (or -1) integer.\n        * @returns {Array} - Array of bytes that represent the DataSize of an Element.\n        */\n        numberToDataSizeByteArray: function(iNumber) {\n            if (iNumber === -1) {\n                return [0xFF];\n            }\n\n            if (iNumber < 0) {\n                throw new Error(\'EBMLUtils::DataSize must be non-negative (or -1). Given value is \' + iNumber);\n            }\n\n            var retVal = null;\n            var bin = null;\n\n            if (iNumber < Math.pow(2, 7) - 1) {\n                retVal = [iNumber & 0xFF];\n            } else if (iNumber < Math.pow(2, 14) - 1) {\n                retVal = [(iNumber >>> 8) & 0xFF, iNumber & 0xFF];\n            } else if (iNumber < Math.pow(2, 21) - 1) {\n                retVal = [(iNumber >>> 16) & 0xFF, (iNumber >>> 8) & 0xFF, iNumber & 0xFF];\n            } else if (iNumber < Math.pow(2, 28) - 1) {\n                retVal = [(iNumber >>> 24) & 0xFF, (iNumber >>> 16) & 0xFF, (iNumber >>> 8) & 0xFF, iNumber & 0xFF];\n            } else if (iNumber < Math.pow(2, 35) - 1) {\n                bin = new JBinary(8);\n                bin.write(\'uint64\', iNumber);\n                bin.seek(3);\n                retVal = bin.read([\'array\', \'uint8\', 5]);\n            } else if (iNumber < Math.pow(2, 42) - 1) {\n                bin = new JBinary(8);\n                bin.write(\'uint64\', iNumber);\n                bin.seek(2);\n                retVal = bin.read([\'array\', \'uint8\', 6]);\n            } else if (iNumber < Math.pow(2, 49) - 1) {\n                bin = new JBinary(8);\n                bin.write(\'uint64\', iNumber);\n                bin.seek(1);\n                retVal = bin.read([\'array\', \'uint8\', 7]);\n            } else if (iNumber < Math.pow(2, 56) - 1) {\n                bin = new JBinary(8);\n                bin.write(\'uint64\', iNumber);\n                bin.seek(0);\n                retVal = bin.read([\'array\', \'uint8\', 8]);\n            } else {\n                throw new Error(\'EBMLUtils::Could not serialize DataSize. Given value is \' + iNumber);\n            }\n\n            // Turn length descriptor bit on\n            if (retVal) {\n                var mask = 0x80 >>> (retVal.length - 1);\n                retVal[0] = retVal[0] | mask;\n            }\n\n            return retVal;\n        },\n\n        /**\n        * Gets a byte array that represents a DataSize of an Element and returns the DataSize number (or -1 for Unknown).\n        *\n        * @param {Array} iByteArray - Array of bytes that represent the DataSize of an Element\n        * @returns {Integer|Uint64} - A positive (or -1) integer. If unsuccessful, returns null.\n        */\n        dataSizeByteArrayToNumber: function(iByteArray) {\n            // Validate input (if not successful, return null)\n            if (!iByteArray ||\n                !iByteArray.length ||\n                iByteArray.length < 0 ||\n                iByteArray.length > 8 ||\n                !this.isByteArray(iByteArray))\n            {\n                return null;\n            }\n\n            var length = iByteArray.length;\n            var mask = 0x80 >>> (length - 1);\n            var unkownDataSize = true;\n\n            // Check for \'Unknown\' special case (return -1 in that case)\n            unkownDataSize = true;\n            for (var j = 0; j < length; j++) {\n                var currentByteMask = (j === 0) ?\n                                        0xFF >>> (length - 1) :\n                                        0xFF;\n\n                if (iByteArray[j] ^ currentByteMask) {\n                    unkownDataSize = false;\n                    break;\n                }\n            }\n\n            if (unkownDataSize) {\n                return -1;\n            }\n\n            // Unset length descriptor bit\n            iByteArray[0] = iByteArray[0] ^ mask;\n\n            return this.byteArrayToUint64(iByteArray);\n        },\n\n        /**\n        * Gets a byte array that represents an EBML Date and returns the corresponding native Date object.\n        *\n        * @param {Array} iByteArray - Array of bytes that represent an EBML Date.\n        * @returns {Date} - A javascript Date object, or null if not successful.\n        */\n        byteArrayToDate: function(iByteArray) {\n            // Validate input (if not successful, return null)\n            if (!iByteArray ||\n                iByteArray.length != 8 ||\n                !this.isByteArray(iByteArray))\n            {\n                return null;\n            }\n\n            var millisecondsSinceMillenium = new JBinary(iByteArray).read(\'int64\') / 1000000;\n            return new Date(millisecondsSinceMillenium + dateMillisecondsOffset);\n        },\n\n        /**\n        * Gets a native Date object as input, and returns the 8-bytes EBML byte array representation of the Date.\n        * For invalid input, returns the current date\'s byte array.\n        *\n        * @param {Date} iDate - A javascript Date object.\n        * @returns {Array} - An array of 8 bytes that represent the Date.\n        */\n        dateToByteArray: function(iDate) {\n            if (!(iDate instanceof Date)) {\n                iDate = new Date();\n            }\n\n            var bin = new JBinary(8);\n            var nanoSecondsSinceMillenium = (iDate.valueOf() * 1000000) - (dateMillisecondsOffset * 1000000);\n\n            bin.write(\'int64\', nanoSecondsSinceMillenium);\n            bin.seek(0);\n\n            return bin.read([\'array\', \'uint8\', 8]);\n        },\n\n        /**\n        * Get a metadata object based on ElementID.\n        * The returned object will contain two properties: \'Name\' and \'Type\' (both properties are Strings).\n        *\n        * @param {Array|String} iElementID - ElementID in either byte array or hex string form.\n        * @returns {Object} - Metadata object (containing String properties \'Name\' and \'Type\').\n        */\n        resolveElementMetadata: function(iElementID) {\n            iElementID = this.byteArrayToHexString(iElementID);\n            return (iElementID in this.elementMetadataMap) ?\n                                this.elementMetadataMap[iElementID] :\n                                this.defaultElementMetadata;\n        },\n\n        /**\n        * A map of ElementIDs and their metadata objects.\n        */\n        elementMetadataMap: {\n            \'[1A][45][DF][A3]\': {\n                Name: \'EBML\',\n                Type: \'Master\'\n            },\n\n            \'[42][86]\': {\n                Name: \'EBMLVersion\',\n                Type: \'UInteger\',\n                Parents: [\n                    \'[1A][45][DF][A3]\'\n                ]\n            },\n\n            \'[42][F7]\': {\n                Name: \'EBMLReadVersion\',\n                Type: \'UInteger\',\n                Parents: [\n                    \'[1A][45][DF][A3]\'\n                ]\n            },\n\n            \'[42][F2]\': {\n                Name: \'EBMLMaxIDLength\',\n                Type: \'UInteger\',\n                Parents: [\n                    \'[1A][45][DF][A3]\'\n                ]\n            },\n\n            \'[42][F3]\': {\n                Name: \'EBMLMaxSizeLength\',\n                Type: \'UInteger\',\n                Parents: [\n                    \'[1A][45][DF][A3]\'\n                ]\n            },\n\n            \'[42][82]\': {\n                Name: \'DocType\',\n                Type: \'String\',\n                Parents: [\n                    \'[1A][45][DF][A3]\'\n                ]\n            },\n\n            \'[42][87]\': {\n                Name: \'DocTypeVersion\',\n                Type: \'UInteger\',\n                Parents: [\n                    \'[1A][45][DF][A3]\'\n                ]\n            },\n\n            \'[42][85]\': {\n                Name: \'DocTypeReadVersion\',\n                Type: \'UInteger\',\n                Parents: [\n                    \'[1A][45][DF][A3]\'\n                ]\n            },\n\n            \'[BF]\': {\n                Name: \'CRC-32\',\n                Type: \'Binary\',\n                Global: true\n            },\n\n            \'[EC]\': {\n                Name: \'Void\',\n                Type: \'Binary\',\n                Global: true\n            },\n\n            \'[1B][53][86][67]\': {\n                Name: \'SignatureSlot\',\n                Type: \'Master\',\n                Global: true\n            },\n\n            \'[7E][8A]\': {\n                Name: \'SignatureAlgo\',\n                Type: \'UInteger\',\n                Parents: [\n                    \'[1B][53][86][67]\'\n                ]\n            },\n\n            \'[7E][9A]\': {\n                Name: \'SignatureHash\',\n                Type: \'UInteger\',\n                Parents: [\n                    \'[1B][53][86][67]\'\n                ]\n            },\n\n            \'[7E][A5]\': {\n                Name: \'SignaturePublicKey\',\n                Type: \'Binary\',\n                Parents: [\n                    \'[1B][53][86][67]\'\n                ]\n            },\n\n            \'[7E][B5]\': {\n                Name: \'Signature\',\n                Type: \'Binary\',\n                Parents: [\n                    \'[1B][53][86][67]\'\n                ]\n            },\n\n            \'[7E][5B]\': {\n                Name: \'SignatureElements\',\n                Type: \'Master\',\n                Parents: [\n                    \'[1B][53][86][67]\'\n                ]\n            },\n\n            \'[7E][7B]\': {\n                Name: \'SignatureElementList\',\n                Type: \'Master\',\n                Parents: [\n                    \'[7E][5B]\'\n                ]\n            },\n\n            \'[65][32]\': {\n                Name: \'SignedElement\',\n                Type: \'Binary\',\n                Parents: [\n                    \'[7E][7B]\'\n                ]\n            }\n        },\n\n        /**\n        * The default metadata object for unknown ElementIDs.\n        */\n        defaultElementMetadata: {\n            Name: \'Unknown\',\n            Type: \'Binary\'\n        }\n    };\n});\n\n/*jslint bitwise: true */\n\ndefine(\'engine/mse/formats/webm/ebml\',[\n    \'jbinary\',\n    \'engine/mse/formats/webm/ebmlutils\'\n],\n\n/**\n* This module defines a JBinary typeset that corresponds to EBML spec.\n* The EBML format is the basis for the matroska (MKV) container (which WebM is a subset of).\n* EBML is to binary data as XML is to textual data.\n* The EBML spec can be found at: http://ebml.sourceforge.net/specs\n*/\nfunction(JBinary, ebmlUtils) {\n    \'use strict\';\n\n    JBinary.prototype.hasType = function(typeName) {\n        return this.typeSet.hasOwnProperty(typeName);\n    };\n\n    return {\n\n        ///////////////////////////////////////////////////\n        //  Global Configuration\n        ///////////////////////////////////////////////////\n\n        \'jBinary.littleEndian\': false,\n        \'jBinary.all\': [\'array\', \'Element\'],\n\n        ///////////////////////////////////////////////////\n        //  Base Components\n        ///////////////////////////////////////////////////\n\n        ElementID: JBinary.Type({\n            read: function(/* context */) {\n                // Probe for length of ElementID without changing position\n                var length = this.binary.skip(0, function() {\n                    return ebmlUtils.byteToLength(this.read(\'uint8\'));\n                });\n\n                if (length <= 0 || length > 4) {\n                    throw new Error(\'EBML::Unknown ElementID length: \' + length + \', at offset: \' + this.binary.tell());\n                }\n\n                return ebmlUtils.byteArrayToHexString(this.binary.read([\'array\', \'uint8\', length]));\n            },\n\n            write: function(data /*, context */) {\n                var byteArray = ebmlUtils.hexStringToByteArray(data);\n                var firstByte = byteArray && byteArray.length ? byteArray[0] : 0x00;\n                var length = byteArray && byteArray.length ? byteArray.length : 0;\n\n                if (ebmlUtils.byteToLength(firstByte) != length) {\n                    throw new Error(\'EBML::Bad ElementID given\');\n                }\n\n                this.binary.write([\'array\', \'uint8\', length], byteArray);\n            }\n        }),\n\n        DataSize: JBinary.Type({\n            read: function(/* context */) {\n                // Probe for length of DataSize without changing position\n                var length = this.binary.skip(0, function() {\n                    return ebmlUtils.byteToLength(this.read(\'uint8\'));\n                });\n\n                if (length <= 0 || length > 8) {\n                    throw new Error(\'EBML::Unknown DataSize length\');\n                }\n\n                return ebmlUtils.dataSizeByteArrayToNumber(this.binary.read([\'array\', \'uint8\', length]));\n            },\n\n            write: function(data /* , context */) {\n                var byteArray = ebmlUtils.numberToDataSizeByteArray(data);\n                this.binary.write([\'array\', \'uint8\', byteArray.length], byteArray);\n            }\n        }),\n\n        Data: JBinary.Type({\n            params: [\'Type\', \'DataSize\', \'Name\'],\n\n            read: function(/* context */) {\n                if (this.Name && this.binary.hasType(this.Name)) {\n                    return this.binary.read([this.Name, this.DataSize]);\n                }\n\n                return this.binary.read([this.Type, this.DataSize]);\n            },\n\n            write: function(data /* , context */) {\n                if (this.Name && this.binary.hasType(this.Name)) {\n                    return this.binary.write([this.Name, this.DataSize], data);\n                }\n\n                return this.binary.write([this.Type, this.DataSize], data);\n            }\n        }),\n\n        ///////////////////////////////////////////////////\n        //  Composite Components\n        ///////////////////////////////////////////////////\n\n        ElementHeader: {\n            _begin: function() {\n                return this.binary.tell();\n            },\n\n            ElementID: \'ElementID\',\n\n            _beginDataSize: function() {\n                return this.binary.tell();\n            },\n\n            _beginData: function() {\n                return this.binary.getContext()._beginDataSize + this.binary.skip(0, function() {\n                    return ebmlUtils.byteToLength(this.read(\'uint8\'));\n                });\n            },\n\n            DataSize: function() {\n                return this.binary.skip(0, function() {\n                    return this.read(\'DataSize\');\n                });\n            },\n\n            _metadata: function() {\n                //console.log(\'Reading ElementID: \' + this.binary.getContext().ElementID + \', DataSize: \' + this.binary.getContext().DataSize + \', Name: \' + ebmlUtils.resolveElementMetadata(this.binary.getContext().ElementID).Name + \', Type: \' + ebmlUtils.resolveElementMetadata(this.binary.getContext().ElementID).Type);\n                return ebmlUtils.resolveElementMetadata(this.binary.getContext().ElementID);\n            },\n\n            Name: function() {\n                return this.binary.getContext()._metadata.Name;\n            },\n\n            Type: function() {\n                return this.binary.getContext()._metadata.Type;\n            }\n        },\n\n        Element: JBinary.Template({\n            baseType: [\'extend\', \'ElementHeader\', {\n                Data: JBinary.Type({\n                    read: function(/* context */) {\n                        return this.binary.read([\'Data\', this.binary.getContext(1).Type, this.binary.getContext(1).DataSize, this.binary.getContext(1).Name]);\n                    },\n\n                    write: function(data /* , context */) {\n                        return this.binary.write([\'Data\', this.binary.getContext(1).Type, this.binary.getContext(1).DataSize, this.binary.getContext(1).Name], data);\n                    }\n                })\n            }],\n\n            read: function(/* context */) {\n                return this.baseRead();\n            },\n\n            write: function(data /* , context */) {\n                var retVal = 0;\n\n                retVal += this.binary.write(\'ElementID\', data.ElementID);\n                retVal += this.binary.write([\'Data\', data.Type, data.DataSize, data.Name], data.Data);\n\n                return retVal;\n            }\n        }),\n\n        ///////////////////////////////////////////////////\n        //  Known Basic Types\n        ///////////////////////////////////////////////////\n\n        Master: JBinary.Type({\n            params: [\'DataSize\'],\n\n            read: function(/* context */) {\n                var retValArray = [];\n                var dataSize = this.binary.read(\'DataSize\');\n                var start = this.binary.tell();\n                var eof;\n                var nextElementIDMeta;\n                var _this = this;\n\n                // If DataSize is unknown - read all sequential Elements that are potential children\n                if (dataSize === -1) {\n                    var myElementID = this.binary.getContext(\'ElementID\').ElementID;\n\n                    var readNextElementID = function() {\n                        eof = _this.binary.tell() === _this.binary.view.byteLength;\n                        if (!eof) {\n                            var nextElementID = _this.binary.skip(0, function() { return this.read(\'ElementID\'); });\n                            nextElementIDMeta = ebmlUtils.resolveElementMetadata(nextElementID);\n                        }\n                    };\n\n                    readNextElementID();\n\n                    while (!eof && nextElementIDMeta && (nextElementIDMeta.Global || (nextElementIDMeta.Parents && nextElementIDMeta.Parents.indexOf(myElementID) >= 0))) {\n                        retValArray.push(this.binary.read(\'Element\'));\n                        readNextElementID();\n                    }\n\n                } else {\n                    while (this.binary.tell() - start < dataSize) {\n                        retValArray.push(this.binary.read(\'Element\'));\n                    }\n                }\n\n                return retValArray;\n            },\n\n            write: function(data /* , context */) {\n                if (!(data instanceof Array)) {\n                    throw new Error(\'EBML::When writing a master element, its data is expected to be an array. Given data: \' + data);\n                }\n\n                var retVal = 0;\n                var dataSize;\n                var dataSizeBinary;\n                var dataSizeBytes;\n                var binary = this.binary;\n                var writeChildren = function() {\n                    var currSize = 0;\n\n                    for (var i = 0; i < data.length; i++) {\n                        currSize +=  binary.write(\'Element\', data[i]);\n                    }\n\n                    return currSize;\n                };\n\n                if (this.DataSize === -1) {\n                    retVal += this.binary.write(\'uint8\', 0xFF);\n                    retVal += writeChildren();\n                } else {\n                    // Skip 8 bytes ahead (leave space to write DataSize), and write the data.\n                    dataSize = this.binary.skip(8, function() {\n                        return writeChildren();\n                    });\n\n                    // Now that we have written the data and know the DataSize, let\'s create an array of 8 bytes with DataSize info.\n                    dataSizeBinary = new JBinary(8);\n                    dataSizeBinary.write(\'uint64\', dataSize);\n                    dataSizeBinary.seek(0);\n                    dataSizeBytes = dataSizeBinary.read([\'array\', \'uint8\', 8]);\n                    dataSizeBytes[0] = dataSizeBytes[0] | 0x01;\n\n                    // Write the DataSize in 8 blank bytes, and skip to end of element\n                    retVal += this.binary.write([\'array\', \'uint8\', 8], dataSizeBytes);\n                    retVal += dataSize;\n                    this.binary.skip(dataSize);\n                }\n\n                return retVal;\n            }\n        }),\n\n        Binary: JBinary.Type({\n            read: function(/* context */) {\n                var dataSize = this.binary.read(\'DataSize\');\n                if (dataSize === -1) {\n                    return this.binary.read([\'blob\']);\n                }\n\n                return this.binary.read([\'blob\', dataSize]);\n            },\n\n            write: function(data /* , context */) {\n                var retVal = 0;\n\n                retVal += this.binary.write(\'DataSize\', data.length);\n                retVal += this.binary.write([\'blob\', data.length], data);\n\n                return retVal;\n            }\n        }),\n\n        UInteger: JBinary.Type({\n            read: function(/* context */) {\n                var dataSize = this.binary.read(\'DataSize\');\n                return ebmlUtils.byteArrayToUint64(this.binary.read([\'array\', \'uint8\', dataSize]));\n            },\n\n            write: function(data /* , context */) {\n                var dataSize = ebmlUtils.getDataSizeByUintValue(data);\n                var retVal = 0;\n\n                retVal += this.binary.write(\'DataSize\', dataSize);\n                retVal += this.binary.write([\'array\', \'uint8\', dataSize], ebmlUtils.Uint64ToByteArray(data, dataSize));\n\n                return retVal;\n            }\n        }),\n\n        SInteger: JBinary.Type({\n            read: function(/* context */) {\n                var dataSize = this.binary.read(\'DataSize\');\n                return ebmlUtils.byteArrayToInt64(this.binary.read([\'array\', \'uint8\', dataSize]));\n            },\n\n            write: function(data /* , context */) {\n                var dataSize = ebmlUtils.getDataSizeByIntValue(data);\n                var retVal = 0;\n\n                retVal += this.binary.write(\'DataSize\', dataSize);\n                retVal += this.binary.write([\'array\', \'uint8\', dataSize], ebmlUtils.Int64ToByteArray(data, dataSize));\n\n                return retVal;\n            }\n        }),\n\n        Date: JBinary.Type({\n            read: function(/* context */) {\n                var dataSize = this.binary.read(\'DataSize\');\n                if (dataSize != 8) {\n                    throw new Error(\'EBML::Data of type Date must have DataSize of exactly 8 bytes. Given \' + dataSize);\n                }\n\n                return ebmlUtils.byteArrayToDate(this.binary.read([\'array\', \'uint8\', dataSize]));\n            },\n\n            write: function(data /* , context */) {\n                var bytes = ebmlUtils.dateToByteArray(data);\n                var retVal = 0;\n\n                retVal += this.binary.write(\'DataSize\', bytes.length);\n                retVal += this.binary.write([\'array\', \'uint8\', bytes.length], bytes);\n\n                return retVal;\n            }\n        }),\n\n        String: JBinary.Type({\n            read: function(/* context */) {\n                var dataSize = this.binary.read(\'DataSize\');\n                return this.binary.read([\'string\', dataSize, \'binary\']);\n            },\n\n            write: function(data /* , context */) {\n                if (typeof data !== \'string\') {\n                    throw new Error(\'EBML::When writing String data, param is expected to be a string. Given value: \' + data);\n                }\n\n                var retVal = 0;\n\n                retVal += this.binary.write(\'DataSize\', data.length);\n                retVal += this.binary.write([\'string\', data.length, \'binary\'], data);\n\n                return retVal;\n            }\n        }),\n\n        UTF8: JBinary.Type({\n            read: function(/* context */) {\n                var dataSize = this.binary.read(\'DataSize\');\n                return this.binary.read([\'string\', dataSize, \'utf8\']);\n            },\n\n            write: function(data /* , context */) {\n                if (typeof data !== \'string\') {\n                    throw new Error(\'EBML::When writing UTF8 data, param is expected to be a string. Given value: \' + data);\n                }\n\n                var retVal = 0;\n\n                retVal += this.binary.write(\'DataSize\', data.length);\n                retVal += this.binary.write([\'string\', data.length, \'utf8\'], data);\n\n                return retVal;\n            }\n        }),\n\n        Float: JBinary.Type({\n            read: function(/* context */) {\n                var dataSize = this.binary.read(\'DataSize\');\n\n                if (dataSize != 4 && dataSize != 8) {\n                    throw new Error(\'EBML::Invalid DataSize for Float type. Expected 4 or 8 bytes, given \' + dataSize);\n                }\n\n                return this.binary.read(dataSize === 4 ? \'float32\' : \'float64\');\n            },\n\n            write: function(data /* , context */) {\n                var retVal = 0;\n\n                retVal += this.binary.write(\'DataSize\', 8);\n                retVal += this.binary.write(\'float64\', data);\n\n                return retVal;\n            }\n        })\n    };\n});\n\n/*jslint bitwise: true */\n\ndefine(\'engine/mse/formats/webm/webm\',[\n    \'jbinary\',\n    \'engine/mse/formats/webm/ebml\',\n    \'engine/mse/formats/webm/ebmlutils\',\n    \'mseUtils\'\n],\n\n/**\n* This module defines a JBinary typeset that corresponds to WebM spec (by extending the EBML module).\n* WebM is an open-source media container format based on Matroska container (MKV).\n* Useful links:\n*\n* - http://www.webmproject.org/docs/container/\n* - http://www.matroska.org/technical/specs/index.html\n* - http://ebml.sourceforge.net/specs/\n*/\nfunction(JBinary, ebml, ebmlUtils, utils) {\n    \'use strict\';\n\n    var webmElementMetadataMap = {\n        \'[18][53][80][67]\': {\n            Name: \'Segment\',\n            Type: \'Master\'\n        },\n\n        \'[11][4D][9B][74]\': {\n            Name: \'SeekHead\',\n            Type: \'Master\',\n            Parents: [\n                \'[18][53][80][67]\'\n            ]\n        },\n\n        \'[4D][BB]\': {\n            Name: \'Seek\',\n            Type: \'Master\',\n            Parents: [\n                \'[11][4D][9B][74]\'\n            ]\n        },\n\n        \'[53][AB]\': {\n            Name: \'SeekID\',\n            Type: \'Binary\',\n            Parents: [\n                \'[4D][BB]\'\n            ]\n        },\n\n        \'[53][AC]\': {\n            Name: \'SeekPosition\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[4D][BB]\'\n            ]\n        },\n\n        \'[15][49][A9][66]\': {\n            Name: \'Info\',\n            Type: \'Master\',\n            Parents: [\n                \'[18][53][80][67]\'\n            ]\n        },\n\n        \'[73][A4]\': {\n            Name: \'SegmentUID\',\n            Type: \'Binary\',\n            Parents: [\n                \'[15][49][A9][66]\'\n            ]\n        },\n\n        \'[2A][D7][B1]\': {\n            Name: \'TimecodeScale\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[15][49][A9][66]\'\n            ]\n        },\n\n        \'[44][89]\': {\n            Name: \'Duration\',\n            Type: \'Float\',\n            Parents: [\n                \'[15][49][A9][66]\'\n            ]\n        },\n\n        \'[44][61]\': {\n            Name: \'DateUTC\',\n            Type: \'Date\',\n            Parents: [\n                \'[15][49][A9][66]\'\n            ]\n        },\n\n        \'[7B][A9]\': {\n            Name: \'Title\',\n            Type: \'UTF8\',\n            Parents: [\n                \'[15][49][A9][66]\'\n            ]\n        },\n\n        \'[4D][80]\': {\n            Name: \'MuxingApp\',\n            Type: \'UTF8\',\n            Parents: [\n                \'[15][49][A9][66]\'\n            ]\n        },\n\n        \'[57][41]\': {\n            Name: \'WritingApp\',\n            Type: \'UTF8\',\n            Parents: [\n                \'[15][49][A9][66]\'\n            ]\n        },\n\n        \'[1F][43][B6][75]\': {\n            Name: \'Cluster\',\n            Type: \'Master\',\n            Parents: [\n                \'[18][53][80][67]\'\n            ]\n        },\n\n        \'[E7]\': {\n            Name: \'Timecode\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[1F][43][B6][75]\'\n            ]\n        },\n\n        \'[AB]\': {\n            Name: \'PrevSize\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[1F][43][B6][75]\'\n            ]\n        },\n\n        \'[A3]\': {\n            Name: \'SimpleBlock\',\n            Type: \'Binary\',\n            Parents: [\n                \'[1F][43][B6][75]\'\n            ]\n        },\n\n        \'[A0]\': {\n            Name: \'BlockGroup\',\n            Type: \'Master\',\n            Parents: [\n                \'[1F][43][B6][75]\'\n            ]\n        },\n\n        \'[A1]\': {\n            Name: \'Block\',\n            Type: \'Binary\',\n            Parents: [\n                \'[A0]\'\n            ]\n        },\n\n        \'[9B]\': {\n            Name: \'BlockDuration\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[A0]\'\n            ]\n        },\n\n        \'[FB]\': {\n            Name: \'ReferenceBlock\',\n            Type: \'SInteger\',\n            Parents: [\n                \'[A0]\'\n            ]\n        },\n\n        \'[75][A2]\': {\n            Name: \'DiscardPadding\',\n            Type: \'SInteger\',\n            Parents: [\n                \'[A0]\'\n            ]\n        },\n\n        \'[8E]\': {\n            Name: \'Slices\',\n            Type: \'Master\',\n            Parents: [\n                \'[A0]\'\n            ]\n        },\n\n        \'[E8]\': {\n            Name: \'TimeSlice\',\n            Type: \'Master\',\n            Parents: [\n                \'[8E]\'\n            ]\n        },\n\n        \'[CC]\': {\n            Name: \'LaceNumber\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[E8]\'\n            ]\n        },\n\n        \'[16][54][AE][6B]\': {\n            Name: \'Tracks\',\n            Type: \'Master\',\n            Parents: [\n                \'[18][53][80][67]\'\n            ]\n        },\n\n        \'[AE]\': {\n            Name: \'TrackEntry\',\n            Type: \'Master\',\n            Parents: [\n                \'[16][54][AE][6B]\'\n            ]\n        },\n\n        \'[D7]\': {\n            Name: \'TrackNumber\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[AE]\'\n            ]\n        },\n\n        \'[73][C5]\': {\n            Name: \'TrackUID\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[AE]\'\n            ]\n        },\n\n        \'[83]\': {\n            Name: \'TrackType\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[AE]\'\n            ]\n        },\n\n        \'[B9]\': {\n            Name: \'FlagEnabled\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[AE]\'\n            ]\n        },\n\n        \'[88]\': {\n            Name: \'FlagDefault\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[AE]\'\n            ]\n        },\n\n        \'[55][AA]\': {\n            Name: \'FlagForced\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[AE]\'\n            ]\n        },\n\n        \'[9C]\': {\n            Name: \'FlagLacing\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[AE]\'\n            ]\n        },\n\n        \'[23][E3][83]\': {\n            Name: \'DefaultDuration\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[AE]\'\n            ]\n        },\n\n        \'[53][6E]\': {\n            Name: \'Name\',\n            Type: \'UTF8\',\n            Parents: [\n                \'[AE]\'\n            ]\n        },\n\n        \'[22][B5][9C]\': {\n            Name: \'Language\',\n            Type: \'String\',\n            Parents: [\n                \'[AE]\'\n            ]\n        },\n\n        \'[86]\': {\n            Name: \'CodecID\',\n            Type: \'String\',\n            Parents: [\n                \'[AE]\'\n            ]\n        },\n\n        \'[63][A2]\': {\n            Name: \'CodecPrivate\',\n            Type: \'Binary\',\n            Parents: [\n                \'[AE]\'\n            ]\n        },\n\n        \'[25][86][88]\': {\n            Name: \'CodecName\',\n            Type: \'UTF8\',\n            Parents: [\n                \'[AE]\'\n            ]\n        },\n\n        \'[56][AA]\': {\n            Name: \'CodecDelay\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[AE]\'\n            ]\n        },\n\n        \'[56][BB]\': {\n            Name: \'SeekPreRoll\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[AE]\'\n            ]\n        },\n\n        \'[E0]\': {\n            Name: \'Video\',\n            Type: \'Master\',\n            Parents: [\n                \'[AE]\'\n            ]\n        },\n\n        \'[9A]\': {\n            Name: \'FlagInterlaced\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[E0]\'\n            ]\n        },\n\n        \'[53][B8]\': {\n            Name: \'StereoMode\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[E0]\'\n            ]\n        },\n\n        \'[53][C0]\': {\n            Name: \'AlphaMode\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[E0]\'\n            ]\n        },\n\n        \'[B0]\': {\n            Name: \'PixelWidth\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[E0]\'\n            ]\n        },\n\n        \'[BA]\': {\n            Name: \'PixelHeight\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[E0]\'\n            ]\n        },\n\n        \'[54][AA]\': {\n            Name: \'PixelCropBottom\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[E0]\'\n            ]\n        },\n\n        \'[54][BB]\': {\n            Name: \'PixelCropTop\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[E0]\'\n            ]\n        },\n\n        \'[54][CC]\': {\n            Name: \'PixelCropLeft\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[E0]\'\n            ]\n        },\n\n        \'[54][DD]\': {\n            Name: \'PixelCropRight\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[E0]\'\n            ]\n        },\n\n        \'[54][B0]\': {\n            Name: \'DisplayWidth\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[E0]\'\n            ]\n        },\n\n        \'[54][BA]\': {\n            Name: \'DisplayHeight\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[E0]\'\n            ]\n        },\n\n        \'[54][B2]\': {\n            Name: \'DisplayUnit\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[E0]\'\n            ]\n        },\n\n        \'[54][B3]\': {\n            Name: \'AspectRatioType\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[E0]\'\n            ]\n        },\n\n        \'[23][83][E3]\': {\n            Name: \'FrameRate\',\n            Type: \'Float\',\n            Parents: [\n                \'[E0]\'\n            ]\n        },\n\n        \'[E1]\': {\n            Name: \'Audio\',\n            Type: \'Master\',\n            Parents: [\n                \'[AE]\'\n            ]\n        },\n\n        \'[B5]\': {\n            Name: \'SamplingFrequency\',\n            Type: \'Float\',\n            Parents: [\n                \'[E1]\'\n            ]\n        },\n\n        \'[78][B5]\': {\n            Name: \'OutputSamplingFrequency\',\n            Type: \'Float\',\n            Parents: [\n                \'[E1]\'\n            ]\n        },\n\n        \'[9F]\': {\n            Name: \'Channels\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[E1]\'\n            ]\n        },\n\n        \'[62][64]\': {\n            Name: \'BitDepth\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[E1]\'\n            ]\n        },\n\n        \'[1C][53][BB][6B]\': {\n            Name: \'Cues\',\n            Type: \'Master\',\n            Parents: [\n                \'[18][53][80][67]\'\n            ]\n        },\n\n        \'[BB]\': {\n            Name: \'CuePoint\',\n            Type: \'Master\',\n            Parents: [\n                \'[1C][53][BB][6B]\'\n            ]\n        },\n\n        \'[B3]\': {\n            Name: \'CueTime\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[BB]\'\n            ]\n\n        },\n\n        \'[B7]\': {\n            Name: \'CueTrackPositions\',\n            Type: \'Master\',\n            Parents: [\n                \'[BB]\'\n            ]\n        },\n\n        \'[F7]\': {\n            Name: \'CueTrack\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[B7]\'\n            ]\n        },\n\n        \'[F1]\': {\n            Name: \'CueClusterPosition\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[B7]\'\n            ]\n        },\n\n        \'[F0]\': {\n            Name: \'CueRelativePosition\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[B7]\'\n            ]\n        },\n\n        \'[B2]\': {\n            Name: \'CueDuration\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[B7]\'\n            ]\n        },\n\n        \'[53][78]\': {\n            Name: \'CueBlockNumber\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[B7]\'\n            ]\n        },\n\n        \'[12][54][C3][67]\': {\n            Name: \'Tags\',\n            Type: \'Master\',\n            Parents: [\n                \'[18][53][80][67]\'\n            ]\n        },\n\n        \'[73][73]\': {\n            Name: \'Tag\',\n            Type: \'Master\',\n            Parents: [\n                \'[12][54][C3][67]\'\n            ]\n        },\n\n        \'[63][C0]\': {\n            Name: \'Targets\',\n            Type: \'Master\',\n            Parents: [\n                \'[73][73]\'\n            ]\n        },\n\n        \'[68][CA]\': {\n            Name: \'TargetTypeValue\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[63][C0]\'\n            ]\n        },\n\n        \'[63][CA]\': {\n            Name: \'TargetType\',\n            Type: \'String\',\n            Parents: [\n                \'[63][C0]\'\n            ]\n        },\n\n        \'[63][C5]\': {\n            Name: \'TagTrackUID\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[63][C0]\'\n            ]\n        },\n\n        \'[67][C8]\': {\n            Name: \'SimpleTag\',\n            Type: \'Master\',\n            Parents: [\n                \'[73][73]\',\n                \'[67][C8]\'\n            ]\n        },\n\n        \'[45][A3]\': {\n            Name: \'TagName\',\n            Type: \'UTF8\',\n            Parents: [\n                \'[67][C8]\'\n            ]\n        },\n\n        \'[44][7A]\': {\n            Name: \'TagLanguage\',\n            Type: \'String\',\n            Parents: [\n                \'[67][C8]\'\n            ]\n        },\n\n        \'[44][84]\': {\n            Name: \'TagDefault\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[67][C8]\'\n            ]\n        },\n\n        \'[44][87]\': {\n            Name: \'TagString\',\n            Type: \'UTF8\',\n            Parents: [\n                \'[67][C8]\'\n            ]\n        },\n\n        \'[44][85]\': {\n            Name: \'TagBinary\',\n            Type: \'Binary\',\n            Parents: [\n                \'[67][C8]\'\n            ]\n        },\n\n        \'[10][43][A7][70]\': {\n            Name: \'Chapters\',\n            Type: \'Master\',\n            Parents: [\n                \'[18][53][80][67]\'\n            ]\n        },\n\n        \'[45][B9]\': {\n            Name: \'EditionEntry\',\n            Type: \'Master\',\n            Parents: [\n                \'[10][43][A7][70]\'\n            ]\n        },\n\n        \'[B6]\': {\n            Name: \'ChapterAtom\',\n            Type: \'Master\',\n            Parents: [\n                \'[45][B9]\',\n                \'[B6]\'\n            ]\n        },\n\n        \'[73][C4]\': {\n            Name: \'ChapterUID\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[B6]\'\n            ]\n        },\n\n        \'[56][54]\': {\n            Name: \'ChapterStringUID\',\n            Type: \'UTF8\',\n            Parents: [\n                \'[B6]\'\n            ]\n        },\n\n        \'[91]\': {\n            Name: \'ChapterTimeStart\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[B6]\'\n            ]\n        },\n\n        \'[80]\': {\n            Name: \'ChapterDisplay\',\n            Type: \'Master\',\n            Parents: [\n                \'[B6]\'\n            ]\n        },\n\n        \'[85]\': {\n            Name: \'ChapString\',\n            Type: \'UTF8\',\n            Parents: [\n                \'[80]\'\n            ]\n        },\n\n        \'[43][7C]\': {\n            Name: \'ChapLanguage\',\n            Type: \'String\',\n            Parents: [\n                \'[80]\'\n            ]\n        }\n    };\n\n    // Add the WebM specific MetaData objects to map\n    utils.extend(ebmlUtils.elementMetadataMap, webmElementMetadataMap);\n\n    // Extend the typeSet\n    utils.extend(ebml, {\n        // Add mimeType for JBinary typeset\n        \'jBinary.mimeType\': \'video/webm\',\n\n        // Add special parsing when reading a SimpleBlock element\'s Data\n        SimpleBlock: JBinary.Template({\n            baseType: \'Binary\',\n\n            read: function(/* context */) {\n                var retVal = this.baseRead();\n\n                // If we\'re not within an Element context, simply return the binary Data.\n                if (!this.binary.getContext(\'DataSize\')) {\n                    return retVal;\n                }\n\n                // Otherwise, extend the Element\'s properties with the SimpleBlock fields.\n                utils.extend(this.binary.getContext(\'DataSize\'),\n                    this.binary.seek(this.binary.tell() - this.binary.getContext(\'DataSize\').DataSize, function()\n                        {\n                            var track = this.read(\'DataSize\');\n                            var timecode = this.read(\'int16\');\n                            var flagsByte = this.read(\'uint8\');\n                            var lacing;\n\n                            if ((0x06 & flagsByte) >>> 1 === 0) {\n                                lacing = \'None\';\n                            } else if ((0x06 & flagsByte) >>> 1 === 1) {\n                                lacing = \'Xiph\';\n                            } else if ((0x06 & flagsByte) >>> 1 === 2) {\n                                lacing = \'FixedSize\';\n                            } else if ((0x06 & flagsByte) >>> 1 === 3) {\n                                lacing = \'EBML\';\n                            }\n\n                            return {\n                                Track: track,\n                                Timecode: timecode,\n                                Keyframe: !!(0x80 & flagsByte),\n                                Invisible: !!(0x08 & flagsByte),\n                                Lacing: lacing,\n                                Discardable: !!(0x01 & flagsByte)\n                            };\n                        }\n                    )\n                );\n\n                return retVal;\n            },\n\n            write: function(data /* , context */) {\n                return this.baseWrite(data);\n            }\n        })\n    });\n\n    return ebml;\n});\n\ndefine(\'engine/mse/models/SegmentMetadata\',[\n],\n\n/**\n* This module defines some properties that constitute a video segment\'s metadata.\n*/\nfunction() {\n    \'use strict\';\n\n    function SegmentMetadata() {\n        // Public Properties\n        this.duration = 0;\n    }\n\n    return SegmentMetadata;\n});\n\ndefine(\'engine/mse/formats/webm/webmUtils\',[\n    \'utils/loggerFactory\',\n    \'jbinary\',\n    \'engine/mse/formats/webm/webm\',\n    \'engine/mse/models/SegmentMetadata\'\n],\n\n/**\n* This module defines some helper methods for working with WebM files\n*/\nfunction(loggerFactory, JBinary, WEBM, SegmentMetadata) {\n    \'use strict\';\n\n    var log = loggerFactory.createLogger(\'webmUtils\');\n    log.disable();\n\n    return {\n        /**\n        * Do we have enough bytes loaded in order to read the next ElementHeader?\n        */\n        canReadElementHeader: function(jBin, bytesLoaded) {\n            return bytesLoaded - jBin.tell() >= 12;\n        },\n\n        /**\n        * Do we have enough bytes loaded in order to read the entire Element?\n        */\n        canReadElement: function(elementHeader, bytesLoaded) {\n            return elementHeader._beginData + elementHeader.DataSize <= bytesLoaded;\n        },\n\n        /**\n        * Read an EBML element.\n        */\n        readElement: function(jBin, elementHeader) {\n            jBin.seek(elementHeader._begin);\n            return jBin.read(\'Element\');\n        },\n\n        /**\n        * Read an EBML element\'s header.\n        */\n        readElementHeader: function(jBin) {\n            return jBin.read(\'ElementHeader\');\n        },\n\n        /**\n        * Skip an EBML element.\n        */\n        skipElement: function(jBin, elementHeader) {\n            jBin.seek(elementHeader._beginData + elementHeader.DataSize);\n        },\n\n        /**\n        * Skip the ElementHeader and place the read-head at top of Data of current Element (for elements of type \'Master\', the Data is also EBML Elements).\n        */\n        skipToSubElements: function(jBin, elementHeader) {\n            jBin.seek(elementHeader._beginData);\n        },\n\n        /**\n        * Extract the duration in seconds, from an \'Info\' Element.\n        */\n        extractDurationFromInfo: function(element) {\n            var retVal = 0;\n\n            for (var i = 0; i < element.Data.length; i++) {\n                if (element.Data[i].Name === \'Duration\') {\n                    retVal = element.Data[i].Data;\n                    break;\n                }\n            }\n\n            return retVal / 1000;\n        },\n\n        /**\n        * Creates a JBinary WebM instance out of array buffer.\n        */\n        createJBinaryInstance: function(arrayBuffer) {\n            return new JBinary(arrayBuffer, WEBM);\n        },\n\n        /**\n        * Parse the currently available WebM bytes and try to read metadata.\n        * If successful, returns a SegmentMetadata object. Otherwise, returns null.\n        * Current implementation only reads the \'duration\'.\n        */\n        parseMetadata: function(arrayBuffer, bytesLoaded) {\n            if (!arrayBuffer) {\n                return null;\n            }\n\n            if (!bytesLoaded && bytesLoaded !== 0) {\n                bytesLoaded = arrayBuffer.byteLength;\n            }\n\n            var jBin = new JBinary(arrayBuffer, WEBM);\n            var retVal = null;\n            var currentHeader;\n\n            while (this.canReadElementHeader(jBin, bytesLoaded) && !retVal) {\n                currentHeader = this.readElementHeader(jBin);\n\n                switch (currentHeader.Name) {\n                    case \'EBML\':\n                    case \'SeekHead\':\n                    case \'Void\':\n                        if (!this.canReadElement(currentHeader, bytesLoaded)) {\n                            return retVal;\n                        }\n\n                        this.skipElement(jBin, currentHeader);\n                        break;\n                    case \'Segment\':\n                        this.skipToSubElements(jBin, currentHeader);\n                        break;\n                    case \'Info\':\n                        retVal = new SegmentMetadata();\n                        retVal.duration = this.extractDurationFromInfo(this.readElement(jBin, currentHeader));\n                        break;\n                    default:\n                        break;\n                }\n            }\n\n            return retVal;\n        }\n    };\n});\n\ndefine(\'engine/mse/formats/unknown/SimpleSegmentParser\',[\n    \'utils/loggerFactory\',\n    \'mseUtils\',\n    \'utils/EventEmitter\'\n],\n\n/**\n* This module defines a class (SimpleSegmentParser) with concrete implementation of ISegmentParser interface.\n* This should be used in cases where setting MSE\'s SourceBuffer\'s timestampOffset property is supported (i.e. Chrome).\n* Use this as a base class for known formats.\n* Basically, this concrete implementation of ISegmentParser will append input file as-is.\n*/\nfunction(loggerFactory, utils, ee) {\n    \'use strict\';\n\n    var log = loggerFactory.createLogger(\'SimpleSegmentParser\');\n\n    function SimpleSegmentParser(iSettings) {\n        this.mBinaryLoader = iSettings.binaryLoader;\n    }\n\n    /////////////////////////////////////////////////////////////////////////////////////////////////\n    // Private Members\n    /////////////////////////////////////////////////////////////////////////////////////////////////\n\n    SimpleSegmentParser.prototype.mSegmentMetadata = null;\n\n    /////////////////////////////////////////////////////////////////////////////////////////////////\n    // Private Methods\n    /////////////////////////////////////////////////////////////////////////////////////////////////\n\n    /** Optionally override in extending classes. Try parse binary data and return a metadata object if successful, null if not. */\n    SimpleSegmentParser.prototype._tryParseMetadata = function() {\n        log.warn(\'Getting metadata from unknown format is not supported.\');\n        return null;\n    };\n\n    /////////////////////////////////////////////////////////////////////////////////////////////////\n    // Public Methods (ISegmentParser implementation)\n    /////////////////////////////////////////////////////////////////////////////////////////////////\n\n    SimpleSegmentParser.prototype.parse = function() {\n        // Do nothing\n    };\n\n    SimpleSegmentParser.prototype.pauseParsing = function() {\n        // Do nothing\n    };\n\n    SimpleSegmentParser.prototype.getInitSegment = function(/* appendingContext, timestampOffset */) {\n        // Do nothing\n    };\n\n    SimpleSegmentParser.prototype.getMediaSegment = function(/* appendingContext, timestampOffset, mediaSegmentTimestamp */) {\n        // Do nothing\n    };\n\n    SimpleSegmentParser.prototype.append = function(appendingContext /* , timestampOffset */) {\n        // GLOBAL APPENDING CONTEXT\n        appendingContext.streamStarted = true;\n\n        // LOCAL APPENDING CONTEXT\n        var appendByteOffset = 0;\n\n        var appendChunk = function() {\n            var chunk = this.mBinaryLoader.arrayBuffer.slice(appendByteOffset, this.mBinaryLoader.bytesLoaded);\n\n            if (chunk.byteLength) {\n                this.trigger(\'append_progress\', chunk);\n                appendByteOffset = this.mBinaryLoader.bytesLoaded;\n            }\n        }.bind(this);\n\n        if (this.mBinaryLoader.completed) {\n            appendChunk();\n            this.trigger(\'append_complete\');\n        } else {\n            this.mBinaryLoader.on(\'progress\', appendChunk);\n            this.mBinaryLoader.one(\'complete\', function() { this.trigger(\'append_complete\'); }.bind(this));\n        }\n    };\n\n    SimpleSegmentParser.prototype.dispose = function() {\n        if (this.mBinaryLoader) {\n            this.mBinaryLoader.removeAllListeners(\'progress\');\n            this.mBinaryLoader.removeAllListeners(\'complete\');\n        }\n        this.mBinaryLoader = null;\n        this.mSegmentMetadata = null;\n    };\n\n    /////////////////////////////////////////////////////////\n    // Public Getters/Setters\n    /////////////////////////////////////////////////////////\n\n    Object.defineProperties(SimpleSegmentParser.prototype, {\n\n        /** Does this parser type require incrementing the MSE SourceBuffer\'s timestampOffset?  */\n        shouldSetTimestampOffset: {\n            get: function() {\n                return true;\n            }\n        },\n\n        /** Getter for metadata object. */\n        metadata: {\n            get: function() {\n                if (!this.mSegmentMetadata) {\n                    this.mSegmentMetadata = this._tryParseMetadata();\n                    if (this.mSegmentMetadata) {\n                        this.trigger(\'metadata\', this.mSegmentMetadata);\n                        this.trigger(\'parse_complete\');\n                    }\n                }\n\n                return this.mSegmentMetadata;\n            }\n        },\n\n        /** Has parsing completed? */\n        parsingCompleted: {\n            get: function() {\n                if (this.metadata) {\n                    return true;\n                }\n\n                return false;\n            }\n        },\n\n        /** The time in seconds that was parsed. */\n        timeParsed: {\n            get: function() {\n                if (this.metadata) {\n                    return this.metadata.duration;\n                }\n\n                return 0.0;\n            }\n        }\n    });\n\n    // Add the EventEmitter functionality\n    utils.extend(SimpleSegmentParser.prototype, ee.prototype);\n\n    return SimpleSegmentParser;\n});\n\ndefine(\'engine/mse/formats/webm/SimpleWebmSegmentParser\',[\n    \'utils/loggerFactory\',\n    \'engine/mse/formats/webm/webmUtils\',\n    \'engine/mse/formats/unknown/SimpleSegmentParser\'\n],\n\n/**\n* This module defines a class (SimpleWebmSegmentParser) with concrete implementation of ISegmentParser interface.\n* This should be used in cases where setting MSE\'s SourceBuffer\'s timestampOffset property is supported (i.e. Chrome).\n* Basically, this concrete implementation of ISegmentParser will append input file as-is.\n*/\nfunction(loggerFactory, webmUtils, SimpleSegmentParser) {\n    \'use strict\';\n\n    var log = loggerFactory.createLogger(\'SimpleWebmSegmentParser\');\n    log.disable();\n\n    function SimpleWebmSegmentParser(iSettings) {\n        SimpleSegmentParser.call(this, iSettings);\n    }\n\n    // Extend the base class SimpleSegmentParser\n    SimpleWebmSegmentParser.prototype = Object.create(SimpleSegmentParser.prototype);\n\n    // Override base implementation\n    SimpleWebmSegmentParser.prototype._tryParseMetadata = function() {\n        return webmUtils.parseMetadata(this.mBinaryLoader.arrayBuffer, this.mBinaryLoader.bytesLoaded);\n    };\n\n    return SimpleWebmSegmentParser;\n});\n\ndefine(\'engine/mse/formats/webm/ContiguousWebmSegmentParser\',[\n    \'utils/loggerFactory\',\n    \'mseUtils\',\n    \'engine/mse/formats/webm/webmUtils\',\n    \'utils/EventEmitter\'\n],\n\n/**\n* This module defines a class (ContiguousWebmSegmentParser) with concrete implementation of ISegmentParser interface.\n* This implementation will parse the source WebM file and create a contiguous WebM stream.\n*/\nfunction(loggerFactory, utils, webmUtils, ee) {\n    \'use strict\';\n\n    var log = loggerFactory.createLogger(\'ContiguousWebmSegmentParser\');\n    log.disable();\n\n    function ContiguousWebmSegmentParser(iSettings) {\n        /////////////////////////////////////////////////////////////////////////////////////////////////\n        // Private Members\n        /////////////////////////////////////////////////////////////////////////////////////////////////\n\n        var mBinaryLoader = iSettings.binaryLoader;\n        var mSegmentMetadata = null;\n        var mInitArrayBuffer = null;\n        var mClusterInfos = [];\n        var mPaused = false;\n        var mParseComplete = false;\n        var mjBin = null;\n        var _this = this;\n\n        /////////////////////////////////////////////////////////////////////////////////////////////////\n        // Private Methods\n        /////////////////////////////////////////////////////////////////////////////////////////////////\n\n        var readInitArrayBuffer = function() {\n            if (!mjBin) {\n                mjBin = webmUtils.createJBinaryInstance(mBinaryLoader.arrayBuffer);\n            }\n\n            var outJBin = webmUtils.createJBinaryInstance(new ArrayBuffer(10 * 1024 /* 10kB should be enough! */));\n            var currentHeader = null;\n            var element = null;\n            var finished = false;\n\n            mjBin.seek(0);\n\n            while (!finished && webmUtils.canReadElementHeader(mjBin, mBinaryLoader.bytesLoaded)) {\n                currentHeader = webmUtils.readElementHeader(mjBin);\n                switch (currentHeader.Name) {\n                    case \'EBML\':\n                    case \'Tracks\':\n                        if (!webmUtils.canReadElement(currentHeader, mBinaryLoader.bytesLoaded)) {\n                            return;\n                        }\n\n                        outJBin.write(\'Element\', webmUtils.readElement(mjBin, currentHeader));\n                        if (currentHeader.Name === \'Tracks\') {\n                            finished = true;\n                        }\n                        break;\n                    case \'Segment\':\n                        outJBin.write(\'ElementID\', currentHeader.ElementID);\n                        outJBin.write(\'DataSize\', -1);\n                        webmUtils.skipToSubElements(mjBin, currentHeader);\n                        break;\n                    case \'Info\':\n                        if (!webmUtils.canReadElement(currentHeader, mBinaryLoader.bytesLoaded)) {\n                            return;\n                        }\n\n                        element = webmUtils.readElement(mjBin, currentHeader);\n                        for (var i = 0; i < element.Data.length; i++) {\n                            if (element.Data[i].Name === \'Duration\') {\n                                element.Data[i].Data = 0;\n                                break;\n                            }\n                        }\n\n                        outJBin.write(\'Element\', element);\n                        break;\n                    default:\n                        if (!webmUtils.canReadElement(currentHeader, mBinaryLoader.bytesLoaded)) {\n                            return;\n                        }\n\n                        webmUtils.skipElement(mjBin, currentHeader);\n                        break;\n                }\n            }\n\n            if (finished) {\n                mInitArrayBuffer = outJBin.slice(0, outJBin.tell(), true).view.buffer;\n            }\n        };\n\n        var doParse = function() {\n            // If we\'ve already disposed of this instance, simply return\n            if (!mBinaryLoader) {\n                return;\n            }\n\n            // Read Metadata\n            if (!_this.metadata) {\n                return;\n            }\n\n            // Prepare init array buffer for file\n            if (!mInitArrayBuffer) {\n                readInitArrayBuffer();\n                if (!mInitArrayBuffer) {\n                    return;\n                }\n            }\n\n            // Read Cluster infos\n            var currentHeader = null;\n            var timecodeHeader;\n            var timecodeElement;\n            var clusterInfo;\n\n            while (webmUtils.canReadElementHeader(mjBin, mBinaryLoader.bytesLoaded)) {\n                if (mPaused) {\n                    break;\n                }\n\n                currentHeader = webmUtils.readElementHeader(mjBin);\n\n                switch (currentHeader.Name) {\n                    case \'Cluster\':\n                        webmUtils.skipToSubElements(mjBin, currentHeader);\n                        if (!webmUtils.canReadElementHeader(mjBin, mBinaryLoader.bytesLoaded)) {\n                            mjBin.seek(currentHeader._begin);\n                            return;\n                        }\n\n                        timecodeHeader = webmUtils.readElementHeader(mjBin);\n                        if (!webmUtils.canReadElement(currentHeader, mBinaryLoader.bytesLoaded)) {\n                            mjBin.seek(currentHeader._begin);\n                            return;\n                        }\n\n                        timecodeElement = webmUtils.readElement(mjBin, timecodeHeader);\n                        clusterInfo = {\n                            startByte: currentHeader._begin,\n                            endByte: currentHeader._beginData + currentHeader.DataSize,\n                            timestamp: timecodeElement.Data,\n                            timecodeStartByte: timecodeHeader._begin,\n                            simpleBlocksStartByte: mjBin.tell(),\n                            simpleBlocksDataSize: currentHeader._beginData + currentHeader.DataSize - mjBin.tell()\n                        };\n                        mClusterInfos.push(clusterInfo);\n                        mjBin.seek(clusterInfo.endByte);\n                        _this.trigger(\'parse_progress\', _this.timeParsed);\n                        break;\n                    default:\n                        if (!webmUtils.canReadElement(currentHeader, mBinaryLoader.bytesLoaded)) {\n                            mjBin.seek(currentHeader._begin);\n                            return;\n                        }\n                        webmUtils.skipElement(mjBin, currentHeader);\n                        break;\n                }\n            }\n\n            if (mjBin.tell() === mBinaryLoader.bytesTotal) {\n                mParseComplete = true;\n                _this.trigger(\'parse_progress\', _this.timeParsed);\n                _this.trigger(\'parse_complete\');\n\n                mBinaryLoader.off(\'progress\', doParse);\n                mBinaryLoader.addedParserProgressListener = false;\n            }\n        };\n\n        var appendArrayBuffer = function(arrayBuffer, clusterInfo, timestampOffsetMs) {\n            if (arrayBuffer && arrayBuffer.byteLength) {\n                _this.trigger(\'append_progress\', arrayBuffer, {\n                    chunkSrcTimestamp: clusterInfo ? clusterInfo.timestamp / 1000.0 : 0,\n                    chunkDstTimestamp: clusterInfo ? (clusterInfo.timestamp + timestampOffsetMs) / 1000.0 : 0,\n                    chunkDuration: null\n                });\n            }\n        };\n\n        var canAppendCluster = function(clusterInfo) {\n            return mBinaryLoader.bytesLoaded >= clusterInfo.endByte;\n        };\n\n        var createTimecodeBytes = function(timecodeTimestamp) {\n            var timecodeJBin = webmUtils.createJBinaryInstance(new ArrayBuffer(17));\n            timecodeJBin.write(\'ElementID\', \'[E7]\');\n            timecodeJBin.write(\'UInteger\', timecodeTimestamp);\n            return timecodeJBin.slice(0, timecodeJBin.tell(), true).view.buffer;\n        };\n\n        var createClusterHeader = function(dataSize) {\n            var clusterJBin = webmUtils.createJBinaryInstance(new ArrayBuffer(20));\n            clusterJBin.write(\'ElementID\', \'[1F][43][B6][75]\');\n            clusterJBin.write(\'DataSize\', dataSize);\n            return clusterJBin.slice(0, clusterJBin.tell(), true).view.buffer;\n        };\n\n        var getClusterBuffer = function(clusterInfo, timestampOffsetMs) {\n            var timecodeArrayBuffer = createTimecodeBytes(clusterInfo.timestamp + timestampOffsetMs);\n            var clusterHeaderArrayBuffer = createClusterHeader(timecodeArrayBuffer.byteLength + clusterInfo.simpleBlocksDataSize);\n            var clusterUint8Array = new Uint8Array(new ArrayBuffer(clusterHeaderArrayBuffer.byteLength + timecodeArrayBuffer.byteLength + clusterInfo.simpleBlocksDataSize));\n\n            clusterUint8Array.set(new Uint8Array(clusterHeaderArrayBuffer), 0);\n            clusterUint8Array.set(new Uint8Array(timecodeArrayBuffer), clusterHeaderArrayBuffer.byteLength);\n            clusterUint8Array.set(new Uint8Array(mBinaryLoader.arrayBuffer.slice(clusterInfo.simpleBlocksStartByte, clusterInfo.endByte)), clusterHeaderArrayBuffer.byteLength + timecodeArrayBuffer.byteLength);\n\n            return clusterUint8Array.buffer;\n        };\n\n        var appendCluster = function(clusterInfo, timestampOffsetMs) {\n            appendArrayBuffer(getClusterBuffer(clusterInfo, timestampOffsetMs), clusterInfo, timestampOffsetMs);\n        };\n\n        var prevClusterInfo = {\n            index: -1,\n            timestamp: 0\n        };\n\n        var getClusterInfoAtTimestamp = function(clusterTimestampMs) {\n            var retVal = null;\n\n            for (var i = prevClusterInfo && prevClusterInfo.timestamp < clusterTimestampMs ? prevClusterInfo.index + 1 : 0; i < mClusterInfos.length; i++) {\n                if (mClusterInfos[i].timestamp >= clusterTimestampMs) {\n                    retVal = mClusterInfos[i];\n                    prevClusterInfo.index = i;\n                    prevClusterInfo.timestamp = mClusterInfos[i].timestamp;\n                    break;\n                }\n            }\n\n            return retVal;\n        };\n\n        /////////////////////////////////////////////////////////////////////////////////////////////////\n        // Public Methods (ISegmentParser implementation)\n        /////////////////////////////////////////////////////////////////////////////////////////////////\n\n        this.parse = function() {\n            mPaused = false;\n            if (!mParseComplete) {\n                if (!mBinaryLoader.addedParserProgressListener) {\n                    mBinaryLoader.addedParserProgressListener = true;\n                    mBinaryLoader.on(\'progress\', doParse);\n                }\n\n                // Schedule parsing but return immediately\n                setTimeout(doParse, 0);\n            }\n        };\n\n        this.pauseParsing = function() {\n            mPaused = true;\n        };\n\n        this.append = function(globalAppendingContext, timestampOffset) {\n            var timestampOffsetMs = timestampOffset * 1000;\n\n            // LOCAL APPENDING CONTEXT\n            var currentClusterIndex = 0;\n\n            var appendInitIfNeeded = function() {\n                if (!globalAppendingContext.streamStarted) {\n                    if (mInitArrayBuffer) {\n                        appendArrayBuffer(_this.getInitSegment(globalAppendingContext, timestampOffset));\n                    }\n                }\n            };\n\n            var appendChunks = function() {\n                appendInitIfNeeded();\n\n                while (currentClusterIndex < mClusterInfos.length && canAppendCluster(mClusterInfos[currentClusterIndex])) {\n                    appendCluster(mClusterInfos[currentClusterIndex], timestampOffsetMs);\n                    currentClusterIndex++;\n                }\n\n                if (mParseComplete && currentClusterIndex === mClusterInfos.length) {\n                    _this.trigger(\'append_complete\');\n\n                    _this.off(\'parse_progress\', appendChunks);\n                    _this.off(\'parse_complete\', appendChunks);\n                    _this.addedParserSelfListeners = false;\n                }\n            };\n\n            if (mParseComplete) {\n                appendChunks();\n            } else {\n                appendChunks();\n                if (!_this.addedParserSelfListeners) {\n                    _this.addedParserSelfListeners = true;\n                    _this.on(\'parse_progress\', appendChunks);\n                    _this.on(\'parse_complete\', appendChunks);\n                }\n            }\n        };\n\n        this.getInitSegment = function(globalAppendingContext /* , timestampOffset */) {\n            globalAppendingContext.streamStarted = true;\n            return mInitArrayBuffer.slice(0, mInitArrayBuffer.byteLength);\n        };\n\n        this.getMediaSegment = function(globalAppendingContext, timestampOffset, mediaSegmentTimestamp) {\n            var clusterInfo = getClusterInfoAtTimestamp(mediaSegmentTimestamp * 1000);\n            if (clusterInfo && canAppendCluster(clusterInfo)) {\n                return getClusterBuffer(clusterInfo, timestampOffset * 1000);\n            }\n\n            return null;\n        };\n\n        this.dispose = function() {\n            if (mBinaryLoader) {\n                mBinaryLoader.off(\'progress\', doParse);\n            }\n            mBinaryLoader = null;\n            mSegmentMetadata = null;\n            mInitArrayBuffer = null;\n            mClusterInfos = [];\n            mPaused = false;\n            mParseComplete = false;\n            mjBin = null;\n        };\n\n        /////////////////////////////////////////////////////////\n        // Public Getters/Setters\n        /////////////////////////////////////////////////////////\n\n        Object.defineProperties(this, {\n\n            /** Does this parser type require incrementing the MSE SourceBuffer\'s timestampOffset?  */\n            shouldSetTimestampOffset: {\n                get: function() {\n                    return false;\n                }\n            },\n\n            /** Getter for metadata object. */\n            metadata: {\n                get: function() {\n                    if (!mSegmentMetadata) {\n                        mSegmentMetadata = webmUtils.parseMetadata(mBinaryLoader.arrayBuffer, mBinaryLoader.bytesLoaded);\n\n                        if (mSegmentMetadata) {\n                            this.trigger(\'metadata\', mSegmentMetadata);\n                        }\n                    }\n\n                    return mSegmentMetadata;\n                }\n            },\n\n            /** Has parsing completed? */\n            parsingCompleted: {\n                get: function() {\n                    return mParseComplete;\n                }\n            },\n\n            /** The time in seconds that was parsed. */\n            timeParsed: {\n                get: function() {\n                    if (mParseComplete) {\n                        return this.metadata.duration;\n                    }\n\n                    if (mClusterInfos.length) {\n                        return (mClusterInfos[mClusterInfos.length - 1].timestamp / 1000);\n                    }\n\n                    return 0.0;\n                }\n            }\n        });\n    }\n\n    // Add the EventEmitter functionality\n    utils.extend(ContiguousWebmSegmentParser.prototype, ee.prototype);\n\n    return ContiguousWebmSegmentParser;\n});\n\ndefine(\'engine/mse/formats/mp4/SimpleMp4SegmentParser\',[\n    \'utils/loggerFactory\',\n    \'engine/mse/formats/unknown/SimpleSegmentParser\'\n],\n\n/**\n* This module defines a class (SimpleMp4SegmentParser) with concrete implementation of ISegmentParser interface.\n* This should be used in cases where setting MSE\'s SourceBuffer\'s timestampOffset property is supported (i.e. Chrome).\n* Basically, this concrete implementation of ISegmentParser will append input file as-is.\n*/\nfunction(loggerFactory, SimpleSegmentParser) {\n    \'use strict\';\n\n    var log = loggerFactory.createLogger(\'SimpleMp4SegmentParser\');\n    log.disable();\n\n    function SimpleMp4SegmentParser(iSettings) {\n        SimpleSegmentParser.call(this, iSettings);\n    }\n\n    // Extend the base class SimpleSegmentParser\n    SimpleMp4SegmentParser.prototype = Object.create(SimpleSegmentParser.prototype);\n\n    // Override base implementation\n    SimpleMp4SegmentParser.prototype._tryParseMetadata = function() {\n        //log.warn(\'Getting metadata from MP4 is not yet supported.\');\n        return null;\n    };\n\n    Object.defineProperties(SimpleMp4SegmentParser.prototype, {\n        /** Has parsing completed? */\n        parsingCompleted: {\n            get: function() {\n                return true;\n            }\n        }\n    });\n\n    return SimpleMp4SegmentParser;\n});\n\ndefine(\'engine/mse/formats/imc/imc\',[\n    \'jbinary\',\n    \'engine/mse/formats/webm/webm\',\n    \'engine/mse/formats/webm/ebmlutils\',\n    \'mseUtils\'\n],\n\n/**\n* This module defines a JBinary typeset that corresponds to the Interlude Media Container (IMC) spec (by extending the WebM module).\n* Spec: https://docs.google.com/a/interlude.fm/document/d/1D2OSPAN4Yn7VrZNuNEkTFUihGcx9sKnuq_3FsOsItxw\n*/\nfunction(JBinary, WEBM, ebmlUtils, utils) {\n\n    \'use strict\';\n\n    var imcElementMetadataMap = {\n        \'[49][4C]\': {\n            Name: \'Interlude\',\n            Type: \'Master\'\n        },\n\n        \'[6F][0A]\': {\n            Name: \'SegmentType\',\n            Type: \'String\',\n            Parents: [\n                \'[49][4C]\'\n            ]\n        },\n\n        \'[6F][0B]\': {\n            Name: \'SegmentTypeVersion\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[49][4C]\'\n            ]\n        },\n\n        \'[6F][0C]\': {\n            Name: \'SegmentTypeReadVersion\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[49][4C]\'\n            ]\n        },\n\n        \'[7F][01]\': {\n            Name: \'IChannelCount\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[15][49][A9][66]\'\n            ]\n        },\n\n        \'[7F][02]\': {\n            Name: \'SourceFormat\',\n            Type: \'String\',\n            Parents: [\n                \'[15][49][A9][66]\'\n            ]\n        },\n\n        \'[16][54][AE][6C]\': {\n            Name: \'IChannels\',\n            Type: \'Master\',\n            Parents: [\n                \'[18][53][80][67]\'\n            ]\n        },\n\n        \'[AD]\': {\n            Name: \'IChannelEntry\',\n            Type: \'Master\',\n            Parents: [\n                \'[16][54][AE][6C]\'\n            ]\n        },\n\n        \'[7F][03]\': {\n            Name: \'IChannelID\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[AD]\'\n            ]\n        },\n\n        \'[7F][FE]\': {\n            Name: \'IChannelSourceFormat\',\n            Type: \'String\',\n            Parents: [\n                \'[AD]\'\n            ]\n        },\n\n        \'[7F][04]\': {\n            Name: \'IChannelTracksDescription\',\n            Type: \'String\',\n            Parents: [\n                \'[AD]\'\n            ]\n        },\n\n        \'[7F][05]\': {\n            Name: \'IChannelByteLength\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[AD]\'\n            ]\n        },\n\n        \'[44][88]\': {\n            Name: \'IChannelDuration\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[AD]\'\n            ]\n        },\n\n        \'[7F][FF]\': {\n            Name: \'IChannelInitSegment\',\n            Type: \'Binary\',\n            Parents: [\n                \'[AD]\'\n            ]\n        },\n\n        \'[8A]\': {\n            Name: \'Chunk\',\n            Type: \'Master\',\n            Parents: [\n                \'[18][53][80][67]\'\n            ]\n        },\n\n        \'[82]\': {\n            Name: \'ChunkIChannelID\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[8A]\'\n            ]\n        },\n\n        \'[87]\': {\n            Name: \'ChunkTimestamp\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[8A]\'\n            ]\n        },\n\n        \'[84]\': {\n            Name: \'ChunkDuration\',\n            Type: \'UInteger\',\n            Parents: [\n                \'[8A]\'\n            ]\n        },\n\n        \'[DA]\': {\n            Name: \'ChunkData\',\n            Type: \'Binary\',\n            Parents: [\n                \'[8A]\'\n            ]\n        }\n    };\n\n    // Add the IMC specific MetaData objects to map\n    utils.extend(ebmlUtils.elementMetadataMap, imcElementMetadataMap);\n\n    // Extend the typeSet\n    return utils.extend({}, WEBM);\n});\n\ndefine(\'engine/mse/formats/imc/imcUtils\',[\n    \'utils/loggerFactory\',\n    \'mseUtils\',\n    \'jbinary\',\n    \'engine/mse/formats/webm/webmUtils\',\n    \'engine/mse/formats/imc/imc\'\n],\n\n/**\n* This module defines some helper methods for working with IMC files\n*/\nfunction(loggerFactory, utils, JBinary, webmUtils, IMC) {\n    \'use strict\';\n\n    var log = loggerFactory.createLogger(\'imcUtils\');\n\n    // CONSTS\n    var DOCTYPEVALUE = \'interlude\';\n    var SEGMENTTYPEVALUES = [\'channels\'];\n\n    log.disable();\n\n    return utils.extend({}, webmUtils, {\n\n        getChildren: function(element, childName, recursive) {\n            recursive = !!(recursive || false);\n            var retVal = [];\n\n            if (!element || element.Type !== \'Master\') {\n                return retVal;\n            }\n\n            for (var i = 0; i < element.Data.length; i++) {\n                if (element.Data[i].Name === childName) {\n                    retVal.push(element.Data[i]);\n                }\n\n                if (recursive && element.Data[i].Type === \'Master\') {\n                    retVal = retVal.concat(this.getChildren(element, childName, recursive));\n                }\n            }\n\n            return retVal;\n        },\n\n        getChild: function(element, childName, recursive) {\n            var children = this.getChildren(element, childName, recursive);\n            return children.length ? children[0] : null;\n        },\n\n        /**\n        * Parse the currently available bytes and determine if this is an IMC file.\n        * Returns true if IMC, false if not IMC, and null if cannot find out (i.e. not enough bytes loaded).\n        */\n        probeIsIMC: function(arrayBuffer, bytesLoaded) {\n            if (!arrayBuffer) {\n                return null;\n            }\n\n            if (!bytesLoaded && bytesLoaded !== 0) {\n                bytesLoaded = arrayBuffer.byteLength;\n            }\n\n            /** 63 is the offset of Segment in IMC files created with InterludeMuxer...\n            * This is the minimal number of bytes needed to read EBML/Interlude headers. */\n            if (bytesLoaded < 63) {\n                return null;\n            }\n\n            var retVal = null;\n            var jBin = new JBinary(arrayBuffer, IMC);\n            var currentHeader;\n            var ebmlElement;\n            var docTypeElement;\n            var interludeElement;\n            var segmentTypeElement;\n\n            try {\n                // Read EBML\n                if (!this.canReadElementHeader(jBin, bytesLoaded)) {\n                    return null;\n                }\n\n                currentHeader = this.readElementHeader(jBin);\n                if (currentHeader.Name === \'EBML\') {\n                    if (!this.canReadElement(currentHeader, bytesLoaded)) {\n                        return null;\n                    }\n                    ebmlElement = this.readElement(jBin, currentHeader);\n                    docTypeElement = this.getChild(ebmlElement, \'DocType\');\n                    if (docTypeElement && docTypeElement.Data === DOCTYPEVALUE) {\n                        retVal = true;\n                    }\n                } else {\n                    retVal = false;\n                }\n\n                // Read Interlude header\n                if (!this.canReadElementHeader(jBin, bytesLoaded)) {\n                    return null;\n                }\n\n                currentHeader = this.readElementHeader(jBin);\n                if (retVal && currentHeader.Name === \'Interlude\') {\n                    if (!this.canReadElement(currentHeader, bytesLoaded)) {\n                        return null;\n                    }\n\n                    interludeElement = this.readElement(jBin, currentHeader);\n                    segmentTypeElement = this.getChild(ebmlElement, \'SegmentType\');\n                    if (segmentTypeElement && segmentTypeElement.Data in SEGMENTTYPEVALUES) {\n                        retVal = true;\n                    }\n                } else {\n                    retVal = false;\n                }\n            } catch (e) {\n                retVal = false;\n            }\n\n            return retVal;\n        },\n\n        /**\n         * Reads a chunk element.\n         * Assumes that jBin\'s position is at the top of Chunk element.\n         * If unsuccessful, will return jBin position to previous position and return null.\n         * If successful, returns a chunk object containing the following fields: \'id\', \'timestamp\', \'duration\' and \'data\'.\n         */\n        readChunk: function(jBin, bytesLoaded) {\n            if (!jBin) {\n                return null;\n            }\n\n            if (!bytesLoaded && bytesLoaded !== 0) {\n                bytesLoaded = jBin.view.byteLength;\n            }\n\n            var startPosition = jBin.tell();\n            var currentHeader;\n            var chunkElement;\n\n            if (!this.canReadElementHeader(jBin, bytesLoaded)) {\n                jBin.seek(startPosition); return null;\n            }\n\n            currentHeader = this.readElementHeader(jBin);\n            if (!this.canReadElement(currentHeader, bytesLoaded)) {\n                jBin.seek(startPosition); return null;\n            }\n\n            chunkElement = this.readElement(jBin, currentHeader);\n            if (!chunkElement || chunkElement.Name !== \'Chunk\') {\n                jBin.seek(startPosition); return null;\n            }\n\n            return {\n                id: this.getChild(chunkElement, \'ChunkIChannelID\').Data,\n                timestamp: this.getChild(chunkElement, \'ChunkTimestamp\').Data,\n                duration: this.getChild(chunkElement, \'ChunkDuration\').Data,\n                data: this.getChild(chunkElement, \'ChunkData\').Data\n            };\n        },\n\n        getChannelMeta: function(ichannelEntryElement) {\n            if (!ichannelEntryElement || ichannelEntryElement.Name !== \'IChannelEntry\') {\n                throw new Error(\'imcUtils::getChannelMeta() - expected IChannelEntry element.\');\n            }\n\n            var byteLengthElement = this.getChild(ichannelEntryElement, \'IChannelByteLength\');\n\n            return {\n                id: this.getChild(ichannelEntryElement, \'IChannelID\').Data,\n                sourceFormat: this.getChild(ichannelEntryElement, \'IChannelSourceFormat\').Data,\n                tracksDescription: this.getChild(ichannelEntryElement, \'IChannelTracksDescription\').Data,\n                duration: this.getChild(ichannelEntryElement, \'IChannelDuration\').Data,\n                byteLength: byteLengthElement ? byteLengthElement.Data : null,\n                initSegment: this.getChild(ichannelEntryElement, \'IChannelInitSegment\').Data\n            };\n        },\n\n        getChannelsMetaArray: function(ichannelsElement) {\n            if (!ichannelsElement || ichannelsElement.Name !== \'IChannels\') {\n                throw new Error(\'imcUtils::getChannelsMetaArray() - expected IChannels element.\');\n            }\n\n            var retVal = [];\n            var childElements = this.getChildren(ichannelsElement, \'IChannelEntry\');\n\n            for (var i = 0; i < childElements.length; i++) {\n                retVal.push(this.getChannelMeta(childElements[i]));\n            }\n\n            return retVal;\n        },\n\n        getChannelsInfoObject: function(infoElement) {\n            if (!infoElement || infoElement.Name !== \'Info\') {\n                throw new Error(\'imcUtils::getChannelsInfoObject() - expected Info element.\');\n            }\n\n            var muxingApp = this.getChild(infoElement, \'MuxingApp\');\n            var writingApp = this.getChild(infoElement, \'WritingApp\');\n\n            muxingApp = muxingApp ? muxingApp.Data : \'\';\n            writingApp = writingApp ? writingApp.Data : \'\';\n\n            return {\n                channelCount: this.getChild(infoElement, \'IChannelCount\').Data,\n                sourceFormat: this.getChild(infoElement, \'SourceFormat\').Data,\n                muxingApp: muxingApp,\n                writingApp: writingApp\n            };\n        },\n\n        createJBinaryInstance: function(arrayBuffer) {\n            if (!arrayBuffer) {\n                return null;\n            }\n\n            return new JBinary(arrayBuffer, IMC);\n        },\n\n        /**\n        * Parse the currently available IMC bytes and try to read metadata.\n        * If successful, returns a SegmentMetadata object. Otherwise, returns null.\n        * Current implementation only reads the \'duration\'.\n        */\n        parseChannelsMetadata: function(arrayBuffer, bytesLoaded) {\n            if (!arrayBuffer) {\n                return null;\n            }\n\n            if (!bytesLoaded && bytesLoaded !== 0) {\n                bytesLoaded = arrayBuffer.byteLength;\n            }\n\n            var jBin = new JBinary(arrayBuffer, IMC);\n            var reachedInfo = false;\n            var currentHeader;\n            var infoElement;\n            var ichannelsElement;\n\n            // Seek to info\n            while (!reachedInfo) {\n                if (!this.canReadElementHeader(jBin, bytesLoaded)) {\n                    return null;\n                }\n\n                currentHeader = this.readElementHeader(jBin);\n\n                switch (currentHeader.Name) {\n                    case \'Segment\':\n                        this.skipToSubElements(jBin, currentHeader);\n                        reachedInfo = true;\n                        break;\n                    default:\n                        if (!this.canReadElement(currentHeader, bytesLoaded)) {\n                            return null;\n                        }\n                        this.skipElement(jBin, currentHeader);\n                        break;\n                }\n            }\n\n            // Read Info Element\n            if (!this.canReadElementHeader(jBin, bytesLoaded)) {\n                return null;\n            }\n\n            currentHeader = this.readElementHeader(jBin);\n            if (!this.canReadElement(currentHeader, bytesLoaded)) {\n                return null;\n            }\n\n            infoElement = this.readElement(jBin, currentHeader);\n\n            // Read IChannels Element\n            if (!this.canReadElementHeader(jBin, bytesLoaded)) {\n                return null;\n            }\n\n            currentHeader = this.readElementHeader(jBin);\n            if (!this.canReadElement(currentHeader, bytesLoaded)) {\n                return null;\n            }\n\n            ichannelsElement = this.readElement(jBin, currentHeader);\n\n            return utils.extend({\n                channels: this.getChannelsMetaArray(ichannelsElement),\n                firstChunkOffset: jBin.tell()\n            }, this.getChannelsInfoObject(infoElement));\n        }\n    });\n});\n\ndefine(\'engine/mse/formats/imc/SimpleImcSegmentParser\',[\n    \'utils/loggerFactory\',\n    \'mseUtils\',\n    \'utils/EventEmitter\',\n    \'engine/mse/formats/imc/imcUtils\',\n    \'engine/mse/models/SegmentMetadata\'\n],\n\n/**\n* This module defines a class (SimpleSimpleImcSegmentParser) with concrete implementation of ISegmentParser interface.\n* It is "Simple" in the sense that it uses direct appending after unwrapping source streams from IMC container.\n* This parser is currently used for Parallel videos, contained in an IMC container.\n*/\nfunction(loggerFactory, utils, ee, imcUtils, SegmentMetadata) {\n    \'use strict\';\n\n    var log = loggerFactory.createLogger(\'SimpleImcSegmentParser\');\n\n    // CONSTS\n    var DEFAULT_PARALLELS_APPEND_AHEAD = 800;\n    var DEFAULT_PARALLELS_INITIAL_APPEND = 2000;\n    var DEFAULT_PLAYBACK_INTERVAL_MS = 150;\n\n    function SimpleImcSegmentParser(iSettings) {\n        // Public\n        this.mBinaryLoader = iSettings.binaryLoader;\n        this.mSegmentObject = iSettings.segment;\n        this.mParallelsAppendAheadOffset = iSettings.parallels && iSettings.parallels.appendOffset ? iSettings.parallels.appendOffset * 1000 : DEFAULT_PARALLELS_APPEND_AHEAD;\n        this.mParallelsInitialAppend = iSettings.parallels && iSettings.parallels.initialAppend ? iSettings.parallels.initialAppend * 1000 : DEFAULT_PARALLELS_INITIAL_APPEND;\n        this.mPlaybackIntervalMs = iSettings.parallels && iSettings.parallels.playbackInterval ? iSettings.parallels.playbackInterval * 1000 : DEFAULT_PLAYBACK_INTERVAL_MS;\n        // Public - event listeners with \'this\' context\n        this.onLoaderProgress = function() {\n            this.doParse();\n        }.bind(this);\n    }\n\n    /////////////////////////////////////////////////////////////////////////////////////////////////\n    // Private Members\n    /////////////////////////////////////////////////////////////////////////////////////////////////\n\n    SimpleImcSegmentParser.prototype.mSegmentMetadata = null;\n    SimpleImcSegmentParser.prototype.mChannelsMetadata = null;\n    SimpleImcSegmentParser.prototype.mTimeParsed = 0.0;\n    SimpleImcSegmentParser.prototype.mPaused = false;\n    SimpleImcSegmentParser.prototype.mParseComplete = false;\n    SimpleImcSegmentParser.prototype.mjBin = null;\n    SimpleImcSegmentParser.prototype.mChannelsMap = null;\n    SimpleImcSegmentParser.prototype.mPlaybackTime = 0.0;\n    SimpleImcSegmentParser.prototype.mPlaybackIntervalRef = null;\n\n    // Parallels Settings\n    SimpleImcSegmentParser.prototype.mParallelsAppendAheadOffset = 0.0;\n    SimpleImcSegmentParser.prototype.mPlaybackIntervalMs = 0.0;\n\n    /////////////////////////////////////////////////////////////////////////////////////////////////\n    // Private Methods\n    /////////////////////////////////////////////////////////////////////////////////////////////////\n\n    /** Try parse binary data and return a metadata object if successful, null if not.\n     *  Also populates this.mChannelsMetadata if successful. */\n    SimpleImcSegmentParser.prototype._tryParseMetadata = function() {\n        if (!this.mChannelsMetadata) {\n            this.mChannelsMetadata = imcUtils.parseChannelsMetadata(this.mBinaryLoader.arrayBuffer, this.mBinaryLoader.bytesLoaded);\n        }\n\n        var retVal = null;\n\n        if (this.mChannelsMetadata) {\n            var durations = [];\n            retVal = new SegmentMetadata();\n            this.mChannelsMap = {};\n\n            for (var i = 0; i < this.mChannelsMetadata.channels.length; i++) {\n                durations.push(this.mChannelsMetadata.channels[i].duration);\n                this.mChannelsMap[this.mChannelsMetadata.channels[i].id] = this.mChannelsMetadata.channels[i];\n                this.mChannelsMap[this.mChannelsMetadata.channels[i].id].mediaSegments = {};\n                this.mChannelsMap[this.mChannelsMetadata.channels[i].id].mediaSegments.lastParsedTimestamp = -1;\n            }\n\n            retVal.duration = Math.min.apply(null, durations) / 1000.0;\n        }\n\n        return retVal;\n    };\n\n    /** Actual parsing of stream. */\n    SimpleImcSegmentParser.prototype.doParse = function() {\n        // If we\'ve already disposed of this instance, simply return\n        if (!this.mBinaryLoader) {\n            return;\n        }\n\n        // Read Metadata\n        if (!this.metadata) {\n            return;\n        }\n\n        // Don\'t parse if paused\n        if (this.mPaused) {\n            return;\n        }\n\n        // Great - we have metadata => we have this.mChannelsMetadata.\n        // Let\'s parse the chunks!\n        if (!this.mjBin) {\n            this.mjBin = imcUtils.createJBinaryInstance(this.mBinaryLoader.arrayBuffer);\n            this.mjBin.seek(this.mChannelsMetadata.firstChunkOffset);\n        }\n\n        // Parse one chunk (this is no longer a eager loop, since we want to avoid long operation to keep worker thread responsive to messages).\n        var currChunk = imcUtils.readChunk(this.mjBin, this.mBinaryLoader.bytesLoaded);\n        if (currChunk) {\n            this.mChannelsMap[currChunk.id].mediaSegments[currChunk.timestamp] = currChunk;\n            this.mChannelsMap[currChunk.id].mediaSegments.lastParsedTimestamp = currChunk.timestamp;\n            this.mTimeParsed = currChunk.timestamp / 1000.0;\n            this.trigger(\'parse_progress\', this.timeParsed);\n        }\n\n        if (this.mjBin.tell() === this.mBinaryLoader.bytesTotal) {\n            this.mParseComplete = true;\n            this.mTimeParsed = this.metadata.duration;\n\n            this.trigger(\'parse_progress\', this.timeParsed);\n            this.trigger(\'parse_complete\');\n\n            this.mBinaryLoader.off(\'progress\', this.onLoaderProgress);\n            this.mBinaryLoader.addedParserProgressListener = false;\n\n            // FOR DEBUGGING\n            /*var printMediaSegment = function(mediaSeg) {\n                log.info(\'      timestamp: \' + mediaSeg.timestamp + \', duration: \' + mediaSeg.duration);\n            };\n            log.enableAll();\n            for (var prop in this.mChannelsMap) {\n                log.info(\'ID: \' + prop);\n                for (var key in this.mChannelsMap[prop].mediaSegments) {\n                    if (key !== \'lastParsedTimestamp\') {\n                        printMediaSegment(this.mChannelsMap[prop].mediaSegments[key]);\n                    }\n                }\n            }*/\n        } else if (!this.mPaused) {\n            // Continue parsing next chunk (schedule so that worker thread is responsive)\n            setTimeout(this.doParse.bind(this), 0);\n        }\n    };\n\n    SimpleImcSegmentParser.prototype.appendArrayBuffer = function(arrayBuffer, chunk, timestampOffsetMs) {\n        if (arrayBuffer instanceof Uint8Array) {\n            arrayBuffer = arrayBuffer.buffer.slice(arrayBuffer.byteOffset, arrayBuffer.byteOffset + arrayBuffer.length);\n        }\n\n        if (arrayBuffer && arrayBuffer.byteLength) {\n            this.trigger(\'append_progress\', arrayBuffer, {\n                chunkSrcTimestamp: chunk ? chunk.timestamp / 1000.0 : 0,\n                chunkDstTimestamp: chunk ? (chunk.timestamp + timestampOffsetMs) / 1000.0 : 0,\n                chunkDuration: chunk ? chunk.duration / 1000.0 : 0\n            });\n        }\n    };\n\n    SimpleImcSegmentParser.prototype.appendChunk = function(chunk, timestampOffsetMs) {\n        this.appendArrayBuffer(chunk.data, chunk, timestampOffsetMs);\n        return chunk.timestamp + chunk.duration;\n    };\n\n    SimpleImcSegmentParser.prototype.doAppendInitSegment = function(appendingContext, buffer) {\n        appendingContext.streamStarted = true;\n        this.appendArrayBuffer(buffer);\n    };\n\n    /////////////////////////////////////////////////////////////////////////////////////////////////\n    // Public Methods (ISegmentParser implementation)\n    /////////////////////////////////////////////////////////////////////////////////////////////////\n\n    SimpleImcSegmentParser.prototype.parse = function() {\n        this.mPaused = false;\n        if (!this.mParseComplete) {\n            if (!this.mBinaryLoader.addedParserProgressListener) {\n                this.mBinaryLoader.addedParserProgressListener = true;\n                this.mBinaryLoader.on(\'progress\', this.onLoaderProgress);\n            }\n\n            // Schedule parsing but return immediately\n            setTimeout(this.doParse.bind(this), 0);\n        }\n    };\n\n    SimpleImcSegmentParser.prototype.pauseParsing = function() {\n        this.mPaused = true;\n    };\n\n    SimpleImcSegmentParser.prototype.timeupdate = function(data) {\n        this.trigger(\'timeupdate\', data);\n    };\n\n    SimpleImcSegmentParser.prototype.switchChannel = function(channelId) {\n        // Map the channelId into numerical IMC channelID\n        if (this.mSegmentObject && this.mSegmentObject.map && channelId in this.mSegmentObject.map) {\n            channelId = this.mSegmentObject.map[channelId];\n        } else if (this.mSegmentObject && this.mSegmentObject.map) {\n            log.warn(\'Ignoring switch channel request - unknown channelId: \' + channelId);\n        }\n\n        if (this.mChannelsMap && channelId in this.mChannelsMap) {\n            this.trigger(\'switch_channel\', channelId);\n        } else if (this.mChannelsMap) {\n            log.warn(\'Ignoring switch channel request - unknown IMC ChannelID: \' + channelId);\n        }\n    };\n\n    SimpleImcSegmentParser.prototype.append = function(appendingContext, timestampOffset) {\n        // LOCAL APPENDING CONTEXT\n        var timestampOffsetMs = timestampOffset * 1000;\n        var appendedInit = false;\n        var currentTimestampOffset = 0;\n        var _this = this;\n        var currChannel = 1;\n        var requestedChannel = 1;\n        var shouldAppendEmergencyChunk = false;\n        var playing = false;\n\n        var appendInitIfNeeded = function() {\n            if (!appendedInit) {\n                if (_this.mChannelsMap && _this.mChannelsMap[currChannel] && _this.mChannelsMap[currChannel].initSegment) {\n                    appendedInit = true;\n                    _this.doAppendInitSegment(appendingContext, _this.mChannelsMap[currChannel].initSegment);\n                }\n            }\n        };\n\n        var getNextChunk = function() {\n            // If not yet initialized, return null\n            if (!_this.mChannelsMap || !_this.mChannelsMap[currChannel]) {\n                return null;\n            }\n\n            // If playback has not yet reached offset, return null\n            if (!shouldAppendEmergencyChunk &&\n                _this.mPlaybackTime < currentTimestampOffset - _this.mParallelsAppendAheadOffset &&\n                currentTimestampOffset >= _this.mParallelsInitialAppend) {\n                return null;\n            }\n\n            var currChunk = null;\n\n            // If a channel switch was requested, and we\'re able to switch at this chunk, perform the switch\n            if (requestedChannel !== currChannel && _this.mChannelsMap[requestedChannel].mediaSegments[currentTimestampOffset]) {\n                currChannel = requestedChannel;\n                _this.trigger(\'channel_switched\', currentTimestampOffset / 1000.0);\n            }\n\n            currChunk = _this.mChannelsMap[currChannel].mediaSegments[currentTimestampOffset];\n\n            // Chunk is missing...\n            if (!currChunk && _this.mChannelsMap[currChannel].mediaSegments.lastParsedTimestamp >= currentTimestampOffset) {\n                log.error(\'Missing IMC chunk for channel \' + currChannel + \' at timestamp \' + currentTimestampOffset);\n            }\n\n            return currChunk;\n        };\n\n        var onSwitchChannel = function(newChannelId) {\n            requestedChannel = newChannelId;\n        };\n\n        var onTimeUpdate = function(data) {\n            // If time is the string \'paused\', trigger the \'paused\' event\n            if (data && data.paused) {\n                playing = false;\n            }\n            // Otherwise, time is a number\n            else if (data && typeof data.time === \'number\') {\n                playing = true;\n                var playbackTime = (data.time * 1000.0) - timestampOffsetMs;\n\n                // If time has not advanced since last tick, declare this an emergency!\n                if (playbackTime === _this.mPlaybackTime) {\n                    shouldAppendEmergencyChunk = true;\n                }\n\n                _this.mPlaybackTime = playbackTime;\n\n                if (_this.mPlaybackTime >= 0) {\n                    appendChunks();\n                }\n            }\n        };\n\n        var appendChunks = function() {\n            appendInitIfNeeded();\n\n            var chunk = getNextChunk();\n            while (chunk) {\n                currentTimestampOffset = _this.appendChunk(chunk, timestampOffsetMs);\n                shouldAppendEmergencyChunk = false;\n                chunk = getNextChunk();\n            }\n\n            if (_this.metadata && currentTimestampOffset >= (_this.metadata.duration * 1000.0)) {\n                _this.trigger(\'append_complete\');\n\n                _this.off(\'parse_progress\', appendChunks);\n                _this.off(\'parse_complete\', appendChunks);\n                _this.off(\'timeupdate\', onTimeUpdate);\n                _this.off(\'switch_channel\', onSwitchChannel);\n                _this.addedParserSelfListeners = false;\n                clearInterval(_this.mPlaybackIntervalRef);\n            }\n        };\n\n        appendChunks();\n        if (!_this.addedParserSelfListeners) {\n            _this.addedParserSelfListeners = true;\n            _this.on(\'parse_progress\', appendChunks);\n            _this.on(\'parse_complete\', appendChunks);\n            _this.on(\'timeupdate\', onTimeUpdate);\n            _this.on(\'switch_channel\', onSwitchChannel);\n        }\n\n        _this.mPlaybackIntervalRef = setInterval(function() {\n            _this.trigger(\'request_timeupdate\');\n        }, _this.mPlaybackIntervalMs);\n    };\n\n    SimpleImcSegmentParser.prototype.getInitSegment = function(/* appendingContext, timestampOffset */) {\n        // Do nothing\n    };\n\n    SimpleImcSegmentParser.prototype.getMediaSegment = function(/* appendingContext, timestampOffset, mediaSegmentTimestamp */) {\n        // Do nothing\n    };\n\n    SimpleImcSegmentParser.prototype.dispose = function() {\n        // Remove binary loader event listeners\n        if (this.mBinaryLoader) {\n            this.mBinaryLoader.off(\'progress\', this.onLoaderProgress);\n        }\n\n        this.mBinaryLoader = null;\n        this.mSegmentMetadata = null;\n        this.mTimeParsed = 0.0;\n        this.mChannelsMetadata = null;\n        this.mPaused = null;\n        this.mParseComplete = false;\n        this.mjBin = null;\n        this.mChannelsMap = null;\n        this.mPlaybackTime = 0.0;\n        this.mParallelsAppendAheadOffset = 0.0;\n        this.mPlaybackIntervalMs = 0.0;\n\n        if (this.mPlaybackIntervalRef) {\n            clearInterval(this.mPlaybackIntervalRef);\n        }\n        this.mPlaybackIntervalRef = null;\n    };\n\n    /////////////////////////////////////////////////////////\n    // Public Getters/Setters\n    /////////////////////////////////////////////////////////\n\n    Object.defineProperties(SimpleImcSegmentParser.prototype, {\n\n        /** Does this parser type require incrementing the MSE SourceBuffer\'s timestampOffset?  */\n        shouldSetTimestampOffset: {\n            get: function() {\n                return true;\n            }\n        },\n\n        /** Getter for metadata object. */\n        metadata: {\n            get: function() {\n                if (!this.mSegmentMetadata) {\n                    this.mSegmentMetadata = this._tryParseMetadata();\n\n                    if (this.mSegmentMetadata) {\n                        this.trigger(\'metadata\', this.mSegmentMetadata);\n                    }\n                }\n\n                return this.mSegmentMetadata;\n            }\n        },\n\n        /** Has parsing completed? */\n        parsingCompleted: {\n            get: function() {\n                return this.mParseComplete;\n            }\n        },\n\n        /** The time in seconds that was parsed. */\n        timeParsed: {\n            get: function() {\n                return this.mTimeParsed;\n            }\n        }\n    });\n\n    // Add the EventEmitter functionality\n    utils.extend(SimpleImcSegmentParser.prototype, ee.prototype);\n\n    return SimpleImcSegmentParser;\n});\n\ndefine(\'engine/mse/utils/PseudoBinaryLoader\',[\n    \'utils/loggerFactory\',\n    \'mseUtils\',\n    \'utils/EventEmitter\'\n],\n\n/**\n* This module loads a single URL to a binary ArrayBuffer in a pseudo-progressive fashion (using Range requests).\n* Note that if using cross-domain URL, the server must support CORS, HEAD request and Range header.\n* Exposes progress/complete events.\n*/\nfunction(loggerFactory, utils, ee) {\n    \'use strict\';\n\n    var log = loggerFactory.createLogger(\'PseudoBinaryLoader\');\n    log.disable();\n\n    function PseudoBinaryLoader(iUrl, iTotalSize) {\n\n        /////////////////////////////////////////////////////////\n        // Private Members\n        /////////////////////////////////////////////////////////\n\n        var mTotalSize = iTotalSize;\n        var mCurrentOffset = 0;\n        var mComplete = false;\n        var mUrl = iUrl;\n        var mArrayBuffer = new ArrayBuffer(mTotalSize);\n        var mArrayBufferTyped = new Uint8Array(mArrayBuffer);\n        var mPaused = false;\n        var mCurrentlyLoading = true;\n        var _this = this;\n\n        /////////////////////////////////////////////////////////\n        // Private Methods\n        /////////////////////////////////////////////////////////\n\n        var notifyProgress = function() {\n            _this.trigger(\'progress\', mCurrentOffset, mTotalSize);\n        };\n\n        var notifyComplete = function() {\n            _this.trigger(\'complete\');\n        };\n\n        var setNextChunkBuffer = function(buffer) {\n            mArrayBufferTyped.set(new Uint8Array(buffer), mCurrentOffset);\n            mCurrentOffset += buffer.byteLength;\n        };\n\n        /////////////////////////////////////////////////////////\n        // Public Methods\n        /////////////////////////////////////////////////////////\n\n        this.load = function() {\n            mCurrentlyLoading = true;\n        };\n\n        this.append = function(buffer) {\n            if (buffer instanceof Uint8Array) {\n                buffer = buffer.buffer.slice(buffer.byteOffset, buffer.byteOffset + buffer.length);\n            }\n            setNextChunkBuffer(buffer);\n            notifyProgress();\n            if (mTotalSize === mCurrentOffset) {\n                mComplete = true;\n                notifyComplete();\n            }\n        };\n\n        this.pause = function() {\n            if (!mComplete) {\n                mPaused = true;\n            }\n        };\n\n        this.resume = function() {\n            mPaused = false;\n            if (mComplete) {\n                notifyComplete();\n            }\n        };\n\n        this.dispose = function() {\n            mTotalSize = 0;\n            mCurrentOffset = 0;\n            mComplete = false;\n            mUrl = null;\n            mArrayBuffer = null;\n            mArrayBufferTyped = null;\n            mPaused = false;\n            mCurrentlyLoading = false;\n            _this = null;\n        };\n\n        /////////////////////////////////////////////////////////\n        // Public Getters/Setters\n        /////////////////////////////////////////////////////////\n\n        Object.defineProperties(this, {\n\n            /** Number of bytes that were loaded and available in arrayBuffer and/or arrayBufferSlice. */\n            bytesLoaded: {\n                get: function() {\n                    return mCurrentOffset;\n                }\n            },\n\n            /** Number of total bytes. */\n            bytesTotal: {\n                get: function() {\n                    return mTotalSize;\n                }\n            },\n\n            /** ArrayBuffer containing bytesTotal bytes, but only first bytesLoaded bytes are populated with data. */\n            arrayBuffer: {\n                get: function() {\n                    return mArrayBuffer;\n                }\n            },\n\n            /** ArrayBuffer containing bytesLoaded bytes and populated with data.\n            * This could be expensive, so whenever possible use \'arrayBuffer\' instead. */\n            arrayBufferSlice: {\n                get: function() {\n                    if (mComplete) {\n                        return mArrayBuffer;\n                    }\n\n                    return mArrayBuffer.slice(0, mCurrentOffset);\n                }\n            },\n\n            /** Boolean - has loading completed. */\n            completed: {\n                get: function() {\n                    return mComplete;\n                }\n            },\n\n            /** Boolean - has loading been paused. */\n            paused: {\n                get: function() {\n                    return mPaused;\n                }\n            },\n\n            /** Boolean - is currently loading? */\n            loading: {\n                get: function() {\n                    return mCurrentlyLoading && !mPaused && !mComplete;\n                }\n            },\n\n            loadStarted: {\n                get: function() {\n                    return mCurrentlyLoading || mTotalSize > 0;\n                }\n            },\n\n            /** String - the source URL of this binary loader. */\n            url:  {\n                get: function() {\n                    return mUrl;\n                }\n            }\n        });\n    }\n\n    // Add the EventEmitter functionality\n    utils.extend(PseudoBinaryLoader.prototype, ee.prototype);\n\n    return PseudoBinaryLoader;\n});\n\ndefine(\'engine/mse/formats/imc/ContiguousImcSegmentParser\',[\n    \'utils/loggerFactory\',\n    \'mseUtils\',\n    \'engine/mse/formats/imc/SimpleImcSegmentParser\',\n    \'engine/mse/utils/PseudoBinaryLoader\'\n],\n\n/**\n* This module defines a class (ContiguousImcSegmentParser) with concrete implementation of ISegmentParser interface.\n* It extends the SimpleImcSegmentParser, to allow parsing and appending of IMC files on environments where direct appending is not available.\n*/\nfunction(loggerFactory, utils, SimpleImcSegmentParser, PseudoBinaryLoader) {\n    \'use strict\';\n\n    var log = loggerFactory.createLogger(\'ContiguousImcSegmentParser\');\n\n    function ContiguousImcSegmentParser(iSettings, iSegmentParserFactory) {\n        SimpleImcSegmentParser.call(this, iSettings);\n\n        // Retain base implementations so they can be extended\n        this.baseDispose = SimpleImcSegmentParser.prototype.dispose;\n\n        // Declare new memebers\n        this.mSegmentParsers = null;\n        this.mSettings = iSettings;\n        this.mSegmentParserFactory = iSegmentParserFactory;\n        this.mPrevChunk = null;\n    }\n\n    // Extend the base class SimpleImcSegmentParser\n    ContiguousImcSegmentParser.prototype = Object.create(SimpleImcSegmentParser.prototype);\n\n    ContiguousImcSegmentParser.prototype.createPseudoBinaryLoader = function(channelMeta) {\n        if (!channelMeta.byteLength) {\n            log.error(\'ContiguousImcSegmentParser - IMC is missing the IChannelByteLength member! Cannot create SegmentParser...\');\n            return null;\n        }\n\n        var retVal = new PseudoBinaryLoader(this.mBinaryLoader.url + \'#\' + channelMeta.id, channelMeta.byteLength);\n        retVal.append(channelMeta.initSegment);\n        return retVal;\n    };\n\n    ContiguousImcSegmentParser.prototype.initSegmentParsers = function() {\n        this.mSegmentParsers = {};\n\n        for (var i = 0; i < this.mChannelsMetadata.channels.length; i++) {\n            var binaryLoader = this.createPseudoBinaryLoader(this.mChannelsMetadata.channels[i]);\n            var segmentParser = this.mSegmentParserFactory.makeSegmentParser(utils.extend({}, this.mSettings, {\n                binaryLoader: binaryLoader,\n                segment: binaryLoader.url\n            }));\n\n            // Begin parsing\n            segmentParser.parse();\n\n            this.mSegmentParsers[this.mChannelsMetadata.channels[i].id] = {\n                binaryLoader: binaryLoader,\n                segmentParser: segmentParser\n            };\n        }\n    };\n\n    // Override base implementation\n    ContiguousImcSegmentParser.prototype.doAppendInitSegment = function(appendingContext, buffer) {\n        if (!appendingContext.streamStarted) {\n            appendingContext.streamStarted = true;\n            this.appendArrayBuffer(buffer);\n        }\n    };\n\n    // Override base implementation\n    ContiguousImcSegmentParser.prototype.appendChunk = function(chunk, timestampOffsetMs) {\n        if (!this.mSegmentParsers) {\n            this.initSegmentParsers();\n        }\n\n        var nextSegmentTs;\n        var buffer;\n\n        this.mSegmentParsers[chunk.id].binaryLoader.append(chunk.data);\n        // Use the previous chunk\'s timestamp (+1 ms) as the timestamp for next media segment (implementor will return first media segment with GTE timestamp).\n        nextSegmentTs = this.mPrevChunk ? this.mPrevChunk.timestamp + 1 : 0;\n        buffer = this.mSegmentParsers[chunk.id].segmentParser.getMediaSegment(null, timestampOffsetMs / 1000.0, nextSegmentTs / 1000.0);\n        this.appendArrayBuffer(buffer, chunk, timestampOffsetMs);\n\n        // Update the PrevChunk member\n        this.mPrevChunk = chunk;\n\n        return chunk.timestamp + chunk.duration;\n    };\n\n    // Override base implementation\n    ContiguousImcSegmentParser.prototype.dispose = function() {\n        this.baseDispose();\n        this.mSegmentParsers = null;\n        this.mSettings = null;\n        this.mSegmentParserFactory = null;\n        this.mPrevChunk = null;\n    };\n\n    Object.defineProperties(ContiguousImcSegmentParser.prototype, {\n        shouldSetTimestampOffset: {\n            get: function() {\n                return false;\n            }\n        }\n    });\n\n    return ContiguousImcSegmentParser;\n});\n\ndefine(\'engine/mse/formats/ts/SimpleTsSegmentParser\',[\n    \'utils/loggerFactory\',\n    \'engine/mse/formats/unknown/SimpleSegmentParser\'\n],\n\n/**\n* This module defines a class (SimpleTsSegmentParser) with concrete implementation of ISegmentParser interface.\n* This should be used in cases where setting MSE\'s SourceBuffer\'s timestampOffset property is supported (i.e. Chrome).\n* Basically, this concrete implementation of ISegmentParser will append input file as-is.\n*/\nfunction(loggerFactory, SimpleSegmentParser) {\n    \'use strict\';\n\n    var log = loggerFactory.createLogger(\'SimpleTsSegmentParser\');\n    log.disable();\n\n    function SimpleTsSegmentParser(iSettings) {\n        SimpleSegmentParser.call(this, iSettings);\n    }\n\n    // Extend the base class SimpleSegmentParser\n    SimpleTsSegmentParser.prototype = Object.create(SimpleSegmentParser.prototype);\n\n    // Override base implementation\n    SimpleTsSegmentParser.prototype._tryParseMetadata = function() {\n        //log.warn(\'Getting metadata from TS is not yet supported.\');\n        return null;\n    };\n\n    Object.defineProperties(SimpleTsSegmentParser.prototype, {\n        /** Has parsing completed? */\n        parsingCompleted: {\n            get: function() {\n                return true;\n            }\n        }\n    });\n\n    return SimpleTsSegmentParser;\n});\n\ndefine(\'engine/mse/factories/segmentParserFactory\',[\n    \'utils/loggerFactory\',\n    \'mseUtils\',\n    \'engine/mse/formats/webm/SimpleWebmSegmentParser\',\n    \'engine/mse/formats/webm/ContiguousWebmSegmentParser\',\n    \'engine/mse/formats/mp4/SimpleMp4SegmentParser\',\n    \'engine/mse/formats/imc/SimpleImcSegmentParser\',\n    \'engine/mse/formats/imc/ContiguousImcSegmentParser\',\n    \'engine/mse/formats/ts/SimpleTsSegmentParser\'\n],\n\n/**\n* Factory methods for concrete ISegmentParser implementations.\n*/\nfunction(loggerFactory, utils, SimpleWebmSegmentParser, ContiguousWebmSegmentParser, SimpleMp4SegmentParser, SimpleImcSegmentParser, ContiguousImcSegmentParser, SimpleTsSegmentParser) {\n    \'use strict\';\n\n    var log = loggerFactory.createLogger(\'segmentParserFactory\');\n    var _directAppendSupported = null;\n\n    var isIMC = function(data) {\n        return (data.segment instanceof Object && data.segment && data.segment.type.startsWith(\'imc\')) ||\n                typeof data.segment === \'string\' && data.segment.substr(0, data.segment.indexOf(\'#\') === -1 ? data.segment.length : data.segment.indexOf(\'#\')).endsWith(\'.imc\');\n    };\n\n    var isDirectAppendSupported = function(env) {\n        if (_directAppendSupported === null) {\n            _directAppendSupported = env ?\n                                            env.browser.name !== \'Firefox\' :\n                                            false;\n        }\n\n        return _directAppendSupported;\n    };\n\n    return {\n        makeSegmentParser: function makeSegmentParser(data) {\n            var retVal = null;\n\n            // Handle special case, where file is IMC\n            if (isIMC(data)) {\n                return isDirectAppendSupported(data.environment) ?\n                                        new SimpleImcSegmentParser(data) :\n                                        new ContiguousImcSegmentParser(data, this);\n            }\n\n            switch (data.format.source) {\n                case \'ts\':\n                    retVal = new SimpleTsSegmentParser(data);\n                    break;\n                case \'webm\':\n                    retVal = isDirectAppendSupported(data.environment) ?\n                                        new SimpleWebmSegmentParser(data) :\n                                        new ContiguousWebmSegmentParser(data);\n                    break;\n                case \'mp4\':\n                    retVal = isDirectAppendSupported(data.environment) ?\n                                        new SimpleMp4SegmentParser(data) :\n                                        null;\n                    break;\n                default:\n                    log.error(\'No compatible ISegmentParser implementation for source format "\' + data.format.source + \'" was found.\');\n                    break;\n            }\n\n            return retVal;\n        }\n    };\n});\n\ndefine(\'engine/mse/interfaces/ISegmentParser\',[\n    \'mseUtils\',\n    \'engine/mse/interfaces/IDisposable\',\n    \'utils/EventEmitter\'\n],\n\n/**\n* ISegmentParser interface.\n*/\nfunction(utils, IDisposable, IEventEmitter) {\n    \'use strict\';\n\n    var ISegmentParser = {\n        ///////////////////////////////////////////////////////////////////\n        // Properties/Getters\n        ///////////////////////////////////////////////////////////////////\n\n        /** Returns the metadata object (or null if not yet parsed). */\n        metadata: {},\n\n        /** Does this implementation require incrementing MSE SourceBuffer\'s timestampOffset property? */\n        shouldSetTimestampOffset: false,\n\n        /** Is parsing complete? */\n        parsingCompleted: false,\n\n        /** How many seconds were parsed out of video segment? */\n        timeParsed: 0.0,\n\n        ///////////////////////////////////////////////////////////////////\n        // Methods\n        ///////////////////////////////////////////////////////////////////\n\n        /** Begin/resume parsing the segment. Will trigger \'parse_progress\' and \'parse_complete\' events. */\n        parse: utils.createMethod(\'parse\'),\n\n        /** Pause any further parsing until next call to parse(). */\n        pauseParsing: utils.createMethod(\'pauseParsing\'),\n\n        /**\n        * Get the init segment\'s buffer. This function allows an external controller to manage the appending process.\n        *\n        * @param {Object} appendingContext - A global appending context object to be read and mutated by this method.\n        * @param {Number} timestampOffset - The timestamp offset (in seconds) since beginning of stream.\n        */\n        getInitSegment: utils.createMethod(\'getInitSegment\'),\n\n        /**\n        * Get a single specific media segment\'s buffer. This function allows an external controller to manage the appending process\n        * (as opposed to append() which is handled for entire file internally).\n        * All "contiguous" parsers should implement this method.\n        *\n        * @param {Object} appendingContext - A global appending context object to be read and mutated by this method.\n        * @param {Number} timestampOffset - The timestamp offset (in seconds) since beginning of stream.\n        * @param {Number} mediaSegmentTimestamp - The timestamp of chunk to be appended (in seconds from start of segment). This is the unique identifier for the chunk.\n        */\n        getMediaSegment: utils.createMethod(\'getMediaSegment\'),\n\n        /**\n        * Begin appending the segment to the playback buffer (cannot be canceled/paused).\n        *\n        * @param {Object} appendingContext - A global appending context object to be read and mutated by this method.\n        * @param {Number} timestampOffset - The timestamp offset (in seconds) since beginning of stream.\n        */\n        append: utils.createMethod(\'append\')\n\n        ///////////////////////////////////////////////////////////////////\n        // Events\n        ///////////////////////////////////////////////////////////////////\n\n        // While these are not enforced by code, all ISegmentParser implementors are expected to trigger the following:\n        // - \'parse_progress\' (param is timeParsed)\n        // - \'parse_complete\' (no params)\n        // - \'append_progress\' (param is arrayBuffer of chunk)\n        // - \'append_complete\' (no params)\n        // - \'metadata\' (param is SegmentMetadata object)\n    };\n\n    return utils.extend(ISegmentParser, IDisposable, IEventEmitter.prototype);\n});\n\ndefine(\'engine/mse/interfaces/IParallelsSegmentParser\',[\n    \'mseUtils\',\n    \'engine/mse/interfaces/ISegmentParser\'\n],\n\n/**\n* IParallelsSegmentParser interface.\n* Extends the ISegmentParser interface, adds \'Parallels\' specific methods.\n*/\nfunction(utils, ISegmentParser) {\n    \'use strict\';\n\n    var IParallelsSegmentParser = {\n\n        /**\n        * Update video playback time (in seconds within node).\n        *\n        * @param {Object} data - A simple object containing the following fields:\n        *                            - {String} currentNodeId\n        *                            - {Number} time\n        *                            - {Boolean} paused\n        */\n        timeupdate: utils.createMethod(\'timeupdate\'),\n\n        /**\n        * Switch to another channel.\n        *\n        * @param {Number} channelId - The channel to switch to.\n        */\n        switchChannel: utils.createMethod(\'switchChannel\')\n\n        /**\n         * While it is not enforced in interface, an implementation of IParallelsSegmentParser should also trigger the following events:\n         *\n         *  - request_timeupdate - Simple event, requesting a tick timeupdate to be called.\n         *  - channel_switched - Event that confirms a channel has been switched, along with the timestamp of the switch (in seconds, relative to node start)\n         */\n    };\n\n    return utils.extend(IParallelsSegmentParser, ISegmentParser);\n});\n\ndefine(\'engine/mse/controllers/ParsingManager\',[\n    \'utils/loggerFactory\',\n    \'engine/mse/controllers/BaseManager\',\n    \'mseUtils\',\n    \'engine/mse/factories/segmentParserFactory\',\n    \'engine/mse/interfaces/ISegmentParser\',\n    \'engine/mse/interfaces/IDisposable\',\n    \'engine/mse/interfaces/IParallelsSegmentParser\'\n],\n\n/**\n* This module handles the video parsing logic of a project.\n*/\nfunction(loggerFactory, BaseManager, utils, segmentParserFactory, ISegmentParser, IDisposable, IParallelsSegmentParser) {\n    \'use strict\';\n\n    var log = loggerFactory.createLogger(\'ParsingManager\');\n    log.setLevel(\'warn\');\n\n    function ParsingManager(iSettings) {\n        BaseManager.apply(this, arguments);\n\n        this.mSegmentParserMap = {};\n        this.mCurrentTimestampOffset = null;\n        this.mAppendingContext = {streamStarted: false};\n        this.mSettings = iSettings;\n\n        // Add listeners\n        this.workerState.on(\'timeupdate\', this.timeupdate);\n        this.workerState.on(\'switchChannel\', this.switchChannel);\n    }\n\n    // Extend the base class SimpleImcSegmentParser\n    ParsingManager.prototype = Object.create(BaseManager.prototype);\n    ParsingManager.prototype.constructor = ParsingManager;\n\n    ///////////////////////////////////////////////////////////////////////////////////\n    // BaseManager Overrides\n    ///////////////////////////////////////////////////////////////////////////////////\n\n    ParsingManager.prototype._pauseNodeHandling = function(nodeId) {\n        this.nodeSegmentParser(nodeId).pauseParsing();\n    };\n\n    ParsingManager.prototype._isNodeHandled = function(nodeId) {\n        return this.nodeSegmentParser(nodeId).parsingCompleted;\n    };\n\n    ParsingManager.prototype._doHandleNode = function(nodeId) {\n        if (this._isNodeHandled(nodeId)) {\n            return;\n        }\n\n        log.info(\'[\' + nodeId + \'] START PARSING\');\n        var parser = this.nodeSegmentParser(nodeId);\n\n        var onProgress = function(timeParsed) {\n            log.info(\'[\' + nodeId + \'] PARSING PROGRESS: \' + timeParsed);\n        }.bind(this);\n\n        var onMetadata = function(segMetadata) {\n            log.info(\'[\' + nodeId + \'] METADATA\');\n            this.segmentMetadata(this.workerState.repo.nodes[nodeId].segment, segMetadata);\n        }.bind(this);\n\n        var onComplete = function() {\n            log.info(\'[\' + nodeId + \'] PARSING COMPLETE: \' + parser.timeParsed);\n\n            // Remove Listeners\n            parser.off(\'parse_progress\', onProgress);\n            parser.off(\'parse_complete\', onComplete);\n            parser.off(\'metadata\', onMetadata);\n            parser.addedParseListeners = false;\n\n            this.trigger(\'handler.complete\', this.onHandlerComplete);\n        }.bind(this);\n\n        // Add Listeners\n        if (!parser.addedParseListeners) {\n            parser.addedParseListeners = true;\n            parser.on(\'parse_progress\', onProgress);\n            parser.on(\'parse_complete\', onComplete);\n            parser.on(\'metadata\', onMetadata);\n        }\n\n        // Begin/resume parsing\n        parser.parse();\n    };\n\n    ParsingManager.prototype._dispose = function() {\n        this.removeCurrentAppendingParserListeners();\n        this.mCurrentTimestampOffset = null;\n        this.mAppendingContext = {streamStarted: false};\n        this.disposeOfAllSegmentParsers();\n\n        // Remove listeners\n        if (this.workerState) {\n            this.workerState.off(\'timeupdate\', this.timeupdate);\n            this.workerState.off(\'switchChannel\', this.switchChannel);\n        }\n    };\n\n    ///////////////////////////////////////////////////////////////////////////////////\n    // Misc Methods\n    ///////////////////////////////////////////////////////////////////////////////////\n\n    ParsingManager.prototype.dataAvailable = function(arrayBuffer, data) {\n        log.debug(\'-> dataAvailable()\');\n        this.trigger(\'dataAvailable\', arrayBuffer, data);\n    };\n\n    ParsingManager.prototype.setTimestampOffset = function(newTimestampOffset) {\n        this.trigger(\'setTimestampOffset\', newTimestampOffset);\n    };\n\n    ParsingManager.prototype.segmentMetadata = function(id, metadata) {\n        this.trigger(\'segmentMetadata\', id, metadata);\n    };\n\n    ParsingManager.prototype.requestTimeUpdate = function() {\n        this.trigger(\'requestTimeUpdate\');\n    };\n\n    ParsingManager.prototype.channelSwitched = function(timestampOffset) {\n        this.trigger(\'channelSwitched\', timestampOffset);\n    };\n\n    ParsingManager.prototype.nodeSegmentParser = function(nodeId) {\n        var binLoader = this.binaryStore.getByNodeId(nodeId);\n\n        if (!binLoader) {\n            throw new Error(\'ParsingManager could not obtain BinaryLoader instance for node: \' + nodeId);\n        }\n\n        if (!this.mSegmentParserMap.hasOwnProperty(binLoader.url)) {\n            var segmentParser = segmentParserFactory.makeSegmentParser(utils.extend({\n                binaryLoader: binLoader,\n                segment: binLoader.url\n            }, this.mSettings));\n            if (!segmentParser.conforms(ISegmentParser)) {\n                throw new Error(\'SegmentParser does not conform to ISegmentParser interface!\');\n            }\n            binLoader.on(\'dispose\', this.onBinaryLoaderDispose);\n\n            this.mSegmentParserMap[binLoader.url] = segmentParser;\n        }\n\n        return this.mSegmentParserMap[binLoader.url];\n    };\n\n    ParsingManager.prototype.segmentParserAtIndex = function(index) {\n        if (index < 0 || index >= this.workerState.playlist.length) {\n            return null;\n        }\n        return this.nodeSegmentParser(this.workerState.playlist[index]);\n    };\n\n    ParsingManager.prototype.currentAppendingSegmentParser = function() {\n        if (this.workerState.appendingIndex >= 0 && this.workerState.appendingIndex < this.workerState.playlist.length) {\n            return this.segmentParserAtIndex(this.workerState.appendingIndex);\n        }\n\n        return null;\n    };\n\n    ParsingManager.prototype.nodeDuration = function(nodeId) {\n        var segParser = this.nodeSegmentParser(nodeId);\n        return (segParser && segParser.metadata) ?\n                            segParser.metadata.duration :\n                            this.workerState.repo.segments[this.workerState.repo.nodes[nodeId].segment].source[this.workerState.format.source].duration;\n    };\n\n    ParsingManager.prototype.currentTimestampOffset = function() {\n        if (this.mCurrentTimestampOffset !== null) {\n            return this.mCurrentTimestampOffset;\n        }\n\n        var retVal = 0;\n\n        for (var i = 0; i < this.workerState.appendingIndex; i++) {\n            retVal += this.nodeDuration(this.workerState.playlist[i]);\n        }\n\n        this.mCurrentTimestampOffset = retVal;\n\n        return retVal;\n    };\n\n    ParsingManager.prototype.onAppendProgress = function(arrayBuffer, data) {\n        data = data || {};\n        data = utils.extend({\n            nodeId: this.workerState.playlist[this.workerState.appendingIndex],\n            nodeTimestampOffset: this.currentTimestampOffset(),\n            nodeDuration: this.nodeDuration(this.workerState.playlist[this.workerState.appendingIndex])\n        }, data);\n        this.dataAvailable(arrayBuffer, data);\n    };\n\n    ParsingManager.prototype.onAppendComplete = function() {\n        var parser = this.currentAppendingSegmentParser();\n\n        // Remove listeners\n        parser.off(\'request_timeupdate\', this.requestTimeUpdate);\n        parser.off(\'append_progress\', this.onAppendProgress);\n        parser.off(\'append_complete\', this.onAppendComplete);\n        parser.off(\'channel_switched\', this.channelSwitched);\n        parser.addedAppendListeners = false;\n\n        // Increment appending index and reset the timestamp offset\n        this.workerState.incrementAppendingIndex();\n        this.mCurrentTimestampOffset = null;\n\n        // Set the MediaSource timestamp offset in preparation for next video segment (if necessary)\n        if (parser.shouldSetTimestampOffset) {\n            this.setTimestampOffset(this.currentTimestampOffset());\n        }\n\n        // Try append next segment\n        this.appendNext();\n    };\n\n    ParsingManager.prototype.appendNext = function() {\n        if (this.workerState.appendingIndex >= this.workerState.playlist.length) {\n            return;\n        }\n\n        log.debug(\'-> appendNext()\');\n\n        var parser = this.currentAppendingSegmentParser();\n\n        // Add listeners\n        if (!parser.addedAppendListeners) {\n            parser.addedAppendListeners = true;\n            parser.on(\'request_timeupdate\', this.requestTimeUpdate);\n            parser.on(\'append_progress\', this.onAppendProgress);\n            parser.on(\'append_complete\', this.onAppendComplete);\n            parser.on(\'channel_switched\', this.channelSwitched);\n        }\n\n        // Start append\n        parser.append(\n            this.mAppendingContext,\n            this.currentTimestampOffset()\n        );\n    };\n\n    ParsingManager.prototype.onPlaylistAppend = function() {\n        log.debug(\'-> onPlaylistAppend()\');\n        BaseManager.prototype.onPlaylistAppend.apply(this, arguments);\n        if ((this.workerState.playlist.length - 1) === this.workerState.appendingIndex) {\n            log.debug(\'-> onPlaylistAppend() -> calling appendNext()\');\n            this.appendNext();\n        }\n    };\n\n    ParsingManager.prototype.timeupdate = function(data) {\n        if (this.workerState.playlist[this.workerState.appendingIndex]) {\n            var parser = this.currentAppendingSegmentParser();\n            if (parser.conforms(IParallelsSegmentParser)) {\n                parser.timeupdate(data);\n            }\n        }\n    };\n\n    ParsingManager.prototype.switchChannel = function(data) {\n        if (this.workerState.playlist[this.workerState.appendingIndex]) {\n            var parser = this.currentAppendingSegmentParser();\n            if (parser.conforms(IParallelsSegmentParser)) {\n                parser.switchChannel(data.channelId);\n            }\n        }\n    };\n\n    ParsingManager.prototype.disposeOfSegmentParser = function(url) {\n        if (this.mSegmentParserMap[url]) {\n            if (this.mSegmentParserMap[url].conforms(IDisposable)) {\n                this.mSegmentParserMap[url].dispose();\n            }\n\n            this.mSegmentParserMap[url].removeAllListeners();\n            delete this.mSegmentParserMap[url];\n        }\n    };\n\n    ParsingManager.prototype.disposeOfAllSegmentParsers = function() {\n        for (var url in this.mSegmentParserMap) {\n            this.disposeOfSegmentParser(url);\n        }\n\n        this.mSegmentParserMap = {};\n    };\n\n    ParsingManager.prototype.removeCurrentAppendingParserListeners = function() {\n        // Remove listeners\n        var parser = this.currentAppendingSegmentParser();\n        if (parser) {\n            parser.off(\'request_timeupdate\', this.requestTimeUpdate);\n            parser.off(\'append_progress\', this.onAppendProgress);\n            parser.off(\'append_complete\', this.onAppendComplete);\n            parser.off(\'channel_switched\', this.channelSwitched);\n            parser.addedAppendListeners = false;\n        }\n    };\n\n    ParsingManager.prototype.onPlaylistReset = function() {\n        log.debug(\'-> onPlaylistReset()\');\n        this.removeCurrentAppendingParserListeners();\n        BaseManager.prototype.onPlaylistReset.apply(this, arguments);\n        this.mCurrentTimestampOffset = null;\n        this.disposeOfAllSegmentParsers();\n    };\n\n    ParsingManager.prototype.onBinaryLoaderDispose = function(url) {\n        this.disposeOfSegmentParser(url);\n    };\n\n    return ParsingManager;\n});\n\ndefine(\'engine/mse/memory/predicates/And\',[\n],\n\nfunction() {\n    \'use strict\';\n\n    var And = function(config, factory) {\n        var predicates = [];\n\n        for (var key in config) {\n            predicates.push(factory(key, config[key]));\n        }\n\n        return function(ctx, nodeId) {\n            var retVal = true;\n\n            for (var i = 0; i < predicates.length; i++) {\n                if (!predicates[i](ctx, nodeId)) {\n                    retVal = false;\n                    break;\n                }\n            }\n\n            return retVal;\n        };\n    };\n\n    return And;\n});\n\ndefine(\'engine/mse/memory/predicates/Or\',[\n],\n\nfunction() {\n    \'use strict\';\n\n    var Or = function(config, factory) {\n        var predicates = [];\n\n        for (var key in config) {\n            predicates.push(factory(key, config[key]));\n        }\n\n        return function(ctx, nodeId) {\n            var retVal = false;\n\n            for (var i = 0; i < predicates.length; i++) {\n                if (predicates[i](ctx, nodeId)) {\n                    retVal = true;\n                    break;\n                }\n            }\n\n            return retVal;\n        };\n    };\n\n    return Or;\n});\n\ndefine(\'engine/mse/memory/predicates/InRepo\',[\n],\n\nfunction() {\n    \'use strict\';\n\n    var InRepo = function(config) {\n        var value = !!config;\n\n        return function(ctx, nodeId) {\n            var retVal = !!ctx.repo.nodes[nodeId];\n\n            if (!value) {\n                retVal = !retVal;\n            }\n\n            return retVal;\n        };\n    };\n\n    return InRepo;\n});\n\ndefine(\'engine/mse/memory/predicates/InFuturePlaylist\',[\n],\n\nfunction() {\n    \'use strict\';\n\n    var InFuturePlaylist = function(config) {\n        var value = !!config;\n\n        return function(ctx, nodeId) {\n            if (!ctx.futurePlaylist) {\n                ctx.futurePlaylist = ctx.playlist.slice(ctx.appendingIndex);\n            }\n\n            var retVal = ctx.futurePlaylist.indexOf(nodeId) >= 0;\n\n            if (!value) {\n                retVal = !retVal;\n            }\n\n            return retVal;\n        };\n    };\n\n    return InFuturePlaylist;\n});\n\ndefine(\'engine/mse/memory/predicates/InNextLevels\',[\n],\n\nfunction() {\n    \'use strict\';\n\n    var DEFAULT_DEPTH = 1;\n\n    var calcNodesUpToDepth = function(resultObject, nodes, nodeId, depth) {\n        if (!depth) {\n            return;\n        }\n\n        for (var i = 0; i < nodes[nodeId].prefetch.length; i++) {\n            if (!resultObject[nodes[nodeId].prefetch[i]]) {\n                resultObject[nodes[nodeId].prefetch[i]] = true;\n                calcNodesUpToDepth(resultObject, nodes, nodes[nodeId].prefetch[i], depth - 1);\n            }\n        }\n    };\n\n    var InFuturePlaylist = function(config) {\n        var depth = config && config.depth;\n        var value = !!(config && config.value);\n\n        if (!depth || depth <= 0) {\n            depth = DEFAULT_DEPTH;\n        }\n\n        return function(ctx, nodeId) {\n            if (!ctx.nodesUpToDepth) {\n                ctx.nodesUpToDepth = {};\n                if (ctx.playlist.length) {\n                    calcNodesUpToDepth(ctx.nodesUpToDepth, ctx.repo.nodes, ctx.playlist[ctx.playlist.length - 1], depth);\n                }\n            }\n\n            var retVal = !!ctx.nodesUpToDepth[nodeId];\n\n            if (!value) {\n                retVal = !retVal;\n            }\n\n            return retVal;\n        };\n    };\n\n    return InFuturePlaylist;\n});\n\ndefine(\'engine/mse/memory/memoryPredicateFactory\',[\n    \'engine/mse/memory/predicates/And\',\n    \'engine/mse/memory/predicates/Or\',\n    \'engine/mse/memory/predicates/InRepo\',\n    \'engine/mse/memory/predicates/InFuturePlaylist\',\n    \'engine/mse/memory/predicates/InNextLevels\',\n    \'utils/loggerFactory\'\n],\n\n/*\n    Example config:\n    ==============\n\n    {\n        or: {\n            inrepo: false,\n            and: {\n                infutureplaylist: false,\n                innextlevels: {\n                    depth: 3,\n                    value: false\n                }\n            }\n        }\n    }\n*/\n\n/**\n * A factory for memory predicate functions.\n * Each of the predicate functions receive a ctx and a nodeId and returns a boolean - whether or not we can dispose of node.\n * The factory message receives and object and creates the predicates accordingly.\n */\nfunction(And, Or, InRepo, InFuturePlaylist, InNextLevels, loggerFactory) {\n    \'use strict\';\n\n    var DEFAULT_PREDICATE = function() { return false; };\n    var log = loggerFactory.createLogger(\'memoryPredicateFactory\');\n    log.setLevel(\'warn\');\n    log.enable();\n\n    var createPredicate = function(type, config) {\n        var retVal;\n\n        if (type && typeof type === \'string\') {\n            type = type.toLowerCase();\n\n            switch (type) {\n                case \'inrepo\':\n                    retVal = new InRepo(config);\n                    break;\n                case \'infutureplaylist\':\n                    retVal = new InFuturePlaylist(config);\n                    break;\n                case \'innextlevels\':\n                    retVal = new InNextLevels(config);\n                    break;\n                case \'and\':\n                    retVal = new And(config, createPredicate);\n                    break;\n                case \'or\':\n                    retVal = new Or(config, createPredicate);\n                    break;\n                default:\n                    log.warn(\'Unknown predicate type \\\'\' + type + \'\\\', using default predicate.\');\n                    retVal = DEFAULT_PREDICATE;\n                    break;\n            }\n        } else if (type && typeof type === \'object\' && Object.keys(type).length === 1) {\n            var key = Object.keys(type)[0];\n            retVal = createPredicate(key, type[key]);\n        }\n\n        if (!retVal) {\n            log.warn(\'Using default predicate - possible bad configuration.\');\n            retVal = DEFAULT_PREDICATE;\n        }\n\n        return retVal;\n    };\n\n    return createPredicate;\n});\n\ndefine(\'engine/mse/controllers/MemoryManager\',[\n    \'utils/loggerFactory\',\n    \'engine/mse/memory/memoryPredicateFactory\'\n],\n\n/**\n * This module is a concrete implementation of the IMemoryManager interface.\n * When video buffers memory reaches a certain size, the algorithm will kick in and start disposing of some buffers.\n */\nfunction(loggerFactory, memoryPredicateFactory) {\n    \'use strict\';\n\n    var DEFAULT_MAX_MEMORY = 100000000; // 100MB\n    var log = loggerFactory.createLogger(\'MemoryManager\');\n    loggerFactory.enableAll();\n    log.setLevel(\'warn\');\n    log.enable();\n\n    function MemoryManager(iSettings) {\n\n        /////////////////////////////////////////////////////////////////////////////////////////////////\n        // Private Members\n        /////////////////////////////////////////////////////////////////////////////////////////////////\n\n        var mBinaryStore = iSettings.binaryStore;\n        var mWorkerState = iSettings.workerState;\n        var mMaxMemory = iSettings.memory && iSettings.memory.maxmemory ? iSettings.memory.maxmemory : DEFAULT_MAX_MEMORY;\n        var mPredicate = memoryPredicateFactory(iSettings.memory && iSettings.memory.logic);\n\n        /////////////////////////////////////////////////////////////////////////////////////////////////\n        // Private Methods\n        /////////////////////////////////////////////////////////////////////////////////////////////////\n\n        var getCtx = function() {\n            return {\n                playlist: mWorkerState.playlist,\n                repo: mWorkerState.repo,\n                appendingIndex: mWorkerState.appendingIndex\n            };\n        };\n\n        var getNodesInMemory = function() {\n            var retVal = {};\n\n            for (var nodeId in mWorkerState.repo.nodes) {\n                var url = mWorkerState.nodeIdToUrl(nodeId);\n                if (mBinaryStore.binaryMap[url] && mBinaryStore.binaryMap[url].bytesTotal) {\n                    retVal[nodeId] = true;\n                }\n            }\n\n            return retVal;\n        };\n\n        var sweep = function() {\n            log.debug(\'-> sweep()\');\n\n            var ctx = getCtx();\n            var nodesInMemory = getNodesInMemory();\n            var toSweep = {};\n            var nodesToSweep = {}; // Approximation, for debugging purposes only!!!\n            var notToSweep = {};\n\n            for (var nodeId in nodesInMemory) {\n                var url = mWorkerState.nodeIdToUrl(nodeId);\n                if (mPredicate(ctx, nodeId) && !notToSweep[url]) {\n                    toSweep[url] = true;\n                    nodesToSweep[nodeId] = true;\n                } else {\n                    notToSweep[url] = true;\n                    if (toSweep[url]) {\n                        delete toSweep[url];\n                    }\n                }\n            }\n\n            toSweep = Object.keys(toSweep);\n\n            if (toSweep.length) {\n                log.info(\'Disposing of: [\' + Object.keys(nodesToSweep).join(\', \') + \']\');\n                mBinaryStore.destroy.apply(mBinaryStore, toSweep);\n                log.info(\'Post sweep memory: \' + mBinaryStore.bytesTotal);\n            }\n        };\n\n        /////////////////////////////////////////////////////////////////////////////////////////////////\n        // Public Methods\n        /////////////////////////////////////////////////////////////////////////////////////////////////\n\n        this.dispose = function() {\n        };\n\n        /////////////////////////////////////////////////////////////////////////////////////////////////\n        // Initialization\n        /////////////////////////////////////////////////////////////////////////////////////////////////\n\n        mBinaryStore.on(\'memory.increased\', function(totalMemory) {\n            log.info(\'memory.increased: \' + totalMemory);\n            if (mMaxMemory && mMaxMemory > 0 && totalMemory >= mMaxMemory) {\n                sweep();\n            }\n        });\n    }\n\n    return MemoryManager;\n});\n\ndefine(\'engine/mse/controllers/MemoryManagerNoop\',[\n],\n\n/**\n* This module is a concrete no-op implementation of the IMemoryManager interface.\n*/\nfunction() {\n    \'use strict\';\n\n    function MemoryManagerNoop() {\n\n        /////////////////////////////////////////////////////////////////////////////////////////////////\n        // Public Methods\n        /////////////////////////////////////////////////////////////////////////////////////////////////\n\n        this.appendNode = function(nodeId) { // jshint ignore:line\n        };\n\n        this.updateRepo = function(repo) { // jshint ignore:line\n        };\n\n        this.reset = function() {\n        };\n\n        this.dispose = function() {\n        };\n    }\n\n    return MemoryManagerNoop;\n});\n\ndefine(\'engine/mse/factories/controllerFactory\',[\n    \'utils/loggerFactory\',\n    \'mseUtils\',\n    \'engine/mse/controllers/LoadingManager\',\n    \'engine/mse/controllers/ParsingManager\',\n    \'engine/mse/controllers/MemoryManager\',\n    \'engine/mse/controllers/MemoryManagerNoop\'\n],\n\n/**\n* Factory methods for concrete LoadingManager/ParsingManager implementations.\n*/\nfunction(loggerFactory, utils, LoadingManager, ParsingManager, MemoryManager, MemoryManagerNoop) {\n    \'use strict\';\n\n    var log = loggerFactory.createLogger(\'mseControllerFactory\');\n    log.disable();\n\n    return {\n        makeLoadingManager: function(settings) {\n            return new LoadingManager(settings);\n        },\n\n        makeParsingManager: function(settings) {\n            return new ParsingManager(settings);\n        },\n\n        makeMemoryManager: function(settings) {\n            var type = settings && settings.memory && settings.memory.type;\n            type = (typeof type === \'string\') ?\n                            type.toLowerCase() :\n                            \'noop\';\n\n            var retVal;\n\n            switch (type) {\n                case \'constrained\':\n                    retVal = new MemoryManager(settings);\n                    break;\n                default:\n                    retVal = new MemoryManagerNoop();\n                    break;\n            }\n\n            return retVal;\n        }\n    };\n});\n\ndefine(\'engine/mse/workers/WorkerState\',[\n    \'utils/loggerFactory\',\n    \'mseUtils\',\n    \'utils/EventEmitter\'\n],\n\nfunction(loggerFactory, utils, ee) {\n    \'use strict\';\n\n    var log = loggerFactory.createLogger(\'WorkerState\');\n    log.setLevel(\'warn\');\n\n    ////////////////////////////////////////////////////////////////////////\n    // CONSTRUCTOR + MEMBERS\n    ////////////////////////////////////////////////////////////////////////\n\n    var WorkerState = function(data) {\n        log.debug(\'-> [constructor]\');\n\n        this.repo = data.repo;\n        this.playlist = data.playlist ? data.playlist : [];\n        this.format = data.format;\n        this.environment = data.environment;\n        this.nodeIdToUrlMap = {};\n        this.nodeIdToChildrenMap = {};\n        this.nodeIdToChunkSizeMap = {};\n        this.appendingIndex = 0;\n        this.currentTime = 0;\n        this.config = data;\n\n        // Autobinding\n        for (var key in this) {\n            if (typeof this[key] === \'function\') {\n                this[key] = this[key].bind(this);\n            }\n        }\n    };\n\n    ////////////////////////////////////////////////////////////////////////\n    // PUBLIC METHODS\n    ////////////////////////////////////////////////////////////////////////\n\n    // REPO HELPERS\n    ////////////////////\n\n    WorkerState.prototype.nodeIdToSegmentId = function(nodeId) {\n        if (!nodeId || !this.repo || !this.repo.nodes || !this.repo.nodes[nodeId]) {\n            log.error(\'No node with id: \' + nodeId);\n            return null;\n        }\n\n        return this.repo.nodes[nodeId].segment;\n    };\n\n    WorkerState.prototype.nodeIdToUrl = function(nodeId) {\n        // Quick access\n        if (!this.nodeIdToUrlMap[nodeId]) {\n            var segId = this.nodeIdToSegmentId(nodeId);\n\n            if (!segId) {\n                log.error(\'No segment for node id: \' + nodeId);\n                return null;\n            }\n\n            this.nodeIdToUrlMap[nodeId] = this.repo.segments[segId].source[this.format.source].url;\n        }\n\n        return this.nodeIdToUrlMap[nodeId];\n    };\n\n    WorkerState.prototype.nodeIdToChildren = function(nodeId) {\n        // Quick access\n        if (!this.nodeIdToChildrenMap[nodeId]) {\n            this.nodeIdToChildrenMap[nodeId] = (this.repo.nodes[nodeId] && this.repo.nodes[nodeId].prefetch) ?\n                                                                                        this.repo.nodes[nodeId].prefetch :\n                                                                                        [];\n        }\n\n        return this.nodeIdToChildrenMap[nodeId];\n    };\n\n    WorkerState.prototype.nodeIdToChunkSize = function(nodeId) {\n        // Quick access\n        if (!this.nodeIdToChunkSizeMap[nodeId]) {\n            var segId = this.nodeIdToSegmentId(nodeId);\n\n            if (!segId) {\n                log.error(\'No segment for node id: \' + nodeId);\n                return null;\n            }\n            this.nodeIdToChunkSizeMap[nodeId] = (this.repo.segments[segId] && this.repo.segments[segId].binaryLoaderChunkSize) ?\n                                                                                        this.repo.segments[segId].binaryLoaderChunkSize :\n                                                                                        null;\n        }\n\n        return this.nodeIdToChunkSizeMap[nodeId];\n    };\n\n    // API\n    ////////////////////\n\n    WorkerState.prototype.appendNode = function(nodeId) {\n        log.debug(\'-> appendNode(\' + nodeId + \')\');\n        this.playlist.push(nodeId);\n        this.trigger(\'workerstate.playlist.append\', nodeId, this.playlist);\n    };\n\n    WorkerState.prototype.updateRepo = function(repo) {\n        log.debug(\'-> updateRepo()\');\n        this.repo = repo;\n        this.nodeIdToUrlMap = {};\n        this.nodeIdToChildrenMap = {};\n        this.nodeIdToChunkSizeMap = {};\n        this.trigger(\'workerstate.repo.updated\');\n    };\n\n    WorkerState.prototype.reset = function() {\n        log.debug(\'-> reset()\');\n        this.playlist = [];\n        this.appendingIndex = 0;\n        this.currentTime = 0;\n        this.trigger(\'workerstate.playlist.reset\', this.playlist);\n    };\n\n    WorkerState.prototype.timeupdate = function(data) {\n        log.debug(\'-> timeupdate()\');\n        this.currentTime = data.time;\n        this.trigger(\'timeupdate\', data);\n    };\n\n    WorkerState.prototype.switchChannel = function(data) {\n        log.debug(\'-> switchChannel()\');\n        this.trigger(\'switchChannel\', data);\n    };\n\n    WorkerState.prototype.incrementAppendingIndex = function() {\n        log.debug(\'-> incrementAppendingIndex()\');\n        this.appendingIndex++;\n        this.trigger(\'workerstate.appendingIndexIncremented\', this.appendingIndex);\n    };\n\n    WorkerState.prototype.dispose = function() {\n        log.debug(\'-> dispose()\');\n        this.trigger(\'workerstate.dispose\');\n    };\n\n    // Add the EventEmitter functionality\n    utils.extend(WorkerState.prototype, ee.prototype);\n\n    return WorkerState;\n});\n\ndefine(\'engine/mse/utils/BinaryLoader\',[\n    \'utils/loggerFactory\',\n    \'mseUtils\',\n    \'utils/EventEmitter\'\n],\n\n/**\n* This module loads a single URL to a binary ArrayBuffer in a pseudo-progressive fashion (using Range requests).\n* Note that if using cross-domain URL, the server must support CORS, HEAD request and Range header.\n* Exposes progress/complete events.\n*/\nfunction(loggerFactory, utils, ee) {\n    \'use strict\';\n\n    var log = loggerFactory.createLogger(\'BinaryLoader\');\n    log.setLevel(\'warn\');\n    log.enable();\n\n    // CONSTS\n    var DEFAULT_CHUNK_SIZE = 256000; // 250kB\n    var INITIAL_RETRIES = 5;\n\n    function BinaryLoader(iUrl, iChunkSize, env) {\n\n        /////////////////////////////////////////////////////////\n        // Private Members\n        /////////////////////////////////////////////////////////\n\n        var mHeadRetries = INITIAL_RETRIES;\n        var mLoadRetries = INITIAL_RETRIES;\n        var mTotalSize = 0;\n        var mCurrentOffset = 0;\n        var mComplete = false;\n        var mUrl = iUrl;\n        var mArrayBuffer = null;\n        var mArrayBufferTyped = null;\n        var mPaused = false;\n        var mCurrentlyLoading = false;\n        var mCurrentXHR = null;\n        var mChunkSize = iChunkSize ? parseInt(iChunkSize, 10) : DEFAULT_CHUNK_SIZE;\n        var _this = this;\n\n        /////////////////////////////////////////////////////////\n        // Private Methods\n        /////////////////////////////////////////////////////////\n\n        var notifyProgress = function() {\n            _this.trigger(\'progress\', mCurrentOffset, mTotalSize);\n        };\n\n        var notifyComplete = function() {\n            _this.trigger(\'complete\');\n        };\n\n        var setNextChunkBuffer = function(buffer) {\n            mArrayBufferTyped.set(new Uint8Array(buffer), mCurrentOffset);\n            mCurrentOffset += buffer.byteLength;\n        };\n\n        var loadNextChunk = function() {\n            mCurrentlyLoading = true;\n\n            mCurrentXHR = new XMLHttpRequest();\n            mCurrentXHR.onreadystatechange = function() {\n                if (mCurrentXHR.readyState !== 4) {\n                    return;\n                }\n\n                if (mCurrentXHR.status === 206 || mCurrentXHR.status === 200) {\n                    mLoadRetries = INITIAL_RETRIES;\n                    setNextChunkBuffer(mCurrentXHR.response);\n\n                    if (mCurrentOffset === mTotalSize) {\n                        mComplete = true;\n                        mCurrentlyLoading = false;\n\n                        // Notify progress (even if this is the completing chunk)\n                        if (!mPaused) {\n                            notifyProgress();\n                        }\n\n                        // Notify complete\n                        if (!mPaused) {\n                            notifyComplete();\n                        }\n                    } else {\n                        // Notify progress\n                        if (!mPaused) {\n                            notifyProgress();\n                        }\n\n                        if (!mPaused) {\n                            loadNextChunk();\n                        } else {\n                            mCurrentlyLoading = false;\n                        }\n                    }\n                } else {\n                    mCurrentlyLoading = false;\n                    var errorMsg = \'Could not get chunk. Server returned \' + mCurrentXHR.status + \' \' + mCurrentXHR.statusText + \' for url \' + mUrl + \'.\';\n\n                    if (mLoadRetries > 0) {\n                        log.error(errorMsg + \' Retrying.\');\n                        mLoadRetries--;\n                        loadNextChunk();\n                    } else {\n                        log.error(errorMsg + \' No more retries for chunk.\');\n                    }\n                }\n            };\n\n            mCurrentXHR.open(\'GET\', mUrl, true);\n            mCurrentXHR.responseType = \'arraybuffer\';\n            mCurrentXHR.setRequestHeader(\'Range\', \'bytes=\' + mCurrentOffset + \'-\' + Math.min(mCurrentOffset + mChunkSize, mTotalSize));\n\n            // Bug fix for Safari incorrect caching\n            // Without this header, response returns from browser cache, disregarding range\n            if (env.browser.name === \'Safari\') {\n                mCurrentXHR.setRequestHeader(\'If-None-Match\', \'webkit-no-cache\');\n            }\n\n            mCurrentXHR.send(null);\n        };\n\n        /////////////////////////////////////////////////////////\n        // Public Methods\n        /////////////////////////////////////////////////////////\n\n        this.load = function() {\n            mCurrentlyLoading = true;\n            mPaused = false;\n\n            mCurrentXHR = new XMLHttpRequest();\n            mCurrentXHR.onreadystatechange = function() {\n                if (mCurrentXHR.readyState !== 4) {\n                    return;\n                }\n\n                if (mCurrentXHR.status === 200) {\n                    mTotalSize = parseInt(mCurrentXHR.getResponseHeader(\'Content-Length\'), 10);\n                    if (mTotalSize > 0) {\n                        _this.trigger(\'bytesTotal\', mTotalSize);\n\n                        // If not disposed...\n                        if (mUrl) {\n                            mArrayBuffer = new ArrayBuffer(mTotalSize);\n                            mArrayBufferTyped = new Uint8Array(mArrayBuffer);\n                            loadNextChunk();\n                        }\n                    } else {\n                        mCurrentlyLoading = false;\n                        log.error(\'Could not get file size\');\n                    }\n                } else {\n                    mCurrentlyLoading = false;\n                    var errorMsg = \'Oh no! Cannot get HEAD of url: \' + mUrl + \'. Server returned \' + mCurrentXHR.status + \' \' + mCurrentXHR.statusText + \'.\';\n\n                    if (mHeadRetries > 0) {\n                        log.error(errorMsg + \' Retrying.\');\n                        mHeadRetries--;\n                        _this.load();\n                    } else {\n                        log.error(errorMsg + \' No more retries for HEAD request.\');\n                    }\n                }\n            };\n\n            mCurrentXHR.open(\'HEAD\', mUrl, true);\n            // Prevent HEAD request to return from browser cache.\n            // Bug fix for Chrome 38 Bug - If request returns from Chrome cache, it no longer includes the Content-Length header.\n            mCurrentXHR.setRequestHeader(\'If-Modified-Since\', \'Sat, 1 Jan 2005 00:00:00 GMT\');\n            mCurrentXHR.send(null);\n        };\n\n        this.pause = function() {\n            if (!mComplete) {\n                mPaused = true;\n            }\n        };\n\n        this.resume = function() {\n            mPaused = false;\n            if (!mCurrentlyLoading) {\n                if (mTotalSize === 0) {\n                    _this.load();\n                } else if (!mComplete) {\n                    loadNextChunk();\n                } else if (mComplete) {\n                    notifyComplete();\n                }\n            }\n        };\n\n        this.dispose = function() {\n            if (mCurrentXHR) {\n                mCurrentXHR.onreadystatechange = null;\n                try {\n                    mCurrentXHR.abort();\n                } catch (e) {}\n            }\n            _this.trigger(\'dispose\', mUrl);\n            mLoadRetries = 0;\n            mHeadRetries = 0;\n            mCurrentXHR = null;\n            mTotalSize = 0;\n            mCurrentOffset = 0;\n            mComplete = false;\n            mUrl = null;\n            mArrayBuffer = null;\n            mArrayBufferTyped = null;\n            mPaused = false;\n            mCurrentlyLoading = false;\n            _this.removeAllListeners();\n            _this = null;\n        };\n\n        /////////////////////////////////////////////////////////\n        // Public Getters/Setters\n        /////////////////////////////////////////////////////////\n\n        Object.defineProperties(this, {\n\n            /** Number of bytes that were loaded and available in arrayBuffer and/or arrayBufferSlice. */\n            bytesLoaded: {\n                get: function() {\n                    return mCurrentOffset;\n                }\n            },\n\n            /** Number of total bytes. */\n            bytesTotal: {\n                get: function() {\n                    return mTotalSize;\n                }\n            },\n\n            /** ArrayBuffer containing bytesTotal bytes, but only first bytesLoaded bytes are populated with data. */\n            arrayBuffer: {\n                get: function() {\n                    return mArrayBuffer;\n                }\n            },\n\n            /** ArrayBuffer containing bytesLoaded bytes and populated with data.\n            * This could be expensive, so whenever possible use \'arrayBuffer\' instead. */\n            arrayBufferSlice: {\n                get: function() {\n                    if (mComplete) {\n                        return mArrayBuffer;\n                    }\n\n                    return mArrayBuffer.slice(0, mCurrentOffset);\n                }\n            },\n\n            /** Boolean - has loading completed. */\n            completed: {\n                get: function() {\n                    return mComplete;\n                }\n            },\n\n            /** Boolean - has loading been paused. */\n            paused: {\n                get: function() {\n                    return mPaused;\n                }\n            },\n\n            /** Boolean - is currently loading? */\n            loading: {\n                get: function() {\n                    return mCurrentlyLoading && !mPaused && !mComplete;\n                }\n            },\n\n            loadStarted: {\n                get: function() {\n                    return mCurrentlyLoading || mTotalSize > 0;\n                }\n            },\n\n            /** String - the source URL of this binary loader. */\n            url: {\n                get: function() {\n                    return mUrl;\n                }\n            }\n        });\n    }\n\n    // Add the EventEmitter functionality\n    utils.extend(BinaryLoader.prototype, ee.prototype);\n\n    return BinaryLoader;\n});\n\ndefine(\'engine/mse/stores/BinaryStore\',[\n    \'utils/loggerFactory\',\n    \'engine/mse/utils/BinaryLoader\',\n    \'engine/mse/interfaces/IDisposable\',\n    \'mseUtils\',\n    \'utils/EventEmitter\'\n],\n\nfunction(loggerFactory, BinaryLoader, IDisposable, utils, ee) {\n    \'use strict\';\n\n    var log = loggerFactory.createLogger(\'BinaryStore\');\n    log.setLevel(\'warn\');\n\n    ////////////////////////////////////////////////////////////////////////\n    // CONSTRUCTOR + MEMBERS\n    ////////////////////////////////////////////////////////////////////////\n\n    var BinaryStore = function(workerState) {\n        this.binaryMap = {};\n        this.bytesTotal = 0;\n        this.workerState = workerState;\n\n        // Autobinding\n        for (var key in this) {\n            if (typeof this[key] === \'function\') {\n                this[key] = this[key].bind(this);\n            }\n        }\n    };\n\n    ////////////////////////////////////////////////////////////////////////\n    // PRIVATE METHODS\n    ////////////////////////////////////////////////////////////////////////\n\n    var addBinaryLoaderEventListeners = function(ctx, binLoader) {\n        binLoader.on(\'complete\', ctx._onBinaryLoaderComplete);\n        binLoader.on(\'bytesTotal\', ctx._onBinaryLoaderBytesTotal);\n    };\n\n    var removeBinaryLoaderEventListeners = function(ctx, binLoader) {\n        binLoader.off(\'complete\', ctx._onBinaryLoaderComplete);\n        binLoader.off(\'bytesTotal\', ctx._onBinaryLoaderBytesTotal);\n    };\n\n    ////////////////////////////////////////////////////////////////////////\n    // LISTENERS\n    ////////////////////////////////////////////////////////////////////////\n\n    BinaryStore.prototype._onBinaryLoaderComplete = function() {\n        log.info(\'BinaryLoader load complete\');\n    };\n\n    BinaryStore.prototype._onBinaryLoaderBytesTotal = function(bytesTotal) {\n        log.info(\'BinaryLoader bytesTotal: \' + bytesTotal);\n        this.bytesTotal += bytesTotal;\n        this.trigger(\'memory.increased\', this.bytesTotal);\n    };\n\n    ////////////////////////////////////////////////////////////////////////\n    // PUBLIC METHODS\n    ////////////////////////////////////////////////////////////////////////\n\n    BinaryStore.prototype.create = function(url, chunkSize) {\n        if (!this.binaryMap.hasOwnProperty(url)) {\n            this.binaryMap[url] = new BinaryLoader(\n                url,\n                chunkSize,\n                this.workerState.environment\n            );\n\n            addBinaryLoaderEventListeners(this, this.binaryMap[url]);\n        }\n\n        return this.binaryMap[url];\n    };\n\n    BinaryStore.prototype.destroy = function(url) {\n        // Support for multiple arguments\n        if (arguments.length > 1) {\n            for (var i = 0; i < arguments.length; i++) {\n                this.destroy(arguments[i]);\n            }\n            return;\n        }\n\n        // Support for single array argument\n        else if (Array.isArray(url)) {\n            this.destroy.apply(this, url);\n            return;\n        }\n\n        if (!this.binaryMap.hasOwnProperty(url)) {\n            log.warn(\'Cannot unload, BinaryLoader not found: \' + url);\n            return;\n        }\n\n        this.bytesTotal -= this.binaryMap[url].bytesTotal;\n\n        if (this.binaryMap[url].conforms(IDisposable)) {\n            this.binaryMap[url].dispose();\n        }\n\n        removeBinaryLoaderEventListeners(this, this.binaryMap[url]);\n        delete this.binaryMap[url];\n    };\n\n    BinaryStore.prototype.getByNodeId = function(nodeId) {\n        return this.create(this.workerState.nodeIdToUrl(nodeId), this.workerState.nodeIdToChunkSize(nodeId));\n    };\n\n    // Add the EventEmitter functionality\n    utils.extend(BinaryStore.prototype, ee.prototype);\n\n    return BinaryStore;\n});\n\n/* global requirejs */\n/* global // */\n/* global self */\n//(\'../../../../vendor/requirejs/require.js\');\n\nrequirejs.config({\n    baseUrl: \'../../../\',\n    packages: [{\n        name: \'jdataview\',\n        location: \'../vendor/jdataview/dist/browser\',\n        main: \'jdataview\'\n    },\n    {\n        name: \'jbinary\',\n        location: \'../vendor/jbinary/dist/browser\',\n        main: \'jbinary\'\n    },\n    {\n        name: \'mseUtils\',\n        location: \'engine/mse/utils\',\n        main: \'mseUtils\'\n    }]\n});\n\nrequire([\n    \'utils/loggerFactory\',\n    \'mseUtils\',\n    \'engine/mse/workers/WorkerMsg\',\n    \'engine/mse/utils/extensions\',\n    \'engine/mse/interfaces/IDisposable\',\n    \'engine/mse/interfaces/ILoadingManager\',\n    \'engine/mse/interfaces/IParsingManager\',\n    \'engine/mse/interfaces/IMemoryManager\',\n    \'engine/mse/factories/controllerFactory\',\n    \'engine/mse/workers/WorkerState\',\n    \'engine/mse/stores/BinaryStore\'\n],\n\n/**\n* Web Worker for loading/parsing/manipulating video files.\n*/\nfunction(loggerFactory, utils, WorkerMsg, extensions, IDisposable, ILoadingManager, IParsingManager, IMemoryManager, factory, WorkerState, BinaryStore) {\n    \'use strict\';\n\n    //////////////////////////////////////////////////////////////////\n    // MEMBERS\n    //////////////////////////////////////////////////////////////////\n\n    var log = loggerFactory.createLogger(\'mseWorker\');\n    var loadingManager = null;\n    var parsingManager = null;\n    var memoryManager = null;\n    var workerState = null;\n    var binaryStore = null;\n    var methods;\n\n    //////////////////////////////////////////////////////////////////\n    // PRIVATE METHODS\n    //////////////////////////////////////////////////////////////////\n\n    var notifyWorkerStarted = function() {\n        log.debug(\'-> notifyWorkerStarted()\');\n        new WorkerMsg(self, \'workerStart\').post();\n    };\n\n    var notifyWorkerReset = function() {\n        log.debug(\'-> notifyWorkerReset()\');\n        new WorkerMsg(self, \'workerReset\').post();\n    };\n\n    var onSetTimestampOffset = function(newTimestampOffset) {\n        log.debug(\'-> onSetTimestampOffset(\' + newTimestampOffset + \')\');\n        new WorkerMsg(self, \'setTimestampOffset\', newTimestampOffset).post();\n    };\n\n    var onDataAvailable = function(arrayBuffer, data) {\n        log.debug(\'-> onDataAvailable()\');\n        new WorkerMsg(self, \'appendArrayBuffer\', data, arrayBuffer).post();\n    };\n\n    var onSegmentMetadata = function(segmentId, segmentMetadata) {\n        log.debug(\'-> onSegmentMetadata(\' + segmentId + \')\');\n        new WorkerMsg(self, \'segmentMetadata\', {id: segmentId, metadata: segmentMetadata}).post();\n    };\n\n    var onRequestTimeUpdate = function() {\n        log.debug(\'-> onRequestTimeUpdate()\');\n        new WorkerMsg(self, \'requestTimeUpdate\').post();\n    };\n\n    var onChannelSwitched = function(timestampOffset) {\n        log.debug(\'-> onChannelSwitched(\' + timestampOffset + \')\');\n        new WorkerMsg(self, \'channelSwitched\', timestampOffset).post();\n    };\n\n    var addControllersListeners = function() {\n        log.debug(\'-> addControllersListeners()\');\n        if (parsingManager) {\n            parsingManager.on(\'setTimestampOffset\', onSetTimestampOffset);\n            parsingManager.on(\'dataAvailable\', onDataAvailable);\n            parsingManager.on(\'segmentMetadata\', onSegmentMetadata);\n            parsingManager.on(\'requestTimeUpdate\', onRequestTimeUpdate);\n            parsingManager.on(\'channelSwitched\', onChannelSwitched);\n        } else {\n            log.error(\'No parsingManager - cannot add listeners\');\n        }\n    };\n\n    var removeControllersListeners = function() {\n        log.debug(\'-> removeControllersListeners()\');\n        if (parsingManager) {\n            parsingManager.off(\'setTimestampOffset\', onSetTimestampOffset);\n            parsingManager.off(\'dataAvailable\', onDataAvailable);\n            parsingManager.off(\'segmentMetadata\', onSegmentMetadata);\n            parsingManager.off(\'requestTimeUpdate\', onRequestTimeUpdate);\n            parsingManager.off(\'channelSwitched\', onChannelSwitched);\n        }\n    };\n\n    //////////////////////////////////////////////////////////////////\n    // HANDLE MESSAGE\n    //////////////////////////////////////////////////////////////////\n\n    addEventListener(\'message\', function(event) {\n        var msg = event.data;\n\n        log.info(\'Worker got message: [\' + msg.name + \'] \' + (msg.data ? msg.data : \'\'));\n\n        if (methods.can(msg.name)) {\n            methods[msg.name](msg.data);\n        } else {\n            switch (msg.name) {\n                default:\n                    log.error(\'Unkown msg: \' + msg.name);\n                    break;\n            }\n        }\n    }, false);\n\n    //////////////////////////////////////////////////////////////////\n    // METHODS EXPOSED TO MASTER WORKER\n    //////////////////////////////////////////////////////////////////\n\n    function Methods() {\n        this.init = function(data) {\n            log.info(\'init\');\n\n            if (workerState && workerState.conforms(IDisposable)) {\n                workerState.dispose();\n            }\n            workerState = new WorkerState(data);\n\n            if (binaryStore && binaryStore.conforms(IDisposable)) {\n                binaryStore.dispose();\n            }\n            binaryStore = new BinaryStore(workerState);\n\n            // Expose workerState and binaryStore instances on data\n            data.workerState = workerState;\n            data.binaryStore = binaryStore;\n\n            if (loadingManager && loadingManager.conforms(IDisposable)) {\n                loadingManager.dispose();\n            }\n            loadingManager = factory.makeLoadingManager(data);\n            if (!loadingManager || !loadingManager.conforms(ILoadingManager)) {\n                throw new Error(\'Failed to create LoadingManager, or concrete LoadingManager does not conform to ILoadingManager interface.\');\n            }\n\n            if (parsingManager && parsingManager.conforms(IDisposable)) {\n                parsingManager.dispose();\n            }\n            parsingManager = factory.makeParsingManager(data);\n            if (!parsingManager || !parsingManager.conforms(IParsingManager)) {\n                throw new Error(\'Failed to create ParsingManager, or concrete ParsingManager does not conform to IParsingManager interface.\');\n            }\n\n            memoryManager = factory.makeMemoryManager(data);\n            if (!memoryManager || !memoryManager.conforms(IMemoryManager)) {\n                throw new Error(\'Failed to create MemoryManager, or concrete MemoryManager does not conform to IMemoryManager interface.\');\n            }\n\n            addControllersListeners();\n        };\n\n        this.timeupdate = function(data) {\n            workerState.timeupdate(data);\n        };\n\n        this.switchChannel = function(data) {\n            workerState.switchChannel(data);\n        };\n\n        this.appendNode = function(nodeId) {\n            workerState.appendNode(nodeId);\n        };\n\n        this.isStarted = function() {\n            notifyWorkerStarted();\n        };\n\n        this.updateRepo = function(repo) {\n            workerState.updateRepo(repo);\n        };\n\n        this.reset = function() {\n            if (workerState) {\n                workerState.reset();\n            }\n\n            notifyWorkerReset();\n        };\n\n        this.dispose = function() {\n            removeControllersListeners();\n\n            if (loadingManager && loadingManager.conforms(IDisposable)) {\n                loadingManager.dispose();\n            }\n\n            if (parsingManager && parsingManager.conforms(IDisposable)) {\n                parsingManager.dispose();\n            }\n\n            if (memoryManager && memoryManager.conforms(IDisposable)) {\n                memoryManager.dispose();\n            }\n\n            if (binaryStore && binaryStore.conforms(IDisposable)) {\n                binaryStore.dispose();\n            }\n\n            if (workerState && workerState.conforms(IDisposable)) {\n                workerState.dispose();\n            }\n\n            loadingManager = null;\n            parsingManager = null;\n            memoryManager = null;\n            workerState = null;\n            binaryStore = null;\n        };\n    }\n\n    // Instantiate methods member\n    methods = new Methods();\n\n    // Disable logger\n    log.setLevel(\'warn\');\n\n    // Notify master worker that we are ready to receive messages!\n    notifyWorkerStarted();\n});\n\ndefine("engine/mse/workers/mseWorker", function(){});\n\n}());';});

/* global MediaSource */

define(
'engine/mse/MseEngine',[
    'engine/BaseEngine',
    'engine/mse/MseVideo',
    'when',
    'utils/utils',
    'engine/mse/workers/WorkerMsg',
    'text!../../../tmp/mseWorker.min.js'
],

/**
* Concrete engine for MSE (Media Source Extension) tech.
*/
function(BaseEngine, MseVideo, when, utils, WorkerMsg, mseWorkerText) {
    'use strict';

    /**
    * Private helper function - gets a format name (String) and playlist, and checks if playlist contains format.
    */
    function doesSegmentContainFormat(iFormatName, iSegment)
    {
        return !!(iSegment && iSegment.source && iSegment.source[iFormatName]);
    }

    /**
    * Private helper function - gets an array of supported formats and a playlist, and chooses/returns chosen format.
    */
    function chooseVideoFormat(iSupportedFormats, iSegment, log)
    {
        var retVal = null;

        for (var i = 0; i < iSupportedFormats.length; i++) {
            if (doesSegmentContainFormat(iSupportedFormats[i].source, iSegment)) {
                retVal = iSupportedFormats[i];
                break;
            }
        }

        if (retVal === null) {
            if (log) {
                log.warn('Current playlist does not fully support any compatible MSE format, using preferred format: ' + iSupportedFormats[0].source);
            }

            retVal = iSupportedFormats[0];
        }

        if (log) {
            log.info('Using MSE format: ' + retVal.source);
        }

        return retVal;
    }

    function modifyWorkerText(workerText) {
        var RELEASE = true;
        var currUrl = location.origin + location.pathname;
        var requireRelativeUrl;
        var baseUrl;

        if (RELEASE) {
            return workerText;
        }

        currUrl = currUrl.substring(0, currUrl.lastIndexOf('/') + 1);
        requireRelativeUrl = require.toUrl('../../../tmp/mseWorker.min.js');
        requireRelativeUrl = requireRelativeUrl.substring(0, requireRelativeUrl.lastIndexOf('/') + 1);
        baseUrl = currUrl + requireRelativeUrl;
        workerText = workerText.replace('importScripts(', 'importScripts(\'' + baseUrl + '\' + ');
        workerText = workerText.replace('baseUrl:', 'baseUrl: \'' + baseUrl + '\' +');

        return workerText;
    }

    /**
     * MseEngine Class
     */
    var MseEngine = BaseEngine.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'MseEngine',
        logLevel: 'warn',
        defaults: {
            parallels: {
                appendOffset: 1.5,
                initialAppend: 2.0,
                playbackInterval: 0.15
            },
            memory: {
                type: 'constrained',
                maxmemory: 100000000, // 100MB
                logic: {
                    and: {
                        infutureplaylist: false,
                        or: {
                            inrepo: false,
                            innextlevels: {
                                depth: 3,
                                value: false
                            }
                        }
                    }
                }
            },
            binaryLoaderChunkSize: 256000, // 250kB
            useVideoBufferForTimestampOffsets: true,
            updateSegmentDurationAccordingToVideoBuffer: true,
            switchChannelTimeout: 3,
            seekTimeout: 15,
            triggerPlayingAfterSeekedTimeoutSeconds: 0.5,
            hideVideoOnReset: false,
            minDurationForceSeeked: 2.0
        },

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ MEMBERS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        mediaSource: null,
        sourceBuffer: null,
        appendQueue: null,
        supportedFormats: null,
        format: null,
        recentAppendMeta: null,
        workerStartedDefer: null,
        workerInitedDefer: null,
        workerReady: false,
        mseWorker: null,
        htmlVideo: null,
        playbackOffset: 0,
        cancelAppends: false,
        origCurrentTimeGetter: null,
        updateWorkerRepoScheduled: false,

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ METHODS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        postSeekAppendFutureNodes: function() {
            for (var i = this.api.exports.currentNodeIndex + 1; i < this.api.exports.playlist.length; i++) {
                this.appendNode(this.api.exports.playlist[i].id);
            }

            this.cancelAppends = false;
        },

        postMessage: function(msgName, msgData) {
            this.log.info('Posting message: [' + msgName + '] ' + (msgData ? msgData : ''));
            new WorkerMsg(this.mseWorker, msgName, msgData).post();
        },

        probeWorkerStarted: function() {
            this.postMessage('isStarted');
        },

        onSourceBufferError: function() {
            this.log.error('MSE SourceBuffer error event.');
        },
        onSourceBufferUpdateStart: function() {},
        onSourceBufferUpdate: function() {},
        onSourceBufferAbort: function() {},
        onSourceBufferUpdateEnd: function() {
            while (this.appendQueue.length && this.sourceBuffer && !this.sourceBuffer.updating) {
                var obj = this.appendQueue[0];
                var meta = obj.meta;
                var data = obj.data;

                // If the current data is a Number, then we need to set the timestampOffset
                if (typeof data === 'number') {
                    data += this.playbackOffset;
                    if (this.sourceBuffer.timestampOffset !== data) {
                        // If configured to calculate offset timestamp from video buffer
                        // or if we don't have the duration of current (soon to be previous) appending segment,
                        // we'll deduce it from sourceBuffer
                        if (this.getSetting('useVideoBufferForTimestampOffsets') || isNaN(data)) {
                            // The timestamp offset should be set to end of current buffer
                            data = this.sourceBuffer.buffered.end(this.sourceBuffer.buffered.length - 1);

                            // If configured to update segment's duration according to video buffer,
                            // or if the segment does not have a duration,
                            // set the duration so that other modules could use it (i.e. CuePoints)
                            if (this.getSetting('updateSegmentDurationAccordingToVideoBuffer') ||
                                    (meta && this.repo.nodes[meta.nodeId] && isNaN(this.repo.nodes[meta.nodeId].segment.duration))) {
                                this.repository.updateSegment({id: this.repo.nodes[meta.nodeId].segment.id, duration: (data - this.sourceBuffer.timestampOffset)});
                            }
                        }

                        try {
                            this.sourceBuffer.timestampOffset = data;
                        } catch (ex) {
                            this.log.error('Error setting SourceBuffer\'s timestampOffset. ' + ex.name + ': ' + ex.message);
                            this.notifyError(49, ex.name, ex.message);
                            break;
                        }
                    }
                    this.appendQueue.shift();
                }

                // Otherwise, if data is the string 'end', then end the stream
                else if (typeof data === 'string' && data === 'end') {
                    try {
                        this.mediaSource.endOfStream();
                        this.appendQueue.shift();
                    } catch (ex) {
                        this.log.error('Error ending MediaSource stream. ' + ex.name + ': ' + ex.message);
                        this.notifyError(51, ex.name, ex.message);
                    }
                    break;
                }

                // Otherwise, data is an ArrayBuffer, let's append it to MSE stream
                else {
                    try {
                        this.sourceBuffer.appendBuffer(data);
                        this.appendQueue.shift();
                    } catch (ex) {
                        this.log.error('Error appending ArrayBuffer to MSE SourceBuffer. ' + ex.name + ': ' + ex.message);
                        this.notifyError(5, ex.name, ex.message);
                    }
                    break;
                }
            }
        },

        createMediaSource: function() {
            var retVal = new MediaSource();

            if (!retVal) {
                this.log.error('Could not create MediaSource instance');
                this.notifyError(6);
                return null;
            }

            retVal.addEventListener('sourceopen', this.onSourceOpen);
            retVal.addEventListener('sourceclose', this.onSourceClose);
            retVal.addEventListener('sourceended', this.onSourceEnded);

            return retVal;
        },

        onSourceOpen: function() {
            this.sourceBuffer = this.mediaSource.addSourceBuffer(this.format.mimeTypeAndCodecs);
            this.appendQueue = [];
            this.sourceBuffer.addEventListener('updateend', this.onSourceBufferUpdateEnd);
            this.sourceBuffer.addEventListener('updatestart', this.onSourceBufferUpdateStart);
            this.sourceBuffer.addEventListener('update', this.onSourceBufferUpdate);
            this.sourceBuffer.addEventListener('error', this.onSourceBufferError);
            this.sourceBuffer.addEventListener('abort', this.onSourceBufferAbort);
        },
        onSourceEnded: function() {},
        onSourceClose: function() {},
        disposeMediaSource: function() {
            if (this.mediaSource && this.sourceBuffer) {
                this.sourceBuffer.removeEventListener('updateend', this.onSourceBufferUpdateEnd);
                this.sourceBuffer.removeEventListener('updatestart', this.onSourceBufferUpdateStart);
                this.sourceBuffer.removeEventListener('update', this.onSourceBufferUpdate);
                this.sourceBuffer.removeEventListener('error', this.onSourceBufferError);
                this.sourceBuffer.removeEventListener('abort', this.onSourceBufferAbort);
                if (this.mediaSource.readyState === 'open') {
                    this.sourceBuffer.abort();
                }
                try {
                    this.mediaSource.removeSourceBuffer(this.sourceBuffer);
                } catch (e) {}
                this.mediaSource.removeEventListener('sourceopen', this.onSourceOpen);
                this.mediaSource.removeEventListener('sourceclose', this.onSourceClose);
                this.mediaSource.removeEventListener('sourceended', this.onSourceEnded);
            }

            this.sourceBuffer = null;
            this.mediaSource = null;
            this.playbackOffset = 0;
        },

        initWithNodeId: function(nodeId) {
            this.format = chooseVideoFormat(this.supportedFormats, this.repo.nodes[nodeId].segment, this.log);

            var d = when.defer();
            var shouldSetSource = false;

            if (!this.mediaSource) {
                this.mediaSource = this.createMediaSource();
                shouldSetSource = true;
            }

            if (!this.mediaSource) {
                this.log.error('MSE is not available');
                this.notifyError(7);
                d.reject();
            } else {
                this.probeWorkerStarted();
                this.workerStartedDefer.promise.done(function() {
                    if (shouldSetSource) {
                        this.htmlVideo.setSrc(window.URL.createObjectURL(this.mediaSource));
                    }
                    this.initWorker();
                    this.workerInitedDefer.resolve();
                    d.resolve();
                }.bind(this));
            }

            return d.promise;
        },

        appendNode: function(nodeId) {
            var d = when.defer();

            // Validate input
            if (!nodeId || !this.repo || !this.repo.nodes || !this.repo.nodes.hasOwnProperty(nodeId)) {
                d.reject();
            }

            // Input is valid, let's try appending node
            else {
                if (!this.workerInitedDefer) {
                    this.workerInitedDefer = when.defer();
                    this.initWithNodeId(nodeId);
                }

                this.workerInitedDefer.promise.done(function() {
                    // If worker has been disposed since promise was made
                    // Simply resolve and return (no need to perform the now irrelevant appendNode operation)
                    if (!this.workerReady) {
                        d.resolve();
                        return;
                    }

                    // If we specifically requested to cancel queued appends, simply resolve and return
                    if (this.cancelAppends) {
                        d.resolve();
                        return;
                    }

                    var node = this.repo.nodes[nodeId];
                    var segId = node.segment.id;
                    var segment = this.repo.segments[segId];

                    if (segment.source[this.format.source]) {

                        // If we have a repo update scheduled, let's perform it now
                        // to prevent a case where the node being appended may not yet exist in worker's repo copy.
                        if (this.updateWorkerRepoScheduled) {
                            this.doUpdateWorkerRepo();
                            this.updateWorkerRepoScheduled = false;
                        }

                        // Send worker 'appendNode' message and resolve
                        this.postMessage('appendNode', nodeId);
                        d.resolve(nodeId);
                    } else {
                        this.log.error('Segment "' + segId + '" does not include "' + this.format.source + '"" source.');
                        this.notifyError(8, segId, this.format.source);
                        d.reject();
                    }
                }.bind(this),
                function() {
                    this.log.error('Unable to initialize worker.');
                    this.notifyError(9);
                    d.reject();
                }.bind(this));
            }

            return d.promise;
        },

        /**
        * Queue an append of a chunk of video and/or set timestampOffset (if param is a number).
        */
        appendChunk: function(arrayBuffer) {
            // Bug fix - sometimes worker append arrives after reset...
            if (!this.sourceBuffer) {
                return;
            }

            this.appendQueue.push(arrayBuffer);

            // If we're not currently in append operation, and the queue consists ONLY of the arrayBuffer we just pushed,
            // Manually trigger the onSourceBufferUpdateEnd function.
            if (!this.sourceBuffer.updating && this.appendQueue.length === 1) {
                this.onSourceBufferUpdateEnd();
            }
        },

        onWorkerMessage: function(event) {
            var msg = event.data;
            this.log.info('Main got message: [' + msg.name + '] ' + (msg.data && msg.data.nodeId ? msg.data.nodeId : (msg.data ? msg.data : '')));

            switch (msg.name) {
                case 'workerStart':
                    this.workerStartedDefer.resolve();
                    break;
                case 'appendArrayBuffer':
                    this.recentAppendMeta = msg.data;
                    this.appendChunk({meta: msg.data, data: msg.arrayBuffer});
                    break;
                case 'setTimestampOffset':
                    this.appendChunk({meta: this.recentAppendMeta, data: msg.data});
                    break;
                case 'segmentMetadata':
                    if (this.repo.segments[msg.data.id] && isNaN(this.repo.segments[msg.data.id].duration)) {
                        this.repository.updateSegment({id: msg.data.id, duration: msg.data.metadata.duration});
                    }
                    break;
                case 'requestTimeUpdate':
                    this.postMessage('timeupdate', {
                        currentNodeId: this.api.exports.currentNode && this.api.exports.currentNode.id,
                        time: this.video.currentTime(),
                        paused: this.htmlVideo.paused()
                    });
                    break;
                case 'channelSwitched':
                    break;
                case 'workerReset':
                    break;
                case 'log':
                    this.log.info(msg.data);
                    break;
                default:
                    this.log.error('Unkown msg: ' + msg.name);
                    this.notifyError(10, msg.name);
                    break;
            }

            this.trigger(msg.name, msg);
        },

        reset: function() {
            var d = when.defer();

            var onPaused = function() {
                this.htmlVideo.off(this.events.pause, onPaused);
                this.disposeMediaSource();
                this.htmlVideo.setSrc(window.URL.createObjectURL(new MediaSource()));
                if (this.getSetting('hideVideoOnReset')) {
                    this.htmlVideo.getElement().style.visibility = 'hidden';
                }
                d.resolve();
            }.bind(this);

            this.workerInitedDefer = null;
            this.disposeWorker();
            this.appendQueue = [];
            this.recentAppendMeta = null;

            if (!this.htmlVideo.paused()) {
                this.htmlVideo.on(this.events.pause, onPaused);
                this.htmlVideo.pause();
            } else {
                onPaused();
            }

            return d.promise;
        },

        endStream: function() {
            if (this.mediaSource && this.mediaSource.readyState === 'open') {
                this.appendChunk({meta: null, data: 'end'});
            }
        },

        disposeWorker: function() {
            this.postMessage('dispose');
            this.workerReady = false;
        },

        initWorker: function() {
            this.postMessage('init', utils.extend(true, {
                repo: this.getWorkerSafeRepo(),
                format: this.format,
                environment: this.environment
            }, this.settings));
            this.workerReady = true;
        },

        switchChannel: function(channelIndex) {
            var d = when.defer();
            var channelId;
            var currentNode = this.api.exports.currentNode;

            if (currentNode &&
                this.recentAppendMeta &&
                this.recentAppendMeta.chunkSrcTimestamp >= 0 &&
                this.recentAppendMeta.chunkDuration >= 0 &&
                this.recentAppendMeta.chunkSrcTimestamp + this.recentAppendMeta.chunkDuration < this.recentAppendMeta.nodeDuration &&
                this.recentAppendMeta.nodeId === currentNode.id) {

                var segUrl = currentNode.segments[channelIndex].source[this.format.source].url;

                channelId = segUrl.indexOf('#') >= 0 ?
                                        parseInt(segUrl.substr(segUrl.indexOf('#') + 1), 10) :
                                        -1;

                if (channelId === -1) {
                    d.reject('Could not find channel ID - did you remember to supply it after hash?');
                } else {
                    var resolved = false;
                    var onChannelSwitched = function(msg) {
                        if (!resolved) {
                            resolved = true;
                            d.resolve({
                                timeInNode: msg.data
                            });
                        }
                    };
                    this.one('channelSwitched', onChannelSwitched, false);

                    setTimeout(function() {
                        if (!resolved) {
                            this.off('channelSwitched', onChannelSwitched);
                            d.reject('Channel switch timed out');
                        }
                    }.bind(this), this.getSetting('switchChannelTimeout') * 1000);

                    this.postMessage('switchChannel', {currentNodeId: currentNode.id, channelId: channelId});
                }
            } else {
                d.reject();
            }

            return d.promise;
        },

        seek: function(nodeId, timeInNode) {
            var d = when.defer();
            var wasPlaying = !this.htmlVideo.paused();
            var currentTime;

            if (typeof timeInNode !== 'number') {
                timeInNode = 0;
            }

            currentTime = this.htmlVideo.currentTime();

            this.htmlVideo.currentTime = function() { return currentTime; };
            this.cancelAppends = true;

            var onWorkerReset = function() {
                this.log.info('[Seek] -> onWorkerReset()');
                this.appendQueue = [];
                this.recentAppendMeta = null;

                // Abort any currently processing appends
                if (this.sourceBuffer && this.mediaSource && this.mediaSource.readyState === 'open') {
                    this.sourceBuffer.abort();
                    this.log.info('[Seek] Calling SourceBuffer.abort()');
                }

                // Set timestamp offset for next append actions
                var bufferOffset = 0;
                if (this.sourceBuffer) {
                    bufferOffset = this.sourceBuffer.buffered.end(this.sourceBuffer.buffered.length - 1);
                    this.sourceBuffer.timestampOffset = bufferOffset;
                }
                this.playbackOffset = bufferOffset;

                // Append node
                this.cancelAppends = false;
                this.appendNode(nodeId).done(function() {
                    this.cancelAppends = true;
                    this.log.info('[Seek] Done appending node');
                    var resolved = false;
                    var onSeeked = function() {
                        this.log.info('[Seek] onSeeked (HTML5 video event), currentTime: ' + this.htmlVideo.getElement().currentTime);
                        this.htmlVideo.off('seeked', onSeeked);

                        if (!resolved) {
                            resolved = true;
                            this.htmlVideo.currentTime = this.origCurrentTimeGetter;

                            if (wasPlaying) {
                                // Ensure that playing event will be fired shortly (required for Android Chrome)
                                this.video.play();
                                setTimeout(this.video.onPlaying, this.getSetting('triggerPlayingAfterSeekedTimeoutSeconds') * 1000);
                            }

                            d.resolve();
                            setTimeout(this.postSeekAppendFutureNodes, 0);
                        }

                    }.bind(this);

                    var onTimeout = function() {
                        this.htmlVideo.off('seeked', onSeeked);

                        if (!resolved) {
                            this.log.info('[Seek] onSeeked timed out');
                            this.htmlVideo.currentTime = this.origCurrentTimeGetter;
                            setTimeout(this.postSeekAppendFutureNodes, 0);
                            d.reject(new Error('Could not complete seek'));
                        }
                    }.bind(this);

                    this.htmlVideo.on('seeked', onSeeked);
                    this.log.info('[Seek] Seeking to: ' + (bufferOffset + timeInNode) + ', bufferOffset: ' + bufferOffset);
                    this.htmlVideo.setCurrentTime(bufferOffset + timeInNode);

                    if (this.repo.nodes[nodeId] &&
                        this.repo.nodes[nodeId].segment &&
                        this.repo.nodes[nodeId].segment.duration <= this.getSetting('minDurationForceSeeked')) {
                        this.log.info('[Seek] forcing onSeeked() since duration is short');
                        onSeeked();
                    }

                    setTimeout(onTimeout, this.getSetting('seekTimeout') * 1000);
                }.bind(this));
            }.bind(this);

            // Reset worker
            this.one('workerReset', onWorkerReset);
            this.postMessage('reset');

            return d.promise;
        },

        getWorkerSafeRepo: function() {
            var retVal = {segments: {}, nodes: {}};

            for (var nodeId in this.repo.nodes) {
                retVal.nodes[nodeId] = this.repo.nodes[nodeId].getWorkerSafeNode();
            }

            for (var segId in this.repo.segments) {
                retVal.segments[segId] = this.repo.segments[segId].getWorkerSafeSegment();
                retVal.segments[segId].binaryLoaderChunkSize = this.repo.segments[segId].data && this.repo.segments[segId].data.binaryLoaderChunkSize ?
                                                                                    this.repo.segments[segId].data.binaryLoaderChunkSize :
                                                                                    this.getSetting('binaryLoaderChunkSize');
            }

            return retVal;
        },

        doUpdateWorkerRepo: function() {
            this.postMessage('updateRepo', this.getWorkerSafeRepo());
        },

        scheduleWorkerRepoUpdate: function() {
            if (!this.updateWorkerRepoScheduled) {
                this.updateWorkerRepoScheduled = true;
                setTimeout(function() {
                    if (this.updateWorkerRepoScheduled) {
                        this.doUpdateWorkerRepo();
                        this.updateWorkerRepoScheduled = false;
                    }
                }.bind(this), 0);
            }
        },

        onRepoChange: function() {
            // We only want to update worker of repo changes if we already inited the worker
            if (this.workerInitedDefer && this.workerInitedDefer.promise.inspect().state === 'fulfilled') {
                // For now, the implementation is simply to update the entire repo in worker
                // If we see that this is too costly, we'll change the implementation

                // Schedule update of entire repo in worker
                // This was done to improve performance (multiple repo changes may be encapsulated into 1 worker message)
                this.scheduleWorkerRepoUpdate();
            }
        },

        onPlaying: function() {
            this.htmlVideo.getElement().style.visibility = 'visible';
        },

        videoTech: function() {
            return 'mse';
        },

        //////////////////////////////////////////////////////////////////////////
        // ---------------------------- CONSTRUCTOR ------------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function(el, core, repo) {
            this._super(core, repo);

            // create video
            this.video = new MseVideo(this.options, this.api, el);
            this.htmlVideo = this.video.htmlVideo;

            // Override currentTime() function to include our playbackOffset
            this.video.currentTime = function() {
                return this.htmlVideo.currentTime() - this.playbackOffset;
            }.bind(this);

            // Keep reference to HTML5 currentTime getter function
            this.origCurrentTimeGetter = this.htmlVideo.currentTime;

            // Add error listeners to submodules
            this.addErrorListener(this.video);

            this.supportedFormats = this.getSetting('formats');
            this.mseWorker = utils.makeWorker(modifyWorkerText(mseWorkerText), this.log);
            this.mseWorker.addEventListener('message', this.onWorkerMessage, false);
            this.workerStartedDefer = when.defer();

            // Hide the video until we start playing.
            this.htmlVideo.on(this.events.playing, this.onPlaying);
            this.htmlVideo.getElement().style.visibility = 'hidden';

            // Define repo getter
            this.defineProperty('repo', function() {
                return {
                    nodes: this.repository.nodeMap,
                    segments: this.repository.segmentMap
                };
            });

            // Listen to repository changes (we only want to update worker when there are node/prefetch changes)
            this.repository.on(/repository\.(segment|node|prefetch)\..*/, this.onRepoChange);
        },

        dispose: function() {
            this._super();

            if (this.mseWorker) {
                this.mseWorker.removeEventListener('message', this.onWorkerMessage);
                this.mseWorker.terminate();
            }

            this.htmlVideo.off(this.events.playing, this.onPlaying);
            this.repository.off(/repository\.(segment|node|prefetch)\..*/, this.onRepoChange);
            this.disposeMediaSource();

            // Bug fix - observed behaviour - navigating away (or page reload) while MediaSource is still attached to video
            // causes severe memory leaks (document, DOM nodes and JS heap memory are retained)
            // This was observed using Android Chrome 39 using dev tools (timeline) and remote debugging
            this.htmlVideo.setSrc(window.URL.createObjectURL(new MediaSource()));
        }
    });

    return MseEngine;
});

define(
'engine/mse/MseFactory',[
    'engine/BaseFactory',
    'engine/mse/MseEngine'
],

/**
* Main factory for MSE (Media Source Extension) tech.
*/
function(BaseFactory, MseEngine) {
    'use strict';

    var MseFactory = BaseFactory.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'MseFactory',

        //////////////////////////////////////////////////////////////////////////
        // ------------------------- PRIVATE METHODS -----------------------------
        //////////////////////////////////////////////////////////////////////////

        /** Overrides base implementation. */
        _createEngine: function() {
            return new MseEngine(this.options, this.el, this.core, this.repo);
        }
    });

    return MseFactory;
});

define('engine/flash/FlashVideo',[
    'engine/BaseVideo',
    'when'
],

/**
 * A concrete implementations of BaseVideo for flash tech.
 */
function(BaseVideo, when) {
    'use strict';

    var FlashVideo = BaseVideo.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'FlashVideo',
        logLevel: 'warn',
        excludeFuncFromAutoDebug: ['onTimeUpdateIntervalTick', 'currentTime'],
        defaults: {
            timeUpdateIntervalMs: 250,
            currentlySeekingTimeoutMs: 350
        },

        //////////////////////////////////////////////////////////////////////////
        // ----------------------------- MEMBERS ---------------------------------
        //////////////////////////////////////////////////////////////////////////

        api: null,
        events: null,
        flashAPI: null,
        flashEventEmitter: null,
        isReadyDefer: null,
        shouldBePlaying: false,
        playing: false,
        forcingPlay: false,
        currentState: 'Idle',
        currentlySeeking: false,
        currentlySeekingTimeoutRef: null,
        timeUpdateIntervalRef: null,
        _muted: false,
        _volume: 1,

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MISC METHODS -------------------------------
        //////////////////////////////////////////////////////////////////////////

        addFlashListeners: function() {
            if (this.flashEventEmitter) {
                this.flashEventEmitter.on('netStatus', this.onNetStatus);
                this.flashEventEmitter.on('stateChanged', this.onStateChanged);
                this.flashEventEmitter.on('controlStateChanged', this.onControlStateChanged);
                this.flashEventEmitter.on('seekComplete', this.onSeekComplete);
            }

            if (this.api) {
                this.api.on(this.events.seeking, this.onAPISeeking);
                this.api.on(this.events.seeked, this.onAPISeeked);
                this.api.on(this.events.ended, this.onAPIEnded);
            }
        },

        removeFlashListeners: function() {
            if (this.flashEventEmitter) {
                this.flashEventEmitter.off('netStatus', this.onNetStatus);
                this.flashEventEmitter.off('stateChanged', this.onStateChanged);
                this.flashEventEmitter.off('controlStateChanged', this.onControlStateChanged);
                this.flashEventEmitter.off('seekComplete', this.onSeekComplete);
            }

            if (this.api) {
                this.api.off(this.events.seeking, this.onAPISeeking);
                this.api.off(this.events.seeked, this.onAPISeeked);
                this.api.off(this.events.ended, this.onAPIEnded);
            }
        },

        onTimeUpdateIntervalTick: function() {
            this.trigger(this.events.timeupdate, this.currentTime());
        },

        startTimeUpdateInterval: function() {
            if (!this.timeUpdateIntervalRef) {
                this.timeUpdateIntervalRef = setInterval(
                    this.onTimeUpdateIntervalTick,
                    this.getSetting('timeUpdateIntervalMs')
                );
            }
        },

        stopTimeUpdateInterval: function() {
            if (this.timeUpdateIntervalRef) {
                clearInterval(this.timeUpdateIntervalRef);
                this.timeUpdateIntervalRef = null;
            }
        },

        //////////////////////////////////////////////////////////////////////////
        // ---------------------- FLASH EVENT LISTENERS -------------------------
        //////////////////////////////////////////////////////////////////////////

        onNetStatus: function(info) {
            this.log.info('NetStatusEvent: ' + info.code);

            switch (info.code) {
                case 'NetStream.Video.DimensionChange':
                    this.onLoadedData();
                    break;
                case 'NetStream.Seek.Notify':
                    this.onSeeked();
                    break;
                case 'NetStream.SeekStart.Notify':
                    this.onSeeking();
                    break;
                case 'NetStream.Buffer.Empty':
                    // waiting or ended!
                    break;
                case 'NetStream.Buffer.Full':
                    if (this.currentState === 'Playing') {
                        this.onPlaying();
                    }
                    break;
                case 'NetStream.Pause.Notify':
                    break;
                case 'NetStream.Unpause.Notify':
                    this.onPlaying();
                    break;
                default:
                    if (info.level && info.level === 'error') {
                        this.onError(info);
                    }
                    break;
            }
        },

        onStateChanged: function(newState) {
            this.log.info('StateChanged: ' + this.currentState + ' ===> ' + newState);

            switch (newState) {
                case 'Idle':
                    this.onTimeUpdateIntervalTick();
                    if (!this.currentlySeeking) {
                        this.onPause();
                        this.flashAPI.clearVideoImage();
                    }
                    break;
                case 'Buffering':
                case 'BufferingToPaused':
                    // this.onWaiting();
                    break;
                case 'Playing':
                    break;
                case 'Paused':
                    break;
                default:
                    break;
            }

            this.currentState = newState;
        },

        onControlStateChanged: function(newControlStateName) {
            switch (newControlStateName) {
                case 'com.interlude.media.playback.playbackStates::StatePlaybackBufferToPlay':
                    this.onWaiting();
                    break;
                default:
                    break;
            }
        },

        onSeekComplete: function() {

        },

        //////////////////////////////////////////////////////////////////////////
        // ---------------------- PSEUDO EVENT LISTENERS -------------------------
        //////////////////////////////////////////////////////////////////////////

        onError: function(e) {
            this.notifyError('Flash video error', e);
        },

        onPause: function() {
            this.playing = false;
            this.shouldBePlaying = false;
            this.stopTimeUpdateInterval();
            this.trigger(this.events.pause);
        },

        onPlay: function() {
            this.startTimeUpdateInterval();
            this.shouldBePlaying = true;

            if (this.forcingPlay) {
                this.forcingPlay = false;
                return;
            }

            this.trigger(this.events.play);
        },

        onPlaying: function() {
            this.shouldBePlaying = true;
            this.playing = true;
            this.startTimeUpdateInterval();
            this.trigger(this.events.playing);
        },

        onWaiting: function() {
            this.trigger(this.events.waiting);
        },

        onLoadedData: function() {
            this.trigger(this.events.loadeddata);
        },

        onSeeking: function() {

        },

        onSeeked: function() {
            this.onCanPlayThrough();
        },

        onEnded: function() {
            this.trigger(this.events.ended);
            this.stopTimeUpdateInterval();
        },

        onCanPlayThrough: function() {
            if (this.shouldBePlaying && !this.playing) {
                this.forcingPlay = true;
                this.play();
            }
        },

        onAPISeeking: function() {
            this.currentlySeeking = true;
            this.playing = false;
            this.currentlySeekingTimeoutRef = setTimeout(function() {
                this.currentlySeeking = false;
            }.bind(this), this.getSetting('currentlySeekingTimeoutMs'));
        },

        onAPISeeked: function() {
            this.currentlySeeking = false;
            if (this.currentlySeekingTimeoutRef) {
                clearTimeout(this.currentlySeekingTimeoutRef);
                this.currentlySeekingTimeoutRef = null;
            }
            this.onCanPlayThrough();
        },

        onAPIEnded: function() {
            this.playing = false;
            this.stopTimeUpdateInterval();
        },

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- API METHODS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        play: function() {
            this.isReadyDefer.promise.done(function() {
                this.onPlay();
                this.flashAPI.play();
            }.bind(this));
        },

        pause: function() {
            this.isReadyDefer.promise.done(function() {
                this.onPause();
                this.flashAPI.pause();
            }.bind(this));
        },

        currentTime: function() {
            if (!this.flashAPI) {
                return 0;
            }

            return this.flashAPI.currentTime();
        },

        getVolume: function() {
            return this._volume;
        },

        setVolume: function(value) {
            if (this._volume !== value) {
                if (value >= 0 && value <= 1) {
                    this._volume = value;
                    this.isReadyDefer.promise.done(function() {
                        if (!this._muted) {
                            this.flashAPI.setVolume(value);
                        }

                        this.trigger(this.events.volumechange);
                    }.bind(this));
                } else {
                    this.log.error('Failed to set the \'volume\' property: The volume provided (' + value + ') is outside the range [0, 1].');
                }
            }
        },

        getMuted: function() {
            return this._muted;
        },

        setMuted: function(value) {
            value = !!value;
            if (this._muted !== value) {
                this._muted = value;
                this.isReadyDefer.promise.done(function() {
                    this.flashAPI.setVolume(value ? 0 : this._volume);
                    this.trigger(this.events.volumechange);
                }.bind(this));
            }
        },

        //////////////////////////////////////////////////////////////////////////
        // --------------------------- CONSTRUCTOR -------------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function(api, swfEmbedder) {
            // Keep references
            this.api = api;
            this.events = api.events;

            // Create a 'flash-is-ready' promise
            this.isReadyDefer = when.defer();

            // Keep reference to SWF object
            swfEmbedder.promise().done(function(obj) {
                this.flashAPI = obj;
                this.flashEventEmitter = swfEmbedder.swfEventEmitter;
                this.addFlashListeners();
                this.isReadyDefer.resolve();
            }.bind(this));

            // Define properties
            this.defineProperty('volume', function() {
                return this.getVolume();
            }, function(value) {
                this.setVolume(value);
            });

            this.defineProperty('muted', function() {
                return this.getMuted();
            }, function(value) {
                this.setMuted(value);
            });

            // Call base ctor - exposes public API.
            this._super.apply(this, arguments);
        },

        dispose: function() {
            this._super();
            this.removeFlashListeners();
            this.stopTimeUpdateInterval();
        }
    });

    return FlashVideo;
});

define('engine/flash/SWFEmbedder',[
    'modules/Base',
    'swfobject',
    'utils/utils',
    'utils/EventEmitter',
    'utils/environment',
    'when'
],

/**
 * Class used for embedding of flash engine SWF.
 */
function(BaseModuleClass, swfobject, utils, EventEmitter, env, when) {
    'use strict';

    var SWFEmbedder = BaseModuleClass.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'SWFEmbedder',
        logLevel: 'warn',
        defaults: {
            width: '100%',
            height: '100%',
            flashvars: {
                jsonConfig: JSON.stringify({
                    wratio: 16,
                    hratio: 9,
                    debug: false,
                    useVideoAcceleration: (function() {
                        // Prefer using StageVideo in all browsers except Chrome.
                        return env.browser.name !== 'Chrome';
                    }()),
                    backgroundColor: 0x000000,
                    globalConfig: {
                        MODE: 'mobile',
                        USE_SYSTEM_TOTAL_MEMORY: true,
                        SET_FLVTAG_BYTEARRAY_LENGTH: false,
                        USER_MAX_MEMORY: 250000000 // 250MB
                    }
                })
            },
            params: {
                quality: 'high',
                bgcolor: '0',
                align: 'TL',
                scale: 'noscale',
                wmode: 'opaque',
                allowscriptaccess: 'always',
                allowfullscreen: 'true'
            },
            attributes: {
                // id: '',
                // name: '',
                align: 'middle',
                style: env.browser.name !== 'IE' ? 'visibility: hidden;' : 'background-color: black;'
            }
        },
        excludeFuncFromAutoDebug: ['getSwfLoadedFuncName', 'getSwfEventHandlerFuncName', 'promise'],

        //////////////////////////////////////////////////////////////////////////
        // ----------------------------- MEMBERS ---------------------------------
        //////////////////////////////////////////////////////////////////////////

        swfObj: null,
        swfVersion: null,
        swfEventEmitter: null,
        swfLoadedDefer: null,
        uid: null,

        //////////////////////////////////////////////////////////////////////////
        // ----------------------------- METHODS ---------------------------------
        //////////////////////////////////////////////////////////////////////////

        getSwfLoadedFuncName: function() {
            return 'swfLoaded' + this.uid;
        },

        getSwfEventHandlerFuncName: function() {
            return 'swfEvent' + this.uid;
        },

        onSwfLoaded: function(swfVersion) {
            // Retain SWF version
            this.swfVersion = swfVersion;
            this.log.info('Successfuly loaded SWF v' + this.swfVersion);

            // We can now show the SWF
            if (env.browser.name !== 'IE') {
                setTimeout(function() {
                    this.swfObj.style.visibility = 'visible';
                }.bind(this), 0);
            }

            // Flash loaded! Resolve promise!
            this.swfLoadedDefer.resolve(this.swfObj);

            // Remove SWF-loaded-function from window
            delete window[this.getSwfLoadedFuncName()];
        },

        onSwfEvent: function(eventName, params) {
            this.swfEventEmitter.trigger(eventName, params);
        },

        promise: function() {
            return this.swfLoadedDefer.promise;
        },

        embedSWF: function(container) {
            // If container is valid, create and append a flash wrapper element
            if (container && container.appendChild) {
                // Create a UUID for the flash object
                this.uid = utils.uuid().replace(/-/g, '');

                // Create a DIV that will be replaced by flash content
                var flashContainer = document.createElement('div');
                flashContainer.setAttribute('id', this.uid);
                container.appendChild(flashContainer);

                // Create a callback function on the window, so we can be notified
                // when SWF has fully loaded
                window[this.getSwfLoadedFuncName()] = this.onSwfLoaded;

                // Create a callback function on the window, so we can be notified
                // of SWF events
                window[this.getSwfEventHandlerFuncName()] = this.onSwfEvent;

                // Switch off the auto hide/show feature of SWFObject
                // This, along with style attribute 'visibility: hidden;' and setting 'visibility: visible;' on load,
                // is a bug fix for white flashing while loading SWF in Chrome and FF (in some circumstances)
                if (env.browser.name !== 'IE') {
                    swfobject.switchOffAutoHideShow();
                }

                // Embed the SWF
                swfobject.embedSWF(
                    this.flashConfig.SWF_URL,               // swfUrlStr
                    this.uid,                               // replaceElemIdStr
                    this.getSetting('width'),               // widthStr
                    this.getSetting('height'),              // heightStr
                    this.flashConfig.MIN_FLASH_VERSION,     // swfVersionStr
                    false,                                  // xiSwfUrlStr
                    utils.extend(                           // flashvarsObj
                        true,
                        {},
                        this.getSetting('flashvars'),
                        {
                            onLoadedJsCallback: this.getSwfLoadedFuncName(),
                            onEventJsCallback: this.getSwfEventHandlerFuncName()
                        }
                    ),
                    this.getSetting('params'),              // parObj
                    this.getSetting('attributes'),          // attObj
                    function(e) {                           // callbackFn
                        if (e.success && e.ref) {
                            this.swfObj = e.ref;
                            this.swfEventEmitter = new EventEmitter();

                            // Bug fix - get around 'Safari Power Saver'
                            // Simply initiate click to prevent Safari from suspending flash.
                            if (env.browser.name === 'Safari') {
                                this.swfObj.click();
                            }
                        } else {
                            this.log.error('Unable to embed SWF');
                            this.notifyError(62);
                        }
                    }.bind(this)
                );
            } else {
                this.log.error('Could not retrieve container element.');
                this.notifyError(27);
            }
        },

        //////////////////////////////////////////////////////////////////////////
        // --------------------------- CONSTRUCTOR -------------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function() {
            // Create defer (promise) for flash loaded
            this.swfLoadedDefer = when.defer();

            // Keep a reference to the flashConfig instance
            this.flashConfig = this.getSetting('flashConfig');
        },

        dispose: function() {
            this._super();

            // Remove SWF-event-function from window
            delete window[this.getSwfEventHandlerFuncName()];
        }
    });

    return SWFEmbedder;
});

define('engine/flash/FlashEngine',[
    'engine/BaseEngine',
    'engine/flash/FlashVideo',
    'engine/flash/SWFEmbedder',
    'when'
],

/**
* FlashEngine - Concrete implementation of BaseEngine for flash tech.
*/
function(BaseEngine, FlashVideo, SWFEmbedder, when) {
    'use strict';

    var FlashEngine = BaseEngine.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'FlashEngine',
        logLevel: 'warn',
        defaults: {
            updateSegmentDurationAccordingToVideoBuffer: true,
            switchChannelTimeout: 4,
            allowOverridingChannelIndexViaUrlHash: true,

            /** Interval in seconds to log current flash memory in bytes. */
            debugFlashMemoryInterval: NaN
        },
        excludeFuncFromAutoDebug: ['logFlashMemory'],

        //////////////////////////////////////////////////////////////////////////
        // ------------------------------ MEMBERS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        api: null,
        events: null,
        environment: null,
        repository: null,
        video: null,
        swfEmbedder: null,
        flashAPI: null,
        flashEventEmitter: null,
        isReadyDefer: null,
        repoSegmentUpdateHandlerEnabled: true,
        origCurrentTimeGetter: null,

        //////////////////////////////////////////////////////////////////////////
        // ------------------------ REPO LISTENERS -------------------------------
        //////////////////////////////////////////////////////////////////////////

        onRepoSegmentUpdated: function(segment) {
            if (this.flashAPI && this.repoSegmentUpdateHandlerEnabled) {
                for (var nodeId in this.repository.nodeMap) {
                    if (this.repository.nodeMap[nodeId].segment === segment) {
                        this.flashAPI.updateNode(this.repository.nodeMap[nodeId].getWorkerSafeNode());
                    }
                }
            }

            this.checkSegmentForFlvSource(segment);
        },

        onRepoNodeAdded: function(node) {
            if (this.flashAPI) {
                this.flashAPI.addNode(node.getWorkerSafeNode());
            }

            if (node && node.segment) {
                this.checkSegmentForFlvSource(node.segment);
            }
        },

        onRepoNodeRemoved: function(node) {
            if (this.flashAPI) {
                this.flashAPI.removeNode(node);
            }
        },

        onRepoNodeUpdated: function(node) {
            if (this.flashAPI) {
                this.flashAPI.updateNode(node.getWorkerSafeNode());
            }
        },

        onRepoPrefetchAdded: function(node, prefetch) { // jshint ignore:line
            if (this.flashAPI) {
                this.flashAPI.updateNode(node.getWorkerSafeNode());
            }
        },

        onRepoPrefetchRemoved: function(node, prefetch) { // jshint ignore:line
            if (this.flashAPI) {
                this.flashAPI.updateNode(node.getWorkerSafeNode());
            }
        },

        //////////////////////////////////////////////////////////////////////////
        // ----------------------- FLASH LISTENERS -------------------------------
        //////////////////////////////////////////////////////////////////////////

        onSegmentParsed: function(data) {
            if (this.getSetting('updateSegmentDurationAccordingToVideoBuffer') && data.segmentId && data.duration) {
                var segment = this.repository.getSegment(data.segmentId);

                if (segment && segment.duration !== data.duration) {
                    this.repoSegmentUpdateHandlerEnabled = false;
                    this.repository.updateSegment({id: data.segmentId, duration: data.duration});
                    this.repoSegmentUpdateHandlerEnabled = true;
                }
            }
        },

        onFlashError: function(e) {
            this.notifyError('Flash error occurred', e);
        },

        //////////////////////////////////////////////////////////////////////////
        // ------------------------- MISC METHODS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        initRepo: function() {
            this.flashAPI.updateRepo(this.getWorkerSafeRepoNodes());
        },

        addFlashListeners: function() {
            if (this.flashEventEmitter) {
                this.flashEventEmitter.on('segmentParsed', this.onSegmentParsed);
                this.flashEventEmitter.on('error', this.onFlashError);
            }
        },

        removeFlashListeners: function() {
            if (this.flashEventEmitter) {
                this.flashEventEmitter.off('segmentParsed', this.onSegmentParsed);
                this.flashEventEmitter.off('error', this.onFlashError);
            }
        },

        addRepoListeners: function() {
            this.repository.on('repository.segment.updated', this.onRepoSegmentUpdated);
            this.repository.on('repository.node.added', this.onRepoNodeAdded);
            this.repository.on('repository.node.removed', this.onRepoNodeRemoved);
            this.repository.on('repository.node.updated', this.onRepoNodeUpdated);
            this.repository.on('repository.prefetch.added', this.onRepoPrefetchAdded);
            this.repository.on('repository.prefetch.removed', this.onRepoPrefetchRemoved);
        },

        removeRepoListeners: function() {
            this.repository.off('repository.segment.updated', this.onRepoSegmentUpdated);
            this.repository.off('repository.node.added', this.onRepoNodeAdded);
            this.repository.off('repository.node.removed', this.onRepoNodeRemoved);
            this.repository.off('repository.node.updated', this.onRepoNodeUpdated);
            this.repository.off('repository.prefetch.added', this.onRepoPrefetchAdded);
            this.repository.off('repository.prefetch.removed', this.onRepoPrefetchRemoved);
        },

        checkSegmentForFlvSource: function(segment) {
            if (segment && segment.source && (!segment.source.flv || !segment.source.flv.url)) {
                this.log.warn('Segment \'' + segment.id + '\' is missing FLV source.');
            }
        },

        getWorkerSafeRepoNodes: function() {
            var retVal = {};

            for (var nodeId in this.repository.nodeMap) {
                retVal[nodeId] = this.repository.nodeMap[nodeId].getWorkerSafeNode();
            }

            return retVal;
        },

        logFlashMemory: function() {
            this.log.info('Flash total memory: ' +
                                (this.flashAPI.systemTotalMemory() / 1000000).toFixed(2) + ' MB, Free memory: ' +
                                (this.flashAPI.systemFreeMemory() / 1000000).toFixed(2) + ' MB');
        },

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- API METHODS --------------------------------
        //////////////////////////////////////////////////////////////////////////

        /** Returns promise. */
        appendNode: function(nodeId) {
            var d = when.defer();

            this.isReadyDefer.promise.done(function() {
                if (this.flashAPI.appendNode(nodeId)) {
                    d.resolve();
                } else {
                    d.reject(new Error('Unable to append node \'' + nodeId + '\', does this node\'s segment contain FLV source?'));
                }
            }.bind(this));

            return d.promise;
        },

        /** Returns promise. */
        reset: function() {
            var d = when.defer();

            this.isReadyDefer.promise.done(function() {
                if (this.flashAPI.reset()) {
                    d.resolve();
                } else {
                    d.reject(new Error('Unable to reset engine'));
                }
            }.bind(this));

            return d.promise;
        },

        /** Does not return anything. */
        endStream: function() {
            this.isReadyDefer.promise.done(function() {
                this.flashAPI.endStream();
            }.bind(this));
        },

        /** Returns promise which is resolved with an object containing the key 'timeInNode'.
         * Rejected with string message. */
        switchChannel: function(channelIndex) {
            var d = when.defer();

            // channelIndex may be overriden by hash portion of URL
            if (this.getSetting('allowOverridingChannelIndexViaUrlHash')) {
                var currentNode = this.api.exports.currentNode;
                var segUrl = (currentNode && currentNode.segments && currentNode.segments.length > channelIndex) ?
                                        currentNode.segments[channelIndex].source.flv.url :
                                        null;
                var hashChannelIndex = (segUrl && segUrl.indexOf('#') >= 0) ?
                                            parseInt(segUrl.substr(segUrl.indexOf('#') + 1), 10) :
                                            -1;
                if (hashChannelIndex >= 0) {
                    channelIndex = hashChannelIndex;
                }
            }

            this.isReadyDefer.promise.done(function() {
                var resolved = false;
                var onSwitched = function() {
                    resolved = true;
                    d.resolve({timeInNode: 0 /** FIXME TODO - currently works as-is */});
                }.bind(this);

                this.flashEventEmitter.one('switchedStream', onSwitched);
                setTimeout(function() {
                    this.flashEventEmitter.off('switchedStream', onSwitched);
                    if (!resolved) {
                        d.reject(new Error('Channel switch timed out'));
                    }
                }.bind(this), this.getSetting('switchChannelTimeout') * 1000);

                if (!this.flashAPI.switchChannel(channelIndex)) {
                    d.reject(new Error('Unable to switch channel'));
                }
            }.bind(this));

            return d.promise;
        },

        /** Returns promise. */
        seek: function(nodeId, timeInNode) {
            var d = when.defer();

            this.isReadyDefer.promise.done(function() {
                var currentTimeValue = this.video.currentTime();

                this.video.currentTime = function() {
                    return currentTimeValue;
                };

                var onSeekComplete = function() {
                    // An ugly hack for seekComplete not being fired at precise times
                    // Improvement - but not bullet proof.
                    // Only resolve once both 'playing' and 'nodeStart' events were fired from flash
                    var fired = 0;
                    var tryResolve = function() {
                        if (fired === 2) {
                            this.log.info(' [SEEK] Resolving seek promise');
                            this.video.currentTime = this.origCurrentTimeGetter;
                            d.resolve();
                        }
                    }.bind(this);

                    this.video.one('playing', function() {
                        this.log.info(' [SEEK] "playing" event handler');
                        fired++;
                        tryResolve();
                    }.bind(this));
                    this.flashEventEmitter.one('nodeStart', function() {
                        this.log.info(' [SEEK] "nodeStart" event handler');
                        fired++;
                        tryResolve();
                    }.bind(this));
                }.bind(this);

                this.flashEventEmitter.one('seekComplete', onSeekComplete);

                if (!this.flashAPI.seek(nodeId, timeInNode)) {
                    this.video.currentTime = this.origCurrentTimeGetter;
                    this.flashEventEmitter.off('seekComplete', onSeekComplete);
                    d.reject(new Error('Unable to perform flash engine seek'));
                }
            }.bind(this));

            return d.promise;
        },

        videoTech: function() { return 'flash'; },

        //////////////////////////////////////////////////////////////////////////
        // ---------------------------- CONSTRUCTOR ------------------------------
        //////////////////////////////////////////////////////////////////////////

        ctor: function(el, core, repo) {
            this._super(core, repo);

            // Create a 'flash-is-ready' promise
            this.isReadyDefer = when.defer();

            // Create SWF embedder instance
            this.swfEmbedder = new SWFEmbedder(this.options);

            // Create video
            this.video = new FlashVideo(this.options, this.api, this.swfEmbedder);

            // Keep reference to original currentTime getter function
            this.origCurrentTimeGetter = this.video.currentTime;

            // Add error listeners to submodules
            this.addErrorListener(this.swfEmbedder);
            this.addErrorListener(this.video);

            // Initiate swf embedding
            this.swfEmbedder.embedSWF(el);

            // Keep reference to SWF object
            this.swfEmbedder.promise().done(function(obj) {
                this.flashAPI = obj;
                this.flashEventEmitter = this.swfEmbedder.swfEventEmitter;
                this.addFlashListeners();
                this.initRepo();
                this.isReadyDefer.resolve();

                if (!isNaN(this.getSetting('debugFlashMemoryInterval', NaN))) {
                    setInterval(this.logFlashMemory, this.getSetting('debugFlashMemoryInterval') * 1000);
                }
            }.bind(this), function() {
                this.log.error('Error loading SWF');
                this.notifyError(63);
            }.bind(this));

            // Listen to repository changes (we only want to update flash when there are node/prefetch changes)
            this.addRepoListeners();
        },

        dispose: function() {
            this._super();
            this.removeFlashListeners();
            this.removeRepoListeners();

            if (this.flashAPI) {
                this.flashAPI.dispose();
            }
        }
    });

    return FlashEngine;
});

define(
'engine/flash/FlashFactory',[
    'engine/BaseFactory',
    'engine/flash/FlashEngine'
],

/**
* Main factory for Flash tech.
*/
function(BaseFactory, FlashEngine) {
    'use strict';

    var FlashFactory = BaseFactory.extend({

        //////////////////////////////////////////////////////////////////////////
        // -------------------------- MODULE CONFIG ------------------------------
        //////////////////////////////////////////////////////////////////////////

        name: 'FlashFactory',

        //////////////////////////////////////////////////////////////////////////
        // ------------------------- PRIVATE METHODS -----------------------------
        //////////////////////////////////////////////////////////////////////////

        /** Overrides base implementation. */
        _createEngine: function() {
            return new FlashEngine(this.options, this.el, this.core, this.repo);
        }
    });

    return FlashFactory;
});

define(
'engine/VideoTechFactory',[
    'modules/Base',
    'engine/EngineTech',
    'engine/hls/HlsFactory',
    'engine/mse/MseFactory',
    'engine/flash/FlashFactory',
    'utils/utils'
],
function(BaseModuleClass, EngineTech, HlsFactory, MseFactory, FlashFactory, utils) {
    'use strict';

    var VideoTechFactory = BaseModuleClass.extend({
        name: 'VideoTechFactory',
        logLevel: 'warn',

        makeFactory: function(el, core, repo) {
            var engineTechDetected = new EngineTech(this.options).detect();
            var factory = null;
            var videoTech = engineTechDetected && engineTechDetected.videoTech ?
                                    engineTechDetected.videoTech :
                                    'unknown';

            switch (videoTech) {
                case 'flash':
                    factory = new FlashFactory(utils.extend(true, {}, this.options, {SWFEmbedder: { flashConfig: engineTechDetected.flashConfig }}), el, core, repo).exports;
                    break;
                case 'hls':
                    factory = new HlsFactory(this.options, el, core, repo).exports;
                    break;
                case 'mse':
                    factory = new MseFactory(utils.extend(true, {}, this.options, {MseEngine: { formats: engineTechDetected.formats }}), el, core, repo).exports;
                    break;
                default:
                    this.log.error('The "' + videoTech + '" factory is not available.');
                    this.notifyError(11, videoTech);
                    break;
            }

            return factory;
        }
    });

    return VideoTechFactory;
});

define(
'Core',[
    'modules/Base',
    'modules/Repository',
    'modules/Controller',
    'modules/CuePoints',
    'modules/Playlist',
    'modules/FullScreen',
    'engine/VideoTechFactory',
    'utils/utils',
    'utils/loggerFactory',
    'utils/environment',
    'utils/EventEmitter',
    'when'
],

function(BaseModuleClass, Repository, Controller, CuePoints, Playlist, FullScreen, VideoTechFactory, utils, loggerFactory, environment, EventEmitter, when) {
    'use strict';

    /**
    * @version 1.0
    * @exports player
    * @namespace player
    */
    var Core = BaseModuleClass.extend({
        name: 'Core',
        logLevel: 'warn',

        /** The container element or selector as provided by client. */
        el: null,

        /** The container HTML DOM element after being resolved (if selector was given). */
        container: null,

        /** The plugins map as provided by client. */
        plugins: null,

        /** The plugin context object that is passed as parameter to every plugin being initialized. */
        pluginCtx: null,

        events: {
            /**
            * Dispatched the video playback position changed as part of normal playback.
            * @alias player.events.timeupdate
            * @memberof! player.events#
            * @event player.events.timeupdate
            */
            timeupdate: 'timeupdate',

            /**
             * Total duration of playback has changed (because of changes to playlist and/or changes in graph/segments).
             * Arguments: (newduration)
             */
            durationchange: 'durationchange',

            volumechange: 'volumechange',
            loadeddata: 'loadeddata',
            playing: 'playing',
            waiting: 'waiting',
            play: 'play',
            pause: 'pause',
            ended: 'ended',

            /** Arguments (isFullscreenBoolean) */
            fullscreenchange: 'fullscreenchange',
            channelswitched: 'channelswitched',
            channelswitching: 'channelswitching',
            seeking: 'seeking',
            seeked: 'seeked',

            /**
             * Node playback has started.
             * Arguments: (node, playlistIndex)
             */
            nodestart: 'nodestart',

            /**
             * Node playback has ended.
             * Arguments: (node, playlistIndex)
             */
            nodeend: 'nodeend',

            /**
             * Node playback reached a critical point and no next node has been pushed to playlist.
             * If another node is not pushed to playlist in response to this event, will trigger the 'ended' event at end of node.
             * Arguments: (node, playlistIndex)
             */
            nodecritical: 'nodecritical',

            /**
             * Dispatched when an error occurred.
             * If no listeners were attached to the 'error' event, error will be thrown instead.
             * Arguments: (errorObject)
             */
            error: 'error',

            playlistreset: 'playlist.reset',
            playlistpush: 'playlist.push'
        },

        /*
         * Factory for creating instances of logger objects.
         * @memberof player
         * @type {object}
         * @namespace player.loggerFactory
         */
        loggerFactory: loggerFactory,

        /*
         * {
         *     ua: "",
         *     browser: {
         *         name: "",
         *         version: "",
         *         major: ""
         *     },
         *     engine: {
         *         name: "",
         *         version: ""
         *     },
         *     os: {
         *         name: "",
         *         version: ""
         *     },
         *     device: {
         *         model: "",
         *         type: "",
         *         vendor: ""
         *     },
         *     cpu: {
         *         architecture: ""
         *     }
         * }
         * @memberof player
         * @type {object}
         * @namespace player.environment
         */
        environment: environment,

        init: function(el, plugins, options) {
            this.el = el;
            this.plugins = plugins;
            this._super(options);
        },

        initPlugin: function(name, options) {
            // Validate that plugin exists
            if (!this.plugins || !this.plugins[name]) {
                this.log.error('Could not find plugin: ' + name);
                this.notifyError(47, name);
                return false;
            }

            // Validate that plugin is a function
            if (typeof this.plugins[name].fn !== 'function') {
                this.log.error('Plugin is not a function: ' + name);
                this.notifyError(48, name);
                return false;
            }

            // Initialize plugin
            try {
                this.plugins[name].fn.call(this.exports, this.pluginCtx, options);
            } catch (e) {
                return false;
            }

            return true;
        },

        exportEventEmitterMethods: function() {
            this.expose({

                /**
                * Adds a listener function to the specified event.
                * The listener will not be added if it is a duplicate.
                * If the listener returns true then it will be removed after it is called.
                * @alias player.api.on
                * @memberof! player.api#
                * @method player.api.on
                * @param {String} evt - Name of the event to attach the listener to.
                * @param {Function} listener - Method to be called when the event is
                * emitted.
                * If the function returns true then it will be removed after calling.
                */
                on: this.on,

                /**
                * Semi-alias of on. It will add a listener that will be
                * automatically removed after it's first execution.
                * @alias player.api.one
                * @memberof! player.api#
                * @method player.api.one
                * @param {String} evt - Name of the event to attach the listener to.
                * @param {Function} listener - Method to be called when the event is
                * emitted.
                * If the function returns true then it will be removed after calling.
                */
                one: this.one,

                /**
                * Removes a listener function from the specified event.
                * @alias player.api.off
                * @memberof! player.api#
                * @method player.api.off
                * @param {String} evt - Name of the event to remove the listener from.
                * @param {Function} listener - Method to remove from the event.
                */
                off: this.off,

                /**
                * Emits an event of your choice.
                * When emitted, every listener attached to that event will be executed.
                * If you pass the optional arguments then those arguments will be
                * passed to every listener upon execution.
                * @alias player.api.trigger
                * @memberof! player.api#
                * @method player.api.trigger
                * @param {String} evt - Name of the event to emit and execute listeners for.
                * @param {...*} [params] - Optional arguments to be passed to each listener.
                */
                trigger: this.trigger
            });
        },

        getContainer: function(el) {
            var retVal = null;

            // If no element provided, log error and return null
            if (!el) {
                this.log.error('No video container element (or selector) provided.');
                this.notifyError(26);
            }

            // Otherwise, if el is a string (selector)
            else if (typeof el === 'string') {
                retVal = document.querySelector(el);
            }

            // Otherwise, el is assumed to be the container <div> element itself.
            else {
                retVal = el;
            }

            return retVal;
        },

        ctor: function() {
            // Expose some of the EventEmitter functionality
            this.exportEventEmitterMethods();

            // Expose the events object
            this.exports.events = this.events;

            // Resolve the container HTML DOM element
            this.container = this.getContainer(this.el);

            // Create required instances
            var repo = new Repository(this.options);
            var videoTechFactory = new VideoTechFactory(this.options).makeFactory(this.container, this, repo);
            var engine = videoTechFactory.makeEngine();
            var playlist = new Playlist(this.options, repo);
            var fullScreen = new FullScreen(this.options, this, this.container);
            var cuePoints = new CuePoints(this.options, this, repo, playlist);
            var controller = new Controller(this.options, playlist, repo, engine, this, cuePoints, fullScreen);
            var audio = null; // TODO: new Audio(this.options, ...);

            // Add error listeners to all submodules
            this.addErrorListener(repo);
            this.addErrorListener(videoTechFactory);
            this.addErrorListener(engine);
            this.addErrorListener(playlist);
            this.addErrorListener(fullScreen);
            this.addErrorListener(cuePoints);
            this.addErrorListener(controller);
            this.addErrorListener(audio);

            // Expose API
            this.expose(engine.video.exports);
            this.expose(controller.exports);
            this.expose(fullScreen.exports);
            this.expose(audio);
            this.exports.repository = repo.exports;
            this.exports.playlist = playlist.exports;
            this.exports.initPlugin = this.initPlugin;

            // Create plugin context (object that is sent as argument when initializing plugins)
            this.pluginCtx = {
                player: this.exports,
                core: this,
                repository: repo,
                engine: engine,
                video: engine.video,
                playlist: playlist,
                fullScreen: fullScreen,
                cuePoints: cuePoints,
                controller: controller,
                audio: audio,
                environment: this.environment,
                loggerFactory: this.loggerFactory,
                BaseModuleClass: BaseModuleClass,
                utils: utils,
                when: when,
                EventEmitter: EventEmitter
            };

            // Init all plugins that were explicitly supplied in options
            var explicitInitPlugins = [];
            if (typeof this.options.plugins === 'object' && !Array.isArray(this.options.plugins)) {
                for (var plugin in this.options.plugins) {
                    if (this.initPlugin(plugin, this.options.plugins[plugin])) {
                        explicitInitPlugins.push(plugin);
                    }
                }
            } else if (Array.isArray(this.options.plugins)) {
                for (var i = 0; i < this.options.plugins.length; i++) {
                    var name = this.options.plugins[i];
                    var pluginOptions = i + 1 < this.options.plugins.length && typeof this.options.plugins[i + 1] === 'object' ?
                                            this.options.plugins[++i] :
                                            {};
                    if (this.initPlugin(name, pluginOptions)) {
                        explicitInitPlugins.push(name);
                    }
                }
            }

            // Init all plugins that were registered as autoInit
            for (var pluginName in this.plugins) {
                if (this.plugins[pluginName].auto && explicitInitPlugins.indexOf(pluginName) === -1) {
                    this.initPlugin(pluginName, {});
                }
            }
        }
    });

    return Core;
});

define('version',{
	//version: 'BAMBOO_VERSION.BAMBOO_BUILD_NUMBER'
	version: '3.0.0.91'
});

/* global requirejs */
requirejs.config({
    packages:
    [
        {
            name: 'when',
            location: '../vendor/when',
            main: 'when'
        }
    ],
    paths: {
        text: '../vendor/requirejs-text/text',
        uuid: '../vendor/node-uuid/uuid',
        swfobject: '../vendor/swfobject/swfobject/swfobject'
    },

    shim: {
        swfobject: {
            exports: 'swfobject'
        }
    }
});

define(
'Player',[
    'modules/Base',
    'Core',
    'engine/EngineTech',
    'version',
    'utils/environment'
],

function(BaseModuleClass, Core, EngineTech, versionObj, env) {
    'use strict';

    // This is a static object of registered plugins
    // Every plugin that's loaded and registers itself will be added to this object.
    // Whenever an InterludePlayer instance is created, it can also initialize any of the plugins that were already registered.
    var plugins = {};

    // Static Boolean variable - is player supported on current environment.
    var supported = null;

    var versionGetterFunc = function() {
        return versionObj.version;
    };
    var envGetterFunc = function() {
        return env;
    };

    // The main Player class, constructed using the Core class.
    var Player = BaseModuleClass.extend({
        name: 'Player',
        logLevel: 'warn',

        /**
         * Player constructor.
         *
         * @param {*} el - The container element to be used by player, or a DOM selector string for the container element.
         * @param {Object} options - Configuration options for player initialization.
         */
        init: function(el, options) {
            // Verify that at least one argument was passed to constructor
            if (!el) {
                throw new Error('Interlude Player\'s constructor must get an element (or selector) as first argument.');
            }

            options = options || {};

            // Call base constructor
            this._super(options);

            // Expose the public API of Core
            this.exports = (new Core(el, plugins, options)).exports;

            // Add the version getter to API
            Object.defineProperty(this.exports, 'version', {enumerable: true, get: versionGetterFunc});
        }
    });

    // This is the constructor of an InterludePlayer instance
    var InterludePlayer = function() {
        var _arguments = arguments;

        function PlayerProxy() {
            return Player.apply(this, _arguments);
        }

        PlayerProxy.prototype = Object.create(Player.prototype);

        return (new PlayerProxy()).exports;
    };

    // Expose a function that statically registers a plugin for later use with instances.
    InterludePlayer.registerPlugin = function(name, fn, autoInit) {
        // Validate that plugin is valid
        if (typeof name !== 'string' || typeof fn !== 'function') {
            if (console && console.error) {
                console.error('Cannot register plugin, incorrect parameters supplied.');
            }

            return;
        }

        // Warn if plugin with this name was already registered
        if (plugins[name] && console && console.warn) {
            console.warn('Plugin "' + name + '" was already registered - overriding.');
        }

        autoInit = !!autoInit || false;

        // Register plugin
        plugins[name] = {auto: autoInit, fn: fn};
    };

    // Expose a static function that checks if player is supported on current environment and returns a Boolean value.
    InterludePlayer.isSupported = function() {
        if (supported === null) {
            var techObject = (new EngineTech({EngineTech: {logLevel: 'disable', throwErrors: false}}).detect());
            supported = !!techObject;

            // Special case - return false for Android which advertises support for HLS
            if (supported && techObject && techObject.videoTech === 'hls' && env.os.name === 'Android') {
                supported = false;
            }

            // Special case - return false for iPhone/iPod (they do not currently support inline-video)
            // TODO - if this an embedded SDK, we should return true
            if (supported && env.os.name === 'iOS' && env.device.type !== 'tablet') {
                supported = false;
            }
        }

        return supported;
    };

    // Expose a static function that checks if current device is supported.
    // This is different from isSupported, since the player might be supported in device, but not in current environment/browser.
    InterludePlayer.isDeviceSupported = function() {
        return InterludePlayer.isSupported() ||
                        (
                            env.os.name === 'Android' &&
                                (
                                    (
                                        env.os.version &&
                                        typeof env.os.version === 'string' &&
                                        env.os.version.charAt(0) >= 4
                                    ) ||
                                    typeof env.os.version === 'undefined'
                                )
                        );
    };

    // Add getters to static API
    Object.defineProperty(InterludePlayer, 'version', {enumerable: true, get: versionGetterFunc});
    Object.defineProperty(InterludePlayer, 'environment', {enumerable: true, get: envGetterFunc});

    // Return a constructor that will return only the exported API of Player
    return InterludePlayer;
});

return require('Player');}));