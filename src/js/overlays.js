define({
    overlays: function(ctx) {
        return [
            ctx.InterludeDecision(
                {
                    fadeInDuration: 0.6,
                    fadeOutDuration: 0.6,
                    userSelectionDisappearDelay: 0,
                    id: 'startContainer',
                    node: 'start',
                    style: {
                        width: '100%',
                        height: '100%',
                        position: 'absolute'
                    }
                }
            )
        ];
    }
});
