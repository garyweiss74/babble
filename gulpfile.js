var fs = require('fs');
var path = require('path');
var request = require('request');
var source = require('vinyl-source-stream');

var gulp = require('gulp');

// Load all gulp plugins automatically and attach
// them to the `plugins` object
var plugins = require('gulp-load-plugins')();

// Temporary solution until gulp 4  https://github.com/gulpjs/gulp/issues/355
var runSequence = require('run-sequence');

var minimist = require('minimist');
var pkg = require('./package.json');
var dirs = pkg['boilerplate-configs'].directories;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
var cmdOptions = minimist(process.argv.slice(2), {
    default: {
        type: 'patch',
        version: null,
        sourceUrl: ''
    }
});

// ---------------------------------------------------------------------
// | Helper tasks                                                      |
// ---------------------------------------------------------------------

gulp.task('archive:create_archive_dir', function () {
    fs.mkdirSync(path.resolve(dirs.archive), '0755');
});

gulp.task('archive:zip', function (done) {

    var archiveName = path.resolve(dirs.archive, pkg.name + '_v' + pkg.version + '.zip');
    var archiver = require('archiver')('zip');
    var files = require('glob').sync('**/*.*', {
        'cwd': dirs.dist,
        'dot': true, // include hidden files
        'nodir': true // exclude directories
    });
    var output = fs.createWriteStream(archiveName);

    archiver.on('error', function (error) {
        done();
        throw error;
    });

    output.on('close', done);

    files.forEach(function (file) {
        var filePath = path.resolve(dirs.dist, file);

        // `archiver.bulk` does not maintain the file
        // permissions, so we need to add files individually
        archiver.append(fs.createReadStream(filePath), {
            'name': file,
            'mode': fs.statSync(filePath)
        });

    });

    archiver.pipe(output);
    archiver.finalize();

});

gulp.task('clean', function (done) {
    require('del')([
        dirs.archive,
        dirs.dist
    ], done);
});

gulp.task('copy', [
    'copy:.htaccess',
    'copy:index.html',
    'copy:main.css',
    'copy:misc'
]);

gulp.task('copy:.htaccess', function () {
    return gulp.src('node_modules/apache-server-configs/dist/.htaccess')
               .pipe(plugins.replace(/# ErrorDocument/g, 'ErrorDocument'))
               .pipe(gulp.dest(dirs.dist));
});

gulp.task('copy:index.html', function () {
    return gulp.src(dirs.src + '/index.html')
               .pipe(gulp.dest(dirs.dist));
});

gulp.task('copy:main.css', function () {

    var banner = '/*! Interlude Boilerplate v' + pkg.version;

    return gulp.src(dirs.src + '/css/main.css')
        .pipe(plugins.header(banner))
        .pipe(plugins.autoprefixer({
            browsers: ['last 2 versions', 'ie >= 8', '> 1%'],
            cascade: false
        }))
        .pipe(gulp.dest(dirs.dist + '/css'));
});

gulp.task('copy:misc', function () {
    return gulp.src([

        // Copy all files
        dirs.src + '/**/*',

        // Exclude the following files
        // (other tasks will handle the copying of these files)
        '!' + dirs.src + '/css/main.css',
        '!' + dirs.src + '/index.html'

    ], {

        // Include hidden files by default
        dot: true

    }).pipe(gulp.dest(dirs.dist));
});

gulp.task('lint:js', function () {
    return gulp.src([
        'gulpfile.js',
        dirs.src + '/js/*.js',
        dirs.test + '/*.js'
    ]).pipe(plugins.jscs())
      .pipe(plugins.jshint())
      .pipe(plugins.jshint.reporter('jshint-stylish'))
      .pipe(plugins.jshint.reporter('fail'));
});

gulp.task('serve', function() {
    return gulp.src('dist')
      .pipe(plugins.webserver({
          host: 'ec2-54-149-127-252.us-west-2.compute.amazonaws.com',
          // host: 're-dash.interlude.fm',
          port: 8080,
          open: false,
          proxies: [{
              source: '/babble',
              target: 'http://ec2-54-149-127-252.us-west-2.compute.amazonaws.com:8000/'
          }]
      }));
});

gulp.task('bump', function() {
    return gulp.src(['./package.json', './bower.json'])
      .pipe(plugins.bump({
          type: cmdOptions.type,
          version: cmdOptions.version
      }))
      .pipe(gulp.dest('./'))
      .pipe(plugins.tap(function(file) {
          var json = JSON.parse(String(file.contents));
          pkg.version = json.version;
      }));
});

function getDirectoryName() {
    return pkg.name + '/' + pkg.version;
}

gulp.task('upload', function() {
    var config = pkg['boilerplate-configs'].s3;
    var s3 = plugins.s3Upload({
        accessKeyId: config.accessKeyId,
        secretAccessKey: config.secretAccessKey
    });

    return gulp.src('./dist/**')
      .pipe(s3({
          Bucket: config.bucket,
          ACL: config.acl,
          keyTransform: function(relativeFilename) {
              return getDirectoryName() + '/' + relativeFilename;
          }
      }));
});


// ---------------------------------------------------------------------
// | Main tasks                                                        |
// ---------------------------------------------------------------------

gulp.task('archive', function (done) {
    runSequence(
        'build',
        'archive:create_archive_dir',
        'archive:zip',
    done);
});

gulp.task('build', function (done) {
    runSequence(
        ['clean', 'lint:js'],
        'copy',
    done);
});

gulp.task('test', function(done) {
    runSequence(
        'build',
        'serve',
    done);
});

gulp.task('deploy', function(done) {
    runSequence(
        'bump',
        'build',
        'upload',
    done);
});

gulp.task('import', function(done) {
    var url = cmdOptions.sourceUrl || (pkg['boilerplate-configs'].sourceUrl);
    if (!url) {
        done('url parameter must be set in package.json or via command line');
    } else {
        request({
            url: url,
            json: true
        }, function (error, response, body) {
            if (error || response.statusCode !== 200) {
                done(error || response.statusCode);
            } else {
                request(body.config.html5.resources.skins)
                  .pipe(source('skins.json'))
                  .pipe(plugins.replace('\\/', '/'))
                  .pipe(plugins.streamify(plugins.jsonFmt(plugins.jsonFmt.PRETTY)))
                  .pipe(gulp.dest(dirs.import));

                request(body.config.html5.resources.overlays)
                  .pipe(source('overlays.js'))
                  .pipe(plugins.streamify(plugins.jsbeautifier({
                      preserveNewlines: false
                  })))
                  .pipe(gulp.dest(dirs.import));

                request(body.config.html5.url)
                  .pipe(source('config.js'))
                  .pipe(plugins.streamify(plugins.jsbeautifier({
                      preserveNewlines: false
                  })))
                  .pipe(gulp.dest(dirs.import));
            }
        });
    }
});

gulp.task('default', ['build']);
